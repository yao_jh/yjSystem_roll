package io.renren.common.sys;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.ExpressUtils;
import io.renren.modules.feature.utils.ForceShuntUtils;
import io.renren.modules.currency.utils.RunDurationShuntUtils;
import io.renren.modules.feature.utils.MotorShuntUtils;
import io.renren.modules.feature.utils.ServoValveShuntUtils;
import io.renren.modules.learn.utils.LearnShuntUtils;
import io.renren.modules.normal.utils.AgcNormalUtils;
import io.renren.modules.normal.utils.ForceNormalUtils;
import io.renren.modules.normal.utils.MotorNormalUtils;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.FrameBase;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.utils.AgcShuntUtils;
import io.renren.modules.warning.utils.WarningUtils;
import org.apache.commons.lang.StringUtils;

import javax.smartcardio.CommandAPDU;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 数据分流器
 */
public class DataDiversionHandler {

    /**
     * 实时数据分流
     * @param jsonStr
     */
    public static void realDataDiversion(String jsonStr){
        if(CommonUtils.isNull(jsonStr)){
            return ;
        }
        //字符转JSON
        JSONArray arr = JSONArray.parseArray(jsonStr);
        if(arr == null || arr.isEmpty()){
            return ;
        }
        //遍历推送的数据
        for(int i=0;i<arr.size();i++){
            //解析推送JSON数据
            readDeviceDataFromJson(arr.getJSONObject(i));
        }
    }

    /**
     * 解析推送JSON数据
     * @param jsonData
     */
    private static void readDeviceDataFromJson(JSONObject jsonData){
        //设备Code
        String deviceCode = jsonData.getString("deviceCode");
        if(deviceCode == null) return;
        //上传时间字符
        String dateStr = jsonData.getString("baseTime");
        //获取时间
        Date time = DateUtils.stringToDate(dateStr, DateUtils.DATE_MSTIME_PATTERN);
        //处理推送数据
        Map<String, PointEntity> dataMap = MainCatche.dataMap.get(deviceCode);
        if(dataMap == null) return;

        //更新设备对接Id
        String dockId = jsonData.getString("deviceId");
        FrameBase frame = MainCatche.frameMap.get(deviceCode);
        if(frame != null){
            frame.setDockId(dockId);
        }

        //解析JSON
        PointEntity point = null;
        BigDecimal val = null;
        //遍历推送的JSON
        for (Map.Entry<String, Object> entry : jsonData.entrySet()) {
            point = dataMap.get(entry.getKey());
            if(point != null) {
                //有公式的先计算
                val = ExpressUtils.consolidateData(point, entry.getValue());
                //处理小数位
                if(point.getFormat() == null){
                    point.setFormat(0);
                }
                val = val.setScale(point.getFormat(), BigDecimal.ROUND_HALF_DOWN);
                //保存实时值结果
                point.setValue(val);
                point.setRealDate(time);

                //加载需要分析的评估包
                loadEstimatePackageUtils(point);
            }
        }
    }

    /**
     * 加载需要分析的评估包
     * 状态发生改变开始计量趋势
     * 初始状态作为下周期判断的依据（数据可能不完整），即程序检测到的最初状态不作为趋势分析的样本
     * @param point
     */
    private static void loadEstimatePackageUtils(PointEntity point){

        //设备运行时间评估工具
        RunDurationShuntUtils.grantShuntHandler(point);

        //轧制力评估包
        ForceShuntUtils.grantShuntHandler(point);
        //电机评估包
        MotorShuntUtils.grantShuntHandler(point);
        //伺服阀评估包，内部調用正态分析检测报警
        ServoValveShuntUtils.grantShuntHandler(point);
        //AGC缸评估包，内部調用正态分析检测报警
        AgcShuntUtils.grantShuntHandler(point);

        //电机正态分析检测报警
        MotorNormalUtils.grantShuntHandler(point);
        //轧制力正太分析检测报警
        ForceNormalUtils.grantShuntHandler(point);

        //自学习
        LearnShuntUtils.grantShuntHandler(point);

        //检测报警
        WarningUtils.checkWarning(point);



    }

}
