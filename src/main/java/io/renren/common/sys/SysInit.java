package io.renren.common.sys;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.Constant;
import io.renren.modules.feature.utils.MotorShuntUtils;
import io.renren.modules.feature.utils.ForceShuntUtils;
import io.renren.modules.feature.utils.ServoValveShuntUtils;
import io.renren.modules.feature.vo.*;
import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.learn.service.LearnConfigService;
import io.renren.modules.learn.utils.LearnShuntUtils;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.operation.service.DeviceService;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.theme.utils.AgcShuntUtils;
import io.renren.modules.theme.utils.TensionShuntUtils;
import io.renren.modules.theme.utils.WelderShuntUtils;
import io.renren.modules.theme.vo.AgcTrendVo;
import io.renren.modules.theme.vo.TensionTrendVo;
import io.renren.modules.theme.vo.WelderVo;
import io.renren.modules.warning.service.WarningConfigService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.util.TypeUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @ClassName: SysInitBean
 * @Description: 初始化
 */
@Component
public class SysInit implements InitializingBean {

	@Autowired
	private DeviceService deviceService;
	@Autowired
	private PointService pointService;
	@Autowired
	private CrewService crewService;
	@Autowired
	private LearnConfigService learnConfigService;
	@Autowired
	private WarningConfigService warningConfigService;

	public void afterPropertiesSet() throws Exception {
		System.out.println("SysInit.");
		// 设置Json输出时首字母不是自动变小写
		TypeUtils.compatibleWithJavaBean = true;
		// 初始化缓存
		//实时数据缓存
		System.out.println("initDataMap.");
		initDataMap();
		//设备缓存
		System.out.println("initDeviceMap.");
		initDeviceMap();
		//机组缓存
		System.out.println("initCrewMap.");
		initCrewMap();

		//分配设备的分析缓存介质
		System.out.println("initDeviceTrendHandMap.");
		initDeviceTrendHandMap();

		//自学习配置
		System.out.println("initLearnConfigMap.");
		initLearnConfigMap();

		//自学习配置
		System.out.println("initWarningConfigMap.");
		initWarningConfigMap();

		if(Constant.shiroUser){
			//订阅iot实时数据
			System.out.println("subscribeReal.");
			Topic topic = new Topic();//创建iot会话
			topic.subscribeRealTopic();//订阅实时
		}
	}

	/**
	 * 初始化报警配置
	 */
	private void initWarningConfigMap() {
		warningConfigService.initWarningConfigMap();
	}

	/**
	 * 初始化自学习配置
	 */
	private void initLearnConfigMap() {
		//学习中的配置记录
		List<LearnConfigEntity> list = learnConfigService.queryListByState(1);
		for (LearnConfigEntity learn : list){
			LearnShuntUtils.learnConfigMap.put(learn.getPointCode(), learn);
		}
		//初始化
		learnConfigService.initLearning();
	}

	/**
	 * 初始化机组
	 */
	private void initCrewMap() {
		List<CrewEntity> list = crewService.list();
		// 遍历缓存机组信息到内存
		for (CrewEntity crew : list) {
			MainCatche.crewMap.put(crew.getCrewId(), crew);
			//R1、E1、R2、E2、F1-7 轧机机组
			if("mill".equals(crew.getType())) {
				ForceShuntUtils.forceTrendMap.put(crew.getCrewId(), new ForceTrendVo(crew.getCrewId()));
			}
			//初始化机组设备信息
			MainCatche.frameMap.put(crew.getCode(), crew);
		}
	}

	/**
	 * 初始化设备趋势
	 */
	private void initDeviceTrendHandMap() {
		//遍历设备
		for(DeviceEntity device : MainCatche.deviceMap.values()){
			// 分配设备的分析缓存介质
			initDeviceTrend(device);
		}
	}

	/**
	 * 分配设备的分析缓存介质
	 * @param device
	 */
	private void initDeviceTrend(DeviceEntity device){
		String typeCode = device.getTypeCode();
		if(CommonUtils.isNull(typeCode)) return ;
		//不同设备设定对应的分析缓存介质
		switch (typeCode){
			case "motor"://电机
				// 电机趋势分析缓存
				MotorShuntUtils.motorTrendMap.put(device.getDeviceId(), new MotorTrendVo());
				break;
			case "servo_valve"://伺服阀
				// 伺服阀趋势分析缓存
				ServoValveShuntUtils.servoValveTrendMap.put(device.getDeviceId(), new ServoValveTrendVo());
				break;
			case "agc"://AGC缸
				// AGC缸操作侧趋势分析缓存
				AgcShuntUtils.agcOsTrendMap.put(device.getDeviceId(), new AgcTrendVo());
				// AGC缸驱动侧趋势分析缓存
				AgcShuntUtils.agcDsTrendMap.put(device.getDeviceId(), new AgcTrendVo());
				break;
			case "welder"://焊机
				// 焊机焊接缓存，没有初始化焊接最新记录
				WelderShuntUtils.welderMap.put(device.getDeviceId(), new WelderVo());
				break;
		}
		//载入张力辊电机
		initTensionTrend(device);
	}

	/**
	 * 载入张力辊组的电机
	 * @param device
	 */
	private void initTensionTrend(DeviceEntity device){
		CrewEntity crew = MainCatche.crewMap.get(device.getCrewId());
		if(crew != null){
			if("tension_roll".equals(crew.getType())){
				TensionShuntUtils.tensionTrendMap.put(device.getCode(), new TensionTrendVo());
			}
		}
	}

	/**
	 * 
	 * @Title: initDataMap
	 * @Description: 初始化 DataMap
	 * @return: void
	 */
	public void initDataMap() {
		if(MainCatche.dataMap == null){
			return;
		}
		//缓存参数实时数据
		List<PointEntity> list = pointService.list();
		for (PointEntity point : list) {
			if(CommonUtils.isNull(point.getClassCode())){
				continue;
			}
			if(point.getValue() == null){
				point.setValue(BigDecimal.ZERO);
			}
			//初始化参数实时结构
			initRealDataMap(point);
		}
	}

	/**
	 * 初始化参数实时结构
	 * @param point
	 */
	private void initRealDataMap(PointEntity point){
		//存储点清单
		MainCatche.realMap.put(point.getCode(), point);

		//存储实时内存
		String code = getCodeByPoint(point);
		if(code == null){
			return;
		}
		Map<String, PointEntity> pointMap = MainCatche.dataMap.get(code);
		if(pointMap == null){
			pointMap = new ConcurrentHashMap<String, PointEntity>();
			MainCatche.dataMap.put(code, pointMap);
		}else{
			pointMap.put(point.getClassCode(), point);
		}

		//释放
		code = null;
	}

	/**
	 * 获取机组code
	 * @param point
	 * @return
	 */
	private String getCodeByPoint(PointEntity point) {
		String code = point.getDeviceCode();
		if(code == null){
			CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
			if(crew != null){
				code =  crew.getCode();
			}
		}
		return  code;
	}


	/**
	 *
	 * @Title: initDeviceMap
	 * @Description: 初始化 deviceMap
	 * @return: void
	 */
	public void initDeviceMap() {
		// 缓存设备信息
		List<DeviceEntity> list = deviceService.list();
		Date date = new Date();
		for (DeviceEntity device : list) {
			if(device.getOnline() == null){
				device.setOnline(1);
			}
			if(device.getOnlineDate() == null){
				device.setOnlineDate(date);
			}
			if(device.getCreateDate() == null){
				device.setCreateDate(date);
			}
			MainCatche.deviceMap.put(device.getCode(), device);
			//初始化机组设备信息
			MainCatche.frameMap.put(device.getCode(), device);
		}
	}

}