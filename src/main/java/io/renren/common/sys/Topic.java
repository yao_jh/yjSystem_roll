package io.renren.common.sys;

import com.alibaba.fastjson.JSONObject;
import com.iot.api.DefaultFactory;
import com.iot.api.IotApi;
import com.iot.api.HttpMethod;
import com.iot.client.sdk.DefaultIotSDK;
import com.iot.client.sdk.IotSDKConfig;
import com.iot.client.sdk.IotSubscriptionSDK;
import com.iot.exception.BadAuthorizedException;

import java.util.HashMap;
import java.util.Map;


/**
 * iot平台数据推送
 */
public class Topic {
    //环境
    private static String active = "prod";
    //服务器地址
    private String PUB_SUB_SERVER = "10.88.246.15";
    //请求端口
    private Integer TCP_PORT = 30046;
    //请求地址
    private static String SERVER_URL = "http://10.88.246.16:30044/";
    //客户端ID
    private static String AUTH_KEY = "OLBgBLPQTQqoBjibiftDLwQZC3CTQjTWO6r2FOtgs1-A";
    //秘钥
    private static String SECRET_KEY = "K09SVL85LR5u_-0qpU8gfSAgG6ENAX_R_GHWwN6vI__iA";
    // 配置Map
    private IotSubscriptionSDK sdk;

    //配置
    Map<String, Object> configs = null;

    public Topic(){
        super();
    }

    public Topic(String authKey){
        super();
        this.AUTH_KEY = authKey;
    }

    /**
     * 获取配置
     * @return
     */
    public Map<String, Object> getConfigs(){
        //单例模式
        if(configs == null){
            configs = new HashMap<>();
        }else{
            return configs;
        }
        //自动调整配置
        autoSetingActive();
        configs.put(IotSDKConfig.CLIENT_ID, AUTH_KEY);
        configs.put(IotSDKConfig.SECRET_KEY, SECRET_KEY);
        configs.put(IotSDKConfig.PUB_SUB_SERVER, PUB_SUB_SERVER);
        configs.put(IotSDKConfig.TCP_PORT, TCP_PORT);
        return configs;
    }

    /**
     * 自动调整配置
     */
    public void autoSetingActive(){
        if(active.equals("dev")){
            //服务器地址
            PUB_SUB_SERVER = "10.88.246.15";
            //请求端口
            TCP_PORT = 30046;
            //请求地址
            SERVER_URL = "http://10.88.246.16:30044/";
            //客户端ID
            AUTH_KEY = "OLBgBLPQTQqoBjibiftDLwQZC3CTQjTWO6r2FOtgs1-A";
            //秘钥
            SECRET_KEY = "K09SVL85LR5u_-0qpU8gfSAgG6ENAX_R_GHWwN6vI__iA";
        }else if(active.equals("prod")){
            //服务器地址
            PUB_SUB_SERVER = "192.168.1.241";
            //请求端口
            TCP_PORT = 30046;
            //请求地址
            SERVER_URL = "http://192.168.1.241:30044/";
            //客户端ID
            AUTH_KEY = "WDPBh3OgSk6SVflkz0NwgAs_YcO-NqR1mn8Eju-seFhA";
            //秘钥
            SECRET_KEY = "K_xrktl3ZT3exvL3F1lNjvQucbEqsCOS4SjUtA__pP1qA";
        }
    }

    /**
     * 订阅实时数据
     */
    public void subscribeRealTopic(){
        try {
            Map<String, Object> configs = getConfigs();
            this.sdk = new DefaultIotSDK(configs);

            sdk.setSubPubCallBack((msg) -> {
                System.out.println(" sub callback 收到订阅消息 : " + msg);
                DataDiversionHandler.realDataDiversion(msg);
            }, (msg) -> {
                System.out.println(" pub callback 收到发布消息 : " + msg);
            });

            sdk.subscript();
            //保证线程存活。如果在容器中睡眠的代码不要
            //TimeUnit.SECONDS.sleep(600);
            //System.out.println("发布消息返回接信息");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 重新连接
     * @param authKey
     */
    public void reconnect(String authKey) {
        if(sdk != null){
            try{
                sdk.reconnection();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 释放连接
     * @param authKey
     */
    public void release(String authKey){
        if(sdk != null){
            try{
                sdk.release();
                sdk = null;
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取平台数据
     * @param param
     * @param address
     * @return
     * @throws BadAuthorizedException
     */
    public static JSONObject getIotRestRequest(JSONObject param, String address) throws BadAuthorizedException {
        //客户端ID
        String AUTH_KEY = "FpvlRfaoRpWIMzKZiXPO8A";
        //秘钥
        String SECRET_KEY = "baxg6Yv2TSqHLLtupunOsQ";
        if(active.equals("dev")){
            //客户端ID
            AUTH_KEY = "alFdrCTnS1-5z2DjtsMrCg";
            //秘钥
            SECRET_KEY = "7AjxbIUiSou4_JTZKesyhw";
        }
        IotApi apiSDK = DefaultFactory.iotapi(SERVER_URL, AUTH_KEY, SECRET_KEY);
        apiSDK.auth();
        //获取租户ID
        String tenantId = apiSDK.getTenantId();
        if(tenantId == null){
            tenantId = "iot1314";
        }
        if(param != null){
            param.put("tenantId", tenantId);
            if(param.get("deviceId") == null){
                return null;
            }
        }else{
            param = new JSONObject();
            param.put("tenantId", tenantId);
        }

        //返回数据
        String result = apiSDK.doAction(address, param.toJSONString(), HttpMethod.POST);
        System.out.println("返回结果: " + result);
        return JSONObject.parseObject(result);
    }


}
