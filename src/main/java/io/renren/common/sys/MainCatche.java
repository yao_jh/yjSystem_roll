package io.renren.common.sys;

import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.FrameBase;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 内存缓冲区
 */
public class MainCatche {
    // 存放com的对象
    //public static Map<String, Module> comMap = new ConcurrentHashMap<>();
    // 通讯参数点
    //public static Map<String, List<Point>> modulePointMap = new ConcurrentHashMap<>();

    /**
     * 系统基础信息缓存
     */
    // 机组 key:crewId
    public static Map<Long, CrewEntity> crewMap = new ConcurrentHashMap<Long, CrewEntity>();
    // 设备 key:deviceCode
    public static Map<String, DeviceEntity> deviceMap = new ConcurrentHashMap<String, DeviceEntity>();
    // 设备机架 key:code
    public static Map<String, FrameBase> frameMap = new ConcurrentHashMap<String, FrameBase>();

    // 存储实时数据 key:deviceCode, key:classCode
    public static Map<String, Map<String, PointEntity>> dataMap = new ConcurrentHashMap<String, Map<String, PointEntity>>();
    // 存储实时数据 key:pointCode
    public static Map<String, PointEntity> realMap = new ConcurrentHashMap<String, PointEntity>();


}
