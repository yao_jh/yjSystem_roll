package io.renren.common.utils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtils;

public class CommonUtils {

	public static Integer defaultRandomStrLength = new Integer(32);

	/**
	 * 分隔符
	 */
	public static final String SPLIT = ",";

	/**
	 * 分隔符\
	 */
	public static final String SPLIT_S = "\\\\";

	/**
	 * 分割符/
	 */
	public static final String SPLIT_ = "/";

	/**
	 * 消息纪录的分隔符号 |
	 */
	public static final String LOG_MESSAGE_SPLIT = "|";

	/**
	 * 修改如果字符串为NULL 则让字符穿为空串
	 */
	public static String nullToString(String arg) {
		if (CommonUtils.isNull(arg) || arg.equalsIgnoreCase("null")) {
			return "";
		} else {
			return arg;
		}
	}

	/**
	 * 
	 * formartNumber:(格式化数据).
	 */
	public static String formartNumber(Object value, String pattern) {
		DecimalFormat formator = new DecimalFormat(pattern);
		return formator.format(value);
	}

	/**
	 * 分解字符串为字符数组 。
	 * 
	 * @param value
	 *            指定字符串
	 * @param split
	 *            切分字符串。
	 * @return 字符串数组。
	 */
	public static String[] stringToArraySplit(String value, String split) {
		String[] result = new String[0];

		if (isNotNull(value) && split != null) {
			result = value.split(split);
		}

		return result;
	}

	/**
	 * 根据指定的字符从字符串中提取0到指定字符索引位置的字符串
	 * 
	 * @param value
	 *            字符串
	 * @param split
	 *            指定字符
	 * @return 提取后的字符穿
	 */
	public static String extractionStringToIndex(String value, String split) {
		String result = "";
		if (isNull(value) && isNull(split)) {
			return result;
		}
		int index = value.indexOf(split);

		if (index <= 0) {
			return value;
		}
		return value.substring(0, index);

	}

	/**
	 * Long[] 转成分隔形式的字符串
	 * 
	 * @param ls
	 * @return
	 */
	public static String longsToString(Long[] ls) {
		StringBuffer sb = new StringBuffer();
		if (ls != null && ls.length > 0) {
			for (int i = 0; i < ls.length; i++) {
				Long l = ls[i];
				sb.append(l.toString());
				sb.append(SPLIT);
			}
			// 去掉最后一个 分隔符，返回
			String str = sb.substring(0, sb.length() - 1);
			return str;
		}
		return "";
	}

	/**
	 * Object[] 转成分隔形式的字符串
	 * 
	 * @param ls
	 * @return
	 */
	public static String objectsToString(Object[] ls) {
		StringBuffer sb = new StringBuffer();
		if (ls != null && ls.length > 0) {
			for (int i = 0; i < ls.length; i++) {
				Object l = ls[i];
				sb.append(l.toString());
				sb.append(SPLIT);
			}
			// 去掉最后一个 分隔符，返回
			String str = sb.substring(0, sb.length() - 1);
			return str;
		}
		return "";
	}

	/**
	 * Collection\<String\> 转成分隔形式的字符串
	 * 
	 * @param strs
	 * @return
	 */
	public static String strCollectionToStringSplit(Collection strs) {
		StringBuffer sb = new StringBuffer();
		if (strs != null && !strs.isEmpty()) {
			Iterator itr = strs.iterator();
			while (itr.hasNext()) {
				try {
					String item = (String) itr.next();
					sb.append(item.toString());
					sb.append(SPLIT);
				} catch (Exception ex) {

				}
			}
			// 去掉最后一个 分隔符，返回
			String str = sb.substring(0, sb.length() - 1);
			return str;
		}
		return "";
	}

	/**
	 * Collection 转成分隔形式的字符串
	 * 
	 * @param objs
	 * @param propertyName
	 * @return
	 */
	public static String objCollectionToStringSplit(Collection objs, String propertyName) {
		StringBuffer sb = new StringBuffer();
		if (objs != null && !objs.isEmpty()) {
			Iterator itr = objs.iterator();
			while (itr.hasNext()) {
				try {
					Object item = itr.next();
					String itemProperty = BeanUtils.getProperty(item, propertyName);
					sb.append(itemProperty);
					sb.append(SPLIT);
				} catch (Exception ex) {

				}
			}
			// 去掉最后一个 分隔符，返回
			String str = sb.length() > 0 ? sb.substring(0, sb.length() - 1) : sb.toString();
			return str;
		}
		return "";
	}

	/**
	 * Collection\<Long\> 转成分隔形式的字符串
	 * 
	 * @param ls
	 * @return
	 */
	public static String longsToString(Collection ls) {
		StringBuffer sb = new StringBuffer();
		if (ls != null && !ls.isEmpty()) {
			Iterator itr = ls.iterator();
			while (itr.hasNext()) {
				try {
					Long l = (Long) itr.next();
					sb.append(l.toString());
					sb.append(SPLIT);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			// 去掉最后一个 分隔符，返回
			String str = sb.substring(0, sb.length() - 1);
			return str;
		}
		return "";
	}

	/**
	 * 分隔形式的字符串转成Long[]
	 * 
	 * @param strSplit
	 * @return
	 */
	public static Long[] stringsToLong(String strSplit) {
		if (strSplit != null && strSplit.trim().length() > 0) {
			String[] strItems = strSplit.split(SPLIT);
			List longList = new ArrayList();
			for (int i = 0; i < strItems.length; i++) {
				String strItm = strItems[i];
				try {
					Long longItem = toLong(strItm);
					longList.add(longItem);
				} catch (Exception ex) {

				}

			}
			Long[] ls = new Long[longList.size()];
			longList.toArray(ls);
			return ls;
		}
		return new Long[] {};
	}

	/**
	 * 分隔形式的字符串转化成List\<Long\>
	 * 
	 * @param str
	 * @return
	 */
	public static List stringsToLongList(String str) {
		if (str != null && str.trim().length() > 0) {
			String[] strItems = str.split(SPLIT);
			List longList = new ArrayList();
			for (int i = 0; i < strItems.length; i++) {
				String strItm = strItems[i];
				try {
					Long longItem = toLong(strItm);
					longList.add(longItem);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
			return longList;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * String[] 转化为 Long[]
	 * 
	 * @param strArray
	 * @return
	 */
	public static Long[] stringArrayToLongArray(String[] strArray) {
		if (strArray != null && strArray.length > 0) {
			Long[] longArray = new Long[strArray.length];
			for (int i = 0; i < strArray.length; i++) {
				longArray[i] = toLong(strArray[i]);
			}
			return longArray;
		}
		return new Long[] {};

	}

	/**
	 * 纪录用户操作的日志，其格式为user|option|message，其中信息最大允许128Byte.
	 * 
	 * @param user
	 *            当前操作用户的登录帐户名
	 * @param option
	 *            操作名称,eg 增加帐户，删除帐户等。
	 * @param msg
	 *            DB发生时的各自段信息。
	 * @return 纪录用户操作的日志
	 */
	public static String getLogMessage(String user, String option, String msg) {
		StringBuffer sbMsg = new StringBuffer();

		sbMsg.append(user);
		sbMsg.append(LOG_MESSAGE_SPLIT);
		sbMsg.append(option);
		sbMsg.append(LOG_MESSAGE_SPLIT);
		sbMsg.append(msg);

		return sbMsg.toString();
	}

	/**
	 * 判断是否是数字(包括小数)
	 * 
	 * @param str
	 *            要判断的字符串
	 * @return true 是数字 false 不是数字
	 */
	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			if (!Character.isDigit(str.charAt(i)) && !".".equals(String.valueOf(str.charAt(i)))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 判断字符串是否为空对象或空字符串，如果是则返回 True.
	 * 
	 * @param msg
	 *            字符串
	 * @return boolean 字符串为空则为True.
	 */
	public static boolean isNull(String msg) {
		return (msg != null && msg.trim().length() > 0) ? false : true;
	}

	/**
	 * 判断字符串是否为空对象或空字符串，如果是则返回 False.
	 * 
	 * @param msg
	 *            字符串
	 * @return boolean 字符串为空则为False.
	 */
	public static boolean isNotNull(String msg) {
		return !isNull(msg);
	}

	/**
	 * @param strSplit
	 * @return
	 */
	public static List stringsSplitToStringList(String strSplit) {
		List strList = new ArrayList();
		if (strSplit != null && strSplit.trim().length() > 0) {
			String[] strItems = strSplit.split(SPLIT);
			for (int i = 0; i < strItems.length; i++) {
				String str = strItems[i];
				if (!isNull(str)) {
					strList.add(str.trim());
				}
			}
		}
		return strList;
	}

	/**
	 * 分隔形式的字符串分隔为字符串数组
	 * 
	 * @param strSplit
	 * @return
	 */
	public static String[] stringsSplitToStrings(String strSplit) {
		List strList = stringsSplitToStringList(strSplit);
		String[] strs = new String[strList.size()];
		strList.toArray(strs);
		return strs;
	}

	/**
	 * String 转Long
	 * 
	 * @param str
	 * @return
	 */
	public static Long toLong(String str) {
		try {
			if (isNotNull(str)) {
				long n = Long.parseLong(str.trim());
				return new Long(n);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Float f = toFloat(str);
			if (f != null) {
				return new Long(f.longValue());
			}
		}
		return null;
	}

	/**
	 * 将String类型转换为Integer
	 */
	public static int toInt(String str) {
		try {

			if (isNotNull(str)) {
				int context = Integer.parseInt(str);
				return context;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return 0;
	}

	/**
	 * 字符串转数值，如果格式不正确以默认值代替。
	 * 
	 * @param value
	 *            输入字符串
	 * @param defValue
	 *            默认值
	 * @return Long类型。
	 */
	public static Long toLong(String value, Long defValue) {
		try {
			if (isNotNull(value)) {
				long n = Long.parseLong(value.trim());
				return new Long(n);
			}
		} catch (Exception ex) {

		}

		return defValue;
	}

	/**
	 * String 转Long
	 * 
	 * @param str
	 * @return
	 */
	public static Float toFloat(String str) {
		try {
			if (isNotNull(str)) {
				float f = Float.parseFloat(str.trim());
				return new Float(f);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * String 转Double
	 * 
	 * @param str
	 * @return
	 */
	public static Double toDouble(String str) {
		try {
			if (isNotNull(str)) {
				double d = Double.parseDouble(str.trim());
				return new Double(d);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * 往URL后面添加一个参数
	 * 
	 * @param url
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	public static String urlAddParam(String url, String paramName, String paramValue) {

		// 判断后面是添加?还是&
		StringBuffer urlSb = new StringBuffer(url);
		char lastchar = url.charAt(url.length() - 1);
		String last4str = url.substring(url.length() - 4);

		// 判断末尾是什么字符
		if (lastchar == '?' || lastchar == '&' || last4str.toUpperCase().equals("&AMP;")) {

		} else {
			if (url.lastIndexOf('?') < 0) {
				urlSb.append("?");
			} else {
				urlSb.append("&amp;");
			}
		}

		// 加参
		urlSb.append(paramName);
		urlSb.append('=');
		urlSb.append(paramValue);

		return urlSb.toString();
	}

	/**
	 * 对密码进行加密
	 * 
	 * @param inputString 接收输入的密码或字符串
	 *            ,进行加密
	 */
	public static String encodeByHashCode(String inputString) {
		return null;
	}

	/**
	 * 解密,严整密码是否正确
	 * 
	 * @param inputString 接收输入参数
	 * 
	 * @return 返回boolean
	 * 
	 *         true验证成功 false验证失败
	 * 
	 */
	public static boolean validatePassWord(String inputString) {
		return true;
	}

	public static String getText(Object obj) {
		if (null == obj) {
			return "";
		}
		return obj.toString();
	}

	/**
	 * 获取随机字符串.
	 * 
	 * @param length
	 *            字符串长度.
	 * @return 随机字符串
	 */
	public static String getRandomString(Integer length) {
		if (length == null || length.longValue() == 0L) {
			length = defaultRandomStrLength;
		}

		char chSt = 'A';
		char chEd = 'Z';
		char[] chs = new char[length.intValue()];

		for (int i = 0; i < length.intValue(); i++) {
			double rdDb = Math.random();
			double dr = rdDb * (chEd - chSt);
			chs[i] = (char) (chSt + dr);
		}

		String str = new String(chs);
		return str;
	}

	/**
	 * 判断字符串数组中有无重复无素
	 * 
	 * @param strs
	 * @param trim
	 * @return
	 */
	public static boolean isArrayHasSaveItem(String[] strs, boolean trim) {
		int size = strs.length;

		// 去空格
		if (trim) {
			for (int k = 0; k < size; k++) {
				String str = strs[k];
				if (str == null) {
					return true;
				}
				strs[k] = str.trim();
			}
		}

		int kMax = size - 1;
		for (int k = 0; k < kMax; k++) {
			String strK = strs[k];
			for (int i = k + 1; i < size; i++) {
				String strI = strs[i];
				if (strK.equals(strI)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 截取字符串，如果字符串 超过125的长度，则截取保留125长度
	 */
	public static String interceptionString(String arg) {
		String str = "";
		String string = nullToString(arg);
		int length = string.length();
		if (length > 0 && length > 125) {
			str = string.substring(0, 125);
		}
		if (length > 0 && length <= 125) {
			return arg;
		}

		return str;
	}

	/**
	 * 截取字符串，如果字符串 默认为125
	 * 
	 * @param arg
	 * @param length
	 * @return
	 */
	public static String interceptionString(String arg, Integer length) {
		try {

			if (null == length || length.intValue() <= 0) {
				return interceptionString(arg);
			} else {
				String str = "";
				String string = nullToString(arg);
				if (isNull(string)) {
					return str;
				}

				if (string.length() <= length.intValue()) {
					str = string.substring(0, string.length());
				} else {
					str = string.substring(0, length.intValue());
				}
				return str;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	/**
	 * 根据当前系统时间获得今天是星期几
	 * 
	 * @param dt
	 * @return 星期几的数字形式
	 */
	public static int getWeekValue(Date dt) {
		Calendar cal = Calendar.getInstance(Locale.CHINA);
		cal.setTime(dt);
		String a = cal.getTimeZone().getDisplayName(Locale.CHINA);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0) {
			w = 0;
		}
		return w;
	}

	public static boolean isAdmin(Long userId) {
		if (userId != null && userId.longValue() < 0) {
			return true;
		}

		return false;
	}

	/**
	 * 判断value是否在Strs字符串中(strs是以','分隔的字符串)
	 * 
	 * @param strs
	 * @param value
	 * @return
	 */
	public static boolean isHaveElement(String strs, String value) {
		if (isNull(strs))
			return false;
		if (isNull(value))
			return false;
		String[] temp = strs.split(SPLIT);
		for (int i = 0; i < temp.length; i++) {
			if (value.equals(temp[i].trim()))
				return true;
		}

		return false;
	}

	/**
	 * 字符串list中是否有该字符串
	 * 
	 * @param list
	 * @param str
	 * @return
	 */
	public static boolean isHaveStrInList(List<String> list, String str) {
		for (String string : list)
			if (string.equals(str))
				return true;
		return false;
	}

	/**
	 * 将String转码——报表导出时的报表名字
	 * 
	 * @param name
	 * @return
	 */
	public static String transcodingForExportReportName(String name) {
		String result = "";
		try {

			result = new String(name.getBytes("GBK"), "ISO8859-1") + ".xls";

		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
		return result;
	}

	/**
	 * 将String转码——报表导出时的报表名字图片
	 * 
	 * @param name
	 * @return
	 */
	public static String transcodingForExportReportNameJPG(String name) {
		String result = "";
		try {

			result = new String(name.getBytes("GBK"), "ISO8859-1");

		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
		return result;
	}

	/**
	 * 数组 转集合
	 * 
	 * @param objects
	 *            数组
	 * @return
	 */
	public static Collection toCollection(Object[] objects) {
		return Arrays.asList(objects);
	}

	/**
	 * 从字符串中取最后面的数字 例如：ABC12deF888 取出的结果是888
	 * 
	 * @param str
	 * @return
	 */
	public static String getLastNumFormString(String str) {
		if (isNull(str))
			return "";
		String numStr = "";
		Pattern p = Pattern.compile("(?<!#|\\d)(?:\\d+(?:[yY]|[Kk](?![vV])))|\\d{3,}(?![kK][vV])");
		Matcher m = p.matcher(str);
		while (m.find()) {
			numStr = m.group();
		}
		return numStr;

	}

	/**
	 * 将字符串中的数字取出并求合
	 * 
	 * @param str
	 * @return
	 */
	public static int getSumNumFromString(String str) {
		int num = 0;
		if (isNull(str))
			return num;

		Pattern p = Pattern.compile("\\d+(,\\d{3})*");
		Matcher m = p.matcher(str);
		while (m.find()) {
			num += (new Integer(m.group())).intValue();
		}

		return num;
	}

	/**
	 * 将字符串中的第一个数字取出
	 * 
	 * @param str
	 * @return
	 */
	public static int getNumberFromString(String str) {
		int num = 0;
		if (isNull(str))
			return num;

		Pattern p = Pattern.compile("\\d+(,\\d{3})*");
		Matcher m = p.matcher(str);
		while (m.find()) {
			return (new Integer(m.group())).intValue();
		}

		return num;
	}

	/**
	 * 转经纬度格式 度分秒改为NTU十进制
	 * 
	 * @param str
	 * @return
	 */
	public static String changeLongitudeAndLatitudeFormat(String str) {
		if (CommonUtils.isNull(str))
			return "";
		String tmp[] = str.split("\\.");
		if (tmp.length != 3 && tmp.length != 2)
			return "";
		double result = 0d;
		String tmpStr = "";
		if (tmp.length == 3) {
			// 暂未用
			result = Double.parseDouble(tmp[0]) + Double.parseDouble(tmp[1]) / 60
					+ Double.parseDouble(tmp[2]) / 60 / 60;
		} else if (tmp.length == 2) {
			result = Double.parseDouble(tmp[0])
					+ Double.parseDouble(tmp[1].substring(0, 2) + "." + tmp[1].substring(2)) / 60;
			DecimalFormat df2 = new DecimalFormat("#.00000");
			tmpStr = String.valueOf(df2.format(result));
		} else {
			tmpStr = str;
		}
		return tmpStr.replaceAll("\\.", "");
	}

	/**
	 * 转经纬度格式 度分秒改为NTU十进制
	 * 
	 * @param str
	 * @return
	 */
	public static String changeLongitudeAndLatitudeFormatOLD(String str) {
		if (CommonUtils.isNull(str))
			return "";
		String tmp[] = str.split("\\.");
		if (tmp.length != 3 && tmp.length != 2)
			return "";
		double result = 0d;
		String tmpStr = "";
		if (tmp.length == 3) {
			result = Double.parseDouble(tmp[0]) + Double.parseDouble(tmp[1]) / 60
					+ Double.parseDouble(tmp[2]) / 60 / 60;
			DecimalFormat df2 = new DecimalFormat("#.00000");
			tmpStr = String.valueOf(df2.format(result));
		} else if (tmp.length == 2) {
			result = Double.parseDouble(tmp[0]) + Double.parseDouble(tmp[1]) / 60;
			DecimalFormat df2 = new DecimalFormat("#.00000");
			tmpStr = String.valueOf(df2.format(result));

		} else {

			tmpStr = str;
		}
		return tmpStr.replaceAll("\\.", "");
	}

	/**
	 * 自动补位。不足两位，前面补0
	 */
	public static String autoFillZeroForTwoStr(String str) {
		if (null == str)
			return str;
		if (str.length() == 0)
			return "00";
		if (str.length() == 1)
			return "0" + str;
		return str;
	}

	/**
	 * 自动补位。不足8位，前面补0
	 */
	public static String autoFillZeroFor8Str(String str) {
		if (null == str)
			return str;
		if (str.length() == 0)
			return "00000000";
		if (str.length() == 1)
			return "0000000" + str;
		if (str.length() == 2)
			return "000000" + str;
		if (str.length() == 3)
			return "00000" + str;
		if (str.length() == 4)
			return "0000" + str;
		if (str.length() == 5)
			return "000" + str;
		if (str.length() == 6)
			return "00" + str;
		if (str.length() == 7)
			return "0" + str;
		return str;
	}

	/**
	 * 计算两点距离; 结果单位: 米
	 * 
	 * @param srcLongitude
	 * @param srcLatitude
	 * @param disLongitude
	 * @param disLatitude
	 * @return
	 */
	public static long getLongitudeAndLatitudeLimit(String srcLongitude, String srcLatitude, String disLongitude,
			String disLatitude) {
		if (CommonUtils.isNull(srcLongitude) || CommonUtils.isNull(srcLatitude) || CommonUtils.isNull(disLongitude)
				|| CommonUtils.isNull(disLatitude))
			return 0;

		srcLongitude = srcLongitude.replace(".", "");
		srcLatitude = srcLatitude.replace(".", "");
		disLongitude = disLongitude.replace(".", "");
		disLatitude = disLatitude.replace(".", "");
		// 如何计算两点间距离
		// 比如：点A的经度为11695400，纬度为3995400。点B的经度为11695300，纬度为3995300。
		// 公式:两点间距离 = [ (A点经度 - B点经度)^2 + (A点纬度 - B点纬度)^2 ] ^ (1/2)
		// = [ (11695400 - 11695300)^2+ (3995400 - 3995300)^2 ] ^(1/2)
		// =(10000+10000) ^ (1/2) =141米
		// 公式说明:加法 + ，减法 - ，乘法 *，除法 /，幂运算 ^(1/2)表示平方根 ^2表示平方
		double sLo = Double.parseDouble(srcLongitude);
		double sLa = Double.parseDouble(srcLatitude);
		double dLo = Double.parseDouble(disLongitude);
		double dLa = Double.parseDouble(disLatitude);

		double result = Math.sqrt((dLo - sLo) * (dLo - sLo) + (dLa - sLa) * (dLa - sLa));
		return (long) result;
	}

	/**
	 * 转16进制acsii码为字符串
	 * 
	 * @param buf
	 * @return
	 */
	public static String getStringforAscii(byte buf[]) {
		String str = "";
		for (int i = 0; i < buf.length; i++) {
			String c = String.valueOf((char) Integer.parseInt(Integer.toHexString(buf[i]), 16));
			str += c;
		}
		return str;
	}

	/**
	 * 十六进制转十进制
	 * 
	 * @param hex
	 * @return
	 */
	public static int getNumFromHex(String hex) {
		return Integer.valueOf(hex, 16);
	}

	public static String[] getStringArryFromList(List<String> list) {
		String[] str = new String[list.size()];
		if (null == list)
			return str;
		for (int i = 0; i < list.size(); i++) {
			str[i] = list.get(i);
		}
		return str;
	}

	/**
	 * 小数点后两位
	 * 
	 * @param value
	 * @return
	 */
	public static double getConvertDoubleTwoDecimal(Double value) {
		if (null == value)
			return 0d;
		long l1 = Math.round(value * 100); // 四舍五入
		double ret = l1 / 100.0; // 注意：使用 100.0 而不是 100
		return ret;
	}

	/**
	 * 小数点后两位
	 * 
	 * @param value
	 * @return
	 */
	public static Float getConvertFloatTwoDecimal(Float value) {
		if (null == value)
			return 0.0F;
		return (float) (Math.round(value * 100)) / 100;
	}

	/**
	 * 判断是否有汉字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isChinese(String str) {
		return !(str.getBytes().length == str.length());
	}

	/**
	 * 补齐不足长度
	 * 
	 * @param length
	 *            长度
	 * @param number
	 *            数字
	 * @return
	 */
	public static String addZero(int length, int number) {
		String f = "%0" + length + "d";
		return String.format(f, number);
	}

	/**
	 * 
	 * @Title: bigNumberToNumber
	 * @Description: 科学记数法转数字
	 * @param val
	 * @return
	 * @return: String
	 */
	public static String bigNumberToNumber(String val) {
		String bigStr = "";
		try {
			BigDecimal big = new BigDecimal(val);
			bigStr = big.toPlainString();
		} catch (Exception e) {
			return null;
		}

		return bigStr;
	}

	public static BigDecimal getBigDecimal( Object value ) {
		BigDecimal ret = null;
		if( value != null ) {
			if( value instanceof BigDecimal ) {
				ret = (BigDecimal) value;
			} else if( value instanceof String ) {
				ret = new BigDecimal( (String) value );
			} else if( value instanceof BigInteger) {
				ret = new BigDecimal( (BigInteger) value );
			} else if( value instanceof Number ) {
				ret = new BigDecimal( ((Number)value).doubleValue() );
			} else {
				throw new ClassCastException("Not possible to coerce ["+value+"] from class "+value.getClass()+" into a BigDecimal.");
			}
		}
		return ret;
	}

	/**
	 * 返回数值,空值返回0
	 * @param val
	 * @return
	 */
	public static BigDecimal getRealValue(BigDecimal val){
		if(val == null){
			return BigDecimal.ZERO;
		}else{
			return val;
		}
	}

	/**
	 * 返回数值,空值返回0
	 * @param value
	 * @return
	 */
	public static BigDecimal getRealBigDecimal(Object value){
		BigDecimal ret = getBigDecimal(value);
		ret = getRealValue(ret);
		return ret;
	}

	/**
	 * 解析值表达式
	 * @param val 数值 (1.0)
	 * @param exp 表达式(例如: 0:停止;1:运行)
	 * @return 转换后的值(例如: 停止),如果没有数值对应,返回null
	 */
	public static String resValExp(BigDecimal val, String exp){
		if (exp == null || "".equals(exp)) return "0";
		String[] itemArr = exp.split(";");
		for(int i=0, len=itemArr.length; i<len; i++){
			if (itemArr[i] == null || "".equals(itemArr[i])) continue;
			String[] conArr = itemArr[i].split(":");
			if (val.doubleValue() == Double.parseDouble(conArr[0])) {
				return conArr[1];
			}
		}
		return "0";
	}

	/**
	 * 对象反射赋值
	 *
	 * @param source 目标对象
	 * @param target 赋值对象
	 */
	public static void copyBeanFieldsValue(Object source, Object target) {
		Field[] fields = target.getClass().getDeclaredFields();
		try {
			for (Field field : fields) {
				String firstLetter = field.getName().substring(0, 1).toUpperCase(); // 首字母大写
				String getMethodName = "get" + firstLetter + field.getName().substring(1);
				String setMethodName = "set" + firstLetter + field.getName().substring(1);

				Method setMethod = target.getClass().getMethod(setMethodName, new Class[] { field.getType() });
				Method getMethod = source.getClass().getMethod(getMethodName);
				Object value = getMethod.invoke(source);
				if(value == null) continue;
				setMethod.invoke(target.getClass(), new Object[] { value });

			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * 获取请求参数ID
	 * @param params
	 * @param key
	 * @return
	 */
	public static Long getLongByParamMap(Map<String, Object> params, String key){
		Map<String, Object> elem = params;
		if(params.containsKey("params")){
			elem = (Map<String, Object>) params.get("params");
		}
		String id = String.valueOf(params.get(key));
		if(id != null){
			return Long.valueOf(id);
		}
		return null;

	}

}