
package io.renren.common.utils;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期处理
 *
 * @author Mark sunlightcs@gmail.com
 */
public class DateUtils {
	/** 时间格式(yyyy-MM-dd) */
	public final static String DATE_PATTERN = "yyyy-MM-dd";
	/** 时间格式(yyyy-MM-dd HH:mm:ss) */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /** 时间格式(yyyy-MM-dd HH:mm:ss.SSS) */
    public final static String DATE_MSTIME_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     * @param date  日期
     * @return  返回yyyy-MM-dd格式日期
     */
	public static String format(Date date) {
        return format(date, DATE_PATTERN);
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     * @param date  日期
     * @param pattern  格式，如：DateUtils.DATE_TIME_PATTERN
     * @return  返回yyyy-MM-dd格式日期
     */
    public static String format(Date date, String pattern) {
        if(date != null){
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }

    /**
     * 字符串转换成日期
     * @param strDate 日期字符串
     */
    public static Date stringToDate(String strDate) {
        if (StringUtils.isBlank(strDate)){
            return null;
        }

        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_PATTERN);
        return fmt.parseLocalDateTime(strDate).toDate();
    }

    /**
     * 字符串转换成日期
     * @param strDate 日期字符串
     * @param pattern 日期的格式，如：DateUtils.DATE_TIME_PATTERN
     */
    public static Date stringToDate(String strDate, String pattern) {
        if (StringUtils.isBlank(strDate)){
            return null;
        }

        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        return fmt.parseLocalDateTime(strDate).toDate();
    }

    /**
     * 根据周数，获取开始日期、结束日期
     * @param week  周期  0本周，-1上周，-2上上周，1下周，2下下周
     * @return  返回date[0]开始日期、date[1]结束日期
     */
    public static Date[] getWeekStartAndEnd(int week) {
        DateTime dateTime = new DateTime();
        LocalDate date = new LocalDate(dateTime.plusWeeks(week));

        date = date.dayOfWeek().withMinimumValue();
        Date beginDate = date.toDate();
        Date endDate = date.plusDays(6).toDate();
        return new Date[]{beginDate, endDate};
    }

    /**
     * 对日期的【秒】进行加/减
     *
     * @param date 日期
     * @param seconds 秒数，负数为减
     * @return 加/减几秒后的日期
     */
    public static Date addDateSeconds(Date date, int seconds) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusSeconds(seconds).toDate();
    }

    /**
     * 对日期的【分钟】进行加/减
     *
     * @param date 日期
     * @param minutes 分钟数，负数为减
     * @return 加/减几分钟后的日期
     */
    public static Date addDateMinutes(Date date, int minutes) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMinutes(minutes).toDate();
    }

    /**
     * 对日期的【小时】进行加/减
     *
     * @param date 日期
     * @param hours 小时数，负数为减
     * @return 加/减几小时后的日期
     */
    public static Date addDateHours(Date date, int hours) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusHours(hours).toDate();
    }

    /**
     * 对日期的【天】进行加/减
     *
     * @param date 日期
     * @param days 天数，负数为减
     * @return 加/减几天后的日期
     */
    public static Date addDateDays(Date date, int days) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(days).toDate();
    }

    /**
     * 对日期的【周】进行加/减
     *
     * @param date 日期
     * @param weeks 周数，负数为减
     * @return 加/减几周后的日期
     */
    public static Date addDateWeeks(Date date, int weeks) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusWeeks(weeks).toDate();
    }

    /**
     * 对日期的【月】进行加/减
     *
     * @param date 日期
     * @param months 月数，负数为减
     * @return 加/减几月后的日期
     */
    public static Date addDateMonths(Date date, int months) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMonths(months).toDate();
    }

    /**
     * 对日期的【年】进行加/减
     *
     * @param date 日期
     * @param years 年数，负数为减
     * @return 加/减几年后的日期
     */
    public static Date addDateYears(Date date, int years) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusYears(years).toDate();
    }

    /**
     * 获取时间差（差多少天）
     *
     * @param stDate
     * @param endDate
     * @return
     */
    public static long getDateDiffForDay(Date stDate, Date endDate) {
        if (stDate == null || endDate == null) {
            return 0;
        }
        long stMiniS = stDate.getTime();
        long endMiniS = endDate.getTime();
        long chaL = endMiniS - stMiniS;
        long day = chaL / (24 * 60 * 60 * 1000);
        return day;
    }

    /**
     * 获取时间差（差多少小时）
     * @param stDate
     * @param endDate
     * @return
     */
    public static long getDateDiffForHour(Date stDate, Date endDate) {
        if (stDate == null || endDate == null) {
            return 0;
        }
        long startTime = stDate.getTime();
        long endTime = endDate.getTime();
        long chaL = endTime - startTime;
        long hours = chaL / (1000 * 60 * 60);// 根据时间差（微秒）计算小时差

        return hours;
    }

    /**
     * 获取时间差（差多少分钟）
     *
     * @param stDate
     * @param endDate
     * @return
     */
    public static long getDateDiffForMini(Date stDate, Date endDate) {
        if (stDate == null || endDate == null) {
            return 0;
        }
        long stMiniS = stDate.getTime();
        long endMiniS = endDate.getTime();
        long chaL = endMiniS - stMiniS;
        long min = chaL / (1000 * 60);
        return min;
    }

    /**
     * 获取时间差（差多少分秒）
     *
     * @param stDate
     * @param endDate
     * @return
     */
    public static long getDateDiffForSecond(Date stDate, Date endDate) {
        if (stDate == null || endDate == null) {
            return 0;
        }
        long stMiniS = stDate.getTime();
        long endMiniS = endDate.getTime();
        long chaL = endMiniS - stMiniS;
        long second = chaL / 1000;
        return second;
    }

    /**
     * 获取时间差（差多少分毫秒）
     *
     * @param stDate
     * @param endDate
     * @return
     */
    public static long getDateDiffForMilis(Date stDate, Date endDate) {
        if (stDate == null || endDate == null) {
            return 0;
        }
        long stMiniS = stDate.getTime();
        long endMiniS = endDate.getTime();
        return endMiniS - stMiniS;
    }

    /**
     * 修改日期到参数日期的凌晨(00:00:00)
     *
     * @return
     */
    public static final Date patchDateToAM(Date src) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(src);
        calendar.set(Calendar.HOUR_OF_DAY, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.MILLISECOND, 000);
        return calendar.getTime();
    }

    /**
     * /** 修改日期到参数日期的午夜(23:59:59)
     *
     * @return
     */
    public static final Date patchDateToMidnight(Date src) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(src);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * 到明天0点
     *
     * @param src
     * @return
     */
    public static final Date patchDateToTomorrowAM(Date src) {
        return patchDateToAM(getTomorrow(new Date()));
    }

    /**
     * 到昨天0点
     *
     * @return
     */
    public static final Date patchDateToYesterdayAM() {
        return patchDateToAM(getYesterday(new Date()));
    }

    /**
     * 根据日期得到第二天的日期
     *
     * @param src
     * @return
     */
    public static final Date getTomorrow(Date src) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(src);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    /**
     * 根据日期得到前一天的日期
     *
     * @param src
     * @return
     */
    public static final Date getYesterday(Date src) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(src);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        return calendar.getTime();
    }

    /**
     * 取得日期所在周的第一天
     *
     * @param date
     * @return
     */
    public static Date patchDateToWeekStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1); // 解决周日会出现 并到下一周的情况
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return calendar.getTime();
    }

    /**
     * 取得日期下周的第一天
     *
     * @param date
     * @return
     */
    public static Date patchDateToNextWeekStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1); // 解决周日会出现 并到下一周的情况
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.add(Calendar.DATE, 7);
        return calendar.getTime();
    }

    /**
     * 取得日期所在周的最后一天
     *
     * @param date
     * @return
     */
    public static Date patchDateToWeekEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1); // 解决周日会出现 并到下一周的情况
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return addDatePart(calendar.getTime(), Calendar.DAY_OF_MONTH, 7);
    }

    /**
     * 修改一个日期对象的某个项的值，增加/减少指定值
     *
     * @param dateSrc
     * @param field
     * @param value
     * @return
     */
    public static Date addDatePart(Date dateSrc, int field, int value) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateSrc);
        calendar.add(field, value);

        return calendar.getTime();
    }

}
