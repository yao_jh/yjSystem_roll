package io.renren.common.utils;

import io.renren.common.sys.MainCatche;
import io.renren.modules.operation.entity.PointEntity;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;

public class ExpressUtils {

    /**
     * 处理公式
     * @param point
     * @param value
     * @return
     */
    public static BigDecimal consolidateData(PointEntity point, Object value){
        String express = point.getExpress();
        BigDecimal val = CommonUtils.getRealBigDecimal(value);
        // 处理公式转换
        if(express != null){
           String code = point.getCode();
            //自身参与运算
            //例: #current#*10;  #current#/10
            String _self = "#" + code + "#";
            if(express.indexOf(_self) > -1){
                express = express.replaceAll(_self, val.toPlainString());
            }
            //处理缺省本身参与运算的公式
            char firstChart = express.charAt(0);
            if(firstChart == '+' || firstChart == '-' ||
                    firstChart == '*' || firstChart == '/' || firstChart == '%'){
                express = val.toPlainString() + express;
            }
            //自身不参与运算
            //带有其他点的运算
            express = operateExpress(express);
            // 计算公式，例:*10; /100
            val = resExpress(express);
        }
        return val;
    }

    /**
     * 公式计算数值
     * @param formula
     * @return
     */
    public static BigDecimal resExpress(String formula) {
        ScriptEngine jse = new ScriptEngineManager().getEngineByName("JavaScript");
        Object value = null;
        try{
            value = jse.eval(formula);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return CommonUtils.getRealBigDecimal(value);
    }

    /**
     * 处理携带点的公式运算
     * 将点名转换为所代表的实际数值
     * @param express
     * @return
     */
    public static String operateExpress(String express){
        int site = express.indexOf("#");
        //site判断是否包含转转
        if(site > -1){
            String plaint = express.substring(site + 1, express.length());
            int next = plaint.indexOf("#");
            if(next > -1){
                String code = plaint.substring(0, next);
                BigDecimal value = getRealValueByCode(code);
                code = "#" + code + "#";
                express = express.replaceAll(code, value.toPlainString());
                //递归调用
                express = operateExpress(express);
            }
        }
        return express;
    }

    /**
     * 获取Code的实时数据
     * @param code
     * @return
     */
    private static BigDecimal getRealValueByCode(String code){
        PointEntity point = MainCatche.realMap.get(code);
        if(point != null){
            return CommonUtils.getRealBigDecimal(point.getValue());
        }
        return BigDecimal.ZERO;
    }
}
