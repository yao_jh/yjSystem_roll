package io.renren.modules.learn.vo;

import lombok.Data;

/**
 * 自学习类型视图
 */
@Data
public class LearnTypeVo {
    /**
     * 名称
     */
    private String name;
    /**
     * 设备类型，motor:电机;mill:轧机;servo_valve:伺服阀
     */
    private String deviceType;
    /**
     * 学习状态 0:停止;1:学习中;2:已完成
     */
    private Integer state;

}
