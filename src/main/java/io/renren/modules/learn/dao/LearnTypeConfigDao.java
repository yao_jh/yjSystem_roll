package io.renren.modules.learn.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.learn.entity.LearnTypeConfigEntity;
import io.renren.modules.learn.vo.LearnTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 自学习类型Dao
 *
 */
@Mapper
public interface LearnTypeConfigDao extends BaseMapper<LearnTypeConfigEntity> {

    /**
     * 更新状态到学习中
     */
    void updateStateForRun();

    /**
     * 更新状态到完成
     */
    void updateStateForFinish();

    /**
     * 更新状态到过期
     * 学习过程中临时设置停止的场景
     */
    void updateStateForOverdue();

    /**
     * 查询列表
     * @param learnTypeVo
     * @return
     */
    List<LearnTypeConfigEntity> selectListByVo(@Param("learnTypeVo") LearnTypeVo learnTypeVo);
}
