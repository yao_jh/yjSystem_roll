package io.renren.modules.learn.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.learn.entity.LearnConfigEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 自学习Dao
 *
 */
@Mapper
public interface LearnConfigDao extends BaseMapper<LearnConfigEntity> {


    /**
     * 查询状态学习配置记录
     * @param state
     * @return
     */
    List<LearnConfigEntity> queryListByState(Integer state);

    /**
     * 更新状态到学习中
     */
    void updateStateForRun();

    /**
     * 更新状态到完成
     */
    void updateStateForFinish();

    /**
     * 更新状态到过期
     * 学习过程中临时设置停止的场景
     */
    void updateStateForOverdue();

    /**
     * 删除类型配置
     * @param typeId
     * @return
     */
    int deleteByTypeId(Long typeId);
}
