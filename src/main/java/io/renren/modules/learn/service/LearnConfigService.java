
package io.renren.modules.learn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.learn.entity.LearnConfigEntity;

import java.util.List;

/**
 * 自学习配置接口
 *
 */
public interface LearnConfigService extends IService<LearnConfigEntity> {


    /**
     * 查询状态学习配置记录
     * @param state
     * @return
     */
    List<LearnConfigEntity> queryListByState(Integer state);

    /**
     * 更新状态到学习中
     * 未开始->学习中
     */
    void updateStateForRun();

    /**
     * 更新状态到完成
     * 学习中->完成
     */
    void updateStateForFinish();

    /**
     * 更新状态到过期
     * 学习过程中临时设置停止的场景
     * 停止->已过期
     */
    void updateStateForOverdue();

    /**
     * 加载自学习配置（学习中的）
     */
    void initLearning();

    /**
     * 删除配置
     * @param typeId
     * @return
     */
    int deleteByTypeId(Long typeId);
}
