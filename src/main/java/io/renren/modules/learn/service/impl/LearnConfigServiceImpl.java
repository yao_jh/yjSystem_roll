package io.renren.modules.learn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.sys.MainCatche;
import io.renren.modules.learn.dao.LearnConfigDao;
import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.learn.service.LearnConfigService;
import io.renren.modules.learn.utils.LearnShuntUtils;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("learnConfigService")
public class LearnConfigServiceImpl extends ServiceImpl<LearnConfigDao, LearnConfigEntity> implements LearnConfigService {


    @Override
    public List<LearnConfigEntity> queryListByState(Integer state) {
        return baseMapper.queryListByState(state);
    }

    @Override
    public void updateStateForRun() {
        baseMapper.updateStateForRun();
    }

    @Override
    public void updateStateForFinish() {
        baseMapper.updateStateForFinish();
    }

    @Override
    public void updateStateForOverdue() {
        baseMapper.updateStateForOverdue();
    }

    @Override
    public void initLearning() {
        //学习中的配置记录
        List<LearnConfigEntity> list = this.queryListByState(1);
        for (LearnConfigEntity learn : list){
            LearnShuntUtils.learnConfigMap.put(learn.getPointCode(), learn);
        }
    }

    @Override
    public int deleteByTypeId(Long typeId) {
        return baseMapper.deleteByTypeId(typeId);
    }
}
