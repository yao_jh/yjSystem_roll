
package io.renren.modules.learn.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import io.renren.modules.learn.entity.LearnTypeConfigEntity;
import io.renren.modules.learn.vo.LearnTypeVo;

import java.io.Serializable;
import java.util.List;


/**
 * 自学习类型配置接口
 *
 */
public interface LearnTypeConfigService extends IService<LearnTypeConfigEntity> {


    /**
     * 更新状态到学习中
     * 未开始->学习中
     */
    void updateStateForRun();

    /**
     * 更新状态到完成
     * 学习中->完成
     */
    void updateStateForFinish();

    /**
     * 更新状态到过期
     * 学习过程中临时设置停止的场景
     * 停止->已过期
     */
    void updateStateForOverdue();

    /**
     * 查询列表
     * @param learnTypeVo
     * @return
     */
    List<LearnTypeConfigEntity> queryListByVo(LearnTypeVo learnTypeVo);

    /**
     * 保存
     * @param entity
     * @return
     */
    boolean save(LearnTypeConfigEntity entity);

    /**
     * 修改
     * @param entity
     * @return
     */
    boolean updateById(LearnTypeConfigEntity entity);

    /**
     * 删除配置
     * @param id
     * @return
     */
    boolean removeById(Long id);
}
