package io.renren.modules.learn.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.sys.MainCatche;
import io.renren.modules.learn.dao.LearnTypeConfigDao;
import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.learn.entity.LearnTypeConfigEntity;
import io.renren.modules.learn.service.LearnConfigService;
import io.renren.modules.learn.service.LearnTypeConfigService;
import io.renren.modules.learn.utils.LearnShuntUtils;
import io.renren.modules.learn.vo.LearnTypeVo;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.DeviceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("learnTypeConfigService")
public class LearnTypeConfigServiceImpl extends ServiceImpl<LearnTypeConfigDao, LearnTypeConfigEntity> implements LearnTypeConfigService {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private LearnConfigService learnConfigService;

    @Override
    public void updateStateForRun() {
        baseMapper.updateStateForRun();
    }

    @Override
    public void updateStateForFinish() {
        baseMapper.updateStateForFinish();
    }

    @Override
    public void updateStateForOverdue() {
        baseMapper.updateStateForOverdue();
    }

    @Override
    public List<LearnTypeConfigEntity> queryListByVo(LearnTypeVo learnTypeVo) {
        return baseMapper.selectListByVo(learnTypeVo);
    }

    @Override
    public boolean save(LearnTypeConfigEntity entity) {
        //保存配置
        super.save(entity);
        //保存自学习配置
        saveLearnConfigByTypeConfig(entity);
        return true;
    }

    @Override
    public boolean updateById(LearnTypeConfigEntity entity) {
        //删除配置
        learnConfigService.deleteByTypeId(entity.getId());
        //保存自学习配置
        saveLearnConfigByTypeConfig(entity);
        return true;
    }

    @Override
    public boolean removeById(Long id) {
        //删除类型配置
        baseMapper.deleteById(id);
        //删除参数配置
        learnConfigService.deleteByTypeId(id);
        return true;
    }

    /**
     * 保存自学习配置
     * @param entity
     */
    private void saveLearnConfigByTypeConfig(LearnTypeConfigEntity entity){
        //设备列表
        List<DeviceEntity> deviceList = deviceService.queryListByPlantIdAndType(entity.getPlantId(),entity.getDeviceType());
        if(deviceList != null){
            //遍历类型的设备，匹配参数类型，进一步创建参数的自学习配置
            for(DeviceEntity device : deviceList){
                Map<String, PointEntity> dataMap = MainCatche.dataMap.get(device.getCode());
                if(dataMap != null){
                    PointEntity point = dataMap.get(entity.getClassCode());
                    if(point != null){
                        //创建设备的参数配置
                        createLearnConfig(point.getCode(), entity);
                    }
                }
            }
        }
    }

    /**
     * 创建参数的自学习配置
     * @param pointCode
     * @param entity
     */
    private void createLearnConfig(String pointCode, LearnTypeConfigEntity entity){
        //参数配置
        LearnConfigEntity config = new LearnConfigEntity();
        BeanUtils.copyProperties(entity, config);
        //设置初始化值
        config.setId(null);
        config.setTypeId(entity.getId());
        config.setPointCode(pointCode);
        config.setState(0);
        config.setCorrect(0);
        //保存
        learnConfigService.save(config);
        //缓存
        LearnShuntUtils.learnConfigMap.put(pointCode, config);
    }

}
