package io.renren.modules.learn.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("tb_learn_config")
public class LearnConfigEntity {

    @TableId
    private Long id;

    /**
     * 类型ID
     * 空：独立配置
     */
    private Long typeId;

    /**
     * 参数code
     */
    private String pointCode;

    /**
     * 学习状态
     * 0:停止;1:学习中;2:已完成;
     */
    private Integer state;

    /**
     * 学习形式
     * 0:均值; 1:最小值;2:最大值;
     */
    private Integer shape;

    /**
     * 启用学习结果
     * 0:不启用;1:启用
     */
    private Integer correct;

    /**
     * 学习值
     */
    private BigDecimal value;

    /**
     * 开始时间
     */
    private Date startDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 备注
     */
    private String remark;

    // 总值
    @Transient
    private transient BigDecimal sum;
    //数量
    @Transient
    private transient Integer num = 0;
    //均值 = 总值/数量
    @Transient
    private transient BigDecimal avg;
    //最小值
    @Transient
    private transient BigDecimal min;
    //最大值
    @Transient
    private transient BigDecimal max;

    /**
     * 学习进度
     * 停止：0%;
     * 学习中：(当前时间-开始时间)/(结束时间-开始时间)
     * 已完成：100%
     */
    @Transient
    private transient Long percent;

    public Long getPercent() {
        if(state == null){
            return 0L;
        }else if(state.intValue() == 0){
            return 0L;
        }else if(state.intValue() == 1){
            //学习进度公式：(当前时间-开始时间)/(结束时间-开始时间)
            Long l = DateUtils.getDateDiffForHour(startDate, new Date());
            Long m = DateUtils.getDateDiffForHour(startDate, endDate);
            return l/m;
        }else{
            return 100L;
        }
    }

    public BigDecimal getValue() {
        if(getShape() == 0){
            value = getAvg();
        }else if(getShape() == 1){
            value = getMin();
        }else if(getShape() == 2){
            value = getMax();
        }
        return value;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
        this.num ++;
    }

    //累加
    public BigDecimal toAccumSum(BigDecimal value){
        setSum(getSum().add(value));
        return getSum();
    }

    public BigDecimal getAvg() {
        if(sum != null && num != null){
            avg = sum.divide(BigDecimal.valueOf(num)).setScale(3, BigDecimal.ROUND_HALF_DOWN);
        }
        return avg;
    }

    public Integer getShape() {
        if(shape == null){
            shape = 0;
        }
        return shape;
    }
}
