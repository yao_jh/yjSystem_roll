package io.renren.modules.learn.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.util.Date;

@Data
@TableName("tb_learn_type_config")
public class LearnTypeConfigEntity {

    @TableId
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 产线ID
     */
    private Long plantId;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 参数归类
     */
    private String classCode;

    /**
     * 学习状态
     * 0:停止;1:学习中;2:已完成;
     */
    private Integer state;

    /**
     * 学习形式
     * 0:均值; 1:最小值;2:最大值;
     */
    private Integer shape;

    /**
     * 开始时间
     */
    private Date startDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 备注
     */
    private String remark;

    @Transient
    private String startDateStr;

    @Transient
    private String endDateStr;


    public Date getStartDate() {
        if (startDate == null && startDateStr != null){
            startDate = DateUtils.stringToDate(startDateStr);
        }
        return startDate;
    }

    public Date getEndDate() {
        if (endDate == null && endDateStr != null){
            endDate = DateUtils.stringToDate(endDateStr);
        }
        return endDate;
    }

    public String getStartDateStr() {
        if (startDate != null && startDateStr == null){
            startDateStr = DateUtils.format(startDate);
        }
        return startDateStr;
    }

    public String getEndDateStr() {
        if (endDate != null && endDateStr == null){
            endDateStr = DateUtils.format(endDate);
        }
        return endDateStr;
    }
}
