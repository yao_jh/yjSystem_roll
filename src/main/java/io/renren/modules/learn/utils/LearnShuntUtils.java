package io.renren.modules.learn.utils;

import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 自学习工具
 */
public class LearnShuntUtils {

    // 每天定时保存更新，到期的不再加载
    // 自学习配置 key:pointCode
    public static Map<String, LearnConfigEntity> learnConfigMap = new ConcurrentHashMap<String, LearnConfigEntity>();

    /**
     * 自学习分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        LearnConfigEntity learnConfig = learnConfigMap.get(point.getCode());
        if(learnConfig != null){
            Integer shape = learnConfig.getShape();
            if(shape == null) shape = 0;
            BigDecimal value = point.getValue();
            switch (shape){
                case 0://累加算均值
                    learnConfig.toAccumSum(value);
                    break;
                case 1://最小
                    BigDecimal min = learnConfig.getMin();
                    if(min == null){
                        min = value;
                    }
                    if(min.compareTo(value) > 0){
                        min = value;
                    }
                    learnConfig.setMin(min);
                    break;
                case 2://最大
                    BigDecimal max = learnConfig.getMax();
                    if(max == null){
                        max = value;
                    }
                    if(max.compareTo(value) < 0){
                        max = value;
                    }
                    learnConfig.setMax(max);
                    break;
            }
        }
    }

}
