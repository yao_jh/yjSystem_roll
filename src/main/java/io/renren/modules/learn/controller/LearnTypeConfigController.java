package io.renren.modules.learn.controller;

import io.renren.common.utils.R;
import io.renren.modules.learn.entity.LearnTypeConfigEntity;
import io.renren.modules.learn.service.LearnTypeConfigService;
import io.renren.modules.learn.vo.LearnTypeVo;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能分析包
 *
 */
@RestController
@RequestMapping("/learnTypeConfig")
@Api("类型自学习配置接口")
public class LearnTypeConfigController extends AbstractController {

    @Autowired
    private LearnTypeConfigService learnTypeConfigService;

    /**
     * 类型配置列表
     */
    @GetMapping("/select")
    @ApiOperation("获取设备/机组实时数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="名称",dataType="String", paramType = "query",example="XXX"),
            @ApiImplicitParam(name="deviceType",value="设备类型，motor:电机;mill:轧机;servo_valve:伺服阀",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="state",value="学习状态 0:停止;1:学习中;2:已完成;",dataType="Integer", paramType = "query")})
    public R select(LearnTypeVo learnTypeVo){
        List<LearnTypeConfigEntity> list = learnTypeConfigService.queryListByVo(learnTypeVo);
        return R.ok().put("list", list);
    }

    /**
     * 新增
     */
    @RequestMapping("/save")
    @ApiOperation("获取设备/机组实时数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="名称",dataType="String", paramType = "query",example="XXX"),
            @ApiImplicitParam(name="deviceType",value="设备类型，motor:电机;mill:轧机;servo_valve:伺服阀",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="classCode",value="参数类型，current:电流;voltage:电压;torque:转矩",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="state",value="学习状态 0:停止;1:学习中;2:已完成;",dataType="Integer", paramType = "query"),
            @ApiImplicitParam(name="shape",value="学习形式 0:均值; 1:最小值;2:最大值;",dataType="Integer", paramType = "query"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="remark",value="描述",dataType="String", paramType = "query")})
    public R save(LearnTypeConfigEntity learnTypeConfigEntity){
        learnTypeConfigService.save(learnTypeConfigEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",dataType="Long", paramType = "query"),
            @ApiImplicitParam(name="name",value="名称",dataType="String", paramType = "query",example="XXX"),
            @ApiImplicitParam(name="deviceType",value="设备类型，motor:电机;mill:轧机;servo_valve:伺服阀",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="classCode",value="参数类型，current:电流;voltage:电压;torque:转矩",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="state",value="学习状态 0:停止;1:学习中;2:已完成;",dataType="Integer", paramType = "query"),
            @ApiImplicitParam(name="shape",value="学习形式 0:均值; 1:最小值;2:最大值;",dataType="Integer", paramType = "query"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="remark",value="描述",dataType="String", paramType = "query")})
    public R update(LearnTypeConfigEntity learnTypeConfigEntity){
        learnTypeConfigService.updateById(learnTypeConfigEntity);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(Long id){
        learnTypeConfigService.removeById(id);
        return R.ok();
    }

}
