package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.theme.dao.RollChangeLogDao;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.vo.AvgAndMinVo;
import io.renren.modules.theme.vo.NewChangeLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("rollChangeLogService")
public class RollChangeLogServiceImpl extends ServiceImpl<RollChangeLogDao, RollChangeLogEntity> implements RollChangeLogService {

    @Autowired
    private RollChangeLogDao rollChangeLogDao;

    @Override
    public List<AvgAndMinVo> selectAvgAndMin(Date startDate, Date endDate,List<Long> crewId) {
        return rollChangeLogDao.selectAvgAndMin(startDate,endDate,crewId.get(0),crewId.get(1),crewId.get(2));
    }

    @Override
    public List<NewChangeLogVo> selectNewLog(Long code) {
        return rollChangeLogDao.selectNewLog(code);
    }

    @Override
    public List<AvgAndMinVo> selectAvgAndMinByCool(Date startDate, Date endDate, List<Long> crewId) {
        return rollChangeLogDao.selectAvgAndMinByCool(startDate,endDate,crewId.get(0),crewId.get(1));
    }

    @Override
    public List<AvgAndMinVo> selectAvgAndMinByCoolRollReady(Date startDate, Date endDate, List<Long> crewId) {
        return rollChangeLogDao.selectAvgAndMinByCoolRollReady(startDate,endDate,crewId.get(0));
    }

    @Override
    public List<AvgAndMinVo> selectAvgAndMinByCoolRoll(Date startDate, Date endDate, List<Long> crewId) {
        return rollChangeLogDao.selectAvgAndMinByCoolRoll(startDate,endDate,crewId.get(0));
    }

    @Override
    public List<NewChangeLogVo> selectNewLogByCoolRoll(Date startDate, Date endDate, List<Long> crewId) {
        return rollChangeLogDao.selectNewLogByCoolRoll(startDate,endDate,crewId.get(0));
    }

    @Override
    public List<NewChangeLogVo> selectNewLogByCoolRollReady(Date startDate, Date endDate, List<Long> crewId) {
        return rollChangeLogDao.selectNewLogByCoolRollReady(startDate,endDate,crewId.get(0));
    }
}
