
package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.theme.dao.AgcAvgtrendDao;
import io.renren.modules.theme.dao.WelderLogDao;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import io.renren.modules.theme.entity.WelderLogEntity;
import io.renren.modules.theme.service.AgcAvgtrendService;
import io.renren.modules.theme.service.WelderLogService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("welderLogService")
public class WelderLogServiceImpl extends ServiceImpl<WelderLogDao, WelderLogEntity> implements WelderLogService {


    @Override
    public List<WelderLogEntity> listByDeviceId(Long deviceId) {
        return this.baseMapper.selectByDeviceId(deviceId);

    }

    @Override
    public Long selecTotalTimes(Long deviceId) {
        return this.baseMapper.selecTotalTimes(deviceId);
    }
}
