
package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.theme.entity.WelderLogEntity;

import java.util.List;

/**
 * 焊机焊接次数和重焊次数接口
 *
 */
public interface WelderLogService extends IService<WelderLogEntity> {

    /**
     * 获取设备历史日志
     * @param deviceId
     * @return
     */
    List<WelderLogEntity> listByDeviceId(Long deviceId);

    /**
     * 获取焊接历史总次数
     * @param deviceId
     * @return
     */
    Long selecTotalTimes(Long deviceId);
}
