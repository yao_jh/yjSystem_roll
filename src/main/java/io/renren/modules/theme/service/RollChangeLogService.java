package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.vo.AvgAndMinVo;
import io.renren.modules.theme.vo.NewChangeLogVo;

import java.util.Date;
import java.util.List;

public interface RollChangeLogService extends IService<RollChangeLogEntity> {

//    int saveBatchByVoList(List<RollChangeLogVo> list);


    /**
     * 获取平均值和最小值（热轧卸卷）
     * @param startDate
     * @param endDate
     * @return
     */
    List<AvgAndMinVo> selectAvgAndMin(Date startDate, Date endDate, List<Long> crewId);
    /**
     * 获取最近一条时间操作
     * @param
     * @param
     * @return
     */

    List<NewChangeLogVo> selectNewLog(Long code);

    /**
     * 获取平均值和最小值（冷轧卸卷）
     * @param startDate
     * @param endDate
     * @return
     */
    List<AvgAndMinVo> selectAvgAndMinByCool(Date startDate, Date endDate, List<Long> crewId);

    /**
     * 获取平均值和最小值（冷轧换辊准备）
     * @param startDate
     * @param endDate
     * @return
     */
    List<AvgAndMinVo> selectAvgAndMinByCoolRollReady(Date startDate, Date endDate, List<Long> crewId);


    /**
     * 获取平均值和最小值（冷轧换辊）
     * @param startDate
     * @param endDate
     * @return
     */
    List<AvgAndMinVo> selectAvgAndMinByCoolRoll(Date startDate, Date endDate, List<Long> crewId);
    /**
     * 获取最近一条时间操作（冷轧换辊准备）
     * @param startDate
     * @param endDate
     * @return
     */
    List<NewChangeLogVo> selectNewLogByCoolRoll(Date startDate, Date endDate, List<Long> crewId);


    /**
     * 获取最近一条时间操作（冷轧换辊）
     * @param startDate
     * @param endDate
     * @return
     */
    List<NewChangeLogVo> selectNewLogByCoolRollReady(Date startDate, Date endDate, List<Long> crewId);


}
