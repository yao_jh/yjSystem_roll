
package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.vo.RigiditySampleVo;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;

import java.util.Date;
import java.util.List;

/**
 * Agc均值趋势接口
 *
 */
public interface RigidityAvgtrendService extends IService<RigidityAvgtrendEntity> {

    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<RigiditySampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    List<RigidityAvgtrendEntity> getListByDate(Long deviceId, Date startDate, Date endDate);

}
