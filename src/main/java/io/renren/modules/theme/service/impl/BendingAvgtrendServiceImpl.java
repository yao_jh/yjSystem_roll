
package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.normal.vo.BendingSampleVo;
import io.renren.modules.theme.dao.BendingAvgtrendDao;
import io.renren.modules.theme.entity.BendingAvgtrendEntity;
import io.renren.modules.theme.service.BendingAvgtrendService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("bendingAvgtrendService")
public class BendingAvgtrendServiceImpl extends ServiceImpl<BendingAvgtrendDao, BendingAvgtrendEntity> implements BendingAvgtrendService {


    @Override
    public List<BendingSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<BendingAvgtrendEntity> getListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, type, side, startDate, endDate);
    }

}
