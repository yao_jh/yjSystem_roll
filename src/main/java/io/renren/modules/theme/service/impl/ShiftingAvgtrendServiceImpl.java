
package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.normal.vo.ShiftingSampleVo;
import io.renren.modules.theme.dao.ShiftingAvgtrendDao;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;
import io.renren.modules.theme.service.ShiftingAvgtrendService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("shiftingAvgtrendService")
public class ShiftingAvgtrendServiceImpl extends ServiceImpl<ShiftingAvgtrendDao, ShiftingAvgtrendEntity> implements ShiftingAvgtrendService {


    @Override
    public List<ShiftingSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<ShiftingAvgtrendEntity> getListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, type, side, startDate, endDate);
    }

}
