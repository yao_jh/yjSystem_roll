package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.theme.dao.RollChangeDao;
import io.renren.modules.theme.entity.RollChangeEntity;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.vo.RollChangeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("rollChangeService")
public class RollChangeServiceImpl extends ServiceImpl<RollChangeDao, RollChangeEntity> implements RollChangeService {
    @Autowired
    private RollChangeDao rollChangeDao;


    @Override
    public int saveByRollChangeVo(RollChangeVo rollChangeVo) {
        RollChangeEntity rollChangeEntity=new RollChangeEntity();
        rollChangeEntity.setCrewId(rollChangeVo.getCrewId());
        rollChangeEntity.setStartDate(rollChangeVo.getEndDate());
        rollChangeEntity.setDate(rollChangeVo.getDate());
        rollChangeEntity.setEndDate(rollChangeVo.getEndDate());
        rollChangeEntity.getDuration(rollChangeVo.getDuration());
        return rollChangeDao.insert(rollChangeEntity);
    }


    @Override
    public int updateByRollChangeVo(RollChangeVo rollChangeVo) {
        RollChangeEntity rollChangeEntity=new RollChangeEntity();
        rollChangeEntity.setCrewId(rollChangeVo.getCrewId());
        rollChangeEntity.setStartDate(rollChangeVo.getEndDate());
        rollChangeEntity.setDate(rollChangeVo.getDate());
        rollChangeEntity.setEndDate(rollChangeVo.getEndDate());
        rollChangeEntity.getDuration(rollChangeVo.getDuration());
        return rollChangeDao.updateById(rollChangeEntity);
    }






}
