
package io.renren.modules.theme.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.normal.vo.RigiditySampleVo;
import io.renren.modules.theme.dao.RigidityAvgtrendDao;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;
import io.renren.modules.theme.service.RigidityAvgtrendService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("rigidityAvgtrendService")
public class RigidityAvgtrendServiceImpl extends ServiceImpl<RigidityAvgtrendDao, RigidityAvgtrendEntity> implements RigidityAvgtrendService {


    @Override
    public List<RigiditySampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<RigidityAvgtrendEntity> getListByDate(Long deviceId, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, startDate, endDate);
    }

}
