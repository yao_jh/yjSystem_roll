
package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.vo.ShiftingSampleVo;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;

import java.util.Date;
import java.util.List;

/**
 * 窜辊均值趋势接口
 *
 */
public interface ShiftingAvgtrendService extends IService<ShiftingAvgtrendEntity> {

    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ShiftingSampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    List<ShiftingAvgtrendEntity> getListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);

}
