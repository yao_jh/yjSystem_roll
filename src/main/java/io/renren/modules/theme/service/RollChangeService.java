package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.theme.entity.RollChangeEntity;
import io.renren.modules.theme.vo.RollChangeVo;

/**
 * 换辊接口
 *
 */
public interface RollChangeService extends IService<RollChangeEntity> {


     int saveByRollChangeVo(RollChangeVo rollChangeVo);


     int updateByRollChangeVo(RollChangeVo rollChangeVo);
}
