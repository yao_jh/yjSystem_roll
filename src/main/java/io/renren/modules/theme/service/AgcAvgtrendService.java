
package io.renren.modules.theme.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.normal.vo.ServoValveSampleVo;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;

import java.util.Date;
import java.util.List;

/**
 * Agc均值趋势接口
 *
 */
public interface AgcAvgtrendService extends IService<AgcAvgtrendEntity> {

    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<AgcSampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    List<AgcAvgtrendEntity> getListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);

}
