package io.renren.modules.theme.controller;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.R;
import io.renren.modules.feature.controller.FeatureController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 碎边剪分析包
 */
@RestController
@RequestMapping("/theme/scrap")
@Api("碎边剪评估包接口")
public class ScrapController extends FeatureController {

    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealList")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealList(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //获取设备实时数据
        R r = super.selectRealList(code);
        return r;
    }
    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealMap")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        return super.selectRealMap(code);
    }
}
