package io.renren.modules.theme.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.R;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.vo.AvgAndMinVo;
import io.renren.modules.theme.vo.HotRollingDecoilerVo;
import io.renren.modules.theme.vo.NewChangeLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@RestController
@RequestMapping("/theme/rollChange")
@Api("换辊接口")
public class RollChangeController {
    @Autowired
    private RollChangeLogService rollChangeLogService;
    @Autowired
    private RollChangeService rollChangeService;

    /**
     * 热轧卸卷
     * @return
     */
    @GetMapping("/selectHotUnwinding")
    @ApiOperation("热轧卸卷")
    @ApiImplicitParams({})
    public R selectHotUnwinding(){
        String now = DateUtil.now();
        Date date = DateUtil.parse(now, "yyyy-MM-dd HH:mm:ss");
        DateTime oldDate = DateUtil.offsetDay(date, -365);
        List<HotRollingDecoilerVo> hotRollingDecoilerVoList=new ArrayList<>();
        List<Long> ids=new ArrayList<>();
        ids.add(23l);
        ids.add(24l);
        ids.add(25l);
        //获取DC1的平均值和最新一条数据
        List<AvgAndMinVo> dcOneAvgList = rollChangeLogService.selectAvgAndMin(oldDate,date,ids);
        List<NewChangeLogVo> dcOneNewList = rollChangeLogService.selectNewLog(23l);
        //最新一条数据
        List<NewChangeLogVo> dcTwoNewList = rollChangeLogService.selectNewLog(24l);
        //最新一条数据
        List<NewChangeLogVo> dcThreeNewList = rollChangeLogService.selectNewLog(25l);
        //将平均值转化成map
        Map<String, AvgAndMinVo> maps = new HashMap<>();
        for (AvgAndMinVo vgAndMinVo : dcOneAvgList) {
            maps.put(vgAndMinVo.getType(), vgAndMinVo);
        }

        for(int i=0;i<dcOneNewList.size();i++) {
            HotRollingDecoilerVo hotRollingDecoilerVo = new HotRollingDecoilerVo();
            hotRollingDecoilerVo.setType(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDurationDcFirst(dcOneNewList.get(i).getDuration());
            hotRollingDecoilerVo.setDurationDcSecond(dcTwoNewList.get(i).getDuration());
            hotRollingDecoilerVo.setDurationDcThree(dcThreeNewList.get(i).getDuration());
            AvgAndMinVo avgVo=maps.get(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDeviationDcFirst(dcOneNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setDeviationDcSecond(dcTwoNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setDeviationDcThree(dcThreeNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setMinDuration(avgVo.getMinDuration());
            hotRollingDecoilerVo.setAvgDuration(avgVo.getAvgDuration().setScale(2, RoundingMode.HALF_UP));
            hotRollingDecoilerVo.setRadio(avgVo.getMinDuration().divide(avgVo.getAvgDuration(),2, BigDecimal.ROUND_HALF_UP));
            hotRollingDecoilerVoList.add(hotRollingDecoilerVo);
        }

        return R.ok().put("data", hotRollingDecoilerVoList);
    }


    /**
     * 热轧卸卷上方对象
     * @return
     */
    @GetMapping("/selectHotUnwindingEnt")
    @ApiOperation("热轧卸卷上方对象")
    @ApiImplicitParams({})
    public R selectHotUnwindingEn(){
        String now = DateUtil.now();
        Date date = DateUtil.parse(now, "yyyy-MM-dd HH:mm:ss");
        DateTime oldDate = DateUtil.offsetDay(date, -365);
        List<HotRollingDecoilerVo> hotRollingDecoilerVoList=new ArrayList<>();
        List<Long> ids=new ArrayList<>();
        ids.add(23l);
        ids.add(24l);
        ids.add(25l);
        //获取DC1的平均值和最新一条数据
        List<AvgAndMinVo> dcOneAvgList = rollChangeLogService.selectAvgAndMin(oldDate,date,ids);
        List<NewChangeLogVo> dcOneNewList = rollChangeLogService.selectNewLog(23l);
        //最新一条数据
        List<NewChangeLogVo> dcTwoNewList = rollChangeLogService.selectNewLog(24l);
        //最新一条数据
        List<NewChangeLogVo> dcThreeNewList = rollChangeLogService.selectNewLog(25l);
        //将平均值转化成map
        Map<String, AvgAndMinVo> maps = new HashMap<>();
        for (AvgAndMinVo vgAndMinVo : dcOneAvgList) {
            maps.put(vgAndMinVo.getType(), vgAndMinVo);
        }

        for(int i=0;i<dcOneNewList.size();i++) {
            HotRollingDecoilerVo hotRollingDecoilerVo = new HotRollingDecoilerVo();
            hotRollingDecoilerVo.setType(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDurationDcFirst(dcOneNewList.get(i).getDuration());
            hotRollingDecoilerVo.setDurationDcSecond(dcTwoNewList.get(i).getDuration());
            hotRollingDecoilerVo.setDurationDcThree(dcThreeNewList.get(i).getDuration());
            AvgAndMinVo avgVo=maps.get(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDeviationDcFirst(dcOneNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setDeviationDcSecond(dcTwoNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setDeviationDcThree(dcThreeNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setMinDuration(avgVo.getMinDuration());
            hotRollingDecoilerVo.setAvgDuration(avgVo.getAvgDuration().setScale(2, RoundingMode.HALF_UP));
            hotRollingDecoilerVo.setRadio(avgVo.getMinDuration().divide(avgVo.getAvgDuration(),2, BigDecimal.ROUND_HALF_UP));
            hotRollingDecoilerVoList.add(hotRollingDecoilerVo);
        }

        return R.ok().put("data", hotRollingDecoilerVoList);
    }



    /**
     * 冷轧卸卷 G3,G4
     * @return
     */
    @GetMapping("/selectCoolUnwinding")
    @ApiOperation("冷轧卸卷 G3,G4")
    @ApiImplicitParams({@ApiImplicitParam(name="code",value="G3默认值是1，G4是2",dataType="Integer", paramType = "query",example="motor1")})
    public R selectCoolUnwinding(Integer code){
        String now = DateUtil.now();
        Date date = DateUtil.parse(now, "yyyy-MM-dd HH:mm:ss");
        DateTime oldDate = DateUtil.offsetDay(date, -365);
        List<HotRollingDecoilerVo> hotRollingDecoilerVoList=new ArrayList<>();
        List<Long> ids=new ArrayList<>();
        List<AvgAndMinVo> dcOneAvgList;
        List<NewChangeLogVo> dcOneNewList;
        List<NewChangeLogVo> dcTwoNewList;
        if(code.equals(1)) {
            ids.add(73l);
            ids.add(74l);
            //获取DC1的平均值和最新一条数据
            dcOneAvgList = rollChangeLogService.selectAvgAndMinByCool(oldDate, date, ids);
            dcOneNewList = rollChangeLogService.selectNewLog(73l);
            //最新一条数据
           dcTwoNewList = rollChangeLogService.selectNewLog(74l);
        }else{
            ids.add(113l);
            ids.add(114l);
            dcOneAvgList = rollChangeLogService.selectAvgAndMinByCool(oldDate, date, ids);
            dcOneNewList = rollChangeLogService.selectNewLog(113l);
            //最新一条数据
            dcTwoNewList = rollChangeLogService.selectNewLog(114l);
        }
        //将平均值转化成map
        Map<String, AvgAndMinVo> maps = new HashMap<>();
        for (AvgAndMinVo vgAndMinVo : dcOneAvgList) {
            maps.put(vgAndMinVo.getType(), vgAndMinVo);
        }

        for(int i=0;i<dcOneNewList.size();i++) {
            HotRollingDecoilerVo hotRollingDecoilerVo = new HotRollingDecoilerVo();
            hotRollingDecoilerVo.setType(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDurationDcFirst(dcOneNewList.get(i).getDuration());
            hotRollingDecoilerVo.setDurationDcSecond(dcTwoNewList.get(i).getDuration());
            AvgAndMinVo avgVo=maps.get(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDeviationDcFirst(dcOneNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setDeviationDcSecond(dcTwoNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setMinDuration(avgVo.getMinDuration());
            hotRollingDecoilerVo.setAvgDuration(avgVo.getAvgDuration().setScale(2, RoundingMode.HALF_UP));
            hotRollingDecoilerVo.setRadio(avgVo.getMinDuration().divide(avgVo.getAvgDuration(),2, BigDecimal.ROUND_HALF_UP));
            hotRollingDecoilerVoList.add(hotRollingDecoilerVo);
        }

        return R.ok().put("data", hotRollingDecoilerVoList);
    }



    /**
     * 冷轧换辊准备 G3,G4     和冷轧换辊
     * @return
     */
    @GetMapping("/selectCoolRoll")
    @ApiOperation("冷轧换辊准备 G3,G4     和冷轧换辊")
    @ApiImplicitParams({@ApiImplicitParam(name="code",value="G3默认值是1，G4是2",dataType="Integer", paramType = "query",example="motor1"),
                        @ApiImplicitParam(name="state",value="换辊准备是1，换辊是2",dataType="Integer", paramType = "query",example="motor1")})
    public R selectCoolRollReady(Integer code, Integer state){
        String now = DateUtil.now();
        Date date = DateUtil.parse(now, "yyyy-MM-dd HH:mm:ss");
        DateTime oldDate = DateUtil.offsetDay(date, -365);
        List<HotRollingDecoilerVo> hotRollingDecoilerVoList=new ArrayList<>();
        List<Long> ids=new ArrayList<>();
        List<AvgAndMinVo> dcOneAvgList;
        List<NewChangeLogVo> dcOneNewList;
        List<NewChangeLogVo> dcTwoNewList;
        if(code.equals(1)) {
            ids.add(54l);
            String head;
            if(state.equals(1)){
                head="spmr_";
                dcOneNewList =  rollChangeLogService.selectNewLogByCoolRollReady(oldDate, date, ids);
                dcOneAvgList =  rollChangeLogService.selectAvgAndMinByCoolRollReady(oldDate, date, ids);
            }else{
                head="spm_";
                dcOneNewList = rollChangeLogService.selectNewLogByCoolRoll(oldDate, date, ids);
                dcOneAvgList = rollChangeLogService.selectAvgAndMinByCool(oldDate, date, ids);

            }
        }else{
            ids.add(94l);
            String head;
            if(state.equals(1)){
                head="spmr_";
                dcOneNewList = rollChangeLogService.selectNewLogByCoolRollReady(oldDate, date, ids);
                dcOneAvgList = rollChangeLogService.selectAvgAndMinByCoolRollReady(oldDate, date, ids);

            }else{
                head="spm_";
                dcOneNewList = rollChangeLogService.selectNewLogByCoolRoll(oldDate, date, ids);
                dcOneAvgList = rollChangeLogService.selectAvgAndMinByCool(oldDate, date, ids);
            }
        }
        //将平均值转化成map
        Map<String, AvgAndMinVo> maps = new HashMap<>();
        for (AvgAndMinVo vgAndMinVo : dcOneAvgList) {
            maps.put(vgAndMinVo.getType(), vgAndMinVo);
        }

        for(int i=0;i<dcOneNewList.size();i++) {
            HotRollingDecoilerVo hotRollingDecoilerVo = new HotRollingDecoilerVo();
            hotRollingDecoilerVo.setType(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDurationDcFirst(dcOneNewList.get(i).getDuration());
            AvgAndMinVo avgVo=maps.get(dcOneNewList.get(i).getType());
            hotRollingDecoilerVo.setDeviationDcFirst(dcOneNewList.get(i).getDuration(). subtract(avgVo.getMinDuration()));
            hotRollingDecoilerVo.setMinDuration(avgVo.getMinDuration());
            hotRollingDecoilerVo.setAvgDuration(avgVo.getAvgDuration().setScale(2, RoundingMode.HALF_UP));
            hotRollingDecoilerVo.setRadio(avgVo.getMinDuration().divide(avgVo.getAvgDuration(),2, BigDecimal.ROUND_HALF_UP));
            hotRollingDecoilerVoList.add(hotRollingDecoilerVo);
        }

        return R.ok().put("data", hotRollingDecoilerVoList);
    }







}
