package io.renren.modules.theme.controller;

import io.renren.modules.feature.controller.FeatureController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 锌锅分析包
 */
@RestController
@RequestMapping("/theme/zinc")
@Api("锌锅评估包接口")
public class ZincController extends FeatureController {


}
