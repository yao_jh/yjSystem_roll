package io.renren.modules.theme.controller;


import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.R;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.theme.entity.WelderLogEntity;
import io.renren.modules.theme.service.WelderLogService;
import io.renren.modules.theme.utils.WelderShuntUtils;
import io.renren.modules.theme.vo.WelderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 焊机焊接分析包
 *
 */
@RestController
@RequestMapping("/theme/welder")
@Api("焊机焊接评估包接口")
public class WelderController {


    @Autowired
    private WelderLogService welderLogService;

    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealMap")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        //返回最新的剪刃次数
        WelderVo vo = WelderShuntUtils.welderMap.get(device.getDeviceId());
        return R.ok().put("data", vo);
    }

    /**
     * 获取动作历史
     * @return
     */
    @GetMapping("/selectHistory")
    @ApiOperation("获取动作历史")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")})
    public R selectAvgtrend(@RequestParam("code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }

        List<WelderLogEntity> list = welderLogService.listByDeviceId(device.getDeviceId());
        return R.ok().put("data", list);
    }

    /**
     * 获取焊接总次数
     * @return
     */
    @GetMapping("/selecTotalTimes")
    @ApiOperation("获取焊接总次数")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")})
    public R selecWeldingTimes(@RequestParam("code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        //历史次数
        Long times = welderLogService.selecTotalTimes(device.getDeviceId());
        if(times == null){
            times = 0L;
        }
        //实时次数
        WelderVo vo = WelderShuntUtils.welderMap.get(device.getDeviceId());
        Long realTimes =  vo.getTotal() == null ? 0L : vo.getTotal();
        // 总次数 = 历史次数 + 实时次数
        Long sumTimes = times + realTimes;
        return R.ok().put("data", sumTimes);
    }

    /**
     * 获取焊接总次数
     * @return
     */
    @GetMapping("/clearing")
    @ApiOperation("清零")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")})
    public R clearing(@RequestParam("code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        WelderVo welderVo = WelderShuntUtils.welderMap.get(device.getDeviceId());
        //保存切次数
        WelderLogEntity logEntity = new WelderLogEntity(welderVo);
        welderLogService.save(logEntity);
        //清0
        welderVo.setTotal(0L);
        return R.ok().put("data", "ok");
    }

    /**
     * 修正历史总次数
     * @return
     */
    @GetMapping("/updateHistoryTotal")
    @ApiOperation("修正历史总次数")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",dataType="Long", paramType = "query"),
            @ApiImplicitParam(name="total",value="更改次数",dataType="Long", paramType = "query")})
    public R clearing(@RequestParam("id") Long id, @RequestParam("total") Long total){
        WelderLogEntity log = welderLogService.getById(id);
        if (log != null){
            //更新历史次数
            log.setTotal(total);
            welderLogService.updateById(log);
        }
        return R.ok().put("data", "ok");
    }

    /**
     * 修正实时总次数
     * @return
     */
    @GetMapping("/updateRealTotal")
    @ApiOperation("修正实时总次数")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="total",value="更改次数",dataType="Long", paramType = "query")})
    public R clearing(@RequestParam("code") String code, @RequestParam("total") Long total){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        //修改实时次数
        WelderVo vo = WelderShuntUtils.welderMap.get(device.getDeviceId());
        vo.setTotal(total);
        return R.ok().put("data", "ok");
    }

}
