package io.renren.modules.theme.controller;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.R;
import io.renren.modules.data.service.DataService;
import io.renren.modules.feature.controller.FeatureController;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.service.RigidityNormalService;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;
import io.renren.modules.theme.service.RigidityAvgtrendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 轧机刚度分析包
 *
 */
@RestController
@RequestMapping("/theme/rigidity")
@Api("轧机刚度评估包接口")
public class RigidityController extends FeatureController {

    @Autowired
    private RigidityAvgtrendService rigidityAvgtrendService;
    @Autowired
    private RigidityNormalService rigidityNormalService;
    @Autowired
    private PointService pointService;
    @Autowired
    private DataService dataService;
    @Autowired
    private CrewService crewService;
    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealList")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealList(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //获取设备实时数据
        R r = super.selectRealList(code);
        return r;
    }

    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealMap")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        return super.selectRealMap(code);
    }

    /**
     * 获取均值趋势
     * @return
     */
    @GetMapping("/selectAvgtrend")
    @ApiOperation("获取均值趋势")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectAvgtrend(@RequestParam("code") String code, @RequestParam("type") Integer type, @RequestParam("side") Integer side,
                            @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        List<RigidityAvgtrendEntity> trendList = rigidityAvgtrendService.getListByDate(device.getDeviceId(),startDate, endDate);
        return R.ok().put("data", trendList);
    }

    /**
     * 获取设备的监测点正态分析
     * @return
     */
    @GetMapping("/selectNormalAnalysis")
    @ApiOperation("获取设备的监测点正态分析")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectNormalAnalysis(@RequestParam("code") String code,
                                  @RequestParam("pointClass") String pointClass,
                                  @RequestParam("startDateStr") String startDateStr,
                                  @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        CrewEntity crew = crewService.queryCrewEntityByCode(code);
        if(crew == null){
            return R.error("机组查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        ///查询正态模型时段的结果
        NormalVo vo = rigidityNormalService.queryCollectByDate(crew.getCrewId(),pointClass,startDate, endDate);
        if(vo != null){
            //创建模型
            StandardNormalModel mode = new StandardNormalModel(vo.getDatas(), vo.getMean());
            //输出正太模型曲线
            vo.setSampleList(mode.getDataSampleList());
            //输出阈值线
            vo.setWarningLines(mode.getWarningLines());
        }else{
            return R.error("无正态分析查询结果，请确认查询时间");
        }
        return R.ok().put("data", vo);
    }

}
