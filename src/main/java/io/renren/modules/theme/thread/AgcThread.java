package io.renren.modules.theme.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.service.ServoValveAvgtrendService;
import io.renren.modules.normal.utils.AgcNormalUtils;
import io.renren.modules.normal.utils.ServoValveNormalUtils;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import io.renren.modules.theme.service.AgcAvgtrendService;

import java.util.Date;

/**
 * 保存AGC均值趋势
 */
public class AgcThread extends Thread{

    private AgcAvgtrendEntity trendEntity;

    public AgcThread(){
        super();
    }

    public AgcThread(AgcAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 设置保存时间
            trendEntity.setDate(new Date());
            //获取spring bean
            AgcAvgtrendService agcAvgtrendService = (AgcAvgtrendService) SpringContextUtils.getBean("agcAvgtrendService");
            //正态分析验证速度趋势报警
            AgcNormalUtils.checkWarningHandler(trendEntity);
            //保存趋势记录
            agcAvgtrendService.save(trendEntity);
        }
    }
}