package io.renren.modules.theme.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.theme.entity.RollChangeEntity;
import io.renren.modules.theme.service.RollChangeService;

import java.util.Date;

public class RollChangeThread extends Thread {
    private RollChangeEntity trendEntity;

    public RollChangeThread(){
        super();
    }

    public RollChangeThread(RollChangeEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 设置保存时间
            trendEntity.setDate(new Date());
            //获取spring bean
            RollChangeService rollChangeService = (RollChangeService) SpringContextUtils.getBean("rollChangeService");
            //保存趋势记录
            rollChangeService.save(trendEntity);
        }
    }
}
