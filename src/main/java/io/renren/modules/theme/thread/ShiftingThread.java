package io.renren.modules.theme.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.utils.ShiftingNormalUtils;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;
import io.renren.modules.theme.service.ShiftingAvgtrendService;

import java.util.Date;

/**
 * 保存AGC均值趋势
 */
public class ShiftingThread extends Thread{

    private ShiftingAvgtrendEntity trendEntity;

    public ShiftingThread(){
        super();
    }

    public ShiftingThread(ShiftingAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 设置保存时间
            trendEntity.setDate(new Date());
            //获取spring bean
            ShiftingAvgtrendService shiftingAvgtrendService = (ShiftingAvgtrendService) SpringContextUtils.getBean("shiftingAvgtrendService");
            //正态分析验证速度趋势报警
            ShiftingNormalUtils.checkWarningHandler(trendEntity);
            //保存趋势记录
            shiftingAvgtrendService.save(trendEntity);
        }
    }
}