package io.renren.modules.theme.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;
import io.renren.modules.theme.service.RigidityAvgtrendService;

import java.util.Date;

/**
 * 保存轧机刚度均值趋势
 */
public class RigidityThread extends Thread{

    private RigidityAvgtrendEntity trendEntity;

    public RigidityThread(){
        super();
    }

    public RigidityThread(RigidityAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 设置保存时间
            trendEntity.setDate(new Date());
            //获取spring bean
            RigidityAvgtrendService rigidityAvgtrendService = (RigidityAvgtrendService) SpringContextUtils.getBean("rigidityAvgtrendService");
            //保存趋势记录
            rigidityAvgtrendService.save(trendEntity);
        }
    }
}