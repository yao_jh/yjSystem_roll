package io.renren.modules.theme.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.theme.entity.BendingAvgtrendEntity;
import io.renren.modules.theme.service.BendingAvgtrendService;

import java.util.Date;

/**
 * 保存弯辊力均值趋势
 */
public class BendingThread extends Thread{

    private BendingAvgtrendEntity trendEntity;

    public BendingThread(){
        super();
    }

    public BendingThread(BendingAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 保存
            trendEntity.setDate(new Date());
            //获取spring bean
            BendingAvgtrendService bendingAvgtrendService = (BendingAvgtrendService) SpringContextUtils.getBean("bendingAvgtrendService");

            bendingAvgtrendService.save(trendEntity);
        }
    }
}