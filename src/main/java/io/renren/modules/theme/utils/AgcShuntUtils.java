package io.renren.modules.theme.utils;

import io.renren.common.sys.MainCatche;
import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import io.renren.modules.theme.thread.AgcThread;
import io.renren.modules.theme.vo.AgcTrendVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * AGC评估包分流工具
 */
public class AgcShuntUtils {

    // Agc操作侧均值趋势 key:deviceId
    public static Map<Long, AgcTrendVo> agcOsTrendMap = new ConcurrentHashMap<Long, AgcTrendVo>();

    // Agc驱动侧均值趋势 key:deviceId
    public static Map<Long, AgcTrendVo> agcDsTrendMap = new ConcurrentHashMap<Long, AgcTrendVo>();

    /**
     * AGC分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        //判断AGC缸OS/DS
        String side = getSideCode(point.getClassCode());
        if(side == null){
            return ;
        }
        //操作侧和传动侧趋势
        AgcTrendVo trend = null;
        if("os".equals(side)){
            trend = agcOsTrendMap.get(point.getDeviceId());
            trend.setSide(0);
        }else {
            trend = agcDsTrendMap.get(point.getDeviceId());
            trend.setSide(1);
        }
        if(trend == null){
            return ;
        }

        //判断轧机状态
        CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
        if(crew.getState() == 1){
            //咬钢事件，停止计量AGC
            if(trend.getStartDate() != null){
                trend = initTrendEntity(point);
            }
        }else if(crew.getState() == 0){
            //过滤AGC参数
            setTrendPoints(point, trend);
        }
    }

    /**
     * 设置AGC均值实体
     * @param point
     * @param trend
     */
    private static void setTrendPoints(PointEntity point, AgcTrendVo trend){
        //抛钢后取样一次
        if(trend.getDate() != null){
            return ;
        }

        if("agc_os_pos_act".equals(point.getClassCode()) || "agc_ds_pos_act".equals(point.getClassCode())){
            //验证单调区间
            positionProcess(point, trend);
            refreshDifference(point, trend);
        }else if("gap_os".equals(point.getClassCode()) || "gap_ds".equals(point.getClassCode())){
            //不断更新辊缝的调整值
            trend.setEndGap(point.getValue());
        }else if("magne_os_en_act".equals(point.getClassCode()) || "magne_ds_en_act".equals(point.getClassCode())){
            //入口磁尺实际值
            trend.setMagneEn(point.getValue());
        }else if("magne_os_ex_act".equals(point.getClassCode()) || "magne_ds_ex_act".equals(point.getClassCode())){
            //出口磁尺实际值
            trend.setMagneEx(point.getValue());
        }
    }

    /**
     *
     * @param act
     * @param trend
     */
    private static void refreshDifference(PointEntity act, AgcTrendVo trend){
        //设定的Code
        String setCode = act.getClassCode().replace("act","set");
        PointEntity set = MeansUtils.getCatchePoint(act.getDeviceCode(), setCode);
        //累加设定和反馈差值，算偏差均值
        trend.toAccumSumDiff(set.getValue(), act.getValue());
    }

    /**
     * AGC缸实际位置计量行程
     * @param point
     * @param trend
     */
    private static void positionProcess(PointEntity point, AgcTrendVo trend){
        //单调区间
        BigDecimal value = point.getValue();
        BigDecimal last = trend.getLastValue();
        //0:上升;1:下降
        Integer state = MeansUtils.getMonotoneState(last, value);
        trend.setLastValue(value);
        if(state == null){
            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            return ;
        }
        //相同单调区间
        if(state.equals(trend.getState())){
            BigDecimal initValue = trend.getInitValue();
            //变化幅度>=2
            if(Math.abs(initValue.subtract(value).doubleValue()) >= 2){
                if(state.equals(0)){//上升
                    trend.setMinValue(initValue);
                    trend.setMaxValue(value);
                    trend.setEndDate(point.getRealDate());
                }else{//下降
                    trend.setMaxValue(initValue);
                    trend.setMinValue(value);
                    trend.setEndDate(point.getRealDate());
                }
            }
        }else{//单调区间变更保存最新的结果并重新构建比较
            if(trend.getEndDate() != null){
                //保存记录
                saveTrendEntity(trend);
                //新的趋势
                trend = initTrendEntity(point);
                trend.setDate(new Date());
            }

            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            trend.setState(state);
        }
    }

    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(AgcTrendVo trend){
        //保存趋势数据
        AgcAvgtrendEntity trendEntity = new AgcAvgtrendEntity(trend);
        //保存线程
        AgcThread thread = new AgcThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param point
     * @return
     */
    private static AgcTrendVo initTrendEntity(PointEntity point){
        AgcTrendVo trend = new AgcTrendVo();
        //trend.setStartDate(point.getRealDate());
        return trend;
    }

    /**
     * 获取AGC缸的操作侧还是传动侧
     * @param code
     * @return
     */
    private static String getSideCode(String code){
        String sideCode = null;
        String codeSide = code.substring(0, 6);
        switch (codeSide){
            case "agc_os" :
            case "gap_os" : sideCode = "os";break;
            case "agc_ds" :
            case "gap_ds" : sideCode = "ds";break;
        }
        return sideCode;
    }

}
