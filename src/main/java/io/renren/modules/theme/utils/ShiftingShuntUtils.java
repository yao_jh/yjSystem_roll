package io.renren.modules.theme.utils;

import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;
import io.renren.modules.theme.thread.ShiftingThread;
import io.renren.modules.theme.vo.ShiftingTrendVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 窜辊动作评估包分流工具
 */
public class ShiftingShuntUtils {

    //窜辊液压缸入口均值趋势 key:deviceId
    public static Map<Long, ShiftingTrendVo> inShiftingMap = new ConcurrentHashMap<Long, ShiftingTrendVo>();
    //窜辊液压缸出口均值趋势 key:deviceId
    public static Map<Long, ShiftingTrendVo> outShiftingMap = new ConcurrentHashMap<Long, ShiftingTrendVo>();

    /**
     * 窜辊动作分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        //判断AGC缸OS/DS
        String side = getSideCode(point.getClassCode());
        if(side == null){
            return ;
        }
        //操作侧和传动侧趋势
        ShiftingTrendVo trend = null;
        if("en".equals(side)){
            trend = inShiftingMap.get(point.getDeviceId());
            trend.setSide(0);
        }else {
            trend = outShiftingMap.get(point.getDeviceId());
            trend.setSide(1);
        }
        if(trend == null){
            return ;
        }
        //过滤窜辊缸参数
        setTrendPoints(point, trend);

    }

    /**
     * 设置窜辊缸均值实体
     * @param point
     * @param trend
     */
    private static void setTrendPoints(PointEntity point, ShiftingTrendVo trend){

        if("cvc_en_act".equals(point.getClassCode()) || "cvc_ex_act".equals(point.getClassCode())){
            //验证单调区间
            positionProcess(point, trend);
        }

    }


    /**
     * 窜辊缸实际位置计量行程
     * @param point
     * @param trend
     */
    private static void positionProcess(PointEntity point, ShiftingTrendVo trend){
        //单调区间
        BigDecimal value = point.getValue();
        BigDecimal last = trend.getLastValue();
        //0:上升;1:下降
        Integer state = MeansUtils.getMonotoneState(last, value);
        trend.setLastValue(value);
        if(state == null){
            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            return ;
        }
        //相同单调区间
        if(state.equals(trend.getState())){
            BigDecimal initValue = trend.getInitValue();
            //变化幅度>=10
            if(Math.abs(initValue.subtract(value).doubleValue()) >= 10){
                if(state.equals(0)){//上升
                    trend.setMinValue(initValue);
                    trend.setMaxValue(value);
                    trend.setEndDate(point.getRealDate());
                }else{//下降
                    trend.setMaxValue(initValue);
                    trend.setMinValue(value);
                    trend.setEndDate(point.getRealDate());
                }
            }
        }else{//单调区间变更保存最新的结果并重新构建比较
            if(trend.getEndDate() != null){
                //保存记录
                saveTrendEntity(trend);
                //新的趋势
                trend = initTrendEntity(point);
                trend.setDate(new Date());
            }

            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            trend.setState(state);
        }
    }

    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(ShiftingTrendVo trend){
        //保存趋势数据
        ShiftingAvgtrendEntity trendEntity = new ShiftingAvgtrendEntity(trend);
        //保存线程
        ShiftingThread thread = new ShiftingThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param point
     * @return
     */
    private static ShiftingTrendVo initTrendEntity(PointEntity point){
        ShiftingTrendVo trend = new ShiftingTrendVo();
        //trend.setStartDate(point.getRealDate());
        return trend;
    }

    /**
     * 获取窜辊缸的入口还是出口
     * @param code
     * @return
     */
    private static String getSideCode(String code){
        String sideCode = null;
        String codeSide = code.substring(0, 6);
        switch (codeSide){
            case "cvc_en" :
                sideCode = "en";
                break;
            case "cvc_ex" :
                sideCode = "ex";
                break;
        }
        return sideCode;
    }
}
