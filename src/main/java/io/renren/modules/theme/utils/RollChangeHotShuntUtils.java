package io.renren.modules.theme.utils;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.thread.RollChangeLogThread;
import io.renren.modules.theme.vo.RollChangeLogVo;
import io.renren.modules.theme.vo.RollChangeVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static cn.hutool.core.date.DateTime.now;

public class RollChangeHotShuntUtils {
    // crew_id,
    private static Map<Long, RollChangeVo> rollMap;

    private static RollChangeService rollChangeService;
    // 主线程游标
    private static Integer currentStep;
    //轧机准备（HGC泄压开始）分支游标
    private static Integer aurrentStep;
    //轧机准备（DS泄压完成）分支游标
    private static Integer burrentStep;
    //轧机准备（OS泄压完成）分支游标
    private static Integer durrentStep;

    //落辊流程（出口导卫拉出完成）分支游标
    private static Integer eurrentStep;

    //落辊流程（OS泄压完成）分支游标
    private static Integer furrentStep;

    //推新辊 分支游标
    private static Integer gurrentStep;

    //推新辊 时间
    private static Date layDate;

    //落辊 时间
    private static Date labDate;

    public static void grantShuntHandler(PointEntity point) {
        RollChangeVo rollChangeVo = new RollChangeVo();
        List<RollChangeLogVo> rollChangeLogList = new ArrayList<>();
        // spmr_autostep_1_start
                //轧机准备
                rollReady(point,rollChangeVo,rollChangeLogList);
                //落辊流程
                rollDown(point,rollChangeVo,rollChangeLogList);
                //横移平台流程图
                movePlatform(point,rollChangeVo,rollChangeLogList);
                //推新辊
                pushNewPoll(point,rollChangeVo,rollChangeLogList);
                //小车轨道落下
                movePlatformByBend(point,rollChangeVo,rollChangeLogList);
                //小车运输旧辊回辊流程
                oldRoll(point,rollChangeVo,rollChangeLogList);
    }



    /**
     * 轧机准备
     *
     * @param
     */
    private static void rollReady(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {

        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(point.getCrewId()*10+1, rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);

        }

        //窜辊到中心位
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            currentStep = 1;
        }
        //HGC泄压开始
        if (point.getClassCode().equals("l_fmc_hgcc") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            aurrentStep = 1;
        }
        //cvc主轴对中
        if (point.getClassCode().equals("l_fmc_cvcm") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(1)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 2;
            }

        }
        //关闭工作辊冷却水
        if (point.getClassCode().equals("l_fmc_cwoff") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(2)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 3;
            }

        }
        //工作辊弯辊打到平衡模式
        if (point.getClassCode().equals("l_fmc_wrbon") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(3)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 4;
            }

        }
        // DS泄压完成
        if (point.getClassCode().equals("l_fmc_hgcds") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (aurrentStep.equals(1)) {
                saveRollChangeList(point, rollChangeLogList);
                burrentStep = 1;
            }

        }
        //OS泄压完成
        if (point.getClassCode().equals("l_fmc_hgcds") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (aurrentStep.equals(1)) {
                saveRollChangeList(point, rollChangeLogList);
                durrentStep = 1;
            }

        }
        // 工作辊弯辊打到平衡模式 和 DS泄压完成 和 OS泄压完成
        if (currentStep.equals(4) && burrentStep.equals(1) && durrentStep.equals(1)) {
            saveRollChangeList(point, rollChangeLogList);
            // 获取rollChangeLogService的值
            RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
            // 获取 map 中设备值中的value
            RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
            // 获取主键
            Long id=rollMap.get(point.getCrewId()).getId();
            // 获取 map 实体中的List
            List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
            // 创建Entity 实体 用于转化 vo和entity
            List<RollChangeLogEntity> rollByList=new ArrayList<>();
            // 转化List中的rollChangeLogService
            for(RollChangeLogVo a:rollAll){
                RollChangeLogEntity rollNew=new RollChangeLogEntity();
                rollNew.setDate(a.getDate());
                rollNew.setPid(id);
                rollNew.setEndDate(a.getEndDate());
                rollNew.setStartDate(a.getStartDate());
                rollNew.setClassCode(a.getClassCode());
                rollByList.add(rollNew);
            }
            // 将转化的List实体插入 rollChangeLog表中
            rollChangeLogService.saveBatch(rollByList);
            //获取父表的最后值 更新父表
            rollChangeVo.setEndDate(point.getRealDate());
            rollChangeService = getRollChangeService();
            rollChangeService.updateByRollChangeVo(rollChangeVo);
        }

    }


    /**
     * 落辊流程
     *
     * @param
     */

    private static void rollDown(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);

        }
        //入口、出口活套提升命令
        if (point.getClassCode().equals("l_fmc_loopre") && point.getValue().compareTo(BigDecimal.valueOf(2)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            currentStep = 100;
        }
        //入口、出口活套提升完成
        if (point.getClassCode().equals("l_fmc_loopr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(100)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 101;
            }

        }



        if(point.getCrewId().equals(16)||point.getCrewId().equals(17)||point.getCrewId().equals(18)||point.getCrewId().equals(19)||point.getCrewId().equals(20)||point.getCrewId().equals(21)) {
            //入口导卫拉出
            if (point.getClassCode().equals("l_fmc_ensgret") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                if (currentStep.equals(101)) {
                    saveRollChangeList(point, rollChangeLogList);
                    currentStep = 102;
                }

            }
            //入口刮水板拉出
            if (point.getClassCode().equals("l_fmc_enwr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                if (currentStep.equals(102)) {
                    saveRollChangeList(point, rollChangeLogList);
                    currentStep = 103;
                }
            }
        }else if(point.getCrewId().equals(15)){
            //入口刮水板拉出
            if (point.getClassCode().equals("l_fmc_enwr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                if (currentStep.equals(101)) {
                    saveRollChangeList(point, rollChangeLogList);
                    currentStep = 103;
                }
            }
        }
        //出口刮水板拉出
        if (point.getClassCode().equals("l_fmc_exwr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            eurrentStep = 1;
        }

        //出口导卫拉出
        if (point.getClassCode().equals("l_fmc_exsgr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (eurrentStep.equals(1)) {
                saveRollChangeList(point, rollChangeLogList);
                eurrentStep = 2;
            }
        }
        //出口导卫拉出完成
        if (point.getClassCode().equals("l_fmc_exsgrd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (eurrentStep.equals(2)) {
                saveRollChangeList(point, rollChangeLogList);
                eurrentStep = 3;
            }
        }
        //操作侧入口刮水板拉出完成
        if (point.getClassCode().equals("l_fmc_exsgrd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(103)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 104;
            }
        }
        //传动侧入口刮水板拉出完成
        if (point.getClassCode().equals("l_fmc_exsgrd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(104)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 105;
            }
        }
        //上垫到2垫
        if (currentStep.equals(105) && eurrentStep.equals(3)) {
            //Y
            if (point.getClassCode().equals("l_fmc_tb2") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0 && !furrentStep.equals(3)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 106;
            } else {
                //N
                //上工作辊落下
                if (point.getClassCode().equals("l_fmc_twrlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                    saveRollChangeList(point, rollChangeLogList);
                    furrentStep = 1;
                }
                //上支撑辊落下
                if (point.getClassCode().equals("l_fmc_tburlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                    if (furrentStep.equals(1)) {
                        saveRollChangeList(point, rollChangeLogList);
                        furrentStep = 2;
                    }
                }
                //上垫到2垫
                if (point.getClassCode().equals("l_fmc_tb2") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                    if (furrentStep.equals(2)) {
                        saveRollChangeList(point, rollChangeLogList);
                        furrentStep = 3;
                    }
                }
                //上工作辊上升
                if (point.getClassCode().equals("l_fmc_twrras") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                    if (furrentStep.equals(3)) {
                        saveRollChangeList(point, rollChangeLogList);
                        furrentStep = 4;
                    }
                }
                //上支撑辊上升
                if (point.getClassCode().equals("l_fmc_tburras") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                    if (furrentStep.equals(4)) {
                        saveRollChangeList(point, rollChangeLogList);
                        furrentStep = 5;
                    }
                }
            }
        }
        //上支撑辊平衡打到on
        if (currentStep.equals(106) || furrentStep.equals(5)) {
            if (point.getClassCode().equals("l_fmc_ubureon") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 107;
            }
        }

        //上工作辊切换到平衡压力50bar  操作侧平衡缸平衡压力50bar
        if (point.getClassCode().equals("l_fmc_ubureon") && point.getValue().compareTo(BigDecimal.valueOf(50)) == 0) {
            if (currentStep.equals(107)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 108;
            }
        }

        //上工作辊切换到平衡压力50bar  操作侧平衡缸平衡压力50bar
        if (point.getClassCode().equals("l_fmc_wrbdsp") && point.getValue().compareTo(BigDecimal.valueOf(50)) == 0) {
            if (currentStep.equals(108)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 109;
            }
        }

        //小车轨道提升
        if (point.getClassCode().equals("l_fmc_carrailift") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(109)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 110;
            }
        }

        //小车轨道提升完成
        if (point.getClassCode().equals("l_fmc_carrailiftr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(110)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 111;
            }
        }
        //判断下阶梯垫是否在2垫？
        if (currentStep.equals(111)) {
            //下垫到2垫
            if (point.getClassCode().equals("l_fmc_sw2") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 112;
            }else{
                //下支撑辊提升
                if (point.getClassCode().equals("l_fmc_bburlift") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0&&furrentStep.equals(5)) {
                    saveRollChangeList(point, rollChangeLogList);
                    furrentStep =6;
                }
                //下阶梯垫调到2垫
                if (point.getClassCode().equals("l_fmc_sw2") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0&&furrentStep.equals(6)) {
                    saveRollChangeList(point, rollChangeLogList);
                    furrentStep = 7;
                }

                //下支撑辊落下
                if (point.getClassCode().equals("l_fmc_bburlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0&&furrentStep.equals(7)) {
                    saveRollChangeList(point, rollChangeLogList);
                    furrentStep = 8;
                }
            }
        }
        // 小车去E13  和 钩子落下
        if(currentStep.equals(112)||furrentStep.equals(8)){
            if (point.getClassCode().equals("l_fmc_car13") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                eurrentStep = 4;
            }
            if (point.getClassCode().equals("l_fmc_car13") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                burrentStep = 5;
            }

        }
        //下接轴抱紧开始
        if(eurrentStep.equals(4)&&burrentStep.equals(5)){
            if (point.getClassCode().equals("l_fmc_bspindles") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 113;
            }
        }
        //下接轴入口侧关闭完成
        if (point.getClassCode().equals("l_fmc_bspindleen") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(113)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 114;
            }
        }
        //下接轴入口侧关闭完成
        if (point.getClassCode().equals("l_fmc_bspindleex") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(114)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 115;
            }
        }

        //下夹紧板打开
        if (point.getClassCode().equals("l_fmc_bkeepero") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(115)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 116;
            }
        }
        //下夹紧板入口侧打开完成
        if (point.getClassCode().equals("l_fmc_bkeeperenr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(116)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 117;
            }
        }
        //下夹紧板出口侧打开完成
        if (point.getClassCode().equals("l_fmc_bkeeperexr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(117)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 118;
            }
        }
        //小车到E11
        if (point.getClassCode().equals("l_fmc_car11") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(118)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 119;
                labDate=point.getRealDate();
            }
        }
        //上工作辊落下
        if (point.getClassCode().equals("l_fmc_twrlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(119)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 120;
            }
        }

        //todo 落辊延时 等数据
            long betweenDay = DateUtil.between(labDate,now(), DateUnit.MS);
            if (betweenDay > 1000 && currentStep.equals(120)){
                saveRollChangeList(point, rollChangeLogList);
                // 获取rollChangeLogService的值
                RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
                // 获取 map 中设备值中的value
                RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
                // 获取主键
                Long id=rollMap.get(point.getCrewId()).getId();
                // 获取 map 实体中的List
                List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
                // 创建Entity 实体 用于转化 vo和entity
                List<RollChangeLogEntity> rollByList=new ArrayList<>();
                // 转化List中的rollChangeLogService
                for(RollChangeLogVo a:rollAll){
                    RollChangeLogEntity rollNew=new RollChangeLogEntity();
                    rollNew.setDate(a.getDate());
                    rollNew.setPid(id);
                    rollNew.setEndDate(a.getEndDate());
                    rollNew.setStartDate(a.getStartDate());
                    rollNew.setClassCode(a.getClassCode());
                    rollByList.add(rollNew);
                }
                // 将转化的List实体插入 rollChangeLog表中
                rollChangeLogService.saveBatch(rollByList);
                //获取父表的最后值 更新父表
                rollChangeVo.setEndDate(point.getRealDate());
                rollChangeService = getRollChangeService();
                rollChangeService.updateByRollChangeVo(rollChangeVo);

        }

    }







    /**
     * 横移平台流程图
     *
     * @param
     */

    private static void movePlatform(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);

        }

        //下夹紧板出口侧打开完成
        if (point.getClassCode().equals("l_fmc_fwd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            // 获取rollChangeLogService的值
            RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
            // 获取 map 中设备值中的value
            RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
            // 获取主键
            Long id=rollMap.get(point.getCrewId()).getId();
            // 获取 map 实体中的List
            List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
            // 创建Entity 实体 用于转化 vo和entity
            List<RollChangeLogEntity> rollByList=new ArrayList<>();
            // 转化List中的rollChangeLogService
            for(RollChangeLogVo a:rollAll){
                RollChangeLogEntity rollNew=new RollChangeLogEntity();
                rollNew.setDate(a.getDate());
                rollNew.setPid(id);
                rollNew.setEndDate(a.getEndDate());
                rollNew.setStartDate(a.getStartDate());
                rollNew.setClassCode(a.getClassCode());
                rollByList.add(rollNew);
            }
            // 将转化的List实体插入 rollChangeLog表中
            rollChangeLogService.saveBatch(rollByList);
            //获取父表的最后值 更新父表
            rollChangeVo.setEndDate(point.getRealDate());
            rollChangeService = getRollChangeService();
            rollChangeService.updateByRollChangeVo(rollChangeVo);
        }
        }




    /**
     * 推新辊
     *
     * @param
     */

    private static void pushNewPoll(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);
        }
        // 获取
        if(point.getClassCode().equals("l_fmc_sw0") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw1") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw2") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw3") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw4") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw5") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw6") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw7") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw8") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw9") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw10") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
        }else  if(point.getClassCode().equals("l_fmc_sw11") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 301;
            }

        //下支撑辊提升
        if(point.getClassCode().equals("l_fmc_passline") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if(currentStep.equals(301)){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 302;
            }

        }
        //下支撑辊落下
        if(point.getClassCode().equals("l_fmc_burlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if(currentStep.equals(302)){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 303;
            }

        }
        //小车到E12
        if(point.getClassCode().equals("l_fmc_car12") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if(currentStep.equals(303)){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 304;
            }

        }
        //小车钩子落下完成
        if(point.getClassCode().equals("l_fmc_carhooklowr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            gurrentStep = 1;
        }

        //小车到E12 和 小车钩子落下完成
        if(currentStep.equals(304)&&gurrentStep.equals(1)){
            //上夹紧板关闭
            if(point.getClassCode().equals("l_fmc_tkeeperc") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 305;
            }
        }

        //上夹紧板入口侧关闭
        if(point.getClassCode().equals("l_fmc_tkeeperenc") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(305)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 306;
            }
        }


        //上夹紧板出口侧关闭
        if(point.getClassCode().equals("l_fmc_tkeeperexc") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(306)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 307;
            }
        }

        //上接轴打开
        if(point.getClassCode().equals("l_fmc_tspindleo") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(307)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 308;
            }
        }

        //上接轴入口侧打开完毕
        if(point.getClassCode().equals("l_fmc_tspindleenod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(307)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 308;
            }
        }

        //上接轴出口侧打开完毕
        if(point.getClassCode().equals("l_fmc_tspindleexod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(307)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 308;
                layDate=point.getRealDate();
            }
        }


        // 上工作辊平衡提升并且延时一秒

        if(point.getClassCode().equals("l_fmc_twrras") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            long betweenDay = DateUtil.between(layDate, point.getRealDate(), DateUnit.MS);
            if(betweenDay>1000&&currentStep.equals(308))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 309;
        }

        // 小车去E13

        if(point.getClassCode().equals("l_fmc_car13") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(309))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 310;
        }

        // 下夹紧板关闭

        if(point.getClassCode().equals("l_fmc_bkeeperc") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(310))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 311;
        }

        // 下夹紧板入口侧关闭完毕

        if(point.getClassCode().equals("l_fmc_bkeeperenod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(311))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 312;
        }

        // 下夹紧板出口侧关闭完毕

        if(point.getClassCode().equals("l_fmc_bkeeperexod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(312))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 313;
        }

        // 下接轴打开

        if(point.getClassCode().equals("l_fmc_tspindleo") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(313))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 314;
        }
        // 下接轴入口侧打开完成

        if(point.getClassCode().equals("l_fmc_bspindleenod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(314))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 315;
        }

        // 下接轴出口侧打开完成

        if(point.getClassCode().equals("l_fmc_bspindleexod") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(315))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 316;
        }
        // 小车去E3

        if(point.getClassCode().equals("l_fmc_car13") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(316))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 317;
        }
        // 小车钩子抬起

        if(point.getClassCode().equals("l_fmc_hookup") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(317))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 318;
        }

        //当前垫数是否等于工作垫数？是向下执行，否跳D列
        if(currentStep.equals(318)) {
            if (point.getClassCode().equals("l_fmc_sw3") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;


            } else if (point.getClassCode().equals("l_fmc_sw4") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            } else if (point.getClassCode().equals("l_fmc_sw5") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {

                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;
            } else if (point.getClassCode().equals("l_fmc_sw6") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            } else if (point.getClassCode().equals("l_fmc_sw7") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {

                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;
            } else if (point.getClassCode().equals("l_fmc_sw8") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            } else if (point.getClassCode().equals("l_fmc_sw9") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            } else if (point.getClassCode().equals("l_fmc_sw10") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            } else if (point.getClassCode().equals("l_fmc_sw11") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 319;

            }
        }
            //下支撑辊提升
            if (point.getClassCode().equals("l_fmc_bburlift") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                if(currentStep.equals(319)){
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 320;
            }
        }
        //小车到E11
        if (point.getClassCode().equals("l_fmc_car11gp") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            if (currentStep.equals(320)) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 321;
            }
        }
        //判断有一个信号来，就正常显示
        if(currentStep.equals(321)) {
            if (point.getClassCode().equals("l_fmc_sw3") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;


            } else if (point.getClassCode().equals("l_fmc_sw4") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            } else if (point.getClassCode().equals("l_fmc_sw5") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {

                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;
            } else if (point.getClassCode().equals("l_fmc_sw6") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            } else if (point.getClassCode().equals("l_fmc_sw7") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {

                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;
            } else if (point.getClassCode().equals("l_fmc_sw8") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            } else if (point.getClassCode().equals("l_fmc_sw9") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            } else if (point.getClassCode().equals("l_fmc_sw10") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            } else if (point.getClassCode().equals("l_fmc_sw11") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
                saveRollChangeList(point, rollChangeLogList);
                currentStep = 322;

            }
        }
        //

//todo

        if(point.getClassCode().equals("l_fmc_burlow") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(322))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 323;
        }


        //小车轨道落下
        if(point.getClassCode().equals("l_fmc_carraildown") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(323))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 324;
        }

        //小车到E6
        if(point.getClassCode().equals("l_fmc_car6gp") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(324))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 325;
        }

        //上工作辊平衡打到on
        if(point.getClassCode().equals("l_fmc_twrras") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(325))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 326;
        }


        //上支撑辊平衡打到on
        if(point.getClassCode().equals("l_fmc_tburbaon") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(326))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 327;
        }

        //入口刮板插入
        if(point.getClassCode().equals("l_fmc_enware") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(327))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 328;
        }

        //出口导位插入
        if(point.getClassCode().equals("l_fmc_exsgre") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(328))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 329;
        }
        //出口导位插入
        if(point.getClassCode().equals("l_fmc_exsgre") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(329))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 330;
        }
        //出口导位插入
        if(point.getClassCode().equals("l_fmc_exsgre") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(330))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 331;
        }

        //出口导卫插入完成
        if(point.getClassCode().equals("l_fmc_exsgred") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(331))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 332;
        }

        //入口导卫插入
        if(point.getClassCode().equals("l_fmc_ensgappr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(332))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 333;
        }

        //出口刮板插入
        if(point.getClassCode().equals("l_fmc_exware") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(333))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 334;
        }

        //出口刮板插入完成
        if(point.getClassCode().equals("l_fmc_exwared") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(334))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 335;
        }


        //活套落下
        if(point.getClassCode().equals("l_fmc_loopdown") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(335))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 336;
        }

        //活套落下完成
        if(point.getClassCode().equals("l_fmc_loopdownd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            saveRollChangeList(point, rollChangeLogList);
            // 获取rollChangeLogService的值
            RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
            // 获取 map 中设备值中的value
            RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
            // 获取主键
            Long id=rollMap.get(point.getCrewId()).getId();
            // 获取 map 实体中的List
            List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
            // 创建Entity 实体 用于转化 vo和entity
            List<RollChangeLogEntity> rollByList=new ArrayList<>();
            // 转化List中的rollChangeLogService
            for(RollChangeLogVo a:rollAll){
                RollChangeLogEntity rollNew=new RollChangeLogEntity();
                rollNew.setDate(a.getDate());
                rollNew.setPid(id);
                rollNew.setEndDate(a.getEndDate());
                rollNew.setStartDate(a.getStartDate());
                rollNew.setClassCode(a.getClassCode());
                rollByList.add(rollNew);
            }
            // 将转化的List实体插入 rollChangeLog表中
            rollChangeLogService.saveBatch(rollByList);
            //获取父表的最后值 更新父表
            rollChangeVo.setEndDate(point.getRealDate());
            rollChangeService = getRollChangeService();
            rollChangeService.updateByRollChangeVo(rollChangeVo);
        }
    }



    /**
     * 横移平台卷曲侧
     *
     * @param
     */

    private static void movePlatformByBend(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);

        }

        //下夹紧板出口侧打开完成
        if (point.getClassCode().equals("l_fmc_wrcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            // 获取rollChangeLogService的值
            RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
            // 获取 map 中设备值中的value
            RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
            // 获取主键
            Long id=rollMap.get(point.getCrewId()).getId();
            // 获取 map 实体中的List
            List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
            // 创建Entity 实体 用于转化 vo和entity
            List<RollChangeLogEntity> rollByList=new ArrayList<>();
            // 转化List中的rollChangeLogService
            for(RollChangeLogVo a:rollAll){
                RollChangeLogEntity rollNew=new RollChangeLogEntity();
                rollNew.setDate(a.getDate());
                rollNew.setPid(id);
                rollNew.setEndDate(a.getEndDate());
                rollNew.setStartDate(a.getStartDate());
                rollNew.setClassCode(a.getClassCode());
                rollByList.add(rollNew);
            }
            // 将转化的List实体插入 rollChangeLog表中
            rollChangeLogService.saveBatch(rollByList);
            //获取父表的最后值 更新父表
            rollChangeVo.setEndDate(point.getRealDate());
            rollChangeService = getRollChangeService();
            rollChangeService.updateByRollChangeVo(rollChangeVo);
        }

    }




    /**
     * 小车运输旧辊回辊流程
     *
     * @param
     */

    private static void oldRoll(PointEntity point, RollChangeVo rollChangeVo, List<RollChangeLogVo> rollChangeLogList) {
        if (point.getClassCode().equals("l_fmc_cvcs") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            //开始为换辊表条件数据
            rollChangeVo.setDate(new Date());
            rollChangeVo.setStartDate(point.getRealDate());
            rollChangeVo.setCrewId(point.getCrewId());
            // 获取service 方法
            rollChangeService = getRollChangeService();
            // 将父信息插入表中 并获取主见id
            rollChangeService.saveByRollChangeVo(rollChangeVo);
            // 将设备标号作为主键，vo实体装进map里
            rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
            //开始为换辊记录表条件
            RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
            // 获取map的值
            RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
            // 将第一个记录值插入map实体中的List中
            rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);

        }

        //下夹紧板出口侧打开完成
        if (point.getClassCode().equals("l_fmc_twrspindlec") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0) {
            saveRollChangeList(point, rollChangeLogList);
            currentStep = 500;
        }

        //上工作辊接轴入口侧抱轴完成
        if(point.getClassCode().equals("l_fmc_twrspindlecend") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(500))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 501;
        }


        //上工作辊接轴出口侧抱轴完成
        if(point.getClassCode().equals("l_fmc_twrspindlecexd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(501))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 502;
        }

        //上工作辊锁紧板打开
        if(point.getClassCode().equals("l_fmc_twrkeepero") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(502))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 503;
        }

        //上工作辊锁紧板入口侧打开完成
        if(point.getClassCode().equals("l_fmc_twrkeepend") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(503))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 504;
        }


        //上工作辊锁紧板出口侧打开完成
        if(point.getClassCode().equals("l_fmc_twrkeepexd") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(504))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 505;
        }

        //小车到E7
        if(point.getClassCode().equals("l_fmc_car7") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(505))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 506;
        }

        //小车到E8
        if(point.getClassCode().equals("l_fmc_car8") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(506))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 507;
        }


        //小车到E6
        if(point.getClassCode().equals("l_fmc_car6") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(507))
                saveRollChangeList(point, rollChangeLogList);
            currentStep = 508;
        }

        //小车钩子落下完成
        if(point.getClassCode().equals("l_fmc_carhooklowr") && point.getValue().compareTo(BigDecimal.valueOf(1)) == 0){
            if(currentStep.equals(508))
                saveRollChangeList(point, rollChangeLogList);
            // 获取rollChangeLogService的值
            RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
            // 获取 map 中设备值中的value
            RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
            // 获取主键
            Long id=rollMap.get(point.getCrewId()).getId();
            // 获取 map 实体中的List
            List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
            // 创建Entity 实体 用于转化 vo和entity
            List<RollChangeLogEntity> rollByList=new ArrayList<>();
            // 转化List中的rollChangeLogService
            for(RollChangeLogVo a:rollAll){
                RollChangeLogEntity rollNew=new RollChangeLogEntity();
                rollNew.setDate(a.getDate());
                rollNew.setPid(id);
                rollNew.setEndDate(a.getEndDate());
                rollNew.setStartDate(a.getStartDate());
                rollNew.setClassCode(a.getClassCode());
                rollByList.add(rollNew);
            }
            // 将转化的List实体插入 rollChangeLog表中
            rollChangeLogService.saveBatch(rollByList);
            //获取父表的最后值 更新父表
            rollChangeVo.setEndDate(point.getRealDate());
            rollChangeService = getRollChangeService();
            rollChangeService.updateByRollChangeVo(rollChangeVo);
        }





    }
















    /**
     * 保存均值趋势信息
     *
     * @param trend
     */
    private static void saveTrendEntity(RollChangeLogEntity trend) {
        //保存线程
        RollChangeLogThread thread = new RollChangeLogThread(trend);
        thread.start();
    }



    /**
     * 保存记录实体
     *
     * @param
     */
    private static RollChangeLogVo saveRollChangeVo(PointEntity point) {
        RollChangeLogVo rollChangeLogVo = new RollChangeLogVo();
        rollChangeLogVo.setStartDate(point.getRealDate());
        rollChangeLogVo.setDate(new Date());
        rollChangeLogVo.setClassCode(point.getClassCode());
        rollChangeLogVo.setPid(rollMap.get(point.getCrewId()).getId());
        return rollChangeLogVo;

    }


    private static void saveRollChangeList(PointEntity point, List<RollChangeLogVo> rollChangeLogList) {
        RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
        rollChangeLogList=rollMap.get(point.getCrewId()).getRollChangeLogVoList();
        rollChangeLogList.get(rollChangeLogList.size() - 1).setEndDate(rollChangeLogVo.getStartDate());
        rollChangeLogList.add(rollChangeLogVo);
    }

    private static RollChangeService getRollChangeService(){
        if(rollChangeService == null){
            rollChangeService = (RollChangeService) SpringContextUtils.getBean("rollChangeService");
        }
        return rollChangeService;
    }


}
