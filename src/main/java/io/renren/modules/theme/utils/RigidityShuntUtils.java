package io.renren.modules.theme.utils;

import io.renren.common.sys.MainCatche;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;
import io.renren.modules.theme.thread.RigidityThread;
import io.renren.modules.theme.vo.RigidityTrendVo;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 轧机刚度评估包分流工具
 */
public class RigidityShuntUtils {

    // 轧机刚度均值趋势 key:deviceId
    public static Map<Long, RigidityTrendVo> rigidityTrendMap = new ConcurrentHashMap<Long, RigidityTrendVo>();

    /**
     * 轧机刚度分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){

        RigidityTrendVo trend = rigidityTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //过滤轧机刚度参数
        boolean flag =  setTrendPoints(point, trend);
        if(flag){
            rigidityTrendMap.put(point.getCrewId(), trend);
        }
    }


    /**
     * 设置轧机刚度均值实体
     * @param point
     * @param trend
     */
    private static boolean setTrendPoints(PointEntity point, RigidityTrendVo trend){
        boolean flag = false;
        BigDecimal value = point.getValue();
        if(value == null) return flag;
        // 开始时间有值开始计算周期趋势均值
        if(trend.getStartDate() == null){
            return flag;
        }
        //零位标定为1的时刻开始进行轧机刚度检测
        if("gap_zero".equals(point.getClassCode())){
            return processJobStatus(point, trend);
        }
        //读取实时数据，当轧制力为15000KN时,磁尺位置
        if(MainCatche.dataMap.get(point.getDeviceCode()).get("force_total_act").getValue()
                .compareTo(BigDecimal.valueOf(15000)) == 0){
            switch(point.getClassCode()) {
                case "magne_ds_en_act":
                    //传动侧入口磁尺实际值
                    trend.setAvgMagInDs(value);
                    flag = true;
                    break;
                case "magne_ds_ex_act":
                    //传动侧出口磁尺实际值
                    trend.setAvgMagOutDs(value);
                    flag = true;
                    break;
                case "magne_os_en_act":
                    //操作侧入口磁尺实际值
                    trend.setAvgMagInOs(value);
                    flag = true;
                    break;
                case "magne_os_ex_act":
                    trend.setAvgMagOutOs(value);
                    //操作侧出口磁尺实际值
                    flag = true;
                    break;
            }
        }
        //读取实时数据，当轧制力为15000KN时,磁尺位置(单侧轧制力大于2500KN开始计算离散点)
        if(MainCatche.dataMap.get(point.getDeviceCode()).get("force_total_act").getValue()
                .compareTo(BigDecimal.valueOf(2500)) > 0) {
            //平均数通过累加器运算
            switch (point.getClassCode()) {
                case "mm_os_pt":
                    // 轧机刚度操作侧(传感器)
                    trend.toAccumSumRigidityOsPt(value);
                    //轧机刚度大于2700为正常阈值范围内的值，用于计算刚度保持率
                    if (value.compareTo(BigDecimal.valueOf(2700)) > 0) {
                        trend.toAccRigidityInOsLc();
                    }
                    flag = true;
                    break;
                case "mm_ds_pt":
                    // 轧机刚度传动侧(传感器)
                    trend.toAccumSumRigidityDsPt(value);
                    if (value.compareTo(BigDecimal.valueOf(2700)) > 0) {
                        trend.toAccRigidityInDsLc();
                    }
                    flag = true;
                    break;
                case "mm_os_lc":
                    // 轧机刚度操作侧(压头)
                    trend.toAccumRigidityOsLc(value);
                    if (value.compareTo(BigDecimal.valueOf(2700)) > 0) {
                        trend.toAccRigidityInOsLc();
                    }
                    flag = true;
                    break;
                case "mm_ds_lc":
                    // 轧机刚度传动侧(压头)
                    trend.toAccumRigidityDsLc(value);
                    if (value.compareTo(BigDecimal.valueOf(2700)) > 0) {
                        trend.toAccRigidityInDsLc();
                    }
                    flag = true;
                    break;
            }
        }
        return flag;
    }
    /**
     * 处理作业状态
     * @param point
     * @param trend
     * @return
     */
    private static boolean processJobStatus(PointEntity point, RigidityTrendVo trend){
        boolean flag = false;
        //零位标定为1的时刻开始进行轧机刚度检测(从轧机刚度静态Map获取零位标定的值，用于判断信号变化)
        Integer state = rigidityTrendMap.get(point.getCrewId()).getState();
        // 更新状态
        if(trend.getStartDate() == null){
            // 系统无起始时间,未检测到零点位置信号
            trend.setState(state);
            flag = true;
        }else if(trend.getState().compareTo(state) != 0){
            if(state == 0) {// 标定结束信号，代表标定测试结束
                // 保存一个周期的事件
                // 开始和结束时间形成闭合的周期
                if(trend.getStartDate() != null){
                    // 保存趋势数据
                    trend.setEndDate(point.getRealDate());
                    saveTrendEntity(trend);
                    // 释放空间
                    trend = null;
                    // 新建趋势
                    trend = initTrendEntity(state);
                }
            }else{// 标定开始
                trend.setStartDate(point.getRealDate());
            }
            trend.setState(state);
            flag = true;
        }
        return flag;
    }
    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(RigidityTrendVo trend){
        //保存趋势数据
        RigidityAvgtrendEntity trendEntity = new RigidityAvgtrendEntity(trend);
        //保存线程
        RigidityThread thread = new RigidityThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param state
     * @return
     */
    private static RigidityTrendVo initTrendEntity(Integer state){
        RigidityTrendVo trend = new RigidityTrendVo();
        //trend.setStartDate(point.getRealDate());
        return trend;
    }



}
