package io.renren.modules.theme.utils;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.thread.RollChangeLogThread;
import io.renren.modules.theme.vo.RollChangeLogVo;
import io.renren.modules.theme.vo.RollChangeVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RollChangeShuntUtils {
    // crew_id,
    private static Map<Long, RollChangeVo> rollMap;

    private static RollChangeService rollChangeService;

    private static Integer currentStep;

    public static void grantShuntHandler(PointEntity point) {
        RollChangeVo rollChangeVo = new RollChangeVo();
        List<RollChangeLogVo> rollChangeLogList = new ArrayList<>();
        if(point.getValue().equals(BigDecimal.ONE)) {
            switch (point.getClassCode()) {
                case "spm_autostep_1_start":
                    //开始为换辊表条件数据
                    rollChangeVo.setDate(new Date());
                    rollChangeVo.setStartDate(point.getRealDate());
                    rollChangeVo.setCrewId(point.getCrewId());
                    // 获取service 方法
                    rollChangeService = getRollChangeService();
                    // 将父信息插入表中 并获取主见id
                    rollChangeService.saveByRollChangeVo(rollChangeVo);
                    // 将设备标号作为主键，vo实体装进map里
                    rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
                    //开始为换辊记录表条件
                    RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
                    // 获取map的值
                    RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
                    // 将第一个记录值插入map实体中的List中
                    rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);
                    //   rollChangeLogList.add(rollChangeLogVo);
                    currentStep = 1;
                    break;
                case "spm_waitspmoffline":
                    if (currentStep.equals(1)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 2;
                    }
                    break;
                case "spm_conwrdata":
                    if (currentStep.equals(2)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 3;
                    }
                    break;
                case "spm_autostep_1_end":
                    if (currentStep.equals(3)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 4;
                    }
                    break;
                case "spm_autostep_2_start":
                    if (currentStep.equals(4)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 5;
                    }
                    break;
                case "spm_spmfull":
                    if (currentStep.equals(5)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 6;
                    }
                    break;
                case "spm_bigautoserch":
                    if (currentStep.equals(6)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 7;
                    }
                    break;
                case "spm_jrlowpos":
                    if (currentStep.equals(7)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 8;
                    }
                    break;
                case "spm_zrlowpos":
                    if (currentStep.equals(8)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 9;
                    }
                    break;
                case "spm_tcyl1shrink":
                    if (currentStep.equals(9)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 10;
                    }
                    break;
                case "spm_tcyl2shrink":
                    if (currentStep.equals(10)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 11;
                    }
                    break;
                case "spm_bcyl1shrink":
                    if (currentStep.equals(11)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 12;
                    }
                    break;
                case "spm_oscarenlock":
                    if (currentStep.equals(12)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 13;
                    }
                    break;
                case "spm_oscarexlock":
                    if (currentStep.equals(13)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 14;
                    }
                    break;
                case "spm_oscarchangepos":
                    if (currentStep.equals(14)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 15;
                    }
                    break;
                case "spm_autostep_2_end":
                    if (currentStep.equals(15)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 16;
                    }
                    break;
                case "spm_autostep_3_start":
                    if (currentStep.equals(16)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 17;
                    }
                    break;
                case "spm_mode":
                    if (currentStep.equals(17)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 18;
                    }
                    break;
                case "spm_tburcylonline":
                    if (currentStep.equals(18)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 19;
                    }
                    break;
                case "spm_tburenlock":
                    if (currentStep.equals(19)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 20;
                    }
                    break;
                case "spm_tburexlock":
                    if (currentStep.equals(20)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 21;
                    }
                    break;
                case "spm_autostep_3_end":
                    if (currentStep.equals(21)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 22;
                    }
                    break;
                case "spm_autostep_4_start":
                    if (currentStep.equals(22)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 23;
                    }
                    break;
                case "spm_wrenenlock":
                    if (currentStep.equals(23)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 24;
                    }
                    break;
                case "spm_wrexenlock":
                    if (currentStep.equals(24)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 25;
                    }
                    break;
                case "spm_autostep_4_end":
                    if (currentStep.equals(25)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 26;
                    }
                    break;
                case "spm_autostep_5_start":
                    if (currentStep.equals(26)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 27;
                    }
                    break;
                case "spm_push":
                    if (currentStep.equals(27)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 28;
                    }
                    break;
                case "spm_newwrpos":
                    if (currentStep.equals(28)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 29;
                    }
                    break;
                case "spm_autostep_5_end":
                    if (currentStep.equals(29)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 30;
                    }
                    break;
                case "spm_autostep_6_start":
                    if (currentStep.equals(30)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 31;
                    }
                    break;
                case "spm_wrenlock":
                    if (currentStep.equals(31)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 32;
                    }
                    break;
                case "spm_wrexlock":
                    if (currentStep.equals(32)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 33;
                    }
                    break;
                case "spm_autostep_6_end":
                    if (currentStep.equals(33)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 34;
                    }
                    break;
                case "spm_autostep_7_start":
                    if (currentStep.equals(34)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 35;
                    }
                    break;
                case "spm_passlineadj":
                    if (currentStep.equals(35)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 36;
                    }
                    break;
                case "spm_newdata":
                    if (currentStep.equals(36)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 37;
                    }
                    break;
                case "spm_autostep_7_end":
                    if (currentStep.equals(37)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 38;
                    }
                    break;
                case "spm_autostep_8_start":
                    if (currentStep.equals(38)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 39;
                    }
                    break;
                case "spm_changepush":
                    if (currentStep.equals(39)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 40;
                    }
                    break;
                case "spm_autostep_8_end":
                    if (currentStep.equals(41)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 42;
                    }
                    break;
                case "spm_autostep_9_start":
                    if (currentStep.equals(42)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 43;
                    }
                    break;
                case "spm_autostep_9_end":
                    if (currentStep.equals(43)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 44;
                    }
                    break;
                case "spm_autostep_10_start":
                    if (currentStep.equals(44)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 45;
                    }
                    break;
                case "spm_oscarenenlock":
                    if (currentStep.equals(45)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 46;
                    }
                    break;
                case "spm_oscarexenlock":
                    if (currentStep.equals(46)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 47;
                    }
                    break;
                case "spm_autostep_10_end":
                    if (currentStep.equals(47)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 48;
                    }
                    break;
                case "spm_autostep_11_start":
                    if (currentStep.equals(48)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 49;
                    }
                    break;
                case "spm_oscarbackpos":
                    if (currentStep.equals(49)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 50;
                    }
                    break;
                case "spm_autostep_11_end":
                    if (currentStep.equals(50)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 51;
                    }
                    break;
                case "spm_autostep_12_start":
                    if (currentStep.equals(51)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 52;
                    }
                    break;
                case "spm_autostep_12_end":
                    if (currentStep.equals(52)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 53;
                    }
                    break;
                case "spm_autostep_13_start":
                    if (currentStep.equals(53)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 54;
                    }
                    break;
                case "spm_autostep_13_end":
                    if (currentStep.equals(54)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 55;
                    }
                    break;
                case "spm_autostep_17_start":
                    if (currentStep.equals(55)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 56;
                    }
                    break;
                case "spm_bcylopen":
                    if (currentStep.equals(56)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 57;
                    }
                    break;
                case "spm_tburexclampclose":
                    if (currentStep.equals(57)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 58;
                    }
                    break;
                case "spm_autostep_17_end":
                    if (currentStep.equals(58)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 59;
                    }
                    break;
                case "spm_autostep_18_start":
                    if (currentStep.equals(59)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 60;
                    }
                    break;
                case "spm_gapmidllepos":
                    if (currentStep.equals(60)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 61;
                    }
                    break;
                case "spm_bwrpos":
                    if (currentStep.equals(61)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 62;
                    }
                    break;
                case "spm_uwrpos":
                    if (currentStep.equals(62)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 63;
                    }
                    break;
                case "spm_smallautoserch":
                    if (currentStep.equals(63)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 64;
                    }
                    break;
                case "spm_autostep_18_end":
                    if (currentStep.equals(64)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 65;
                    }
                    break;
                case "spm_autostep_19_start":
                    if (currentStep.equals(65)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 66;
                    }
                    break;
                case "spm_ccylbackpos":
                    if (currentStep.equals(66)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 67;
                    }
                    break;
                case "spm_autostep_19_end":
                    if (currentStep.equals(67)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 68;
                    }
                    break;
                case "spm_autostep_22_start":
                    if (currentStep.equals(68)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 69;
                    }
                    break;
                case "spm_scarwaitpos":
                    if (currentStep.equals(69)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 70;
                    }
                    break;
                case "spm_autostep_23_start":
                    if (currentStep.equals(70)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 71;
                    }
                    break;
                case "spm_dsenclose":
                    if (currentStep.equals(71)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 72;
                    }
                    break;
                case "spm_dsexclose":
                    if (currentStep.equals(72)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 73;
                    }
                    break;
                case "spm_autostep_23_end":
                    if (currentStep.equals(73)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 74;
                    }
                    break;
                case "spm_autostep_24_start":
                    if (currentStep.equals(74)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 75;
                    }
                    break;
                case "spm_crcylbackpos":
                    if (currentStep.equals(75)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 76;
                    }
                    break;
                case "spm_passlineadjd":
                    if (currentStep.equals(76)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 77;
                    }
                    break;
                case "spm_autostep_24_end":
                    if (currentStep.equals(77)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 78;
                    }
                    break;
                case "spm_autostep_25_start":
                    if (currentStep.equals(78)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 79;
                    }
                    break;
                case "spm_spmclose":
                    if (currentStep.equals(79)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 80;
                    }
                    break;
                case "spm_autostep_25_end":
                    if (currentStep.equals(80)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 81;
                    }
                    break;
                case "spm_osclose":
                    if (currentStep.equals(81)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 82;
                    }
                    break;
                case "spm_auto_23_end":
                    if (currentStep.equals(81)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 82;
                    }
                    break;
                case "spm_scarfarspm":
                    if (currentStep.equals(82)) {
                        saveRollChangeList(point, rollChangeLogList);
                        // 获取rollChangeLogService的值
                        RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
                        // 获取 map 中设备值中的value
                        RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
                        // 获取主键
                        Long id=rollMap.get(point.getCrewId()).getId();
                        // 获取 map 实体中的List
                        List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
                        // 创建Entity 实体 用于转化 vo和entity
                        List<RollChangeLogEntity> rollByList=new ArrayList<>();
                        // 转化List中的rollChangeLogService
                        for(RollChangeLogVo a:rollAll){
                            RollChangeLogEntity rollNew=new RollChangeLogEntity();
                            rollNew.setDate(a.getDate());
                            rollNew.setPid(id);
                            rollNew.setEndDate(a.getEndDate());
                            rollNew.setStartDate(a.getStartDate());
                            rollNew.setClassCode(a.getClassCode());
                            rollByList.add(rollNew);
                        }
                        // 将转化的List实体插入 rollChangeLog表中
                        rollChangeLogService.saveBatch(rollByList);
                        //获取父表的最后值 更新父表
                        rollChangeVo.setEndDate(point.getRealDate());
                        rollChangeService = getRollChangeService();
                        rollChangeService.updateByRollChangeVo(rollChangeVo);
                    }
                    break;
            }
        }
    }

    /**
     * 保存均值趋势信息
     *
     * @param trend
     */
    private static void saveTrendEntity(RollChangeLogEntity trend) {
        //保存线程
        RollChangeLogThread thread = new RollChangeLogThread(trend);
        thread.start();
    }

    /**
     * 保存记录实体
     *
     * @param
     */
    private static RollChangeLogVo saveRollChangeVo(PointEntity point) {
        RollChangeLogVo rollChangeLogVo = new RollChangeLogVo();
        rollChangeLogVo.setStartDate(point.getRealDate());
        rollChangeLogVo.setDate(new Date());
        rollChangeLogVo.setClassCode(point.getClassCode());
        rollChangeLogVo.setPid(rollMap.get(point.getCrewId()).getId());
        return rollChangeLogVo;

    }


    private static void saveRollChangeList(PointEntity point, List<RollChangeLogVo> rollChangeLogList) {
        RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
        rollChangeLogList=rollMap.get(point.getCrewId()).getRollChangeLogVoList();
        rollChangeLogList.get(rollChangeLogList.size() - 1).setEndDate(rollChangeLogVo.getStartDate());
        rollChangeLogList.add(rollChangeLogVo);
    }

    private static RollChangeService getRollChangeService(){
        if(rollChangeService == null){
            rollChangeService = (RollChangeService) SpringContextUtils.getBean("rollChangeService");
        }
        return rollChangeService;
    }
}
