package io.renren.modules.theme.utils;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.currency.utils.CrewStateUtils;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.BendingAvgtrendEntity;
import io.renren.modules.theme.thread.BendingThread;
import io.renren.modules.theme.vo.BendingTrendVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 弯辊力评估包分流工具
 */
public class BendingShuntUtils {

    // 弯辊力均值趋势 key:crewId
    public static Map<Long, BendingTrendVo> bendingTrendMap = new ConcurrentHashMap<Long, BendingTrendVo>();

    /**
     * 弯辊力分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        BendingTrendVo trend = bendingTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //过滤弯辊力参数
        boolean flag =  setTrendPoints(point, trend);
        if(flag){
            bendingTrendMap.put(point.getCrewId(), trend);
        }
    }

    /**
     * 设置弯辊力均值实体
     * @param point
     * @param trend
     */
    private static boolean setTrendPoints(PointEntity point, BendingTrendVo trend){
        boolean flag = false;
        BigDecimal value = point.getValue();
        // 开始时间有值开始计算周期趋势均值
        if(trend.getStartDate() == null){
            return flag;
        }
        //获取历史统计的最大总弯辊力
        BigDecimal maxBending = trend.getMaxBendingTotal();
        //获取历史统计的最小总弯辊力
        BigDecimal minBending = trend.getMinBendingTotal();

        //获取上次操作侧弯轨力，用于计算弯轨力最大波动辐值
        BigDecimal lastAvgBendingOs = trend.getAvgBendingOs();
        //获取上次传动侧弯轨力，用于计算弯轨力最大波动辐值
        BigDecimal lastAvgBendingDs = trend.getAvgBendingDs();

        if(value == null) return flag;
        //平均数通过累加器运算
        switch(point.getClassCode()){
            case "wr_os_act":
                // 弯辊力操作侧
                trend.toAccumSumBendingOs(value);
                // 总弯辊力
                trend.toAccumBendingTotal(value);
                //验证最大波动辐度(操作侧)
                checkMaxWave(trend,lastAvgBendingOs,1);
                //验证最大最小弯辊力
                checkMaxOrMinBending(trend,maxBending,minBending);
                ///处理作业状态
                processJobStatus(point,trend);
                flag = true;
                break;
            case "wr_ds_act":
                // 弯辊力传动侧
                trend.toAccumSumBendingDs(value);
                // 总弯辊力
                trend.toAccumBendingTotal(value);
                //验证最大波动辐度(传动侧)
                checkMaxWave(trend,lastAvgBendingDs,2);
                //验证最大最小弯辊力
                checkMaxOrMinBending(trend,maxBending,minBending);
                //处理作业状态
                processJobStatus(point,trend);
                flag = true;
                break;
        }
        return flag;

    }

    /**
     * 处理作业状态
     * @param point
     * @param trend
     * @return
     */
    private static boolean processJobStatus(PointEntity point, BendingTrendVo trend){
        boolean flag = false;
        Integer state = CrewStateUtils.getCrewMillState(point);
        // 更新状态
        if(trend.getState() == null){
            // 系统首次运行，更新状态
            trend.setState(state);
            flag = true;
        }else if(trend.getState().compareTo(state) != 0){
            //获取轧制状态变化的实时时间
            Date changeDate = point.getRealDate();
            if(state == 0) {// 抛钢
                // 保存一个周期的事件
                // 开始和结束时间形成闭合的周期
                if(trend.getStartDate() != null){
                    // 保存趋势数据
                    trend.setEndDate(point.getRealDate());
                    // 轧制时间
                    trend.setRollTime(
                            DateUtils.getDateDiffForSecond(trend.getStartDate(), trend.getEndDate())
                    );
                    //取样时间大于5秒
                    if(DateUtils.getDateDiffForSecond(changeDate, trend.getEndDate())>=5){
                        saveTrendEntity(trend);
                    }
                    // 释放空间
                    trend = null;
                    // 新建趋势
                    trend = initTrendEntity(point, state);
                }
            }else{// 咬钢
                trend.setStartDate(point.getRealDate());
            }
            trend.setState(state);
            flag = true;
        }
        return flag;
    }

    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(BendingTrendVo trend){
        //保存趋势数据
        BendingAvgtrendEntity trendEntity = new BendingAvgtrendEntity(trend);
        //保存线程
        BendingThread thread = new BendingThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param point
     * @param state
     * @return
     */
    private static BendingTrendVo initTrendEntity(PointEntity point, Integer state){
        CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
        if(crew == null){
            return null;
        }
        BendingTrendVo trend = new BendingTrendVo(crew.getCrewId());
        //trend.setStartDate(point.getRealDate());
        trend.setState(state);
        return trend;
    }
    /**
     * 验证总弯辊力（最大/最小）
     * @param trend
     * @param maxBending
     * @param minBending
     * @return
     */
    private static void checkMaxOrMinBending(BendingTrendVo trend,BigDecimal maxBending,BigDecimal minBending){

        //最大弯辊力
        if(trend.getMaxBendingTotal() == null){
            trend.setMaxBendingTotal(trend.getAvgBendingTotal());
        }else if(maxBending.compareTo(trend.getAvgBendingTotal()) < 0){
            trend.setMaxBendingTotal(trend.getAvgBendingTotal());
        }
        //最小弯辊力
        if(trend.getMinBendingTotal() == null){
            trend.setMaxBendingTotal(trend.getAvgBendingTotal());
        }else if(minBending.compareTo(trend.getAvgBendingTotal()) > 0){
            trend.setMaxBendingTotal(trend.getAvgBendingTotal());
        }
    }
    /**
     * 验证波动幅度（操作侧）
     * @param trend
     * @param lastAvgBending 上个弯轨力值
     * @param flag（1.操作侧，2.传动侧）
     * @return
     */
    private static void checkMaxWave(BendingTrendVo trend,BigDecimal lastAvgBending,Integer flag){
        BigDecimal wave = BigDecimal.ZERO;
        //操作侧
        if(flag == 1){
            //波动幅度
            wave = CommonUtils.getRealBigDecimal(
                    Math.abs(
                            (lastAvgBending.subtract(trend.getAvgBendingOs())).doubleValue()
                    )
            );
            //最大波动幅度
            if(trend.getMaxChangeValueOs() == null){
                trend.setMaxChangeValueOs(wave);
            }else if(wave.compareTo(trend.getMaxChangeValueOs()) > 0){
                trend.setMaxChangeValueOs(wave);
            }
        }
        //传动侧
        else if(flag == 2){
            //波动幅度
            wave = CommonUtils.getRealBigDecimal(
                    Math.abs(
                            (lastAvgBending.subtract(trend.getAvgBendingDs())).doubleValue()
                    )
            );
            //最大波动幅度
            if(trend.getMaxChangeValueDs() == null){
                trend.setMaxChangeValueDs(wave);
            }else if(wave.compareTo(trend.getMaxChangeValueDs()) > 0){
                trend.setMaxChangeValueDs(wave);
            }
        }
    }
}
