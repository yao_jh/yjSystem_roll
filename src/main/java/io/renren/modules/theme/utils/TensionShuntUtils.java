package io.renren.modules.theme.utils;

import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.event.entity.EventLogEntity;
import io.renren.modules.event.thread.EventThread;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.feature.thread.MotorThread;
import io.renren.modules.feature.vo.MotorTrendVo;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.vo.TensionTrendVo;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 张力辊打滑评估包分流工具
 */
public class TensionShuntUtils {

    //张力辊趋势 key:deviceCode
    public static Map<String, TensionTrendVo> tensionTrendMap = new ConcurrentHashMap<String, TensionTrendVo>();

    /**
     * 张力辊打滑分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        TensionTrendVo trend = tensionTrendMap.get(point.getDeviceCode());
        if(trend == null){
            return ;
        }
        //设置张力辊均值
        setTrendPoints(point, trend);
    }

    private static void setTrendPoints(PointEntity point, TensionTrendVo trend) {
        //实时值
        BigDecimal value = point.getValue();
        if ("speed_act".equals(point.getClassCode())) {
            BigDecimal last = trend.getLastSpeed();
            //0:上升;1:下降
            Integer state = MeansUtils.getMonotoneState(last, value);
            trend.setLastSpeed(value);
            if (state == null) {
                return ;
            }
            //相同单调区间
            if (state.equals(trend.getState())) {
                trend.setMaxSpeed(value);
            } else {
                if(trend.getStartDate() == null){
                    trend.setMinSpeed(value);
                    trend.setStartDate(point.getRealDate());
                }else{
                    trend.setEndDate(point.getRealDate());
                    BigDecimal diffSpeed = trend.getMaxSpeed().subtract(trend.getMinSpeed());
                    if(Math.abs(diffSpeed.doubleValue()) > 5){//速度增量>5
                        Integer relative = compareRelativeTrend(trend);
                        if (relative < 0) {
                            //保存打滑
                            saveTrendEntity(trend);
                            // 释放空间
                            trend = null;
                            // 新建趋势
                            trend = initTrendEntity(state);
                        }
                    }
                }
            }
        }else if("torque".equals(point.getClassCode())){
            //单调区间开始
            if(trend.getStartDate() != null){
                //样本的开始点设置转矩起始值
                if(trend.getMinTorque() == null){
                    trend.setMinTorque(value);
                }
                trend.setMaxTorque(value);
            }
        }
    }

    /**
     * 比较出打滑相对趋势
     * @param trend
     * @return
     */
    private static Integer compareRelativeTrend(TensionTrendVo trend) {
        BigDecimal speedDiff = trend.getMaxSpeed().subtract(trend.getMinSpeed());
        BigDecimal torqueDiff = trend.getMaxTorque().subtract(trend.getMinTorque());
        BigDecimal product = speedDiff.multiply(torqueDiff);
        if(BigDecimal.ZERO.compareTo(product) < 0){
            return 1;
        }
        return -1;
    }

    /**
     * 保存打滑信息
     * @param trend
     */
    private static void saveTrendEntity(TensionTrendVo trend){
        EventLogEntity log = trend.createEventLog();
        //保存事件
        EventThread thread = new EventThread(log);
        thread.start();
    }

    /**
     * 初始化趋势
     * @param state
     * @return
     */
    private static TensionTrendVo initTrendEntity(Integer state){
        TensionTrendVo trend = new TensionTrendVo();
        //trend.setStartDate(point.getRealDate());
        trend.setState(state);
        return trend;
    }

}