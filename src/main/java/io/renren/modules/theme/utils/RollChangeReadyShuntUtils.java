package io.renren.modules.theme.utils;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.thread.RollChangeLogThread;
import io.renren.modules.theme.vo.RollChangeLogVo;
import io.renren.modules.theme.vo.RollChangeVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RollChangeReadyShuntUtils {

    // crew_id,
    private static Map<Long, RollChangeVo> rollMap;

    private static RollChangeService rollChangeService;

    private static Integer currentStep;

    public static void grantShuntHandler(PointEntity point) {
        RollChangeVo rollChangeVo = new RollChangeVo();
        List<RollChangeLogVo> rollChangeLogList = new ArrayList<>();
        if(point.getValue().equals(BigDecimal.ONE)) {
            switch (point.getClassCode()) {
                case "spmr_autostep_1_start":
                    //开始为换辊表条件数据
                    rollChangeVo.setDate(new Date());
                    rollChangeVo.setStartDate(point.getRealDate());
                    rollChangeVo.setCrewId(point.getCrewId());
                    // 获取service 方法
                    rollChangeService = getRollChangeService();
                    // 将父信息插入表中 并获取主见id
                    rollChangeService.saveByRollChangeVo(rollChangeVo);
                    // 将设备标号作为主键，vo实体装进map里
                    rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
                    //开始为换辊记录表条件
                    RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
                    // 获取map的值
                    RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
                    // 将第一个记录值插入map实体中的List中
                    rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);
                 //   rollChangeLogList.add(rollChangeLogVo);
                    currentStep = 1;
                    break;
                case "spmr_autostep_1_end":
                    if (currentStep.equals(1)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 2;
                    }

                    break;
                case "spmr_autostep_2_start":
                    if (currentStep.equals(2)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 3;
                    }
                    break;
                case "spmr_osdooro":
                    if (currentStep.equals(3)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 4;
                    }
                    break;
                case "spmr_dsendooro":
                    if (currentStep.equals(4)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 5;
                    }
                    break;
                case "spmr_dsexdooro":
                    if (currentStep.equals(5)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 6;
                    }
                    break;
                case "spmr_autostep_2_end":
                    if (currentStep.equals(6)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 7;
                    }
                    break;
                case "spmr_autostep_3_start":
                    if (currentStep.equals(7)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 8;
                    }
                    break;
                case "spmr_autostep_3_end":
                    if (currentStep.equals(8)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 9;
                    }
                    break;
                case "spmr_autostep_4_start":
                    if (currentStep.equals(9)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 10;
                    }
                    break;
                case "spmr_scchangepos":
                    if (currentStep.equals(10)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 11;
                    }
                    break;
                case "spmr_autostep_4_end":
                    if (currentStep.equals(11)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 12;
                    }
                    break;
                case "spmr_autostep_5_start":
                    if (currentStep.equals(12)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 13;
                    }
                    break;
                case "spmr_enlock":
                    if (currentStep.equals(13)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 14;
                    }
                    break;
                case "spmr_exlock":
                    if (currentStep.equals(14)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 15;
                    }
                    break;
                case "spmr_autostep_5_end":
                    if (currentStep.equals(15)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 16;
                    }
                    break;
                case "spmr_autostep_6_start":
                    if (currentStep.equals(16)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 17;
                    }
                    break;
                case "spmr_wrpready":
                    if (currentStep.equals(17)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 18;
                    }
                    break;
                case "spmr_autostep_6_end":
                    if (currentStep.equals(18)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 19;
                    }
                    break;
                case "spmr_autostep_7_start":
                    if (currentStep.equals(19)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 20;
                    }
                    break;
                case "spmr_changeready":
                    if (currentStep.equals(20)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 21;
                    }
                    break;
                case "spmr_autostep_7_end":
                    if (currentStep.equals(21)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 22;
                    }
                    break;
                case "spmr_autostep_8_start":
                    if (currentStep.equals(22)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 23;
                    }
                    break;
                case "spmr_autostep_8_end":
                    if (currentStep.equals(23)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 24;
                    }
                    break;
                case "spm_waitspmoffline1":
                    if (currentStep.equals(24)) {
                        saveRollChangeList(point, rollChangeLogList);
                        // 获取rollChangeLogService的值
                        RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
                        // 获取 map 中设备值中的value
                        RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
                        // 获取主键
                        Long id=rollMap.get(point.getCrewId()).getId();
                        // 获取 map 实体中的List
                        List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
                        // 创建Entity 实体 用于转化 vo和entity
                        List<RollChangeLogEntity> rollByList=new ArrayList<>();
                        // 转化List中的rollChangeLogService
                        for(RollChangeLogVo a:rollAll){
                            RollChangeLogEntity rollNew=new RollChangeLogEntity();
                            rollNew.setDate(a.getDate());
                            rollNew.setPid(id);
                            rollNew.setEndDate(a.getEndDate());
                            rollNew.setStartDate(a.getStartDate());
                            rollNew.setClassCode(a.getClassCode());
                            rollByList.add(rollNew);
                        }
                        // 将转化的List实体插入 rollChangeLog表中
                        rollChangeLogService.saveBatch(rollByList);
                        //获取父表的最后值 更新父表
                        rollChangeVo.setEndDate(point.getRealDate());
                        rollChangeService = getRollChangeService();
                        rollChangeService.updateByRollChangeVo(rollChangeVo);
                    }
                    break;
            }
        }
    }

    /**
     * 保存均值趋势信息
     *
     * @param trend
     */
    private static void saveTrendEntity(RollChangeLogEntity trend) {
        //保存线程
        RollChangeLogThread thread = new RollChangeLogThread(trend);
        thread.start();
    }

    /**
     * 保存记录实体
     *
     * @param
     */
    private static RollChangeLogVo saveRollChangeVo(PointEntity point) {
        RollChangeLogVo rollChangeLogVo = new RollChangeLogVo();
        rollChangeLogVo.setStartDate(point.getRealDate());
        rollChangeLogVo.setDate(new Date());
        rollChangeLogVo.setClassCode(point.getClassCode());
        rollChangeLogVo.setPid(rollMap.get(point.getCrewId()).getId());
        return rollChangeLogVo;

    }


    private static void saveRollChangeList(PointEntity point, List<RollChangeLogVo> rollChangeLogList) {
        RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
        rollChangeLogList=rollMap.get(point.getCrewId()).getRollChangeLogVoList();
        rollChangeLogList.get(rollChangeLogList.size() - 1).setEndDate(rollChangeLogVo.getStartDate());
        rollChangeLogList.add(rollChangeLogVo);
    }

    private static RollChangeService getRollChangeService(){
        if(rollChangeService == null){
            rollChangeService = (RollChangeService) SpringContextUtils.getBean("rollChangeService");
        }
        return rollChangeService;
    }

}