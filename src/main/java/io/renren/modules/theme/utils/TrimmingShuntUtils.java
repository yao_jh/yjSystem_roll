package io.renren.modules.theme.utils;


import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.vo.TrimmingVo;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 切边剪评估包分流工具
 */
public class TrimmingShuntUtils {

    //机组Id key:crewId
    public static Map<Long, TrimmingVo> trimmingMap = new ConcurrentHashMap<Long, TrimmingVo>();

    /**
     * 切边剪分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        TrimmingVo vo = trimmingMap.get(point.getCrewId());
        //平均数通过累加器运算s
        switch(point.getClassCode()){
            case "stm1_active":
                // 1号刀头激活
                if(vo != null){
                    BigDecimal value = point.getValue();
                    //检测到1号刀头激活
                    if(value.compareTo(BigDecimal.ONE) == 0){
                        //状态发生改变才是一个周期
                        if(value.compareTo(vo.getFirstValue()) != 0){
                            //1号刀头更换次数加1
                            Long firstCount = vo.getFirstCount();
                            if(firstCount == null){
                                firstCount = 0L;
                            }
                            //更新1号刀头
                            vo.setFirstCount(firstCount + 1);
                            vo.setFirstDate(point.getRealDate());
                        }
                    }
                    //更新1号刀头最新状态
                    vo.setFirstValue(value);
                }
                break;
            case "stm2_active":
                // 2号刀头激活
                if(vo != null){
                    BigDecimal value = point.getValue();
                    //检测到2号刀头激活
                    if(value.compareTo(BigDecimal.ONE) == 0){
                        //状态发生改变才是一个周期
                        if(value.compareTo(vo.getSecondValue()) != 0){
                            //2号刀头更换次数加1
                            Long secondCount = vo.getSecondCount();
                            if(secondCount == null){
                                secondCount = 0L;
                            }
                            //更新2号刀头
                            vo.setFirstCount(secondCount + 1);
                            vo.setFirstDate(point.getRealDate());
                        }
                    }
                    //更新2号刀头最新状态
                    vo.setSecondValue(value);
                }
                break;
            case "stm3_active":
                // 3号刀头激活
                if(vo != null){
                    BigDecimal value = point.getValue();
                    //检测到3号刀头激活
                    if(value.compareTo(BigDecimal.ONE) == 0){
                        //状态发生改变才是一个周期
                        if(value.compareTo(vo.getThirdValue()) != 0){
                            //3号刀头更换次数加1
                            Long thirdCount = vo.getThirdCount();
                            if(thirdCount == null){
                                thirdCount = 0L;
                            }
                            //更新3号刀头
                            vo.setThirdCount(thirdCount + 1);
                            vo.setThirdDate(point.getRealDate());
                        }
                    }
                    //更新3号刀头最新状态
                    vo.setThirdValue(value);
                }
                break;
            case "stm4_active":
                // 4号刀头激活
                if(vo != null){
                    BigDecimal value = point.getValue();
                    //检测到4号刀头激活
                    if(value.compareTo(BigDecimal.ONE) == 0){
                        //状态发生改变才是一个周期
                        if(value.compareTo(vo.getFourthValue()) != 0){
                            //4号刀头更换次数加1
                            Long fourthCount = vo.getFourthCount();
                            if(fourthCount == null){
                                fourthCount = 0L;
                            }
                            //更新4号刀头
                            vo.setFourthCount(fourthCount + 1);
                            vo.setFourthDate(point.getRealDate());
                        }
                    }
                    //更新4号刀头最新状态
                    vo.setFourthValue(value);
                }
                break;
         }
    }

}