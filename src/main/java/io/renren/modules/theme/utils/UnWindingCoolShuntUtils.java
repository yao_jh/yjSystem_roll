package io.renren.modules.theme.utils;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.service.RollChangeLogService;
import io.renren.modules.theme.service.RollChangeService;
import io.renren.modules.theme.thread.RollChangeLogThread;
import io.renren.modules.theme.vo.RollChangeLogVo;
import io.renren.modules.theme.vo.RollChangeVo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class UnWindingCoolShuntUtils {
    // crew_id,
    private static Map<Long, RollChangeVo> rollMap;

    private static RollChangeService rollChangeService;

    private static Integer currentStep;

    public static void grantShuntHandler(PointEntity point) {
        RollChangeVo rollChangeVo = new RollChangeVo();
        List<RollChangeLogVo> rollChangeLogList = new ArrayList<>();
        if(point.getValue().equals(BigDecimal.ONE)) {
            switch (point.getClassCode()) {
                case "tr_autostep_1_start":
                    //开始为换辊表条件数据
                    rollChangeVo.setDate(new Date());
                    rollChangeVo.setStartDate(point.getRealDate());
                    rollChangeVo.setCrewId(point.getCrewId());
                    // 获取service 方法
                    rollChangeService = getRollChangeService();
                    // 将父信息插入表中 并获取主见id
                    rollChangeService.saveByRollChangeVo(rollChangeVo);
                    // 将设备标号作为主键，vo实体装进map里
                    rollMap.put(rollChangeVo.getCrewId(), rollChangeVo);
                    //开始为换辊记录表条件
                    RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
                    // 获取map的值
                    RollChangeVo rollChangeVoDto=rollMap.get(point.getCrewId());
                    // 将第一个记录值插入map实体中的List中
                    rollChangeVoDto.getRollChangeLogVoList().add(rollChangeLogVo);
                    //   rollChangeLogList.add(rollChangeLogVo);
                    currentStep = 1;
                    break;
                case "tr_autostep_1_end":
                    if (currentStep.equals(1)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 2;
                    }
                    break;
                case "tr_autostep_2_start":
                    if (currentStep.equals(2)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 3;
                    }
                    break;
                case "tr_cardischargpos":
                    if (currentStep.equals(3)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 4;
                    }
                    break;
                case "tr_caraxispos":
                    if (currentStep.equals(4)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 5;
                    }
                    break;
                case "tr_autodspeed":
                    if (currentStep.equals(5)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 6;
                    }
                    break;
                case "tr_autostep_2_end":
                    if (currentStep.equals(6)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 7;
                    }
                    break;
                case "tr_autostep_3_start":
                    if (currentStep.equals(7)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 8;
                    }
                    break;
                case "tr_cardischarghigh":
                    if (currentStep.equals(8)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 9;
                    }
                    break;
                case "tr_coiltail":
                    if (currentStep.equals(9)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 10;
                    }
                    break;
                case "tr_autostep_3_end":
                    if (currentStep.equals(10)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 11;
                        //  BigDecimal.valueOf(3);
                    }
                    break;
                case "tr_autostep_5_start":
                    if (currentStep.equals(11)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 12;
                    }
                    break;
                case "tr_carcertenpos":
                    if (currentStep.equals(12)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 13;
                    }
                    break;
                case "tr_autostep_5_end":
                    if (currentStep.equals(13)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 14;
                    }
                    break;
                case "tr_autostep_6_start":
                    if (currentStep.equals(14)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 15;
                    }
                    break;
                case "tr_carlowpr":
                    if (currentStep.equals(15)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 16;
                    }
                    break;
                case "tr_autostep_6_end":
                    if (currentStep.equals(16)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 17;
                    }
                    break;
                case "tr_autostep_7_start":
                    if (currentStep.equals(17)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 18;
                    }
                    break;
                case "tr_mdshrink":
                    if (currentStep.equals(18)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 19;
                    }
                    break;
                case "tr_autostep_7_end":
                    if (currentStep.equals(19)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 20;
                    }
                    break;
                case "tr_autostep_8_start":
                    if (currentStep.equals(20)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 21;
                    }
                    break;
                case "tr_usroffline":
                    if (currentStep.equals(21)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 22;
                    }
                    break;
                case "tr_dsroffline":
                    if (currentStep.equals(22)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 23;
                    }
                    break;
                case "tr_mgoffline":
                    if (currentStep.equals(23)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 24;
                    }
                    break;
                case "tr_sudown":
                    if (currentStep.equals(24)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 25;
                    }
                    break;
                case "tr_srshrink":
                    if (currentStep.equals(25)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 26;
                    }
                    break;
                case "tr_autostep_8_end":
                    if (currentStep.equals(26)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 27;
                    }
                    break;
                case "tr_autostep_9_start":
                    if (currentStep.equals(27)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 28;
                    }
                    break;
                case "tr_osuoffline":
                    if (currentStep.equals(28)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 29;
                    }
                    break;
                case "tr_dsuoffline":
                    if (currentStep.equals(29)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 30;
                    }
                    break;
                case "tr_mdspeedzero":
                    if (currentStep.equals(30)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 31;
                    }
                    break;
                case "tr_autostep_9_end":
                    if (currentStep.equals(31)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 32;
                    }
                    break;
                case "tr_autostep_10_start":
                    if (currentStep.equals(32)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 33;
                    }
                    break;
                case "tr_coil1800":
                    if (currentStep.equals(33)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 34;
                    }
                    break;
                case "tr_autostep_10_end":
                    if (currentStep.equals(34)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 35;
                    }
                    break;
                case "tr_autostep_11_start":
                    if (currentStep.equals(35)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 36;
                    }
                    break;
                case "tr_carwaitpos":
                    if (currentStep.equals(36)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 37;
                    }
                    break;
                case "tr_autostep_11_end":
                    if (currentStep.equals(37)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 38;
                    }
                    break;
                case "tr_autostep_12_start":
                    if (currentStep.equals(38)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 39;
                    }
                    break;
                case "tr_autostep_12_end":
                    if (currentStep.equals(39)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 40;
                    }
                    break;
                case "tr_autostep_13_start":
                    if (currentStep.equals(41)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 42;
                    }
                    break;
                case "tr_cardischargehigh":
                    if (currentStep.equals(42)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 43;
                    }
                    break;
                case "tr_saddlefree":
                    if (currentStep.equals(43)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 44;
                    }
                    break;
                case "tr_autostep_13_end":
                    if (currentStep.equals(44)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 45;
                    }
                    break;
                case "tr_autostep_14_start":
                    if (currentStep.equals(45)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 46;
                    }
                    break;
                case "tr_carsaddlepos":
                    if (currentStep.equals(46)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 47;
                    }
                    break;
                case "tr_autostep_14_end":
                    if (currentStep.equals(47)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 48;
                    }
                    break;
                case "tr_autostep_15_start":
                    if (currentStep.equals(48)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 49;
                    }
                    break;
                case "tr_carlowestpos":
                    if (currentStep.equals(49)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 50;
                    }
                    break;
                case "tr_autostep_15_end":
                    if (currentStep.equals(50)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 51;
                    }
                    break;
                case "tr_autostep_17_start":
                    if (currentStep.equals(51)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 52;
                    }
                    break;
                case "tr_autostep_17_end":
                    if (currentStep.equals(52)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 53;
                    }
                    break;
                case "tr_carlowpre":
                    if (currentStep.equals(53)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 54;
                    }
                    break;
                case "tr_carexwaitpos":
                    if (currentStep.equals(54)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 55;
                    }
                    break;
                case "tr_decoil":
                    if (currentStep.equals(55)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 56;
                    }
                    break;
                case "tr_carplantpos":
                    if (currentStep.equals(56)) {
                        saveRollChangeList(point, rollChangeLogList);
                        currentStep = 57;
                    }
                    break;
                case "tr_carexwaitpos1":
                    if (currentStep.equals(57)) {
                        saveRollChangeList(point, rollChangeLogList);
                        // 获取rollChangeLogService的值
                        RollChangeLogService rollChangeLogService = (RollChangeLogService) SpringContextUtils.getBean("rollChangeLogService");
                        // 获取 map 中设备值中的value
                        RollChangeVo rollChangeVoDtoLast=rollMap.get(point.getCrewId());
                        // 获取主键
                        Long id=rollMap.get(point.getCrewId()).getId();
                        // 获取 map 实体中的List
                        List<RollChangeLogVo>  rollAll=rollChangeVoDtoLast.getRollChangeLogVoList();
                        // 创建Entity 实体 用于转化 vo和entity
                        List<RollChangeLogEntity> rollByList=new ArrayList<>();
                        // 转化List中的rollChangeLogService
                        for(RollChangeLogVo a:rollAll){
                            RollChangeLogEntity rollNew=new RollChangeLogEntity();
                            rollNew.setDate(a.getDate());
                            rollNew.setPid(id);
                            rollNew.setEndDate(a.getEndDate());
                            rollNew.setStartDate(a.getStartDate());
                            rollNew.setClassCode(a.getClassCode());
                            rollByList.add(rollNew);
                        }
                        // 将转化的List实体插入 rollChangeLog表中
                        rollChangeLogService.saveBatch(rollByList);
                        //获取父表的最后值 更新父表
                        rollChangeVo.setEndDate(point.getRealDate());
                        rollChangeService = getRollChangeService();
                        rollChangeService.updateByRollChangeVo(rollChangeVo);
                    }
                    break;
            }
        }

    }


    /**
     * 保存均值趋势信息
     *
     * @param trend
     */
    private static void saveTrendEntity(RollChangeLogEntity trend) {
        //保存线程
        RollChangeLogThread thread = new RollChangeLogThread(trend);
        thread.start();
    }

    /**
     * 保存记录实体
     *
     * @param
     */
    private static RollChangeLogVo saveRollChangeVo(PointEntity point) {
        RollChangeLogVo rollChangeLogVo = new RollChangeLogVo();
        rollChangeLogVo.setStartDate(point.getRealDate());
        rollChangeLogVo.setDate(new Date());
        rollChangeLogVo.setClassCode(point.getClassCode());
        rollChangeLogVo.setPid(rollMap.get(point.getCrewId()).getId());
        return rollChangeLogVo;

    }


    private static void saveRollChangeList(PointEntity point, List<RollChangeLogVo> rollChangeLogList) {
        RollChangeLogVo rollChangeLogVo = saveRollChangeVo(point);
        rollChangeLogList=rollMap.get(point.getCrewId()).getRollChangeLogVoList();
        rollChangeLogList.get(rollChangeLogList.size() - 1).setEndDate(rollChangeLogVo.getStartDate());
        rollChangeLogList.add(rollChangeLogVo);
    }

    private static RollChangeService getRollChangeService(){
        if(rollChangeService == null){
            rollChangeService = (RollChangeService) SpringContextUtils.getBean("rollChangeService");
        }
        return rollChangeService;
    }
}
