package io.renren.modules.theme.utils;


import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.vo.WelderVo;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 焊机评估包分流工具
 */
public class WelderShuntUtils {

    //焊机焊接 key:deviceId
    public static Map<Long, WelderVo> welderMap = new ConcurrentHashMap<Long, WelderVo>();

    /**
     * 焊机焊接分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        if("weld_sig".equals(point.getClassCode())){
            WelderVo vo = welderMap.get(point.getDeviceId());
            if(vo != null){
                BigDecimal value = point.getValue();
                //检测到焊接完成
                if(value.compareTo(BigDecimal.ONE) == 0){
                    //状态发生改变才是一个周期
                    if(value.compareTo(vo.getValue()) != 0){
                        //次数加1
                        Long total = vo.getTotal();
                        if(total == null){
                            total = 0L;
                        }
                        //更新焊机
                        vo.setTotal(total + 1);
                        vo.setDate(point.getRealDate());
                    }
                }
                //更新最新状态
                vo.setValue(value);
            }

        }
    }

}