package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.theme.vo.AgcTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * AGC均值趋势表
 */
@Data
@TableName("tb_agc_avgtrend")
public class AgcAvgtrendEntity {

    @TableId
    private Long id;
    //设备ID
    private Long deviceId;

    //动作速度
    private BigDecimal speed;
    //动作时间
    private BigDecimal duration;
    //结束辊缝
    private BigDecimal endGap;
    //速度与平均动作速度偏差
    private BigDecimal speedDiff;
    //平均辊缝偏差
    private BigDecimal gapDiff;
    //入口磁尺
    private BigDecimal magneEn;
    //出口磁尺
    private BigDecimal magneEx;
    //设定和反馈平均偏差
    private BigDecimal avgDiff;

    //分类 0:行程2-4;1:行程4-10;2:行程10-20;3:行程>=20;
    private Integer type;
    //边侧 0:操作侧;1:驱动侧
    private Integer side;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public AgcAvgtrendEntity(){
        super();
    }

    public AgcAvgtrendEntity(AgcTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

}
