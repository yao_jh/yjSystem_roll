package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.theme.vo.RigidityTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description 轧机刚度值表
 * @author gaosiji
 * @create 2020-09-25 09:57
 */
@Data
@TableName("tb_rigidity_avgtrend")
public class RigidityAvgtrendEntity {

    @TableId
    private Long id;
    //设备ID
    private Long deviceId;

    //平均轧轧机刚度操作侧(压头) = 总数/次数
    private BigDecimal avgRigidityOsPt;

    //平均轧轧机刚度传动侧(压头) = 总数/次数
    private BigDecimal avgRigidityDsPt;

    //平均轧轧机刚度操作侧(传感器) = 总数/次数
    private BigDecimal avgRigidityOsLc;

    //平均轧轧机刚度传动侧(传感器) = 总数/次数
    private BigDecimal avgRigidityDsLc;

    //对向刚度（压头）操作侧 - 传动侧
    private BigDecimal rigidityOpLc;

    //对向刚度（传感器）操作侧 - 传动侧
    private BigDecimal rigidityOpPt;

    //压头操作侧刚度保持率
    private BigDecimal keepOsPt;

    //压头传动侧侧刚度保持率
    private BigDecimal keepDsPt;

    //传感器刚度保持率
    private BigDecimal keepOsLc;

    //传感器刚度保持率
    private BigDecimal keepDsLc;

    //零点位置
    private BigDecimal posZero;
    //入口零点差
    private BigDecimal inZeroDiff;
    //出口零点差
    private BigDecimal outZeroDiff;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public RigidityAvgtrendEntity(){
        super();
    }

    public RigidityAvgtrendEntity(RigidityTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

}
