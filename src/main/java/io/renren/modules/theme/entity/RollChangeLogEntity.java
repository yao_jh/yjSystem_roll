package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.util.Date;

/**
 * 换辊记录
 */
@Data
@TableName("tb_roll_change_log")
public class RollChangeLogEntity {

    @TableId
    private Long id;
    //设备ID
    private Long pid;
    //记录时间
    private Date date;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //进行步骤
    private String classCode;
    //动作时长
    @Transient
    private Long duration;

    public Long getDuration() {
        return startDate.getTime() - endDate.getTime();
    }
}
