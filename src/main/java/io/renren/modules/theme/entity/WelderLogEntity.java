package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.theme.vo.WelderVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * 焊接日志表
 * 每次清零保存统计次数
 */
@Data
@TableName("tb_welder_log")
public class WelderLogEntity {

    @TableId
    private Long id;

    //设备ID
    private Long deviceId;
    //焊接次数
    private Long total;
    //记录时间
    private Date date;

    public WelderLogEntity(){
        super();
    }

    public WelderLogEntity(WelderVo welderVo){
        BeanUtils.copyProperties(welderVo, this);
    }

}
