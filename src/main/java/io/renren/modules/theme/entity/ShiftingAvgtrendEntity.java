package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.theme.vo.ShiftingTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 窜辊动作均值趋势表
 */
@Data
@TableName("tb_shifting_avgtrend")
public class ShiftingAvgtrendEntity {

    @TableId
    private Long id;
    //设备ID
    private Long deviceId;
    //动作速度
    private BigDecimal speed;
    //动作时间(稳态时间)
    private BigDecimal duration;
    //行程
    private BigDecimal distance;
    //边侧 0:入口;1:出口
    private Integer side;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public ShiftingAvgtrendEntity(){
        super();
    }

    public ShiftingAvgtrendEntity(ShiftingTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

}
