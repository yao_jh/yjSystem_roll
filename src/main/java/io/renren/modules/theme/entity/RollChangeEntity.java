package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.util.Date;

/**
 * 换辊
 */
@Data
@TableName("tb_roll_change")
public class RollChangeEntity {
    @TableId
    private Long id;
    //设备ID
    private Long crewId;
    //记录时间
    private Date date;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    @Transient
    private Long duration;

    public Long getDuration(Long duration) {
        return startDate.getTime() - endDate.getTime();
    }

}
