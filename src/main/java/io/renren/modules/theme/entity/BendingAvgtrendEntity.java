package io.renren.modules.theme.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.theme.vo.BendingTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 窜辊动作均值趋势表
 */
@Data
@TableName("tb_bending_avgtrend")
public class BendingAvgtrendEntity {

    @TableId
    private Long id;
    //机组ID
    private Long crewId;
    //平均弯辊力操作侧OS = 总数/次数
    private BigDecimal avgBendingOs;
    //平均弯辊力传动侧DS = 总数/次数
    private BigDecimal avgBendingDs;
    //平均总弯辊力
    private BigDecimal avgBendingTotal;
    //最大总弯辊力
    private BigDecimal maxBendingTotal;
    //最小总弯辊力
    private BigDecimal minBendingTotal;
    //平均弯辊力偏差（OS-DS）
    private BigDecimal avgBendingDiff;
    //最大弯辊力波动辐值（操作侧）
    private BigDecimal maxChangeValueOs;
    //最大弯辊力波动辐值（传动侧）
    private BigDecimal maxChangeValueDs;
    //轧制时间
    private Long rollTime;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public BendingAvgtrendEntity(){
        super();
    }

    public BendingAvgtrendEntity(BendingTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

}
