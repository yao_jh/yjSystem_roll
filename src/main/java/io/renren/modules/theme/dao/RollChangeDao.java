package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.theme.entity.RollChangeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 换辊Dao
 */
@Mapper
public interface RollChangeDao extends BaseMapper<RollChangeEntity> {

}
