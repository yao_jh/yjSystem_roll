
package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * AGC均值趋势Dao
 */
@Mapper
public interface AgcAvgtrendDao extends BaseMapper<AgcAvgtrendEntity> {

    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<AgcSampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    List<AgcAvgtrendEntity> selectListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);
}