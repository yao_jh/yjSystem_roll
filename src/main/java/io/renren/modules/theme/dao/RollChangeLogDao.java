package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.theme.entity.RollChangeLogEntity;
import io.renren.modules.theme.vo.AvgAndMinVo;
import io.renren.modules.theme.vo.NewChangeLogVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * 换辊记录Dao
 */
@Mapper
public interface RollChangeLogDao extends BaseMapper<RollChangeLogEntity> {

    List<AvgAndMinVo> selectAvgAndMin(Date startDate, Date endDate, Long crewIdOne, Long crewIdTwo, Long crewIdThree);


    List<NewChangeLogVo> selectNewLog(Long code);


    List<AvgAndMinVo> selectAvgAndMinByCool(Date startDate, Date endDate, Long crewIdOne, Long crewIdTwo);

    List<AvgAndMinVo> selectAvgAndMinByCoolRollReady(Date startDate, Date endDate, Long crewIdOne);

    List<AvgAndMinVo> selectAvgAndMinByCoolRoll(Date startDate, Date endDate, Long crewIdOne);

    List<NewChangeLogVo> selectNewLogByCoolRoll(Date startDate, Date endDate, Long crewIdOne);

    List<NewChangeLogVo> selectNewLogByCoolRollReady(Date startDate, Date endDate, Long crewIdOne);
}
