
package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.vo.ShiftingSampleVo;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * 窜辊均值趋势Dao
 */
@Mapper
public interface ShiftingAvgtrendDao extends BaseMapper<ShiftingAvgtrendEntity> {

    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ShiftingSampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    List<ShiftingAvgtrendEntity> selectListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);
}