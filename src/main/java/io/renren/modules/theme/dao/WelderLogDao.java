
package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.theme.entity.WelderLogEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * 焊机焊接次数和重焊次数Dao
 */
@Mapper
public interface WelderLogDao extends BaseMapper<WelderLogEntity> {


    /**
     * 查询设备的焊接日志
     * @param deviceId
     * @return
     */
    List<WelderLogEntity> selectByDeviceId(Long deviceId);

    /**
     * 获取焊接历史总次数
     * @param deviceId
     * @return
     */
    Long selecTotalTimes(Long deviceId);
}