
package io.renren.modules.theme.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.vo.RigiditySampleVo;
import io.renren.modules.theme.entity.RigidityAvgtrendEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * 轧机刚度均值趋势Dao
 */
@Mapper
public interface RigidityAvgtrendDao extends BaseMapper<RigidityAvgtrendEntity> {

    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<RigiditySampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    List<RigidityAvgtrendEntity> selectListByDate(Long deviceId, Date startDate, Date endDate);
}