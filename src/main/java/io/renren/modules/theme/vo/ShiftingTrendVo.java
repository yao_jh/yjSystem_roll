package io.renren.modules.theme.vo;

import io.renren.common.utils.DateUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 窜辊均值趋势视图
 */
@Data
public class ShiftingTrendVo {

    //设备ID
    private Long deviceId;
    //动作速度
    private BigDecimal speed;
    //动作时间(稳态时间)
    private BigDecimal duration;
    //边侧 0:入口;1:出口
    private Integer side;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    //行程
    private BigDecimal distance;
    //最小位移
    private BigDecimal minValue;
    //最大位移
    private BigDecimal maxValue;
    //初始量
    private BigDecimal initValue;
    //上一个值
    private BigDecimal lastValue;
    //0:上升;1:下降
    private Integer state;

    //入口平均速度
    public BigDecimal getSpeed() {
        //时间毫秒
        Long milis = getDuration();
        if(milis.compareTo(0L) != 0){
            //移动距离 = 最大位移-最小位移
            BigDecimal value = getTrip();
            //速度 = 移动距离/时间
            speed = value.divide(BigDecimal.valueOf(milis)).setScale(3, BigDecimal.ROUND_HALF_DOWN);
        }
        return speed;
    }

    //移动距离 = 最大位移-最小位移
    public BigDecimal getTrip() {
        distance = maxValue.subtract(minValue);
        return distance;
    }
    //时间段
    public Long getDuration() {
        return DateUtils.getDateDiffForMilis(startDate, endDate);
    }

}
