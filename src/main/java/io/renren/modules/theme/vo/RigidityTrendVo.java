package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 轧机刚度均值趋势视图
 */
@Data
public class RigidityTrendVo {

    //设备ID
    private Long deviceId;

    //平均轧轧机刚度操作侧(压头) = 总数/次数
    private BigDecimal avgRigidityOsLc;
    private BigDecimal sumRigidityOsLc;//总数
    private long numRigidityOsLc = 0;//次数
    private long numRigidityInOsLc = 0;//次数（阈值）

    //平均轧轧机刚度传动侧(压头) = 总数/次数
    private BigDecimal avgRigidityDsLc;
    private BigDecimal sumRigidityDsLc;//总数
    private long numRigidityDsLc = 0;//次数
    private long numRigidityInDsLc = 0;//次数（阈值）

    //平均轧轧机刚度操作侧(传感器) = 总数/次数
    private BigDecimal avgRigidityOsPt;
    private BigDecimal sumRigidityOsPt;//总数
    private long numRigidityOsPt = 0;//次数
    private long numRigidityInOsPt = 0;//次数（阈值）

    //平均轧轧机刚度传动侧(传感器) = 总数/次数
    private BigDecimal avgRigidityDsPt;
    private BigDecimal sumRigidityDsPt;//总数
    private long numRigidityDsPt = 0;//次数
    private long numRigidityInDsPt = 0;//次数（阈值）

    //平均操作侧入口磁尺 = 总数/次数
    private BigDecimal avgMagInOs;
    private BigDecimal sumMagInOs;//总数
    private long numMagInOs = 0;//次数

    //平均传动侧入口磁尺 = 总数/次数
    private BigDecimal avgMagInDs;
    private BigDecimal sumMagInDs;//总数
    private long numMagInDs = 0;//次数

    //平均操作侧出口磁尺 = 总数/次数
    private BigDecimal avgMagOutOs;
    private BigDecimal sumMagOutOs;//总数
    private long numMagOutOs = 0;//次数

    //平均传动侧出口磁尺 = 总数/次数
    private BigDecimal avgMagOutDs;
    private BigDecimal sumMagOutDs;//总数
    private long numMagOutDs = 0;//次数

    //对向刚度（压头）
    private BigDecimal rigidityOpPt;
    //对向刚度（传感器）
    private BigDecimal rigidityOpLc;
   /* //同向刚度（操作侧）
    private BigDecimal rigidityOpOs;
    //同向刚度（传动侧侧）
    private BigDecimal rigidityOpDs;*/

    //压头操作侧刚度保持率
    private BigDecimal keepOsPt;
    //压头传动侧侧刚度保持率
    private BigDecimal keepDsPt;
    //传感器刚度保持率
    private BigDecimal keepOsLc;
    //传感器刚度保持率
    private BigDecimal keepDsLc;

    //零点位置
    private BigDecimal posZero;
    //入口零点差
    private BigDecimal inZeroDiff;
    //出口零点差
    private BigDecimal outZeroDiff;


    //轧机刚度测试，零位标定信号开始、结束（0.结束，1.开始）
    private Integer state;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    /**
     * 累加轧制刚度操作侧(压头)
     * @param value
     */
    public void toAccumSumRigidityOsPt(BigDecimal value){
        setSumRigidityOsPt(getSumRigidityOsPt().add(value));
    }
    public void setSumRigidityOsPt(BigDecimal sum) {
        this.sumRigidityOsPt = sum;
        this.numRigidityOsPt ++;
    }
    public void toAccRigidityInOsPt() {
        this.numRigidityInOsPt ++;
    }
    public BigDecimal getKeepOsPt() {
        if(keepOsPt == null) {
            if (numRigidityOsPt > 0 && numRigidityInOsPt > 0) {
                return BigDecimal.valueOf(numRigidityInOsPt).divide(BigDecimal.valueOf(numRigidityOsPt))
                        .setScale(3, BigDecimal.ROUND_HALF_DOWN);
            }
        }
       return keepOsPt;
    }
    public BigDecimal getAvgRigidityOsPt() {
        if(avgRigidityOsPt == null){
            if(numRigidityOsPt > 0 && sumRigidityOsPt != null){
                return sumRigidityOsPt.divide(BigDecimal.valueOf(numRigidityOsPt)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgRigidityOsPt;
    }

    /**
     * 累加轧机刚度传动侧(压头)
     * @param value
     */
    public void toAccumSumRigidityDsPt(BigDecimal value){
        setSumRigidityDsPt(getSumRigidityDsPt().add(value));
    }
    public void setSumRigidityDsPt(BigDecimal sum) {
        this.sumRigidityDsPt = sum;
        this.numRigidityDsPt ++;
    }
    public void toAccRigidityInDsPt() {
        this.numRigidityInDsPt ++;
    }
    public BigDecimal getAvgRigidityDsPt() {
        if(avgRigidityDsPt == null){
            if(numRigidityDsPt > 0 && sumRigidityDsPt != null){
                return sumRigidityDsPt.divide(BigDecimal.valueOf(numRigidityDsPt)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgRigidityDsPt;
    }
    public BigDecimal getKeepDsPt() {
        if(keepDsPt == null) {
            if (numRigidityDsPt > 0 && numRigidityInDsPt > 0) {
                return BigDecimal.valueOf(numRigidityInDsPt).divide(BigDecimal.valueOf(numRigidityDsPt))
                        .setScale(3, BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return keepDsPt;
    }
    /**
     * 累加轧机刚度操作侧(传感器)
     * @param value
     */
    public void toAccumRigidityOsLc(BigDecimal value){
        setSumRigidityOsLc(getSumRigidityOsLc().add(value));
    }
    public void setSumRigidityOsLc(BigDecimal sum) {
        this.sumRigidityOsLc = sum;
        this.numRigidityOsLc ++;
    }
    public void toAccRigidityInOsLc() {
        this.numRigidityInOsLc ++;
    }
    public BigDecimal getAvgRigidityOsLc() {
        if(avgRigidityOsLc == null){
            if(numRigidityOsLc > 0 && sumRigidityOsLc != null){
                return sumRigidityOsLc.divide(BigDecimal.valueOf(numRigidityOsLc)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgRigidityOsLc;
    }
    public BigDecimal getKeepOsLc() {
        if(keepOsLc == null) {
            if (numRigidityOsLc > 0 && numRigidityInOsLc > 0) {
                return BigDecimal.valueOf(numRigidityInOsLc).divide(BigDecimal.valueOf(numRigidityOsLc))
                        .setScale(3, BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return keepOsLc;
    }
    /**
     * 累加轧机刚度传动侧(传感器)
     * @param value
     */
    public void toAccumRigidityDsLc(BigDecimal value){
        setSumRigidityDsLc(getSumRigidityDsLc().add(value));
    }
    public void setSumRigidityDsLc(BigDecimal sum) {
        this.sumRigidityDsLc = sum;
        this.numRigidityDsLc ++;
    }
    public void toAccRigidityInDsLc() {
        this.numRigidityInDsLc ++;
    }
    public BigDecimal getAvgRigidityDsLc() {
        if(avgRigidityDsLc == null){
            if(numRigidityDsLc > 0 && sumRigidityDsLc != null){
                return sumRigidityDsLc.divide(BigDecimal.valueOf(numRigidityDsLc)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgRigidityDsLc;
    }
    public BigDecimal getKeepDsLc() {
        if(keepOsLc == null) {
            if (numRigidityDsLc > 0 && numRigidityInDsLc > 0) {
                return BigDecimal.valueOf(numRigidityInDsLc).divide(BigDecimal.valueOf(numRigidityDsLc))
                        .setScale(3, BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return keepDsLc;
    }
   /**
     * 累加操作侧入口磁尺
     * @param value
     */
    public void toAccumMagInOs(BigDecimal value){
        setSumMagInOs(getSumMagInOs().add(value));
    }
    public void setSumMagInOs(BigDecimal sum) {
        this.sumMagInOs = sum;
        this.numMagInOs ++;
    }

    public BigDecimal getAvgMagInOs() {
        if(avgMagInOs == null){
            if(numMagInOs > 0 && sumMagInOs != null){
                return sumMagInOs.divide(BigDecimal.valueOf(numMagInOs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgMagInOs;
    }
    /**
     * 累加操作侧出口磁尺
     * @param value
     */
    public void toAccumMagOutOs(BigDecimal value){
        setSumMagOutOs(getSumMagOutOs().add(value));
    }
    public void setSumMagOutOs(BigDecimal sum) {
        this.sumMagOutOs = sum;
        this.numMagOutOs ++;
    }

    public BigDecimal getAvgMagOutOs() {
        if(avgMagOutOs == null){
            if(numMagOutOs > 0 && sumMagOutOs != null){
                return sumMagOutOs.divide(BigDecimal.valueOf(numMagOutOs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgMagInOs;
    }
    /**
     * 累加传动侧入口磁尺
     * @param value
     */
    public void toAccumMagInDs(BigDecimal value){
        setSumMagInDs(getSumMagInDs().add(value));
    }
    public void setSumMagInDs(BigDecimal sum) {
        this.sumMagInDs = sum;
        this.numMagInDs ++;
    }

    public BigDecimal getAvgMagInDs() {
        if(avgMagInDs == null){
            if(numMagInDs > 0 && sumMagInDs != null){
                return sumMagInDs.divide(BigDecimal.valueOf(numMagInDs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgMagInOs;
    }
    /**
     * 累加传动侧出口磁尺
     * @param value
     */
    public void toAccumMagOutDs(BigDecimal value){
        setSumMagOutDs(getSumMagOutDs().add(value));
    }
    public void setSumMagOutDs(BigDecimal sum) {
        this.sumMagOutDs = sum;
        this.numMagOutDs ++;
    }

    public BigDecimal getAvgMagOutDs() {
        if(avgMagOutDs == null){
            if(numMagOutDs > 0 && sumMagOutDs != null){
                return sumMagOutDs.divide(BigDecimal.valueOf(numMagOutDs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgMagOutOs;
    }
    /**
     * 入口零点差
     *
     */
    public BigDecimal getInZeroDiff() {
        if(avgMagInOs != null && avgMagInDs != null ){
            return avgMagInOs.subtract(avgMagInDs);
        }
        return inZeroDiff;
    }
    /**
     * 出口零点差
     *
     */
    public BigDecimal getOutZeroDiff() {
        if(avgMagOutOs != null && avgMagOutDs != null ){
            return avgMagOutOs.subtract(avgMagOutDs);
        }
        return inZeroDiff;
    }
    /**
     * 对向刚度（压头）操作侧 - 传动侧
     *
     */
    public BigDecimal getOutRigidityOpLc(){
        if(rigidityOpLc == null){
            if(avgRigidityOsLc != null && avgRigidityDsLc != null ){
                return avgRigidityOsLc.subtract(avgRigidityDsLc);
            }
        }
        return rigidityOpLc;
    }

    /**
     * 对向刚度（传感器）操作侧 - 传动侧
     *
     */
    public BigDecimal getOutRigidityOpPt(){
        if(rigidityOpPt == null){
            if(avgRigidityOsPt != null && avgRigidityDsPt != null ){
                return avgRigidityOsPt.subtract(avgRigidityDsPt);
            }
        }
        return rigidityOpPt;
    }
}
