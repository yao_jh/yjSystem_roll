package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 弯辊动作均值趋势视图
 */
@Data
public class BendingTrendVo {

    //机组ID
    private Long crewId;

    //平均弯辊力操作侧OS = 总数/次数
    private BigDecimal avgBendingOs;
    private BigDecimal sumBendingOs;//总数
    private long numBendingOs = 0;//次数

    //平均弯辊力传动侧DS = 总数/次数
    private BigDecimal avgBendingDs;
    private BigDecimal sumBendingDs;//总数
    private long numBendingDs = 0;//次数

    //平均总弯辊力
    private BigDecimal avgBendingTotal;
    private BigDecimal sumBendingTotal;//总数
    private long numBendingTotal = 0;//次数

    //最大总弯辊力
    private BigDecimal maxBendingTotal;
    //最小总弯辊力
    private BigDecimal minBendingTotal;

    //平均弯辊力偏差（OS-DS）
    private BigDecimal avgBendingDiff;

    //最大弯辊力波动辐值（操作侧）
    private BigDecimal maxChangeValueOs;
    //最大弯辊力波动辐值（传动侧）
    private BigDecimal maxChangeValueDs;

    //轧制时间
    private Long rollTime;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;
    //状态 咬钢抛钢
    private Integer state;

    //上一个操作侧弯轨力值
    private BigDecimal lastAvgBendingOs;
    //上一个传动侧弯轨力值
    private BigDecimal lastAvgBendingDs;
    /**
     * 累加弯辊力操作侧OS
     * @param value
     */
    public void toAccumSumBendingOs(BigDecimal value){
        setSumBendingOs(getSumBendingOs().add(value));
    }
    public void setSumBendingOs(BigDecimal sum) {
        this.sumBendingOs = sum;
        this.numBendingOs ++;
    }

    public BigDecimal getAvgForceOs() {
        if(avgBendingOs == null){
            if(numBendingOs > 0 && sumBendingOs != null){
                return sumBendingOs.divide(BigDecimal.valueOf(numBendingOs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgBendingOs;
    }
    /**
     * 累加弯辊力传动侧DS
     * @param value
     */
    public void toAccumSumBendingDs(BigDecimal value){

        setSumBendingDs(getSumBendingDs().add(value));
    }
    public void setSumBendingDs(BigDecimal sum) {
        this.sumBendingDs = sum;
        this.numBendingDs ++;
    }

    public BigDecimal getAvgBendingDs() {
        if(avgBendingDs == null){
            if(numBendingDs > 0 && sumBendingDs != null){
                return sumBendingDs.divide(BigDecimal.valueOf(numBendingDs)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgBendingDs;
    }

    /**
     * 累加总轧制力操作侧TOTAL
     * @param value
     */
    public void toAccumBendingTotal(BigDecimal value){
        setSumBendingTotal(getSumBendingTotal().add(value));
    }
    public void setSumBendingTotal(BigDecimal sum) {
        this.sumBendingTotal = sum;
        this.numBendingTotal ++;
    }

    public BigDecimal getAvgBendingTotal() {
        if(avgBendingTotal == null){
            if(numBendingTotal > 0 && sumBendingTotal != null){
                return sumBendingTotal.divide(BigDecimal.valueOf(numBendingTotal)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgBendingTotal;
    }
    /**
     * 平均轧制力差值
     */
    private BigDecimal getAvgBendingDiff(){
        if(avgBendingDiff == null ) {
            if (avgBendingOs != null && avgBendingDs != null) {
                return avgBendingOs.subtract(avgBendingDs);
            }
        }
        return avgBendingDiff;
    }

    public BendingTrendVo(){
        super();
    }

    public BendingTrendVo(Long crewId){
        this.crewId = crewId;
    }


}
