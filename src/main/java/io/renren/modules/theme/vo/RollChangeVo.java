package io.renren.modules.theme.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 *   换辊和卸卷
 */
@Data
public class RollChangeVo {
    private Long id;
    //设备ID
    private Long crewId;
    //记录时间
    private Date date;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;

    private Long duration;

    private List<RollChangeLogVo> rollChangeLogVoList;

    public Long getDuration() {
        return startDate.getTime() - endDate.getTime();
    }
}
