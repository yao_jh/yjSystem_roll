package io.renren.modules.theme.vo;

import lombok.Data;

import java.util.Date;

/**
 * 换辊记录
 */
@Data
public class RollChangeLogVo {
    //设备ID
    private Long pid;
    //记录时间
    private Date date;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //进行步骤
    private String classCode;
    //动作时长

    private Long duration;

    public Long getDuration() {
        return startDate.getTime() - endDate.getTime();
    }
}
