package io.renren.modules.theme.vo;

import io.renren.common.sys.MainCatche;
import io.renren.modules.event.entity.EventLogEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.warning.entity.WarningLogEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 张力辊单调区间趋势视图
 */
@Data
public class TensionTrendVo {

    //设备Code
    private String deviceCode;
    //上一个数
    private BigDecimal lastSpeed;
    //起始速度
    private BigDecimal minSpeed;
    //结束速度
    private BigDecimal maxSpeed;
    //起始转矩
    private BigDecimal minTorque;
    //结束转矩
    private BigDecimal maxTorque;
    //0:上升;1:下降
    private Integer state;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;

    /**
     *  创建张力辊电机的打滑报警
     */
    public EventLogEntity createEventLog(){
        //打滑报警事件
        EventLogEntity log = null;
        //验证设备是否存在
        DeviceEntity device = MainCatche.deviceMap.get(deviceCode);
        if(device != null){
            //打滑报警事件
            log = new EventLogEntity();
            log.setCrewId(device.getCrewId());
            log.setDeviceId(device.getDeviceId());
            log.setStartDate(startDate);
            log.setEndDate(endDate);
            log.setLevel(2);
            log.setState(1);
            log.setInfo("电机打滑");
        }
        return log;
    }


}
