package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 焊机焊接视图
 */
@Data
public class WelderVo {

    //设备ID
    private Long deviceId;
    //焊接次数
    private Long total;
    //记录时间
    private Date date;
    //上一个数
    private BigDecimal value;

}
