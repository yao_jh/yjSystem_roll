package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 *热轧卸卷
 */
@Data
public class HotRollingDecoilerVo {
//class_code
 private String type;

//dc1动作时间
 private BigDecimal durationDcFirst;


 //dc1 偏差时间
 private BigDecimal deviationDcFirst;

 //dc2动作时间
 private BigDecimal durationDcSecond;


 //dc2 偏差时间
 private BigDecimal deviationDcSecond;


 //dc3 动作时间
 private BigDecimal durationDcThree;


 //dc3 偏差时间
 private BigDecimal deviationDcThree;

// 平均时长
 private BigDecimal avgDuration;

//最小时长
 private BigDecimal minDuration;

//比例
 private BigDecimal radio;

















}
