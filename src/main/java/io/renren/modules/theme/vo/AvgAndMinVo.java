package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 平均值和最小值和class_code
 */
@Data
public class AvgAndMinVo {
    //class_code
    private String type;

    //平均时间
    private BigDecimal avgDuration;

    //最小时间

    private BigDecimal minDuration;





}
