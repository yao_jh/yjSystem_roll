package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 切边剪均值趋势视图
 */
@Data
public class TrimmingVo {

    //设备ID
    private Long deviceId;
    //1#切边剪剪切次数
    private Long firstCount;
    //2#切边剪剪切次数
    private Long secondCount;
    //3#切边剪剪切次数
    private Long thirdCount;
    //4#切边剪剪切次数
    private Long fourthCount;
    //1#切边剪上一个数
    private BigDecimal firstValue;
    //2#切边剪上一个数
    private BigDecimal secondValue;
    //3#切边剪上一个数
    private BigDecimal thirdValue;
    //4#切边剪上一个数
    private BigDecimal fourthValue;
    //1#记录时间
    private Date firstDate;
    //2#记录时间
    private Date secondDate;
    //3#记录时间
    private Date thirdDate;
    //4#记录时间
    private Date fourthDate;



}
