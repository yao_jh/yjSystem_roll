package io.renren.modules.theme.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 平均值和最小值和class_code
 */
@Data
public class NewChangeLogVo {

    //class_code
    private String type;

    //时间
    private BigDecimal duration;

}
