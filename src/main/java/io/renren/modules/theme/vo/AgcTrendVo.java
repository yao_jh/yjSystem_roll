package io.renren.modules.theme.vo;

import io.renren.common.utils.DateUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * AGC均值趋势视图
 */
@Data
public class AgcTrendVo {

    //设备ID
    private Long deviceId;

    //动作速度
    private BigDecimal speed;
    //动作时间
    private BigDecimal duration;
    //结束辊缝
    private BigDecimal endGap;
    //入口磁尺
    private BigDecimal magneEn;
    //出口磁尺
    private BigDecimal magneEx;
    //设定和反馈平均偏差
    private BigDecimal avgDiff;

    //分类 0:行程2-4;1:行程4-10;2:行程10-20;3:行程>=20;
    private Integer type;
    //边侧 0:操作侧;1:驱动侧
    private Integer side;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    //行程
    private BigDecimal distance;
    //最大速度
    private BigDecimal minValue;
    //最小速度
    private BigDecimal maxValue;
    //初始量
    private BigDecimal initValue;
    //上一个值
    private BigDecimal lastValue;
    //0:上升;1:下降
    private Integer state;

    private BigDecimal sumDiff;
    private long numDiff = 0L;

    public BigDecimal getSpeed() {
        //时间毫秒
        Long milis = getDuration();
        if(milis.compareTo(0L) != 0){
            //移动距离 = 最大位移-最小位移
            BigDecimal value = getTrip();
            //速度 = 移动距离/时间
            speed = value.divide(BigDecimal.valueOf(milis)).setScale(3, BigDecimal.ROUND_HALF_DOWN);
        }
        return speed;
    }

    //移动距离 = 最大位移-最小位移
    public BigDecimal getTrip() {
        distance = maxValue.subtract(minValue);
        return distance;
    }

    public Long getDuration() {
        return DateUtils.getDateDiffForMilis(startDate, endDate);
    }

    //0:行程2-4; 1:行程4-10; 2:行程10-20; 3:行程>=20;
    public Integer getType() {
        //移动距离 = 最大位移-最小位移
        BigDecimal value = getTrip();
        if(value.compareTo(BigDecimal.valueOf(2)) >= 0){
            if(value.compareTo(BigDecimal.valueOf(4)) < 0){
                type = 0;
            }else if(value.compareTo(BigDecimal.valueOf(10)) < 0){
                type = 1;
            }else if(value.compareTo(BigDecimal.valueOf(20)) < 0){
                type = 2;
            }else {
                type = 3;
            }
        }
        return type;
    }

    //累加偏差
    public BigDecimal toAccumSumDiff(BigDecimal set, BigDecimal act){
        if(set == null || act == null) return getSumDiff();
        if(set.compareTo(BigDecimal.ZERO) == 0 || act.compareTo(BigDecimal.ZERO) == 0) return getSumDiff();
        //偏差 = 实际-设定
        BigDecimal value = act.subtract(set);
        //設置
        setSumDiff(getSumDiff().add(value));
        return getSumDiff();
    }

    //设置偏差
    public void setSumDiff(BigDecimal sumDiff) {
        this.sumDiff = sumDiff;
        this.numDiff ++;
    }

    //获取偏差均值
    //均值=总和/次数
    public BigDecimal getAvgDiff() {
        if(numDiff > 0 && sumDiff != null){
            return sumDiff.divide(BigDecimal.valueOf(numDiff)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgDiff;
    }

}
