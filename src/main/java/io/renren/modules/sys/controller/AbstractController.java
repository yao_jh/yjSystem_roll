/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 *
 * @author Mark sunlightcs@gmail.com
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected SysUserEntity getUser() {
		Subject sub = SecurityUtils.getSubject();
		SysUserEntity user = (SysUserEntity) sub.getPrincipal();
		return user;
	}

	protected Long getUserId() {
		SysUserEntity userEntity = getUser();
		if(userEntity == null){
			return 1L;
		}else {
			return userEntity.getUserId();
		}
	}
}
