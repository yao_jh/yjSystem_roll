package io.renren.modules.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.iot.exception.BadAuthorizedException;
import io.renren.common.sys.Topic;
import io.renren.common.utils.R;
import io.renren.modules.sys.dao.SysUserTokenDao;
import io.renren.modules.sys.entity.SysUserTokenEntity;
import io.renren.modules.sys.form.SysLoginForm;
import io.renren.modules.sys.oauth2.TokenGenerator;
import io.renren.modules.sys.service.SysUserTokenService;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenDao, SysUserTokenEntity> implements SysUserTokenService {

	//12小时后过期
	private final static int EXPIRE = 3600 * 12;

	@Override
	public R createToken(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();
		//保存Token信息
		saveOrUpdateToken(userId, token);

		R r = R.ok().put("token", token).put("expire", EXPIRE);
		return r;
	}

	@Override
	public R createTokenFromIot(SysLoginForm form) {
		//生成一个token
		String token = null;
		//格式化请求参数
		JSONObject param = new JSONObject();
		param.put("username",form.getUsername());
		param.put("password",form.getDecPwd());

		//请求地址
		String address = "/iot-basis/api/auth/userLogin";
		try{
			JSONObject jsonObject = Topic.getIotRestRequest(param, address);
			if(jsonObject != null && "success".equals(jsonObject.getString("code"))){
				token = jsonObject.getString("data");
			}
		} catch (BadAuthorizedException e) {
			e.printStackTrace();
		}

		//保存Token信息
		saveOrUpdateToken(form.getUserId(), token);

		R r = R.ok().put("token", token).put("expire", EXPIRE);
		return r;
	}

	/**
	 * 更新/保存Token信息
	 * @param userId
	 * @param token
	 */
	private void saveOrUpdateToken(long userId, String token){
		//当前时间
		Date now = new Date();
		//过期时间
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//判断是否生成过token
		SysUserTokenEntity tokenEntity = this.getById(userId);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//保存token
			this.save(tokenEntity);
		}else{
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			//更新token
			this.updateById(tokenEntity);
		}
	}

	@Override
	public void logout(long userId) {
		//生成一个token
		String token = TokenGenerator.generateValue();

		//修改token
		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}

}
