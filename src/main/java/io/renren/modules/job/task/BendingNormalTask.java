package io.renren.modules.job.task;

import io.renren.modules.normal.service.BendingNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成窜辊正态分析
 * 周一0点执行一次
 * bendingNormalTask 为spring bean的名称
 *
 */
@Component("bendingNormalTask")
public class BendingNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private BendingNormalService bendingNormalService;

	@Override
	public void run(String params){
		bendingNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
