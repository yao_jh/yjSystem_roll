package io.renren.modules.job.task;

import io.renren.modules.normal.service.ShiftingNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成窜辊正态分析
 * 周一0点执行一次
 * shiftingNormalTask 为spring bean的名称
 *
 */
@Component("shiftingNormalTask")
public class ShiftingNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShiftingNormalService shiftingNormalService;

	@Override
	public void run(String params){
		shiftingNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
