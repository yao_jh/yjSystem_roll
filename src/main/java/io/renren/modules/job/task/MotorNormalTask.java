package io.renren.modules.job.task;

import io.renren.modules.normal.service.MotorNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成电机正态分析
 * 周一0点执行一次
 * motorNormalWeekTask 为spring bean的名称
 *
 */
@Component("motorNormalTask")
public class MotorNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MotorNormalService motorNormalService;

	@Override
	public void run(String params){
		motorNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
