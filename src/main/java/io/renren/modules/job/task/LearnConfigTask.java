package io.renren.modules.job.task;

import io.renren.modules.learn.entity.LearnConfigEntity;
import io.renren.modules.learn.service.LearnConfigService;
import io.renren.modules.learn.utils.LearnShuntUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 更新自学习时器
 * 1天执行一次
 * learnConfigTask 为spring bean的名称
 *
 */
@Component("learnConfigTask")
public class LearnConfigTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private LearnConfigService learnTypeConfigService;

	@Autowired
	private LearnConfigService learnConfigService;

	@Override
	public void run(String params){
		//保存配置学习值
		saveLearnForValue();
		//更新状态
		updateTypeState();
		updateConfigState();
		//重新载入学习配置
		initLearnConfig();
		//logger.debug("更新自学习定时任务正在执行");
	}

	/**
	 * 保存配置学习值
	 */
	private void saveLearnForValue(){
		Map<String, LearnConfigEntity> learnMap = LearnShuntUtils.learnConfigMap;
		for(LearnConfigEntity learn : learnMap.values()){
			learnConfigService.updateById(learn);
		}
	}

	/**
	 * 更新类型配置状态
	 */
	private void updateTypeState(){
		//同步学习中
		learnTypeConfigService.updateStateForRun();
		//同步完成
		learnTypeConfigService.updateStateForFinish();
		//同步过期
		learnTypeConfigService.updateStateForOverdue();
	}

	/**
	 * 更新逐条配置状态
	 * 每天执行一次更新计划状态
	 */
	private void updateConfigState(){
		//同步学习中
		learnConfigService.updateStateForRun();
		//同步完成
		learnConfigService.updateStateForFinish();
		//同步过期
		learnConfigService.updateStateForOverdue();
	}

	/**
	 * 重新载入学习配置
	 */
	private void initLearnConfig(){
		learnConfigService.initLearning();
	}

}
