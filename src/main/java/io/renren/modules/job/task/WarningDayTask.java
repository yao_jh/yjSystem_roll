package io.renren.modules.job.task;

import io.renren.modules.warning.service.WarningLogDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 报警日统计表
 * 0点执行一次
 * warningTask 为spring bean的名称
 *
 */
@Component("warningDayTask")
public class WarningDayTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private WarningLogDayService warningLogDayService;

	@Override
	public void run(String params){
		warningLogDayService.saveTotalDayLog();
	}
}
