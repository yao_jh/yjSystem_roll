package io.renren.modules.job.task;

import io.renren.modules.normal.service.AgcNormalService;
import io.renren.modules.normal.service.ServoValveNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成AGC正态分析
 * 周一0点执行一次
 * agcNormalTask 为spring bean的名称
 *
 */
@Component("agcNormalTask")
public class AgcNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private AgcNormalService agcNormalService;

	@Override
	public void run(String params){
		agcNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
