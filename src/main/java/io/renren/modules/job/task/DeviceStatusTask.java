package io.renren.modules.job.task;

import io.renren.common.sys.MainCatche;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.service.DeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 更新设备状态定时器
 * 30分钟执行一次
 * deviceStatusTask 为spring bean的名称
 *
 */
@Component("deviceStatusTask")
public class DeviceStatusTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private DeviceService deviceService;

	@Override
	public void run(String params){
		Map<String, DeviceEntity> deviceMap = MainCatche.deviceMap;
		for(DeviceEntity device : deviceMap.values()){
			if(device.getToUpdate()){
				deviceService.update(device);
				device.setToUpdate(false);
			}
		}
		//logger.debug("更新设备状态定时任务正在执行");
	}
}
