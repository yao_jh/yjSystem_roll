package io.renren.modules.job.task;

import io.renren.modules.warning.utils.WarningUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 更新报警定时器
 * 1分钟执行一次
 * warningTask 为spring bean的名称
 *
 */
@Component("warningTask")
public class WarningTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(String params){
		WarningUtils.cleanOverWarningMap();
		WarningUtils.checkDurationWarning();
		//logger.debug("更新报警定时任务正在执行");
	}
}
