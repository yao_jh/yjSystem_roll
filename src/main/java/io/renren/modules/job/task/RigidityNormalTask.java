package io.renren.modules.job.task;

import io.renren.modules.normal.service.RigidityNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成轧机刚度正态分析
 * 周一0点执行一次
 * rigidityNormalTask 为spring bean的名称
 *
 */
@Component("rigidityNormalTask")
public class RigidityNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private RigidityNormalService rigidityNormalService;

	@Override
	public void run(String params){
		rigidityNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
