package io.renren.modules.job.task;

import io.renren.modules.normal.service.ServoValveNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成伺服阀正态分析
 * 周一0点执行一次
 * servoValveNormalTask 为spring bean的名称
 *
 */
@Component("servoValveNormalTask")
public class ServoValveNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ServoValveNormalService servoValveNormalService;

	@Override
	public void run(String params){
		servoValveNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
