package io.renren.modules.job.task;

import io.renren.modules.normal.service.ForceNormalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 按周生成轧机正态分析
 * 周一0点执行一次
 * motorNormalWeekTask 为spring bean的名称
 *
 */
@Component("forceNormalTask")
public class ForceNormalTask implements ITask {
	//private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ForceNormalService forceNormalService;

	@Override
	public void run(String params){
		forceNormalService.saveWeekNormalDis();
		//logger.debug("创建正态分析样本定时任务");
	}

}
