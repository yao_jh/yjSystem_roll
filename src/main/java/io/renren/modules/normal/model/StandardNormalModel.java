package io.renren.modules.normal.model;

import io.renren.common.utils.Constant;
import lombok.Data;

import java.math.BigDecimal;
import java.util.*;

/**
 * 正态分布模型
 */
@Data
public class StandardNormalModel {

    //平均值
    private BigDecimal mean;
    //标准差
    private BigDecimal variance;

    //阈值区域
    private BigDecimal area1 = BigDecimal.valueOf(1.96);
    private BigDecimal area2 = BigDecimal.valueOf(2.58);

    // 输入样本
    private List<BigDecimal> dataList;

    //求均值=数据值x的总和/数据个数
    public BigDecimal initMean (){
        if(dataList == null) return null;
        //总个数
        int count = dataList.size();
        //总和
        BigDecimal sum = BigDecimal.ZERO;

        for(BigDecimal value : dataList){
            sum = sum.add(value);
        }
        mean = sum.divide(BigDecimal.valueOf(count),3, BigDecimal.ROUND_HALF_UP);
        return mean;
    }

    //求标准差variance=sqrt([(x1-mean)^2 +...(xn-mean)^2]/n)
    public BigDecimal initVariance(){
        if(dataList == null) return null;
        //标准差之和
        BigDecimal varianceRaw = BigDecimal.ZERO;
        //差值
        BigDecimal diff = null;
        for(BigDecimal val : dataList){
            diff = val.subtract(mean);
            varianceRaw = varianceRaw.add(diff.multiply(diff));
        }

        variance = varianceRaw.divide(BigDecimal.valueOf(dataList.size()),3, BigDecimal.ROUND_HALF_UP);
        return variance;
    }

    //求标准差variance=sqrt([(x1-mean)^2 +...(xn-mean)^2]/n)
    public BigDecimal initVariance(String datas){
        if(datas == null) return null;
        //标准差之和
        BigDecimal varianceRaw = BigDecimal.ZERO;
        //差值
        BigDecimal diff = null;
        String[] ds = datas.split(",");

        for(String val : ds){
            diff = new BigDecimal(val).subtract(mean);
            varianceRaw = varianceRaw.add(diff.multiply(diff));
        }

        variance = varianceRaw.divide(BigDecimal.valueOf(ds.length),3, BigDecimal.ROUND_HALF_UP);
        return variance;
    }

    /**
     * 检测报警等級
     * @param value
     * @return
     */
    public Integer checkWarningLevel(BigDecimal value){
        //判断是否是3级报警
        boolean flag = checkWarningForLevel3(value);
        if(flag){
            return 3;
        }
        //判断是否是2级报警
        flag = checkWarningForLevel2(value);
        if(flag){
            return 2;
        }
        //判断是否是1级报警
        flag = checkWarningForLevel1(value);
        if(flag){
            return 1;
        }
        return 0;
    }

    /**
     * 检测是否属于一级报警
     * @param value
     * @return
     */
    private boolean checkWarningForLevel1(BigDecimal value) {
        boolean flag = false;
        //μ-1.96σ
        BigDecimal downMinLimit = mean.subtract(variance.multiply(area1));
        //μ-σ
        BigDecimal downMaxLimit = mean.subtract(variance);

        //μ+σ
        BigDecimal upMinLimit = mean.add(variance);
        //μ+1.96σ
        BigDecimal upMaxLimit = mean.add(variance.multiply(area1));

        //判断是否落在下限区域
        if(value.compareTo(downMinLimit) >= 0 && value.compareTo(downMaxLimit) <= 0){
            flag = true;
        }
        //判断是否落在上限区域
        if(value.compareTo(upMinLimit) >= 0 && value.compareTo(upMaxLimit) <= 0){
            flag = true;
        }

        return flag;
    }

    /**
     * 检测是否属于二级报警
     * @param value
     * @return
     */
    private boolean checkWarningForLevel2(BigDecimal value) {
        boolean flag = false;
        //μ-2.58σ
        BigDecimal downMinLimit = mean.subtract(variance.multiply(area2));
        //μ-1.96σ
        BigDecimal downMaxLimit = mean.subtract(variance.multiply(area1));

        //μ+1.96σ
        BigDecimal upMinLimit = mean.add(variance.multiply(area1));
        //μ+2.58σ
        BigDecimal upMaxLimit = mean.add(variance.multiply(area2));

        //判断是否落在下限区域
        if(value.compareTo(downMinLimit) >= 0 && value.compareTo(downMaxLimit) <= 0){
            flag = true;
        }
        //判断是否落在上限区域
        if(value.compareTo(upMinLimit) >= 0 && value.compareTo(upMaxLimit) <= 0){
            flag = true;
        }

        return flag;
    }

    /**
     * 检测是否属于三级报警
     * @param value
     * @return
     */
    private boolean checkWarningForLevel3(BigDecimal value) {
        boolean flag = false;
        //μ-2.58σ
        BigDecimal downMaxLimit = mean.subtract(variance.multiply(area2));

        //μ+2.58σ
        BigDecimal upMinLimit = mean.add(variance.multiply(area2));

        //判断是否落在下限区域
        if(value.compareTo(downMaxLimit) <= 0){
            flag = true;
        }
        //判断是否落在上限区域
        if(value.compareTo(upMinLimit) >= 0 ){
            flag = true;
        }

        return flag;
    }

    /**
     * 返回正太分布的阈值线
     * @return
     */
    public List<BigDecimal> getWarningLines(){
        //报警直线
        //μ-2.58σ
        BigDecimal line1 = mean.subtract(variance.multiply(area2));
        //μ-1.96σ
        BigDecimal line2 = mean.subtract(variance.multiply(area1));
        //μ-σ
        BigDecimal line3 = mean.subtract(variance);
        //μ+σ
        BigDecimal line4 = mean.add(variance);
        //μ+1.96σ
        BigDecimal line5 = mean.add(variance.multiply(area1));
        //μ+2.58σ
        BigDecimal line6 = mean.add(variance.multiply(area2));

        //返回阈值线列表
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        list.add(line1);
        list.add(line2);
        list.add(line3);
        list.add(line4);
        list.add(line5);
        list.add(line6);

        return list;
    }

    //随机生成满足均值和方差的正态分布数值作为参考
    public List<Map> getDataSampleList() {
        //μ-3σ
        BigDecimal min = mean.subtract(variance.multiply(BigDecimal.valueOf(3)));
        //μ+3σ
        BigDecimal max = mean.add(variance.multiply(BigDecimal.valueOf(3)));
        BigDecimal step = max.subtract(min).divide(BigDecimal.valueOf(Constant.modeTimes - 1),3, BigDecimal.ROUND_HALF_DOWN);

        //生成的随机数存储到list中
        List<Map> list = new ArrayList<Map>();
        BigDecimal x = min;
        Map<String, BigDecimal> map = null;
        for(int i = 0; i < Constant.modeTimes ; i++){
            double sqrt2PI = Math.sqrt(2* Math.PI);
            //System.out.println(sqrt2PI);
            BigDecimal f1 = variance.multiply(BigDecimal.valueOf(sqrt2PI));

            BigDecimal var = (x.subtract(mean)).multiply(x.subtract(mean));
            BigDecimal vari = variance.multiply(variance).multiply(BigDecimal.valueOf(2));
            BigDecimal f2 = var.divide(vari,3, BigDecimal.ROUND_HALF_UP);

            BigDecimal y = BigDecimal.valueOf(1).divide(f1.multiply(BigDecimal.valueOf(Math.exp(f2.doubleValue()))),3, BigDecimal.ROUND_HALF_UP);

            map = new HashMap<String, BigDecimal>();
            map.put("x",x);
            map.put("y",y.setScale(3,BigDecimal.ROUND_HALF_UP));
            list.add(map);

            x = x.add(step);
        }

        return list;
    }

    public StandardNormalModel(){
        super();
    }

    public StandardNormalModel(List<BigDecimal> dataList){
        this.dataList = dataList;
        //求均值
        initMean();
        //求标准差
        initVariance();
    }

    public StandardNormalModel(BigDecimal mean, BigDecimal variance){
        this.mean = mean;
        this.variance = variance;
    }

    public StandardNormalModel(String datas, BigDecimal mean){
        this.mean = mean;
        //求标准差
        initVariance(datas);
    }

}





