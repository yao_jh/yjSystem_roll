
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.entity.ServoValveNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * 伺服阀正态分析接口
 *
 */
public interface ServoValveNormalService extends IService<ServoValveNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Date startDate, Date endDate);
}
