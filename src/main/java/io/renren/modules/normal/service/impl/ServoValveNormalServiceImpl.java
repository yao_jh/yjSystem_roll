
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.service.ServoValveAvgtrendService;
import io.renren.modules.normal.dao.ServoValveNormalDao;
import io.renren.modules.normal.entity.ServoValveNormalEntity;
import io.renren.modules.normal.service.ServoValveNormalService;
import io.renren.modules.normal.thread.ServoValveNormalThread;
import io.renren.modules.normal.utils.ServoValveNormalUtils;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.normal.vo.ServoValveSampleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * 伺服阀正态分析记录
 */
@Service("servoValveNormalService")
public class ServoValveNormalServiceImpl extends ServiceImpl<ServoValveNormalDao, ServoValveNormalEntity> implements ServoValveNormalService {

    @Autowired
    private ServoValveAvgtrendService servoValveAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long deviceId, Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(deviceId, startDate, endDate);
    }

    /**
     * 更新统计超限次数
     */
    private void updateNormalTotal(){
        for (ServoValveNormalEntity entity : ServoValveNormalUtils.servoNormalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    /**
     * 创建新的周期正态分析
     */
    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<ServoValveSampleVo> list = servoValveAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            ServoValveNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            ServoValveNormalThread thread = new ServoValveNormalThread(ServoValveNormalUtils.servoNormalMap);
            thread.start();
        }
    }

}
