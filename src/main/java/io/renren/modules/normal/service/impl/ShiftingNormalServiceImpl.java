
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.dao.ShiftingNormalDao;
import io.renren.modules.normal.entity.ShiftingNormalEntity;
import io.renren.modules.normal.service.ShiftingNormalService;
import io.renren.modules.normal.thread.ShiftingNormalThread;
import io.renren.modules.normal.utils.ShiftingNormalUtils;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.normal.vo.ShiftingSampleVo;
import io.renren.modules.theme.service.ShiftingAvgtrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * 窜辊动作正态分析记录
 */
@Service("shiftingNormalService")
public class ShiftingNormalServiceImpl extends ServiceImpl<ShiftingNormalDao, ShiftingNormalEntity> implements ShiftingNormalService {

    @Autowired
    private ShiftingAvgtrendService shiftingAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long deviceId, Integer side, Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(deviceId, side, startDate, endDate);
    }

    /**
     * 更新分析状态的模型报警次数
     *
     */
    private void updateNormalTotal(){
        for (ShiftingNormalEntity entity : ShiftingNormalUtils.shiftingNormalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    /**
     * 创建新的周期正态分析
     */
    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<ShiftingSampleVo> list = shiftingAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            ShiftingNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            ShiftingNormalThread thread = new ShiftingNormalThread();
            thread.start();
        }
    }

}
