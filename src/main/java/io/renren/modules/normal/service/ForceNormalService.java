
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * 轧机正态分析接口
 *
 */
public interface ForceNormalService extends IService<ForceNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param crewId
     * @param classCode
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long crewId, String classCode, Date startDate, Date endDate);
}
