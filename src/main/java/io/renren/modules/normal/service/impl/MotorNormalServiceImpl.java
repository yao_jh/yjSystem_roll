
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.feature.service.MotorAvgtrendService;
import io.renren.modules.normal.dao.MotorNormalDao;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.service.MotorNormalService;
import io.renren.modules.normal.thread.MotorNormalThread;
import io.renren.modules.normal.utils.MotorNormalUtils;
import io.renren.modules.normal.vo.MotorSampleVo;
import io.renren.modules.normal.vo.NormalVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * 电机正态分析记录
 */
@Service("motorNormalService")
public class MotorNormalServiceImpl extends ServiceImpl<MotorNormalDao, MotorNormalEntity> implements MotorNormalService {

    @Autowired
    private MotorAvgtrendService motorAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long deviceId, String classCode, Integer state, Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(deviceId, classCode, state, startDate, endDate);
    }

    /**
     * 更新统计超限次数
     */
    private void updateNormalTotal(){
        //遍历空载
        toUpdateForNormal(MotorNormalUtils.motorNoLoadNormalMap);
        //遍历有载
        toUpdateForNormal(MotorNormalUtils.motorHasLoadNormalMap);
    }

    /**
     * 更新分析状态的模型报警次数
     * @param motorNormalMap
     */
    private void toUpdateForNormal(Map<String, MotorNormalEntity> motorNormalMap){
        for (MotorNormalEntity entity : motorNormalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<MotorSampleVo> list = motorAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            MotorNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            MotorNormalThread thread = new MotorNormalThread();
            thread.start();
        }
    }

}
