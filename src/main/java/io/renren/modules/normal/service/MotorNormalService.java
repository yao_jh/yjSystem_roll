
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * 电机正态分析接口
 *
 */
public interface MotorNormalService extends IService<MotorNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param deviceId
     * @param classCode
     * @param state
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, String classCode, Integer state, Date startDate, Date endDate);
}
