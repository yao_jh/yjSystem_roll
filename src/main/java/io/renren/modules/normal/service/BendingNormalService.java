
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.BendingNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * 弯辊正态分析接口
 *
 */
public interface BendingNormalService extends IService<BendingNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param crewId
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long crewId, Date startDate, Date endDate);
}
