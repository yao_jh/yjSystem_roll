
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.AgcNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * AGC正态分析接口
 *
 */
public interface AgcNormalService extends IService<AgcNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);
}
