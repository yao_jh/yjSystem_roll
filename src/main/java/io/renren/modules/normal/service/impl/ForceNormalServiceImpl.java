
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.service.ForceAvgtrendService;
import io.renren.modules.normal.dao.ForceNormalDao;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.service.ForceNormalService;
import io.renren.modules.normal.thread.ForceNormalThread;
import io.renren.modules.normal.utils.ForceNormalUtils;
import io.renren.modules.normal.vo.ForceSampleVo;
import io.renren.modules.normal.vo.NormalVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * 电机正态分析记录
 */
@Service("forceNormalService")
public class ForceNormalServiceImpl extends ServiceImpl<ForceNormalDao, ForceNormalEntity> implements ForceNormalService {

    @Autowired
    private ForceAvgtrendService forceAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long crewId, String classCode, Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(crewId, classCode, startDate, endDate);
    }

    /**
     * 更新统计超限次数
     */
    private void updateNormalTotal(){
        for (ForceNormalEntity entity : ForceNormalUtils.forceNormalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<ForceSampleVo> list = forceAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            ForceNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            ForceNormalThread thread = new ForceNormalThread(ForceNormalUtils.forceNormalMap);
            thread.start();
        }
    }

}
