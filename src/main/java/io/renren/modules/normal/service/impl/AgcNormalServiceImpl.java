
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.dao.AgcNormalDao;
import io.renren.modules.normal.entity.AgcNormalEntity;
import io.renren.modules.normal.service.AgcNormalService;
import io.renren.modules.normal.thread.AgcNormalThread;
import io.renren.modules.normal.utils.AgcNormalUtils;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.theme.service.AgcAvgtrendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * AGC正态分析记录
 */
@Service("agcNormalService")
public class AgcNormalServiceImpl extends ServiceImpl<AgcNormalDao, AgcNormalEntity> implements AgcNormalService {

    @Autowired
    private AgcAvgtrendService agcAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(deviceId, type, side, startDate, endDate);
    }

    /**
     * 更新统计超限次数
     */
    private void updateNormalTotal(){
        // 行程2-4
        toUpdateForNormal(AgcNormalUtils.agcNormal2_4Map);
        // 行程4-10
        toUpdateForNormal(AgcNormalUtils.agcNormal4_10Map);
        // 行程10-20
        toUpdateForNormal(AgcNormalUtils.agcNormal10_20Map);
        // 行程>=20
        toUpdateForNormal(AgcNormalUtils.agcNormal20_nMap);
    }

    /**
     * 更新分析状态的模型报警次数
     * @param normalMap
     */
    private void toUpdateForNormal(Map<String, AgcNormalEntity> normalMap){
        for (AgcNormalEntity entity : normalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    /**
     * 创建新的周期正态分析
     */
    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<AgcSampleVo> list = agcAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            AgcNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            AgcNormalThread thread = new AgcNormalThread();
            thread.start();
        }
    }

}
