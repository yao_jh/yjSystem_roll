
package io.renren.modules.normal.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.dao.RigidityNormalDao;
import io.renren.modules.normal.entity.RigidityNormalEntity;
import io.renren.modules.normal.service.RigidityNormalService;
import io.renren.modules.normal.utils.RigidityNormalUtils;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.normal.vo.RigiditySampleVo;
import io.renren.modules.theme.service.RigidityAvgtrendService;
import io.renren.modules.theme.thread.RigidityThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * 轧机刚度正态分析记录
 */
@Service("rigidityNormalService")
public class RigidityNormalServiceImpl extends ServiceImpl<RigidityNormalDao, RigidityNormalEntity> implements RigidityNormalService {

    @Autowired
    private RigidityAvgtrendService rigidityAvgtrendService;

    @Override
    public void saveWeekNormalDis() {
        //保存统计超限次数
        updateNormalTotal();
        //创建新的周期正态分析
        createNewNormalDis();
    }

    @Override
    public NormalVo queryCollectByDate(Long crewId, String pointClass,Date startDate, Date endDate) {
        return baseMapper.queryCollectByDate(crewId, pointClass, startDate, endDate);
    }

    /**
     * 更新统计超限次数
     */
    private void updateNormalTotal(){
        for (RigidityNormalEntity entity : RigidityNormalUtils.rigidityNormalMap.values()) {
            if(entity == null){
                continue ;
            }
            //保存统计超限次数
            if(entity.getTotal() > 0){
                if(entity.getId() != null){
                    this.baseMapper.updateById(entity);
                }
            }
        }
    }

    /**
     * 创建新的周期正态分析
     */
    private void createNewNormalDis(){
        //分析时间
        //Date date = new Date();
        Date yesDate = DateUtils.patchDateToYesterdayAM();
        //收集样本时间段 上周1-上周日
        Date startDate = DateUtils.patchDateToAM(DateUtils.patchDateToWeekStart(yesDate));
        Date endDate = DateUtils.patchDateToMidnight(yesDate);
        //拿到周记录的趋势样本
        List<RigiditySampleVo> list = rigidityAvgtrendService.getMergeListByDate(startDate, endDate);
        if(list != null){
            RigidityNormalUtils.toAnalysisSampleForList(list);
            //插入新纪录
            RigidityThread thread = new RigidityThread();
            thread.start();
        }
    }

}
