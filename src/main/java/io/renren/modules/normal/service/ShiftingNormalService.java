
package io.renren.modules.normal.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.normal.entity.ShiftingNormalEntity;
import io.renren.modules.normal.vo.NormalVo;

import java.util.Date;

/**
 * 窜辊正态分析接口
 *
 */
public interface ShiftingNormalService extends IService<ShiftingNormalEntity> {


    /**
     * 生成周分析结果
     */
    void saveWeekNormalDis();

    /**
     * 查询分析结果
     * @param deviceId
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Integer side, Date startDate, Date endDate);
}
