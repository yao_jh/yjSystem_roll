package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName BendingSampleVo
 * @Deacription 弯辊动作正态输入样本
 * @Author gsj
 * @Date 2020/10/15 22:21
 * @Version 1.0
 **/
@Data
public class BendingSampleVo {
    //机组ID
    private Long crewId;

    //平均弯辊力操作侧OS数集
    private String bendingOs;
    //平均弯辊力操作侧OS均值
    private BigDecimal avgBendingOs;

    //平均弯辊力操作侧OS数集
    private String bendingDs;
    //平均弯辊力操作侧OS均值
    private BigDecimal avgBendingDs;

}
