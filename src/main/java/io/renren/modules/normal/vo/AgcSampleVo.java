package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName AgcSampleVo
 * @Deacription AGC正态输入样本
 * @Author lee
 * @Date 2020/8/9 22:21
 * @Version 1.0
 **/
@Data
public class AgcSampleVo {
    //设备ID
    private Long deviceId;
    //分类 0:行程2-4;1:行程4-10;2:行程10-20;3:行程>=20;
    private Integer type;
    //边侧 0:操作侧;1:驱动侧
    private Integer side;

    //速度数集
    private String speed;
    //速度均值
    private BigDecimal avgSpeed;

    //辊缝数集
    private String gap;
    //辊缝均值
    private BigDecimal avgGap;

    //偏差数集
    private String diff;
    //偏差均值
    private BigDecimal avgDiff;

}
