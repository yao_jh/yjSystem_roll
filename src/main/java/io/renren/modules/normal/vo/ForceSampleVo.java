package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ForceSampleVo
 * @Deacription 轧制力正态输入样本
 * @Author lee
 * @Date 2020/8/9 22:21
 * @Version 1.0
 **/
@Data
public class ForceSampleVo {
    //设备ID
    private Long crewId;

    //操作侧(压头)数集
    private String forceOsPt;
    //操作侧(压头)均值
    private BigDecimal avgForceOsPt;

    //传动侧(压头)
    private String forceDsPt;
    //传动侧(压头)均值
    private BigDecimal avgForceDsPt;

    //操作侧(传感器)
    private String forceOsLc;
    //操作侧(传感器)
    private BigDecimal avgForceOsLc;

    //传动侧(传感器)
    private String forceDsLc;
    //传动侧(传感器)
    private BigDecimal avgForceDsLc;

    //总轧制力数集
    private String forceTotal;
    //总轧制力均值
    private BigDecimal avgForceTotal;

    //偏差(压头)数集
    private String forceDiffOsDsPt;
    //偏差(压头)均值
    private BigDecimal avgForceDiffOsDsPt;

}
