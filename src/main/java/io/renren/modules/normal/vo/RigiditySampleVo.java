package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ForceSampleVo
 * @Deacription 轧机刚度正态输入样本
 * @Author gsj
 * @Date 2020/10/9 22:21
 * @Version 1.0
 **/
@Data
public class RigiditySampleVo {
    //设备ID
    private Long crewId;

    //操作侧轧机刚度(压头)
    private String rigidityOsPt;
    //操作侧轧机刚度(压头)均值
    private BigDecimal avgRigidityOsPt;

    //传动侧轧机刚度(压头)
    private String rigidityDsPt;
    //传动侧轧机刚度(压头)均值
    private BigDecimal avgRigidityDsPt;

    //操作侧轧机刚度(传感器)
    private String rigidityOsLc;
    //操作侧轧机刚度(传感器)均值
    private BigDecimal avgRigidityOsLc;

    //传动侧轧机刚度(传感器)
    private String rigidityDsLc;
    //传动侧轧机刚度(传感器)均值
    private BigDecimal avgRigidityDsLc;

}
