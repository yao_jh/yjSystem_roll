package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @ClassName NormalVo
 * @Deacription 正态分布
 * @Author lee
 * @Date 2020/8/9 22:21
 * @Version 1.0
 **/
@Data
public class NormalVo {
    //平均值
    private BigDecimal mean;
    //方差
    private BigDecimal variance;

    //1级报警超限次数
    private Long total_war1;
    //2级报警超限次数
    private Long total_war2;
    //3级报警超限次数
    private Long total_war3;

    //輸入数据集
    private String datas;

    //超限次数
    private Long total;
    //报警阈值线
    private List<BigDecimal> warningLines;
    //输出样本集
    private List<Map> sampleList;

    public Long getTotal() {
        return total_war1 + total_war2 + total_war3;
    }
}
