package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ShiftingSampleVo
 * @Deacription 窜辊动作正态输入样本
 * @Author gsj
 * @Date 2020/10/14 22:21
 * @Version 1.0
 **/
@Data
public class ShiftingSampleVo {
    //设备ID
    private Long deviceId;
    //边侧 0:操作侧;1:驱动侧
    private Integer side;

    //速度数集
    private String speed;
    //速度均值
    private BigDecimal avgSpeed;

}
