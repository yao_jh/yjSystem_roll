package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName MotorSampleVo
 * @Deacription 电机正态输入样本
 * @Author lee
 * @Date 2020/8/9 22:21
 * @Version 1.0
 **/
@Data
public class MotorSampleVo {
    //设备ID
    private Long deviceId;
    //状态 0:空载;1:有载
    private Integer state;

    //功率数集
    private String power;
    //功率均值
    private BigDecimal avgPower;

    //温度数集
    private String temp;
    //温度均值
    private BigDecimal avgTemp;

    //速度数集
    private String speed;
    //速度均值
    private BigDecimal avgSpeed;

    //电流数集
    private String current_;
    //电流均值
    private BigDecimal avgCurrent;

    //转矩数集
    private String torque;
    //转矩均值
    private BigDecimal avgTorque;

}
