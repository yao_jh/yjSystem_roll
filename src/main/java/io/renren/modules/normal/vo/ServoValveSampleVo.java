package io.renren.modules.normal.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ServoValveSampleVo
 * @Deacription 伺服阀正态输入样本
 * @Author lee
 * @Date 2020/8/9 22:21
 * @Version 1.0
 **/
@Data
public class ServoValveSampleVo {
    //设备ID
    private Long deviceId;
    //状态 0:上升;1:下降
    private Integer state;

    //速度数集
    private String speed;
    //速度均值
    private BigDecimal avgSpeed;

}
