
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.BendingNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 弯辊正态分析Dao
 *
 */
@Mapper
public interface BendingNormalDao extends BaseMapper<BendingNormalEntity> {

    /**
     * 查询分析结果
     * @param crewId
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long crewId, Date startDate, Date endDate);
}
