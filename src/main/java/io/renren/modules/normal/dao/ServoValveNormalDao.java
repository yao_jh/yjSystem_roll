
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.entity.ServoValveNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 伺服阀正态分析Dao
 *
 */
@Mapper
public interface ServoValveNormalDao extends BaseMapper<ServoValveNormalEntity> {

    /**
     * 查询分析结果
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Date startDate, Date endDate);
}
