
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.AgcNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 伺服阀正态分析Dao
 *
 */
@Mapper
public interface AgcNormalDao extends BaseMapper<AgcNormalEntity> {

    /**
     * 查询分析结果
     * @param deviceId
     * @param type
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate);
}
