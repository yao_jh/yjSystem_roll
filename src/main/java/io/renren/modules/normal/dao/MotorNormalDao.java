
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 电机正态分析Dao
 *
 */
@Mapper
public interface MotorNormalDao extends BaseMapper<MotorNormalEntity> {

    /**
     * 查询分析结果
     * @param deviceId
     * @param classCode
     * @param state
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, String classCode, Integer state, Date startDate, Date endDate);
}
