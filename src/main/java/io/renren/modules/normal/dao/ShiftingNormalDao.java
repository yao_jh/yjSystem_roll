
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.ShiftingNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 窜辊正态分析Dao
 *
 */
@Mapper
public interface ShiftingNormalDao extends BaseMapper<ShiftingNormalEntity> {

    /**
     * 查询分析结果
     * @param deviceId
     * @param side
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long deviceId, Integer side, Date startDate, Date endDate);
}
