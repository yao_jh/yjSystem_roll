
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 轧制力正态分析Dao
 *
 */
@Mapper
public interface ForceNormalDao extends BaseMapper<ForceNormalEntity> {

    /**
     * 查询分析结果
     * @param crewId
     * @param classCode
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long crewId, String classCode, Date startDate, Date endDate);
}
