
package io.renren.modules.normal.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.normal.entity.RigidityNormalEntity;
import io.renren.modules.normal.vo.NormalVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;


/**
 * 轧机刚度正态分析Dao
 *
 */
@Mapper
public interface RigidityNormalDao extends BaseMapper<RigidityNormalEntity> {

    /**
     * 查询分析结果
     * @param crewId
     * @param pointClass
     * @param startDate
     * @param endDate
     * @return
     */
    NormalVo queryCollectByDate(Long crewId, String pointClass, Date startDate, Date endDate);
}
