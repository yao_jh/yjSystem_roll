package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.entity.ServoValveNormalEntity;
import io.renren.modules.normal.service.ForceNormalService;
import io.renren.modules.normal.service.ServoValveNormalService;

import java.util.Map;

/**
 * 保存轧机正态分析结果
 */
public class ServoValveNormalThread extends Thread{

    private Map<Long, ServoValveNormalEntity> servoValveNormalMap;

    public ServoValveNormalThread(){
        super();
    }

    public ServoValveNormalThread(Map<Long, ServoValveNormalEntity> servoValveNormalMap){
        this.servoValveNormalMap = servoValveNormalMap;
    }

    @Override
    public void run() {
        synchronized (servoValveNormalMap){
            //获取spring bean
            ServoValveNormalService servoValveNormalService = (ServoValveNormalService) SpringContextUtils.getBean("servoValveNormalService");
            for (ServoValveNormalEntity entity : servoValveNormalMap.values()) {
                if(entity == null){
                    continue ;
                }
                if(entity.getId() != null){
                    //更新统计超限次数
                    if(entity.getTotal() > 0) {
                        servoValveNormalService.updateById(entity);
                    }
                }else{
                    //保存新的
                    servoValveNormalService.save(entity);
                }
            }
        }
    }
}