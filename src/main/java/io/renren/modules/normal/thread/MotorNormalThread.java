package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.service.MotorNormalService;
import io.renren.modules.normal.utils.MotorNormalUtils;

import java.util.Map;

/**
 * 保存电机正态分析结果
 */
public class MotorNormalThread extends Thread{

    public MotorNormalThread(){
        super();
    }

    @Override
    public void run() {
        //保存空载
        toUpdateForNormal(MotorNormalUtils.motorNoLoadNormalMap);
        //保存有载
        toUpdateForNormal(MotorNormalUtils.motorHasLoadNormalMap);
    }

    /**
     * 更新电机正太分析报警次数
     * @param motorNormalMap
     */
    private void toUpdateForNormal(Map<String, MotorNormalEntity> motorNormalMap){
        synchronized (motorNormalMap) {
            //获取spring bean
            MotorNormalService motorNormalService = (MotorNormalService) SpringContextUtils.getBean("motorNormalService");
            //遍历查看是否更新
            for (MotorNormalEntity entity : motorNormalMap.values()) {
                if (entity == null) {
                    continue;
                }
                if (entity.getId() != null) {
                    //更新统计超限次数
                    if (entity.getTotal() > 0) {
                        motorNormalService.updateById(entity);
                    }
                } else {
                    //保存新的
                    motorNormalService.save(entity);
                }
            }
        }
    }
}