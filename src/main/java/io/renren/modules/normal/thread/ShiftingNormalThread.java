package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.ShiftingNormalEntity;
import io.renren.modules.normal.service.ShiftingNormalService;

import java.util.Map;

/**
 * 保存窜辊动作正态分析结果
 */
public class ShiftingNormalThread extends Thread{

    private Map<String, ShiftingNormalEntity> shiftingNormalMap;

    public ShiftingNormalThread(){
        super();
    }

    public ShiftingNormalThread(Map<String, ShiftingNormalEntity> shiftingNormalMap){
        this.shiftingNormalMap = shiftingNormalMap;
    }

    @Override
    public void run() {
        synchronized (shiftingNormalMap){
            //获取spring bean
            ShiftingNormalService shiftingNormalService = (ShiftingNormalService) SpringContextUtils.getBean("rigidityNormalService");
            for (ShiftingNormalEntity entity : shiftingNormalMap.values()) {
                if(entity == null){
                    continue ;
                }
                if(entity.getId() != null){
                    //更新统计超限次数
                    if(entity.getTotal() > 0) {
                        shiftingNormalService.updateById(entity);
                    }
                }else{
                    //保存新的
                    shiftingNormalService.save(entity);
                }
            }
        }
    }
}