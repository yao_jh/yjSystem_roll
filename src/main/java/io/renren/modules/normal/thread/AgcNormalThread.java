package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.AgcNormalEntity;
import io.renren.modules.normal.service.AgcNormalService;
import io.renren.modules.normal.utils.AgcNormalUtils;

import java.util.Map;

/**
 * 保存轧机正态分析结果
 */
public class AgcNormalThread extends Thread{

    public AgcNormalThread(){
        super();
    }


    @Override
    public void run() {
        // 行程2-4
        toUpdateForNormal(AgcNormalUtils.agcNormal2_4Map);
        // 行程4-10
        toUpdateForNormal(AgcNormalUtils.agcNormal4_10Map);
        // 行程10-20
        toUpdateForNormal(AgcNormalUtils.agcNormal10_20Map);
        // 行程>=20
        toUpdateForNormal(AgcNormalUtils.agcNormal20_nMap);
    }

    /**
     * 更新电机正太分析报警次数
     * @param normalMap
     */
    private void toUpdateForNormal(Map<String, AgcNormalEntity> normalMap){
        synchronized (normalMap) {
            //获取spring bean
            AgcNormalService agcNormalService = (AgcNormalService) SpringContextUtils.getBean("agcNormalService");
            //遍历查看是否更新
            for (AgcNormalEntity entity : normalMap.values()) {
                if (entity == null) {
                    continue;
                }
                if (entity.getId() != null) {
                    //更新统计超限次数
                    if (entity.getTotal() > 0) {
                        agcNormalService.updateById(entity);
                    }
                } else {
                    //保存新的
                    agcNormalService.save(entity);
                }
            }
        }
    }
}