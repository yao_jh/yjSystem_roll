package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.service.ForceNormalService;

import java.util.Map;

/**
 * 保存轧机正态分析结果
 */
public class ForceNormalThread extends Thread{

    private Map<String, ForceNormalEntity> forceNormalMap;

    public ForceNormalThread(){
        super();
    }

    public ForceNormalThread(Map<String, ForceNormalEntity> forceNormalMap){
        this.forceNormalMap = forceNormalMap;
    }

    @Override
    public void run() {
        synchronized (forceNormalMap){
            //获取spring bean
            ForceNormalService forceNormalService = (ForceNormalService) SpringContextUtils.getBean("forceNormalService");
            for (ForceNormalEntity entity : forceNormalMap.values()) {
                if(entity == null){
                    continue ;
                }
                if(entity.getId() != null){
                    //更新统计超限次数
                    if(entity.getTotal() > 0) {
                        forceNormalService.updateById(entity);
                    }
                }else{
                    //保存新的
                    forceNormalService.save(entity);
                }
            }
        }
    }
}