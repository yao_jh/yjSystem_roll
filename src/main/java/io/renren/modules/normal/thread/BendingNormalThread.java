package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.BendingNormalEntity;
import io.renren.modules.normal.service.BendingNormalService;

import java.util.Map;

/**
 * 保存弯辊动作正态分析结果
 */
public class BendingNormalThread extends Thread{

    private Map<String, BendingNormalEntity> bendingNormalMap;

    public BendingNormalThread(){
        super();
    }

    public BendingNormalThread(Map<String, BendingNormalEntity> bendingNormalMap){
        this.bendingNormalMap = bendingNormalMap;
    }

    @Override
    public void run() {
        synchronized (bendingNormalMap){
            //获取spring bean
            BendingNormalService bendingNormalService = (BendingNormalService) SpringContextUtils.getBean("bendingNormalService");
            for (BendingNormalEntity entity : bendingNormalMap.values()) {
                if(entity == null){
                    continue ;
                }
                if(entity.getId() != null){
                    //更新统计超限次数
                    if(entity.getTotal() > 0) {
                        bendingNormalService.updateById(entity);
                    }
                }else{
                    //保存新的
                    bendingNormalService.save(entity);
                }
            }
        }
    }
}