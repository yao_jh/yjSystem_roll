package io.renren.modules.normal.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.normal.entity.RigidityNormalEntity;
import io.renren.modules.normal.service.RigidityNormalService;

import java.util.Map;

/**
 * 保存轧机正态分析结果
 */
public class RigidityNormalThread extends Thread{

    private Map<String, RigidityNormalEntity> rigidityNormalMap;

    public RigidityNormalThread(){
        super();
    }

    public RigidityNormalThread(Map<String, RigidityNormalEntity> rigidityNormalMap){
        this.rigidityNormalMap = rigidityNormalMap;
    }

    @Override
    public void run() {
        synchronized (rigidityNormalMap){
            //获取spring bean
            RigidityNormalService rigidityNormalService = (RigidityNormalService) SpringContextUtils.getBean("rigidityNormalService");
            for (RigidityNormalEntity entity : rigidityNormalMap.values()) {
                if(entity == null){
                    continue ;
                }
                if(entity.getId() != null){
                    //更新统计超限次数
                    if(entity.getTotal() > 0) {
                        rigidityNormalService.updateById(entity);
                    }
                }else{
                    //保存新的
                    rigidityNormalService.save(entity);
                }
            }
        }
    }
}