package io.renren.modules.normal.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 伺服阀正态分布
 */
@Data
@TableName("tb_servo_valve_normal")
public class ServoValveNormalEntity {

    @TableId
    private Long id;
    //设备ID
    private Long deviceId;
    //状态 0:上升;1:下降
    private Integer state;
    //平均值
    private BigDecimal mean;
    //方差
    private BigDecimal variance;

    //1级报警超限次数
    private Long total_war1 = 0L;
    //2级报警超限次数
    private Long total_war2 = 0L;
    //3级报警超限次数
    private Long total_war3 = 0L;
    //记录时间
    private Date date;

    @Transient
    private transient Long total;

    public Long getTotal() {
        return total_war1 + total_war2 + total_war3;
    }
}
