package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.entity.RigidityNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.RigiditySampleVo;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.utils.RigidityShuntUtils;
import io.renren.modules.theme.vo.RigidityTrendVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName RigidityNormalUtils
 * @Deacription 轧机刚度正态分析工具
 * @Author gsj
 * @Date 2020/10/5 16:35
 * @Version 1.0
 **/
public class RigidityNormalUtils {
    // 轧制力正态分析 key:crewId
    public static Map<String, RigidityNormalEntity> rigidityNormalMap = new ConcurrentHashMap<String, RigidityNormalEntity>();

    /**
     * 轧制力正态分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        //正态分析对象依赖均值趋势，先判断是否分析这个设备
        RigidityTrendVo trend = RigidityShuntUtils.rigidityTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //键=crewId
        String key = point.getCrewId().toString() + "_";
        //查找正态阈值设置
        RigidityNormalEntity normal = rigidityNormalMap.get(key + point.getClassCode());
        if(normal != null){
            BigDecimal value = point.getValue();
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<RigiditySampleVo> list) {
        for(RigiditySampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(RigiditySampleVo sample) {
        //键名 crewId+code
        String key = sample.getCrewId().toString();
        //操作侧(压头)
        if(CommonUtils.isNotNull(sample.getRigidityOsLc())){
            RigidityNormalEntity rigidityOsLc = createNewNormal(sample,"mm_os_lc");
            structureAnalysisNormal(rigidityOsLc, sample.getRigidityOsLc(), sample.getAvgRigidityOsLc());
            rigidityNormalMap.put(key+"_mm_os_lc", rigidityOsLc);
        }
        //传动侧(压头)
        if(CommonUtils.isNotNull(sample.getRigidityDsLc())) {
            RigidityNormalEntity rigidityDsLc = createNewNormal(sample, "mm_ds_lc");
            structureAnalysisNormal(rigidityDsLc, sample.getRigidityDsLc(), sample.getAvgRigidityDsLc());
            rigidityNormalMap.put(key + "_mm_ds_lc", rigidityDsLc);
        }
        //操作侧(传感器)
        if(CommonUtils.isNotNull(sample.getRigidityOsPt())) {
            RigidityNormalEntity rigidityOsPt = createNewNormal(sample, "mm_os_pt");
            structureAnalysisNormal(rigidityOsPt, sample.getRigidityOsPt(), sample.getAvgRigidityOsPt());
            rigidityNormalMap.put(key + "_mm_os_pt", rigidityOsPt);
        }
        //传动侧(传感器)
        if(CommonUtils.isNotNull(sample.getRigidityDsPt())) {
            RigidityNormalEntity rigidityDsPt = createNewNormal(sample, "mm_ds_pt");
            structureAnalysisNormal(rigidityDsPt, sample.getRigidityDsPt(), sample.getAvgRigidityDsPt());
            rigidityNormalMap.put(key + "_mm_ds_pt", rigidityDsPt);
        }
    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static RigidityNormalEntity createNewNormal(RigiditySampleVo sample, String key){
        RigidityNormalEntity normal = new RigidityNormalEntity();
        normal.setCrewId(sample.getCrewId());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(RigidityNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }
}
