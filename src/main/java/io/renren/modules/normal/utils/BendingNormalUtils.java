package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.entity.BendingNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.BendingSampleVo;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.theme.utils.BendingShuntUtils;
import io.renren.modules.theme.vo.BendingTrendVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName BendingNormalUtils
 * @Deacription 弯辊力正态分析工具
 * @Author gsj
 * @Date 2020/10/15 21:35
 * @Version 1.0
 **/
public class BendingNormalUtils {
    // key:deviceId_side_classCode
    public static Map<String, BendingNormalEntity> bendingNormalMap = new ConcurrentHashMap<String, BendingNormalEntity>();


    /**
     * 弯辊力正态分流发生器
     * @param point
     */
    public static void checkWarningHandler(PointEntity point){
        //正态分析对象依赖均值趋势，先判断是否分析这个设备
        BendingTrendVo trend = BendingShuntUtils.bendingTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //键=crewId
        String key = point.getCrewId().toString() + "_";
        //查找正态阈值设置
        BendingNormalEntity normal = bendingNormalMap.get(key + point.getClassCode());
        if(normal != null){
            BigDecimal value = point.getValue();
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<BendingSampleVo> list) {
        for(BendingSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(BendingSampleVo sample) {
        //键名 crewId+code
        String key = sample.getCrewId().toString();
        //操作侧
        if(CommonUtils.isNotNull(sample.getBendingOs())){
            BendingNormalEntity bendingOs = createNewNormal(sample,"wr_os_act");
            structureAnalysisNormal(bendingOs, sample.getBendingOs(), sample.getAvgBendingOs());
            bendingNormalMap.put(key+"_wr_os_act", bendingOs);
        }
        //传动侧
        if(CommonUtils.isNotNull(sample.getBendingDs())) {
            BendingNormalEntity bendingDs = createNewNormal(sample, "wr_ds_act");
            structureAnalysisNormal(bendingDs, sample.getBendingDs(), sample.getAvgBendingDs());
            bendingNormalMap.put(key + "_wr_ds_act", bendingDs);
        }
    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static BendingNormalEntity createNewNormal(BendingSampleVo sample, String key){
        BendingNormalEntity normal = new BendingNormalEntity();
        normal.setCrewId(sample.getCrewId());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(BendingNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }
}
