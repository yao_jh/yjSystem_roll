package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.utils.ServoValveShuntUtils;
import io.renren.modules.feature.vo.ServoValveTrendVo;
import io.renren.modules.normal.entity.ServoValveNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.ServoValveSampleVo;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ServoValveNormalUtils
 * @Deacription 伺服阀正态分析工具
 * @Author lee
 * @Date 2020/8/9 21:35
 * @Version 1.0
 **/
public class ServoValveNormalUtils {
    // 伺服阀正态分析 key:deviceId
    public static Map<Long, ServoValveNormalEntity> servoNormalMap = new ConcurrentHashMap<Long, ServoValveNormalEntity>();

    /**
     * 伺服阀正态分流发生器
     * @param trendEntity
     */
    public static void checkWarningHandler(ServoValveAvgtrendEntity trendEntity){
        //查找正态阈值设置
        ServoValveNormalEntity normal = servoNormalMap.get(trendEntity.getDeviceId());
        if(normal != null){
            BigDecimal value = trendEntity.getSpeed();
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<ServoValveSampleVo> list) {
        for(ServoValveSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(ServoValveSampleVo sample) {
        if(CommonUtils.isNotNull(sample.getSpeed())){
            ServoValveNormalEntity entity = createNewNormal(sample);
            structureAnalysisNormal(entity, sample.getSpeed(), sample.getAvgSpeed());
            servoNormalMap.put(sample.getDeviceId(), entity);
        }

    }

    /**
     * 创建模型实体
     * @param sample
     * @return
     */
    private static ServoValveNormalEntity createNewNormal(ServoValveSampleVo sample){
        ServoValveNormalEntity normal = new ServoValveNormalEntity();
        normal.setDeviceId(sample.getDeviceId());
        normal.setState(sample.getState());
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(ServoValveNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }

}
