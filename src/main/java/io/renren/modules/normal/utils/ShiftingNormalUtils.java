package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.entity.ShiftingNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.ShiftingSampleVo;
import io.renren.modules.theme.entity.ShiftingAvgtrendEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ShiftingNormalUtils
 * @Deacription 窜辊正态分析工具
 * @Author gsj
 * @Date 2020/10/14 21:35
 * @Version 1.0
 **/
public class ShiftingNormalUtils {
    // key:deviceId_side_classCode
    public static Map<String, ShiftingNormalEntity> shiftingNormalMap = new ConcurrentHashMap<String, ShiftingNormalEntity>();


    /**
     * 窜辊正态分流发生器
     * @param trendEntity
     */
    public static void checkWarningHandler(ShiftingAvgtrendEntity trendEntity){
        //动作速度
        proTributaryWarning(trendEntity, "speed");
    }

    /**
     * 验证窜辊报警分流
     * @param trendEntity
     * @param classCode
     */
    private static void proTributaryWarning(ShiftingAvgtrendEntity trendEntity, String classCode){
        //键=deviceId_side_classCode
        String key = getKeyByTrend(trendEntity, classCode);
        //查找正态阈值设置
        ShiftingNormalEntity normal = shiftingNormalMap.get(key + classCode);
        if(normal != null){
            BigDecimal value = BigDecimal.ZERO;
            //构建标准模型
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<ShiftingSampleVo> list) {
        for(ShiftingSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(ShiftingSampleVo sample) {
        //键名 deviceId+ClassCode
        String key = getKeyBySampleVo(sample);
        //速度
        if(CommonUtils.isNotNull(sample.getSpeed())){
            ShiftingNormalEntity speed = createNewNormal(sample,"cvc_en_act");
            structureAnalysisNormal(speed, sample.getSpeed(), sample.getAvgSpeed());
            shiftingNormalMap.put(key+"_cvc_en_act", speed);
        }
    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static ShiftingNormalEntity createNewNormal(ShiftingSampleVo sample, String key){
        ShiftingNormalEntity normal = new ShiftingNormalEntity();
        normal.setDeviceId(sample.getDeviceId());
        normal.setSide(sample.getSide());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(ShiftingNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }

    /**
     * 获取主键
     * 键=deviceId_side_classCode
     * @param trendEntity
     * @return
     */
    private static String getKeyByTrend(ShiftingAvgtrendEntity trendEntity, String classCode){
        //键=deviceId_side_classCode
        StringBuffer key = new StringBuffer(trendEntity.getDeviceId().toString());
        key.append("_");
        key.append(trendEntity.getSide().toString());
        key.append("_");
        key.append(classCode);
        return key.toString();
    }

    /**
     * 获取主键
     * 键=deviceId_side
     * @param sample
     * @return
     */
    private static String getKeyBySampleVo(ShiftingSampleVo sample){
        //键=deviceId_side_classCode
        StringBuffer key = new StringBuffer(sample.getDeviceId().toString());
        key.append("_");
        key.append(sample.getSide().toString());
        return key.toString();
    }

}
