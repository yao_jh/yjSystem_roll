package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.normal.entity.AgcNormalEntity;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.normal.vo.MotorSampleVo;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName AgcNormalUtils
 * @Deacription AGC正态分析工具
 * @Author lee
 * @Date 2020/8/9 21:35
 * @Version 1.0
 **/
public class AgcNormalUtils {
    // 行程2-4 key:deviceId_side_classCode
    public static Map<String, AgcNormalEntity> agcNormal2_4Map = new ConcurrentHashMap<String, AgcNormalEntity>();
    // 行程4-10 key:deviceId_side_classCode
    public static Map<String, AgcNormalEntity> agcNormal4_10Map = new ConcurrentHashMap<String, AgcNormalEntity>();
    // 行程10-20 key:deviceId_side_classCode
    public static Map<String, AgcNormalEntity> agcNormal10_20Map = new ConcurrentHashMap<String, AgcNormalEntity>();
    // 行程>=20 key:deviceId_side_classCode
    public static Map<String, AgcNormalEntity> agcNormal20_nMap = new ConcurrentHashMap<String, AgcNormalEntity>();

    /**
     * AGC正态分流发生器
     * @param trendEntity
     */
    public static void checkWarningHandler(AgcAvgtrendEntity trendEntity){
        //动作速度
        proTributaryWarning(trendEntity, "speed");
        //辊缝均值
        proTributaryWarning(trendEntity, "gap");
        //偏差均值
        proTributaryWarning(trendEntity, "diff");
    }

    /**
     * 验证AGC报警分流
     * @param trendEntity
     * @param classCode
     */
    private static void proTributaryWarning(AgcAvgtrendEntity trendEntity, String classCode){
        //获取模型
        Map<String, AgcNormalEntity> agcNormalMap = getNormalMapByType(trendEntity.getType());
        //键=deviceId_side_classCode
        String key = getKeyByTrend(trendEntity, classCode);
        //查找正态阈值设置
        AgcNormalEntity normal = agcNormalMap.get(key);
        if(normal != null){
            BigDecimal value = BigDecimal.ZERO;
            if("speed".equals(classCode)){
                value = trendEntity.getSpeed();
                //速度与平均动作速度偏差
                trendEntity.setSpeedDiff(value.subtract(normal.getMean()));
            }else if("gap".equals(classCode)){
                value = trendEntity.getEndGap();
                //平均辊缝偏差
                trendEntity.setGapDiff(value.subtract(normal.getMean()));
            }else if("diff".equals(classCode)){
                value = trendEntity.getAvgDiff();
                //平均偏差(设定和实际)
                trendEntity.setAvgDiff(value.subtract(normal.getMean()));
            }
            //构建标准模型
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<AgcSampleVo> list) {
        for(AgcSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(AgcSampleVo sample) {
        //获取模型
        Map<String, AgcNormalEntity> agcNormalMap = getNormalMapByType(sample.getType());
        //键名 deviceId+ClassCode
        String key = getKeyBySampleVo(sample);
        //速度
        if(CommonUtils.isNotNull(sample.getSpeed())){
            AgcNormalEntity speed = createNewNormal(sample,"speed");
            structureAnalysisNormal(speed, sample.getSpeed(), sample.getAvgSpeed());
            agcNormalMap.put(key+"_speed", speed);
        }
        //辊缝
        if(CommonUtils.isNotNull(sample.getGap())) {
            AgcNormalEntity gap = createNewNormal(sample, "gap");
            structureAnalysisNormal(gap, sample.getGap(), sample.getAvgGap());
            agcNormalMap.put(key + "_gap", gap);
        }
        //偏差
        if(CommonUtils.isNotNull(sample.getDiff())) {
            AgcNormalEntity diff = createNewNormal(sample, "diff");
            structureAnalysisNormal(diff, sample.getDiff(), sample.getAvgDiff());
            agcNormalMap.put(key + "_diff", diff);
        }

    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static AgcNormalEntity createNewNormal(AgcSampleVo sample, String key){
        AgcNormalEntity normal = new AgcNormalEntity();
        normal.setDeviceId(sample.getDeviceId());
        normal.setSide(sample.getSide());
        normal.setType(sample.getType());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(AgcNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }

    /**
     * 获取不同行程类型的缓存模型
     * @param type
     * @return
     */
    private static Map<String, AgcNormalEntity> getNormalMapByType(Integer type){
        Map<String, AgcNormalEntity> agcNormalMap = null;
        switch (type){
            case 0: agcNormalMap = agcNormal2_4Map; break;
            case 1: agcNormalMap = agcNormal4_10Map; break;
            case 2: agcNormalMap = agcNormal10_20Map; break;
            case 3: agcNormalMap = agcNormal20_nMap; break;
        }
        return agcNormalMap;
    }

    /**
     * 获取主键
     * 键=deviceId_side_classCode
     * @param trendEntity
     * @return
     */
    private static String getKeyByTrend(AgcAvgtrendEntity trendEntity, String classCode){
        //键=deviceId_side_classCode
        StringBuffer key = new StringBuffer(trendEntity.getDeviceId().toString());
        key.append("_");
        key.append(trendEntity.getSide().toString());
        key.append("_");
        key.append(classCode);
        return key.toString();
    }

    /**
     * 获取主键
     * 键=deviceId_side
     * @param sample
     * @return
     */
    private static String getKeyBySampleVo(AgcSampleVo sample){
        //键=deviceId_side_classCode
        StringBuffer key = new StringBuffer(sample.getDeviceId().toString());
        key.append("_");
        key.append(sample.getSide().toString());
        return key.toString();
    }

}
