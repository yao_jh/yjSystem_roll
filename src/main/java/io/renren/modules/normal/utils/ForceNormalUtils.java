package io.renren.modules.normal.utils;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.utils.ForceShuntUtils;
import io.renren.modules.feature.vo.ForceTrendVo;
import io.renren.modules.normal.entity.ForceNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.ForceSampleVo;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ForceNormalUtils
 * @Deacription 轧制力正态分析工具
 * @Author lee
 * @Date 2020/8/9 21:35
 * @Version 1.0
 **/
public class ForceNormalUtils {
    // 轧制力正态分析 key:crewId
    public static Map<String, ForceNormalEntity> forceNormalMap = new ConcurrentHashMap<String, ForceNormalEntity>();

    /**
     * 轧制力正态分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        //正态分析对象依赖均值趋势，先判断是否分析这个设备
        ForceTrendVo trend = ForceShuntUtils.forceTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //键=crewId
        String key = point.getCrewId().toString() + "_";
        //查找正态阈值设置
        ForceNormalEntity normal = forceNormalMap.get(key + point.getClassCode());
        if(normal != null){
            BigDecimal value = point.getValue();
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<ForceSampleVo> list) {
        for(ForceSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(ForceSampleVo sample) {
        //键名 crewId+code
        String key = sample.getCrewId().toString();
        //操作侧(压头)
        if(CommonUtils.isNotNull(sample.getForceOsPt())){
            ForceNormalEntity forceOsPt = createNewNormal(sample,"force_os_pt");
            structureAnalysisNormal(forceOsPt, sample.getForceOsPt(), sample.getAvgForceOsPt());
            forceNormalMap.put(key+"_force_os_pt", forceOsPt);
        }
        //传动侧(压头)
        if(CommonUtils.isNotNull(sample.getForceDsPt())) {
            ForceNormalEntity forceDsPt = createNewNormal(sample, "force_ds_pt");
            structureAnalysisNormal(forceDsPt, sample.getForceDsPt(), sample.getAvgForceDsPt());
            forceNormalMap.put(key + "_force_ds_pt", forceDsPt);
        }
        //操作侧(传感器)
        if(CommonUtils.isNotNull(sample.getForceOsLc())) {
            ForceNormalEntity forceOsLc = createNewNormal(sample, "force_os_lc");
            structureAnalysisNormal(forceOsLc, sample.getForceOsLc(), sample.getAvgForceOsLc());
            forceNormalMap.put(key + "_force_os_lc", forceOsLc);
        }
        //传动侧(传感器)
        if(CommonUtils.isNotNull(sample.getForceDsLc())) {
            ForceNormalEntity forceDsLc = createNewNormal(sample, "force_ds_lc");
            structureAnalysisNormal(forceDsLc, sample.getForceDsLc(), sample.getAvgForceDsLc());
            forceNormalMap.put(key + "_force_ds_lc", forceDsLc);
        }
        //总轧制力
        if(CommonUtils.isNotNull(sample.getForceTotal())) {
            ForceNormalEntity forceTotal = createNewNormal(sample, "force_total_act");
            structureAnalysisNormal(forceTotal, sample.getForceTotal(), sample.getAvgForceTotal());
            forceNormalMap.put(key + "_force_total_act", forceTotal);
        }
        //偏差(压头)
        if(CommonUtils.isNotNull(sample.getForceDiffOsDsPt())) {
            ForceNormalEntity forceDiffOsDsPt = createNewNormal(sample, "force_diff_pt");
            structureAnalysisNormal(forceDiffOsDsPt, sample.getForceDiffOsDsPt(), sample.getAvgForceDiffOsDsPt());
            forceNormalMap.put(key + "_force_diff_pt", forceDiffOsDsPt);
        }
    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static ForceNormalEntity createNewNormal(ForceSampleVo sample, String key){
        ForceNormalEntity normal = new ForceNormalEntity();
        normal.setCrewId(sample.getCrewId());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(ForceNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }

}
