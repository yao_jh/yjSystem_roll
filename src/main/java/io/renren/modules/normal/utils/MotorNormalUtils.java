package io.renren.modules.normal.utils;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.utils.MotorShuntUtils;
import io.renren.modules.feature.vo.MotorTrendVo;
import io.renren.modules.normal.entity.MotorNormalEntity;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.vo.MotorSampleVo;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName MotorNormalUtils
 * @Deacription 电机正态分析工具
 * @Author lee
 * @Date 2020/8/9 21:35
 * @Version 1.0
 **/
public class MotorNormalUtils {
    // 空载电机正态分析 key:deviceId_classCode
    public static Map<String, MotorNormalEntity> motorNoLoadNormalMap = new ConcurrentHashMap<String, MotorNormalEntity>();
    // 有载电机正态分析 key:deviceId_classCode
    public static Map<String, MotorNormalEntity> motorHasLoadNormalMap = new ConcurrentHashMap<String, MotorNormalEntity>();

    /**
     * 电机正态分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        //正态分析对象依赖均值趋势，先判断是否分析这个设备
        MotorTrendVo trend = MotorShuntUtils.motorTrendMap.get(point.getDeviceId());
        if(trend == null){
            return ;
        }
        //设备状态,有载/空载
        DeviceEntity device = MainCatche.deviceMap.get(point.getDeviceCode());
        Integer state = device.getJobStatus();
        if(state == null){
            return ;
        }
        //键=deviceId_classCode
        String key = point.getDeviceId().toString()+"_"+point.getClassCode();
        //查找正态阈值设置
        MotorNormalEntity normal = null;
        if(state == 0){
            normal = motorNoLoadNormalMap.get(key);
        }else{
            normal = motorHasLoadNormalMap.get(key);
        }
        if(normal != null){
            BigDecimal value = point.getValue();
            StandardNormalModel mode = new StandardNormalModel(normal.getMean(), normal.getVariance());
            //检查超限等级
            Integer level = mode.checkWarningLevel(value);
            if(level > 0){
                //超限累加超限次数
                if(level == 1){
                    normal.setTotal_war1(normal.getTotal_war1() + 1);
                }else if(level == 2){
                    normal.setTotal_war2(normal.getTotal_war2() + 1);
                }else{
                    normal.setTotal_war3(normal.getTotal_war3() + 1);
                }
            }
        }
    }

    /**
     * 分析设备样本
     * @param list
     * @return
     */
    public static void toAnalysisSampleForList(List<MotorSampleVo> list) {
        for(MotorSampleVo sample : list){
            toAnalysisSample(sample);
        }
    }

    /**
     * 分析设备状态样本
     * @param sample
     * @return
     */
    public static void toAnalysisSample(MotorSampleVo sample) {
        Map<String, MotorNormalEntity> motorNormalMap = null;
        if(sample.getState() == null){
            return;
        }else if(sample.getState() == 0){
            motorNormalMap = motorNoLoadNormalMap;
        }else{
            motorNormalMap = motorHasLoadNormalMap;
        }
        //键名 device+state+code
        String key = sample.getDeviceId().toString();
        //功率
        if(CommonUtils.isNotNull(sample.getPower())){
            MotorNormalEntity power = createNewNormal(sample,"power");
            structureAnalysisNormal(power, sample.getPower(), sample.getAvgPower());
            motorNormalMap.put(key+"_power", power);
        }
        //温度
        if(CommonUtils.isNotNull(sample.getTemp())) {
            MotorNormalEntity temp = createNewNormal(sample, "temp");
            structureAnalysisNormal(temp, sample.getTemp(), sample.getAvgTemp());
            motorNormalMap.put(key + "_temp", temp);
        }
        //速度
        if(CommonUtils.isNotNull(sample.getSpeed())) {
            MotorNormalEntity speed = createNewNormal(sample, "speed_act");
            structureAnalysisNormal(speed, sample.getSpeed(), sample.getAvgSpeed());
            motorNormalMap.put(key + "_speed_act", speed);
        }
        //速度
        if(CommonUtils.isNotNull(sample.getCurrent_())) {
            MotorNormalEntity current = createNewNormal(sample, "current");
            structureAnalysisNormal(current, sample.getCurrent_(), sample.getAvgCurrent());
            motorNormalMap.put(key + "_current", current);
        }
        //转矩
        if(CommonUtils.isNotNull(sample.getTorque())) {
            MotorNormalEntity torque = createNewNormal(sample, "torque");
            structureAnalysisNormal(torque, sample.getTorque(), sample.getAvgTorque());
            motorNormalMap.put(key + "_torque", torque);
        }
    }

    /**
     * 创建模型实体
     * @param sample
     * @param key
     * @return
     */
    private static MotorNormalEntity createNewNormal(MotorSampleVo sample, String key){
        MotorNormalEntity normal = new MotorNormalEntity();
        normal.setDeviceId(sample.getDeviceId());
        normal.setState(sample.getState());
        normal.setClassCode(key);
        normal.setDate(DateUtils.patchDateToAM(new Date()));
        return normal;
    }

    /**
     * 分析模型样本
     * @param normal
     * @param datas
     * @param mean
     */
    private static void structureAnalysisNormal(MotorNormalEntity normal, String datas, BigDecimal mean){
        StandardNormalModel modelMin = new StandardNormalModel(datas, mean);
        //建立标准差
        modelMin.initVariance();

        //设置均值和标准差
        normal.setMean(modelMin.getMean());
        normal.setVariance(modelMin.getVariance());
    }

}
