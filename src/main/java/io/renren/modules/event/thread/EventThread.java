package io.renren.modules.event.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.event.entity.EventLogEntity;
import io.renren.modules.event.service.EventLogService;
import io.renren.modules.normal.utils.AgcNormalUtils;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import io.renren.modules.theme.service.AgcAvgtrendService;

import java.util.Date;

/**
 * 保存事件
 */
public class EventThread extends Thread{

    private EventLogEntity logEntity;

    public EventThread(){
        super();
    }

    public EventThread(EventLogEntity logEntity){
        this.logEntity = logEntity;
    }

    @Override
    public void run() {
        synchronized (logEntity){
            logEntity.setDate(new Date());
            //获取spring bean
            EventLogService eventLogService = (EventLogService) SpringContextUtils.getBean("eventLogService");
            //保存趋势记录
            eventLogService.save(logEntity);
        }
    }
}