
package io.renren.modules.event.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.event.entity.EventLogEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 事件Dao
 *
 */
@Mapper
public interface EventLogDao extends BaseMapper<EventLogEntity> {


}
