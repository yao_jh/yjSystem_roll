
package io.renren.modules.event.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.event.entity.EventLogEntity;


/**
 * 事件接口
 *
 */
public interface EventLogService extends IService<EventLogEntity> {


}
