
package io.renren.modules.event.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.normal.vo.AgcSampleVo;
import io.renren.modules.theme.dao.AgcAvgtrendDao;
import io.renren.modules.theme.entity.AgcAvgtrendEntity;
import io.renren.modules.theme.service.AgcAvgtrendService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("eventLogService")
public class EventLogServiceImpl extends ServiceImpl<AgcAvgtrendDao, AgcAvgtrendEntity> implements AgcAvgtrendService {


    @Override
    public List<AgcSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<AgcAvgtrendEntity> getListByDate(Long deviceId, Integer type, Integer side, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, type, side, startDate, endDate);
    }

}
