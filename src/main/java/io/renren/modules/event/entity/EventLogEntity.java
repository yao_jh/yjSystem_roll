package io.renren.modules.event.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import io.renren.modules.warning.entity.WarningLogBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import java.util.Date;

@Data
@TableName("tb_event_log")
public class EventLogEntity{

    @TableId
    private Long id;

    /**
     * 事件等级
     * 1-4级别
     */
    private Integer level;

    /**
     * 机组ID
     */
    private Long crewId;

    /**
     * 设备ID
     */
    private Long deviceId;

    /**
     * 事件类型
     * 0:消息;1:张力辊组打滑
     */
    private Integer state;

    /**
     * 提示信息
     */
    private String info;

    /**
     * 开始时间
     */
    private Date startDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 确认时间
     */
    private Date confirmDate;

    /**
     * 保存时间
     */
    private Date date;

}
