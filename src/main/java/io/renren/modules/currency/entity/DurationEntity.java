package io.renren.modules.currency.entity;

import io.renren.common.utils.DateUtils;
import io.renren.modules.operation.entity.DeviceEntity;
import lombok.Data;

import java.util.Date;

/**
 * 时长基表
 */
@Data
public class DurationEntity {

    //设备ID
    private Long deviceId;

    //设备状态 0:下机;1:上机;
    //运行时长 0:停止;1:运行
    private Integer state;

    //开始时间
    private Date startDate;

    //结束时间
    private Date endDate;

    //时长
    private Long duration;

    //记录时间
    private Date date;

    //原因
    //0:故障;1:检修;2:计划停机;3:工艺调整;4:外部原因;5:其它
    private Integer reason;

    //专业
    //0:机械;1:液压;2:低压传动;3:低压传动电机;4:一级自动化;5:仪表;6:生产工艺
    private Integer major;

    //描述
    private String remark;

    public DurationEntity(){
        super();
    }

    /**
     * 运行信息
     * @param device
     */
    public DurationEntity(DeviceEntity device){
        this.deviceId = device.getDeviceId();
        this.state = device.getState();
        this.startDate = device.getStartDate();
        this.endDate = device.getEndDate();
        this.duration = DateUtils.getDateDiffForMini(startDate, endDate);
    }

    /**
     * 上下机信息
     * @param device
     */
    public void setOnlineState(DeviceEntity device){
        Date date = new Date();
        this.deviceId = device.getDeviceId();
        this.state = device.getOnline();
        this.startDate = device.getOnlineDate() == null ? date : device.getOnlineDate();
        this.endDate = date;
        this.duration = DateUtils.getDateDiffForHour(startDate, endDate);
    }

}
