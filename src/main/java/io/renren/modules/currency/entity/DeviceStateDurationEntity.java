package io.renren.modules.currency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.operation.entity.DeviceEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 设备状态信息表
 * 0:下机; 1:上机;
 */
@Data
@TableName("tb_device_state_duration")
@EqualsAndHashCode(callSuper = false)
public class DeviceStateDurationEntity extends DurationEntity{

    //ID
    @TableId
    private Long id;

    public DeviceStateDurationEntity(){
        super();
    }

    public DeviceStateDurationEntity(DeviceEntity device){
        super.setOnlineState(device);
    }

}
