package io.renren.modules.currency.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.operation.entity.DeviceEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

/**
 * 设备运行时长表
 * 0:停止; 1:运行;
 */
@Data
@TableName("tb_device_run_duration")
@EqualsAndHashCode(callSuper = false)
public class DeviceRunDurationEntity extends DurationEntity{

    @TableId
    private Long id;

    @Transient
    private transient Long crewId;

    public DeviceRunDurationEntity(){
        super();
    }

    public DeviceRunDurationEntity(DeviceEntity device){
        super(device);
    }
}
