package io.renren.modules.currency.utils;

import io.renren.common.sys.MainCatche;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;

public class CrewStateUtils {

    /**
     * 获取机组轧制状态
     * @param point
     * @return
     */
    public static Integer getCrewMillState(PointEntity point){
        Integer state = null;
        if("force_total_act".equals(point.getClassCode())){
            CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
            BigDecimal quantity = crew.getQuantity();
            //总轧制力大于标定值则咬钢状态
            if(point.getValue().compareTo(quantity) >= 0){
                state = 1;
            }else{
                state = 0;
            }
            //更新机组状态
            crew.setState(state);
        }
        return state;
    }

    /**
     * 获取机组轧制状态
     * @param point
     * @return
     */
    public static Integer getRollerState(PointEntity point){
        Integer state = null;
        if("speed_act".equals(point.getClassCode())){
            BigDecimal value = point.getValue();
            if(value == null){
                return null;
            }
            //速度4.5以上是有载以下是空载
            if(value.compareTo(BigDecimal.valueOf(4.5)) >= 0){
                state = 1;
            }else{
                state = 0;
            }
        }
        return state;
    }

    /**
     * 获取机组code
     * @param point
     * @return
     */
    public static String getCodeByPoint(PointEntity point) {
        String code = point.getDeviceCode();
        if(code == null){
            CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
            if(crew != null){
                code =  crew.getCode();
            }
        }
        return  code;
    }

    /**
     * 获取工作状态
     * @param point
     * @return
     */
    public static Integer getJobStatus(PointEntity point){
        Integer state = null;
        //存储实时内存
        String code = getCodeByPoint(point);
        if(code == null){
            return null;
        }
        Map<String, PointEntity> pointMap = MainCatche.dataMap.get(code);
        if(pointMap == null){
            return null;
        }
        PointEntity pointRec = null;
        //机组ID39以后是冷轧
        if(point.getCrewId().compareTo(39L) > 0){
            //冷轧场景
            //验证速度,速度0停止,>0运行。一次启停算一个周期
            pointRec = pointMap.get("speed_actual");
            if(pointRec != null){
                state = pointRec.getValue().compareTo(BigDecimal.ZERO) > 0 ? 1 : 0;
            }
        }else{
            //热轧场景
            //使用机组判断咬钢和抛钢
            pointRec = pointMap.get("force_total_act");
            if(pointRec != null){  //主电机
                state = getCrewMillState(pointRec);
            }else{
                //辊道电机
                //速度判定有载/空载状态
                pointRec = pointMap.get("speed_act");
                if(pointRec != null){
                    state = getRollerState(pointRec);
                }
            }

        }
        //更新状态
        if(state != null){
            DeviceEntity device = MainCatche.deviceMap.get(point.getDeviceCode());
            device.setJobStatus(state);
        }
        return state;
    }

}
