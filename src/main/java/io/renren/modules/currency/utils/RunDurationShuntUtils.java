package io.renren.modules.currency.utils;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.currency.thread.RunDurationThread;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 运行时间评估包工具
 */
public class RunDurationShuntUtils {

    /**
     * 运行状态分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        if("run_sig".equals(point.getClassCode())){//有设备状态检测信号
            //验证设备状态
            Integer state = point.getValue().intValue();
            grantDeviceState(point, state);
        }else if("speed_act".equals(point.getClassCode())){
            //没有设备状态检测信号
            //验证速度,速度0停止
            //设备状态
            Integer state = point.getValue().compareTo(BigDecimal.ZERO) > 0 ? 1 : 0;
            grantDeviceState(point, state);
        }
    }

    /**
     * 处理设备状态
     * @param point
     * @param state
     */
    private static void grantDeviceState(PointEntity point, Integer state){
        //设备
        DeviceEntity device = MainCatche.deviceMap.get(point.getDeviceCode());
        if(device == null) return ;
        boolean flag = false;
        if(device.getState() == null){
            device.setState(state);
            device.setStartDate(point.getRealDate());
            flag = true;
        }else if(state.compareTo(device.getState()) != 0){
            if(device.getStartDate() != null){
                //状态改变记录结束时间
                device.setEndDate(point.getRealDate());
                saveRunDurationEntity(device);
            }
            //更新设备状态
            device.setState(state);
            device.setStartDate(point.getRealDate());
            device.setEndDate(null);
            flag = true;
        }

        // 更新设备状态
        if(flag){
            updateDeviceState(device);
        }
    }

    /**
     * 更新运行状态
     * @param device
     */
    private static void updateDeviceState(DeviceEntity device){
       //更新设备状态线程
        /*DeviceStateThread thread = new DeviceStateThread(device);
        thread.start();*/

        //更新设备缓存状态信息
        device.setToUpdate(true);
        MainCatche.deviceMap.put(device.getCode(), device);
    }

    /**
     * 保存运行状态
     * @param device
     */
    private static void saveRunDurationEntity(DeviceEntity device){
        //保存趋势数据
        DeviceRunDurationEntity runDuration = new DeviceRunDurationEntity(device);
        //保存线程
        RunDurationThread thread = new RunDurationThread(runDuration);
        thread.start();
    }

}
