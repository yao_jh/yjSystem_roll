package io.renren.modules.currency.utils;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 通用工具类
 */
public class MeansUtils {

    /**
     * 获取单调区间
     * @param last
     * @param value
     * @return 0:上升;1:下降
     */
    public static Integer getMonotoneState(BigDecimal last, BigDecimal value) {
        Integer monotone = null;
        if(last != null){
            if(last.compareTo(value) < 0){
                monotone = 0;
            }else {
                monotone = 1;
            }
        }
        return monotone;
    }

    /**
     * 获取查询条件
     * @param code
     * @return
     */
    public static QueryParam getParamsByCode(String code){
        //查询条件
        Long crewId = queryCrewIdByCode(code);
        Long deviceId = null;
        if(crewId == null){
            DeviceEntity device = MainCatche.deviceMap.get(code);
            if(device != null){
                deviceId = device.getDeviceId();
                crewId = device.getCrewId();
            }else{
                return null;
            }
        }
        QueryParam params = new QueryParam();
        params.setCode(code);
        params.setCrewId(crewId);
        params.setDeviceId(deviceId);
        return params;
    }

    /**
     * 获取机组ID
     * @param code
     * @return
     */
    public static Long queryCrewIdByCode(String code) {
        CrewEntity crewEntity = null;
        if(CommonUtils.isNull(code)){
            return null;
        }
        //遍历查找机组
        for(CrewEntity crew : MainCatche.crewMap.values()){
            if(code.equals(crew.getCode())){
                crewEntity = crew;
                break;
            }
        }
        if(crewEntity != null){
            return crewEntity.getCrewId();
        }
        return null;
    }

    /**
     * 获取缓冲参数实体
     * @param deviceCode
     * @param classCode
     * @return
     */
    public static PointEntity getCatchePoint(String deviceCode, String classCode){
        PointEntity point = null;
        Map<String, PointEntity> dataMap =  MainCatche.dataMap.get(deviceCode);
        if(dataMap != null){
            point = dataMap.get(classCode);
        }
        return point;
    }

}
