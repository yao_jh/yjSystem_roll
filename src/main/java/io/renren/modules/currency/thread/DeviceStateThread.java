package io.renren.modules.currency.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.service.DeviceService;


/**
 * 更新设备状态
 */
public class DeviceStateThread extends Thread{

    private DeviceEntity device;

    public DeviceStateThread(){
        super();
    }

    public DeviceStateThread(DeviceEntity device){
        this.device = device;
    }

    @Override
    public void run() {
        synchronized (device){
            // 更新状态
            //获取spring bean
            DeviceService deviceService = (DeviceService) SpringContextUtils.getBean("deviceService");

            deviceService.update(device);
        }
    }
}