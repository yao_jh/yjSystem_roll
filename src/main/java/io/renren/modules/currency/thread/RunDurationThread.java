package io.renren.modules.currency.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import io.renren.modules.currency.service.DeviceRunDurationService;
import io.renren.modules.rules.utils.RunDurationRulesUtils;

import java.util.Date;

/**
 * 设备运行时长
 */
public class RunDurationThread extends Thread{

    private DeviceRunDurationEntity runDuration;

    public RunDurationThread(){
        super();
    }

    public RunDurationThread(DeviceRunDurationEntity runDuration){
        this.runDuration = runDuration;
    }

    @Override
    public void run() {
        synchronized (runDuration){
            // 保存
            runDuration.setDate(new Date());
            //获取spring bean
            DeviceRunDurationService deviceRunDurationService = (DeviceRunDurationService) SpringContextUtils.getBean("deviceRunDurationService");
            deviceRunDurationService.save(runDuration);
            //检查运行时间报警
            RunDurationRulesUtils.checkRulesWarning(runDuration);
        }
    }
}