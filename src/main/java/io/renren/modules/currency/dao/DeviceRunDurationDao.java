
package io.renren.modules.currency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * 设备运行时长Dao
 *
 */
@Mapper
public interface DeviceRunDurationDao extends BaseMapper<DeviceRunDurationEntity> {

    /**
     * 统计设备时长和次数
     * @param deviceId
     * 0:停止; 1:运行
     * @return
     */
    List<Map> countDurationByState(Long deviceId);
}
