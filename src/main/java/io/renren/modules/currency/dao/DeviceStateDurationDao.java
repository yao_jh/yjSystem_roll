
package io.renren.modules.currency.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.currency.entity.DeviceStateDurationEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 设备状态信息Dao
 *
 */
@Mapper
public interface DeviceStateDurationDao extends BaseMapper<DeviceStateDurationEntity> {

    /**
     * 设备状态时长
     * @param deviceId
     * @param state
     * @return
     */
    Long countDurationByState(Long deviceId, Integer state);
}
