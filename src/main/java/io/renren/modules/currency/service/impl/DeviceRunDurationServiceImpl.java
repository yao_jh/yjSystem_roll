
package io.renren.modules.currency.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.currency.dao.DeviceRunDurationDao;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import io.renren.modules.currency.service.DeviceRunDurationService;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.warning.entity.WarningLogEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("deviceRunDurationService")
public class DeviceRunDurationServiceImpl extends ServiceImpl<DeviceRunDurationDao, DeviceRunDurationEntity> implements DeviceRunDurationService {

    @Override
    public Map countDurationByState(Long deviceId) {
        // 返回次数统计
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("stopDuration",0L);//停机时长
        map.put("runDuration",0L);//运行时长
        map.put("stopNums",0L);//停机次数
        map.put("runNums",0L);//运行次数

        //查询开机和停机次数记录
        List<Map> list = baseMapper.countDurationByState(deviceId);
        if(list == null || list.isEmpty()){
            return map;
        }

        //更新次数
        String state = null;
        Long duration = 0L;
        Long nums = 0L;
        for(Map res : list){
            duration = Long.parseLong(res.get("duration").toString());
            nums = Long.parseLong(res.get("nums").toString());
            // 0:停机;1:运行
            state = res.get("state").toString();
            if("0".equals(state)){
                map.put("stopDuration", duration);//停机时长
                map.put("stopNums", nums);//停机次数
            }else{
                map.put("runDuration", duration);//运行时长
                map.put("runNums", nums);//运行次数
            }
        }

        return map;
    }

    @Override
    public PageUtils queryPageDownListByParam(Map<String, Object> params) {
        Long deviceId = CommonUtils.getLongByParamMap(params, "deviceId");
        if(deviceId == null){
            return null;
        }
        IPage<DeviceRunDurationEntity> page = this.page(
                new Query<DeviceRunDurationEntity>().getPage(params),
                new QueryWrapper<DeviceRunDurationEntity>().eq("device_id", deviceId).between("date", params.get("startDateStr"), params.get("endDateStr")).orderByDesc("date")
        );
        return new PageUtils(page);
    }
}
