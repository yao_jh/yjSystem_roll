
package io.renren.modules.currency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.currency.entity.DeviceStateDurationEntity;

/**
 * 设备状态记录接口
 *
 */
public interface DeviceStateDurationService extends IService<DeviceStateDurationEntity> {

    /**
     * 设备状态时长
     * @param deviceId
     * @param state
     * @return
     */
    Long countDurationByState(Long deviceId, Integer state);
}
