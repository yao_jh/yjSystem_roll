
package io.renren.modules.currency.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.currency.dao.DeviceStateDurationDao;
import io.renren.modules.currency.entity.DeviceStateDurationEntity;
import io.renren.modules.currency.service.DeviceStateDurationService;
import org.springframework.stereotype.Service;


@Service("deviceStateDurationService")
public class DeviceStateDurationServiceImpl extends ServiceImpl<DeviceStateDurationDao, DeviceStateDurationEntity> implements DeviceStateDurationService {

    @Override
    public Long countDurationByState(Long deviceId, Integer state) {
        return baseMapper.countDurationByState(deviceId, state);
    }
}
