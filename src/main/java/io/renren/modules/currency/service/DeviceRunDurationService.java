
package io.renren.modules.currency.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;

import java.util.Map;

/**
 * 设备运行时长接口
 *
 */
public interface DeviceRunDurationService extends IService<DeviceRunDurationEntity> {

    /**
     * 统计设备运行时长和次数
     * @param deviceId
     * 0:停止; 1:运行
     * @return
     */
    Map countDurationByState(Long deviceId);

    /**
     * 查询停机明细
     * @param pageMap
     * @return
     */
    PageUtils queryPageDownListByParam(Map<String, Object> pageMap);
}
