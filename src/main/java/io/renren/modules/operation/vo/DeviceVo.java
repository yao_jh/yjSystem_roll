package io.renren.modules.operation.vo;

import io.renren.modules.operation.entity.DeviceEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DeviceVo {
    //编号
    private String code;
    //名称
    private String name;
    //设备ID
    private Long deviceId;
    //配置ID
    private Long configId;
    //状态
    private Integer state;
    //备注
    private String remark;

    public DeviceVo (){
        super();
    }

    public DeviceVo (DeviceEntity deviceEntity){
        this.deviceId = deviceEntity.getDeviceId();
        this.code = deviceEntity.getCode();
        this.name = deviceEntity.getName();
    }


}
