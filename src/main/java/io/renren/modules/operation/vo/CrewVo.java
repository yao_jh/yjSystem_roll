package io.renren.modules.operation.vo;

import io.renren.modules.operation.entity.CrewEntity;
import lombok.Data;

import java.util.List;

@Data
public class CrewVo {
    //机组
    private Long crewId;
    //编号
    private String code;
    //名称
    private String name;
    //设备列表
    private List<DeviceVo> deviceVoList;

    public CrewVo(CrewEntity crewEntity){
        this.crewId = crewEntity.getCrewId();
        this.code = crewEntity.getCode();
        this.name = crewEntity.getName();
    }
}
