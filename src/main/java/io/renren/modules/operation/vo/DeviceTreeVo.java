package io.renren.modules.operation.vo;

import lombok.Data;

@Data
public class DeviceTreeVo {
    //设备ID
    private String deviceId;
    //设备名称
    private String deviceName;
    //设备编号
    private String deviceCode;
    //状态
    private Integer status;
    //模型编号
    private String modelCode;
    //模型名称
    private String modelName;

}
