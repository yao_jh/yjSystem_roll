package io.renren.modules.operation.vo;

import lombok.Data;

import java.util.List;

/**
 * 树结构
 */
@Data
public class TreeVo {
    //编码
    private String code;
    //文本
    private String label;
    //图标
    private String iconClass;
    //子节点
    private List<TreeVo> children;
}
