package io.renren.modules.operation.vo;

import io.renren.modules.operation.entity.PlantEntity;
import lombok.Data;

import java.util.List;

@Data
public class PlantVo {
    //产线ID
    private Long plantId;
    //产线名称
    private String name;
    //机组列表
    private List<CrewVo> crewVoList;

    public PlantVo(PlantEntity plant){
        this.plantId = plant.getPlantId();
        this.name = plant.getName();
    }
}
