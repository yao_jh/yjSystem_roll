package io.renren.modules.operation.vo;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.operation.entity.PointEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 参数点视图
 */
@Data
public class PointVo {

    /**
     * 唯一编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 单位
     */
    private String unit;

    /**
     * 类编号
     */
    private String classCode;

    /**
     * 更新时间
     */
    private Date realDate;

    /**
     * 转义
     * 0:开;1:关
     */
    private String expValue;

    /**
     * 实时值
     */
    private BigDecimal value;

    /**
     * 实时值字符串
     */
    private String realValue;

    /**
     * 时间字符
     */
    private String dateStr;

    public PointVo(){
        super();
    }

    public PointVo(PointEntity point){
        BeanUtils.copyProperties(point, this);
    }


    public String getRealValue() {
        if(CommonUtils.isNotNull(expValue)){
            realValue = CommonUtils.resValExp(value, expValue);
        }else{
            realValue = value.toString();
        }
        return realValue;
    }

    public String getDateStr() {
        if(CommonUtils.isNull(dateStr)){
            if(realDate != null)
                dateStr = DateUtils.format(realDate, DateUtils.DATE_MSTIME_PATTERN);
        }
        return dateStr;
    }

}
