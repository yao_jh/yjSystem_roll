
package io.renren.modules.operation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.operation.dao.CrewDao;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.service.CrewService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("crewService")
public class CrewServiceImpl extends ServiceImpl<CrewDao, CrewEntity> implements CrewService {

	@Override
	public List<CrewEntity> queryListByPlantId(Long plantId) {
		return baseMapper.queryListByPlantId(plantId);
	}

	@Override
	public CrewEntity queryCrewEntityByCode(String code) {
		CrewEntity crewEntity = null;
		if(CommonUtils.isNull(code)){
			return null;
		}
		//遍历查找机组
		for(CrewEntity crew : MainCatche.crewMap.values()){
			if(code.equals(crew.getCode())){
				crewEntity = crew;
				break;
			}
		}
		return crewEntity;
	}

	@Override
	public String getCrewCodeById(Long crewId) {
		CrewEntity crew = MainCatche.crewMap.get(crewId);
		if(crew != null){
			return crew.getCode();
		}
		return  null;
	}

}
