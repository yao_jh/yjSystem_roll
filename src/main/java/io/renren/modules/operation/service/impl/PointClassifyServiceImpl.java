
package io.renren.modules.operation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.operation.dao.PointClassifyDao;
import io.renren.modules.operation.entity.PointClassifyEntity;
import io.renren.modules.operation.service.PointClassifyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("pointClassifyService")
public class PointClassifyServiceImpl extends ServiceImpl<PointClassifyDao, PointClassifyEntity> implements PointClassifyService {

	@Override
	public List<PointClassifyEntity> queryListByParams(Map<String, Object> params) {
		return baseMapper.queryListByParams(params);
	}
}
