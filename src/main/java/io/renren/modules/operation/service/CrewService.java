
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.operation.entity.CrewEntity;

import java.util.List;

/**
 * 机组接口
 *
 */
public interface CrewService extends IService<CrewEntity> {

	/**
	 * 获取产线的机组信息
	 * @param plantId
	 * @return
	 */
	List<CrewEntity> queryListByPlantId(Long plantId);

	/**
	 * 根据Code获取机组
	 * @param code
	 * @return
	 */
	CrewEntity queryCrewEntityByCode(String code);

	/**
	 * 根据Id获取机组Code
	 * @param crewId
	 * @return
	 */
	String getCrewCodeById(Long crewId);

}
