
package io.renren.modules.operation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.operation.dao.PlantDao;
import io.renren.modules.operation.entity.PlantEntity;
import io.renren.modules.operation.service.PlantService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("plantService")
public class PlantServiceImpl extends ServiceImpl<PlantDao, PlantEntity> implements PlantService {

	@Override
	public List<PlantEntity> queryListByUserId(Long userId) {
		return baseMapper.queryListByUserId(userId);
	}

}
