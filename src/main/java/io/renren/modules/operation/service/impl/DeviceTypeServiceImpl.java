
package io.renren.modules.operation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.modules.operation.dao.DeviceTypeDao;
import io.renren.modules.operation.entity.DeviceTypeEntity;
import io.renren.modules.operation.service.DeviceTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("deviceTypeService")
public class DeviceTypeServiceImpl extends ServiceImpl<DeviceTypeDao, DeviceTypeEntity> implements DeviceTypeService {

	@Override
	public List<DeviceTypeEntity> queryListByCode(String code) {
		return baseMapper.queryListByCode(code);
	}

}
