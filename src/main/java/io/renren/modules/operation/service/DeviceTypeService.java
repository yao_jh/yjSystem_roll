
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.DeviceTypeEntity;

import java.util.List;
import java.util.Map;


/**
 * 设备类型接口
 *
 */
public interface DeviceTypeService extends IService<DeviceTypeEntity> {

    /**
     * 查询设备类型
     * @param code
     * @return
     */
    List<DeviceTypeEntity> queryListByCode(String code);

}
