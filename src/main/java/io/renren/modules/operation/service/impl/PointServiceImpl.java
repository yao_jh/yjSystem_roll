
package io.renren.modules.operation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.dao.PointDao;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.DeviceService;
import io.renren.modules.operation.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Service("pointService")
public class PointServiceImpl extends ServiceImpl<PointDao, PointEntity> implements PointService {

	@Autowired
	private DeviceService deviceService;

	@Override
	public List<PointEntity> queryListByDeviceId(Long deviceId) {
		return baseMapper.queryListByDeviceId(deviceId);
	}

	@Override
	public List<PointEntity> queryListByParams(QueryParam params) {
		return baseMapper.queryListByParams(params);
	}

	@Override
	public List<PointEntity> queryListByLikeClassCode(Long deviceId, String likeCode) {
		return baseMapper.queryListByLikeClassCode(deviceId, likeCode);
	}

	@Override
	public List<PointEntity> queryListByCrewLikeClassCode(Long crewId, String likeCode) {
		return baseMapper.queryListByCrewLikeClassCode(crewId, likeCode);
	}

	@Override
	public List<PointEntity> queryListByMode(Long deviceId, Integer mode) {
		return baseMapper.queryListByMode(deviceId, mode);
	}

	@Override
	public List<PointEntity> queryListForAnalysisByParams(QueryParam params) {
		return baseMapper.queryListForAnalysisByParams(params);
	}

	@Override
	public boolean saveOrUpdate(PointEntity entity) {
		boolean flag = false;
		completionAttributeForPoint(entity);

		if(entity.getPointId() == null){
			flag = SqlHelper.retBool(baseMapper.insert(entity));
			toSaveCatcheMap(entity);
		}else{
			//处理掉空属性(空属性不更新，保留原来的属性)
			entity = toUpdateCatcheMap(entity);
			//保存修改
			flag = SqlHelper.retBool(baseMapper.updateById(entity));

		}
		return flag;
	}

	/**
	 * 补全设备属性
	 * @param entity
	 */
	private void completionAttributeForPoint(PointEntity entity){
		String deviceCode = entity.getDeviceCode();
		Long deviceId = entity.getDeviceId();
		//补全Code
		if(deviceCode == null){
			if(deviceId != null){
				DeviceEntity device = deviceService.queryEntityById(deviceId);
				deviceCode = device.getCode();
				entity.setDeviceCode(deviceCode);
			}
		}else if(deviceId == null){
			//补全Id
			if(deviceCode != null){
				DeviceEntity device = MainCatche.deviceMap.get(deviceCode);
				deviceId = device.getDeviceId();
				entity.setDeviceId(deviceId);
			}
		}
	}

	/**
	 * 保存参数缓存
	 * @param entity
	 */
	private void toSaveCatcheMap(PointEntity entity){
		Map<String, PointEntity> dataMap = MainCatche.dataMap.get(entity.getDeviceCode());
		entity.setValue(BigDecimal.ZERO);
		if(dataMap != null){
			dataMap.put(entity.getClassCode(), entity);
		}else{
			dataMap = new ConcurrentHashMap<String, PointEntity>();
			dataMap.put(entity.getClassCode(), entity);
			//保存到内存
			MainCatche.dataMap.put(entity.getDeviceCode(), dataMap);
			MainCatche.realMap.put(entity.getCode(), entity);
		}
	}

	/**
	 * 更新参数缓存
	 * @param entity
	 */
	private PointEntity toUpdateCatcheMap(PointEntity entity){
		//处理后参数
		PointEntity target = MainCatche.realMap.get(entity.getCode());
		if(target != null){
			CommonUtils.copyBeanFieldsValue(entity, target);
			//dataMap.put(entity.getClassCode(), target);
		}
		return target;
	}


}
