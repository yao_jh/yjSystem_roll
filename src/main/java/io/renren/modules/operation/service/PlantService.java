
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.operation.entity.PlantEntity;

import java.util.List;

/**
 * 产线
 *
 */
public interface PlantService extends IService<PlantEntity> {

	/**
	 * 获取用户的产线
	 * @param userId
	 * @return
	 */
	List<PlantEntity> queryListByUserId(Long userId);

}
