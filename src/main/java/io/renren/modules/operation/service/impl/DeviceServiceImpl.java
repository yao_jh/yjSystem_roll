
package io.renren.modules.operation.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.iot.exception.BadAuthorizedException;
import io.renren.common.sys.MainCatche;
import io.renren.common.sys.Topic;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.currency.entity.DeviceStateDurationEntity;
import io.renren.modules.operation.dao.DeviceDao;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.DeviceTypeEntity;
import io.renren.modules.operation.entity.PlantEntity;
import io.renren.modules.operation.service.DeviceService;
import io.renren.modules.operation.service.DeviceTypeService;
import io.renren.modules.operation.service.PlantService;
import io.renren.modules.operation.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("deviceService")
public class DeviceServiceImpl extends ServiceImpl<DeviceDao, DeviceEntity> implements DeviceService {

	@Autowired
	private PlantService plantService;

	@Autowired
	private DeviceTypeService deviceTypeService;


	@Override
	public List<DeviceEntity> queryListByCrewId(Long crewId) {
		Map<String, DeviceEntity> map = MainCatche.deviceMap;
		List<DeviceEntity> list = new ArrayList<DeviceEntity>();
		//遍历map中的值
		for (DeviceEntity device : map.values()) {
			if(crewId == device.getCrewId()){
				list.add(device);
			}
		}
		return list;

	}

	@Override
	public List<DeviceEntity> queryListByCrewIdAndTypeCode(Long crewId, String typeCode) {
		Map<String, DeviceEntity> map = MainCatche.deviceMap;
		List<DeviceEntity> list = new ArrayList<DeviceEntity>();
		//遍历map中的值
		for (DeviceEntity device : map.values()) {
			if(crewId == device.getCrewId()){
				if(typeCode.equals(device.getTypeCode())){
					list.add(device);
				}
			}
		}
		return list;
	}

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		Long crewId = CommonUtils.getLongByParamMap(params, "crewId");
		if(crewId == null){
			return null;
		}
		IPage<DeviceEntity> page = this.page(
				new Query<DeviceEntity>().getPage(params),
				new QueryWrapper<DeviceEntity>().eq("crew_id", crewId)
		);
		return new PageUtils(page);
	}


	@Override
	public List<TreeVo> queryTreeVoList(Long userId, String typeCode) {
		if(CommonUtils.isNull(typeCode)){
			return null;
		}
		//从产线开始，形成树结构
		List<PlantEntity> plantList = plantService.queryListByUserId(userId);
		//返回设备树
		return getTreeVoForPlants(plantList, typeCode);
	}

	@Override
	public List<TreeVo> queryTreeVoListFromIot(String typeCode) {
		if(CommonUtils.isNull(typeCode)){
			return null;
		}
		////从产线开始，形成树结构
		List<PlantEntity> plantList = new ArrayList<PlantEntity>();
		List<DeviceTypeEntity> list = deviceTypeService.queryListByCode(typeCode);
		if(list != null){
			//地址
			String address = "/saas-basis/api/tree/device/list-by-model-code";
			//请求参数
			JSONObject param = new JSONObject();
			DeviceTypeEntity obj = list.get(0);
			param.put("modelCode", obj.getCode());
			try{
				JSONObject jsonObject = Topic.getIotRestRequest(param, address);
				if(jsonObject != null && "success".equals(jsonObject.getString("code"))){
					// 返回权限产线
					plantList = getPlantListByDeviceJson(jsonObject);
				}
			} catch (BadAuthorizedException e) {
				e.printStackTrace();
			}

		}
		return getTreeVoForPlants(plantList, typeCode);
	}

	@Override
	public List<DeviceEntity> queryListByPlantIdAndType(Long plantId, String deviceType) {
		Map<String, DeviceEntity> map = MainCatche.deviceMap;
		List<DeviceEntity> list = new ArrayList<DeviceEntity>();
		CrewEntity crew = null;
		//遍历map中的值
		for (DeviceEntity device : map.values()) {
			crew = MainCatche.crewMap.get(device.getCrewId());
			if(plantId == crew.getPlantId()){
				if(deviceType.equals(device.getTypeCode())){
					list.add(device);
				}
			}
		}
		return list;
	}

	/**
	 * 获取产线设备树
	 * @param plantList
	 * @param typeCode
	 * @return
	 */
	private List<TreeVo> getTreeVoForPlants(List<PlantEntity> plantList, String typeCode){
		//返回设备树
		List<TreeVo> list = new ArrayList<TreeVo>();
		TreeVo treeVo= null;
		for(PlantEntity plant : plantList){
			treeVo = new TreeVo();
			treeVo.setCode(plant.getCode());
			treeVo.setLabel(plant.getName());
			treeVo.setIconClass("icon-shebeishu1");
			treeVo.setChildren(getCrewTreeByPlantId(plant.getPlantId(), typeCode));
			list.add(treeVo);
		}
		return list;
	}

	/**
	 * 获取产线机组树
	 * @param plantId
	 * @return
	 */
	private List<TreeVo> getCrewTreeByPlantId(Long plantId, String typeCode){
		List<TreeVo> list = new ArrayList<TreeVo>();
		Map<Long, CrewEntity> crewMap = MainCatche.crewMap;
		TreeVo treeVo = null;
		//二级机组
		for(CrewEntity crew : crewMap.values()){
			if(crew.getPlantId().compareTo(plantId) == 0){
				treeVo = new TreeVo();
				treeVo.setCode(crew.getCode());
				treeVo.setLabel(crew.getName());
				treeVo.setCode(crew.getCode());
				treeVo.setIconClass("icon-shebeishu");
				//三级设备
				if(crew.getType() == null){
					List<TreeVo> child = getDeviceTreeByCrewId(crew.getCrewId(), typeCode);
					treeVo.setChildren(child);
				}
				list.add(treeVo);
				treeVo = null;
			}
		}
		if(list.isEmpty()){
			return null;
		}
		return list;
	}

	/**
	 * 获取机组设备结构
	 * @param crewId
	 * @return
	 */
	private List<TreeVo> getDeviceTreeByCrewId(Long crewId, String typeCode){
		List<TreeVo> list = new ArrayList<TreeVo>();
		Map<String, DeviceEntity> deviceMap = MainCatche.deviceMap;
		TreeVo treeVo = null;
		//遍历设备,查找隶属机组的固定类型设备
		for(DeviceEntity device : deviceMap.values()){
			if(device.getCrewId().compareTo(crewId) == 0){
				if(typeCode.equals(device.getTypeCode())){
					treeVo = new TreeVo();
					treeVo.setLabel(device.getName());
					treeVo.setCode(device.getCode());
					list.add(treeVo);
					treeVo = null;
				}
			}
		}
		if(list.isEmpty()){
			return null;
		}
		return list;
	}

	@Override
	public DeviceEntity queryEntityById(Long id) {
		DeviceEntity deviceEntity = null;
		Map<String, DeviceEntity> map  = MainCatche.deviceMap;
		for(DeviceEntity device: map.values()){
			if(device.getDeviceId().equals(id)){
				deviceEntity = device;
				break;
			}
		}
		return deviceEntity;
	}

	/**
	 * 修改设备配置
	 * @param device
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(DeviceEntity device) {
		if(device.getOnline() != null){
			DeviceEntity device2 = MainCatche.deviceMap.get(device.getCode());
			if(device.getOnline().compareTo(device2.getOnline()) != 0){
				//保存在线状态
				DeviceStateDurationEntity stateDuration = new DeviceStateDurationEntity(device);
			}
		}
		this.updateById(device);
	}


	/**
	 * 根据设备查找产线
	 * @param json
	 * @return
	 */
	private List<PlantEntity> getPlantListByDeviceJson(JSONObject json){
		//解析json
		//JSONObject json = JSONObject.parseObject(jsonStr);
		String data = json.getString("data");
		//json转list
		List<DeviceTreeVo> list = JSONArray.parseArray(data, DeviceTreeVo.class);
		List<PlantEntity> all = plantService.list();
		int size = all.size();
		all = null;
		DeviceEntity device = null;
		Map<Long, Long> map = new HashMap<Long, Long>();
		CrewEntity crew = null;
		//遍历查找权限产线
		for(DeviceTreeVo vo : list){
			device = MainCatche.deviceMap.get(vo.getDeviceCode());
			crew = MainCatche.crewMap.get(device.getCrewId());
			//添加产线
			if(!map.containsKey(crew.getPlantId())){
				map.put(crew.getPlantId(), crew.getPlantId());
			}
			if(map.size() >= size){
				break;
			}
		}
		//获取产线列表
		List<PlantEntity> plantList = new ArrayList<PlantEntity>();
		PlantEntity plantEntity = null;
		for(Long id : map.values()){
			plantEntity = plantService.getById(id);
			plantList.add(plantEntity);
			plantEntity = null;
		}
		return plantList;
	}


	/**
	 * 单参数Map
	 * @param key
	 * @param value
	 * @return
	 */
	private Map<String, String> getSingleParamMap(String key, String value){
		Map<String, String> param = new HashMap<String, String>();
		param.put(key,value);
		return param;
	}


}
