
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.operation.entity.PointClassifyEntity;

import java.util.List;
import java.util.Map;

/**
 * 归类接口
 *
 */
public interface PointClassifyService extends IService<PointClassifyEntity> {

	/**
	 * 获取归类信息
	 * @param params
	 * @return
	 */
	List<PointClassifyEntity> queryListByParams(Map<String, Object> params);

}
