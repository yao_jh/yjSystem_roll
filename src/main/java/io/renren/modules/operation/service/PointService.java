
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.PointEntity;

import java.util.List;

/**
 * 参数接口
 *
 */
public interface PointService extends IService<PointEntity> {

    /**
     * 获取设备参数
     * @param deviceId
     * @return
     */
	List<PointEntity> queryListByDeviceId(Long deviceId);

    /**
     * 获取设备类型参数
     * @param params
     * @return
     */
    List<PointEntity> queryListByParams(QueryParam params);

    /**
     * 获取参数类型，模糊查询类型
     * @param deviceId
     * @param classCode
     * @return
     */
    List<PointEntity> queryListByLikeClassCode(Long deviceId, String classCode);

    /**
     * 获取参数类型，模糊查询类型
     * @param crewId
     * @param classCode
     * @return
     */
    List<PointEntity> queryListByCrewLikeClassCode(Long crewId, String classCode);

    /**
     * 获取设备参数
     * @param deviceId
     * @return
     */
    List<PointEntity> queryListByMode(Long deviceId, Integer mode);

    /**
     * 获取分析对象参数点
     * @param param
     * @return
     */
    List<PointEntity> queryListForAnalysisByParams(QueryParam param);

    /**
     * 保存或修改点信息
     * @param entity
     * @return
     */
    boolean saveOrUpdate(PointEntity entity);

}
