
package io.renren.modules.operation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iot.exception.BadAuthorizedException;
import io.renren.common.utils.PageUtils;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.vo.DeviceVo;
import io.renren.modules.operation.vo.TreeVo;

import java.util.List;
import java.util.Map;

/**
 * 设备接口
 *
 */
public interface DeviceService extends IService<DeviceEntity> {

	/**
	 * 获取机组设备列表
	 * @param crewId
	 * @return
	 */
	List<DeviceEntity> queryListByCrewId(Long crewId);

	/**
	 * 获取机组设备列表
	 * @param crewId
	 * @return
	 */
	List<DeviceEntity> queryListByCrewIdAndTypeCode(Long crewId, String typeCode);

	/**
	 * 获取分页的设备信息
	 * @param params
	 * @return
	 */
	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 根据Id获取实体
	 * @param id
	 * @return
	 */
	DeviceEntity queryEntityById(Long id);

	/**
	 * 修改配置
	 * @param device
	 */
	void update(DeviceEntity device);

	/**
	 * 获取设备树
	 * @param userId
	 * @param typeCode
	 * @return
	 */
	List<TreeVo> queryTreeVoList(Long userId, String typeCode);


	/**
	 * 获取设备树从平台
	 * @param typeCode
	 * @return
	 */
	List<TreeVo> queryTreeVoListFromIot(String typeCode);

	/**
	 * 获取产线的类型设备
	 * @param plantId
	 * @param deviceType
	 * @return
	 */
	List<DeviceEntity> queryListByPlantIdAndType(Long plantId, String deviceType);
}
