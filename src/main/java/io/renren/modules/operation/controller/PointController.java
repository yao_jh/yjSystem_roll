
package io.renren.modules.operation.controller;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.R;
import io.renren.modules.operation.entity.PointClassifyEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 参数Controller
 *
 */
@RestController
@RequestMapping("/operation/point")
@Api("参数接口")
public class PointController extends AbstractController {

	@Autowired
	private PointService pointService;

	/**
	 * 获取设备参数列表
	 */
	@GetMapping("/select")
	@ApiOperation("获取参数列表")
	@ApiImplicitParam(name="deviceId",value="设备ID",dataType="Long", paramType = "query",example="1")
	public R select(Long deviceId){
		List<PointEntity> list = pointService.queryListByDeviceId(deviceId);
		return R.ok().put("list", list);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@ApiOperation("参数保存")
	public R save(@RequestBody PointEntity point){
		pointService.saveOrUpdate(point);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("参数修改")
	public R update(@RequestBody PointEntity point){
		pointService.saveOrUpdate(point);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("参数删除")
	public R delete(Long id, String code){
		pointService.removeById(id);
		MainCatche.dataMap.remove(code);
		return R.ok();
	}

}
