
package io.renren.modules.operation.controller;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.service.DeviceService;
import io.renren.modules.operation.vo.TreeVo;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 设备Controller
 *
 */
@RestController
@RequestMapping("/operation/device")
@Api("设备接口")
public class DeviceController extends AbstractController {

	@Autowired
	private DeviceService deviceService;

	/**
	 * 设备列表
	 */
	@GetMapping("/select")
	@ApiOperation("设备列表")
	public R select(Long crewId){
		List<DeviceEntity> list = deviceService.queryListByCrewId(crewId);
		return R.ok().put("list", list);
	}

	/**
	 * 分页列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@ApiOperation("分页列表")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = deviceService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 设备树列表
	 * @param typeCode
	 * 	motor: 主电机
	 *
	 * @return
	 */
	@GetMapping("/selectTree")
	@ApiOperation("设备树列表")
	public R deviceTree(String typeCode){
		List<TreeVo> list = null;
		if(CommonUtils.isNull(typeCode)){
			R.error("无设备类型请求");
		}
		/*if(Constant.shiroUser){//iot平台
			deviceService.queryTreeVoListFromIot(typeCode);
		}else{//独立系统设备树*/
			Long userId = getUserId();
			if(userId == null) userId = 1L;
			list = deviceService.queryTreeVoList(userId, typeCode);
		//}
		return R.ok().put("list", list);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@ApiOperation("保存")
	public R save(@RequestBody DeviceEntity device){
		deviceService.save(device);
		MainCatche.deviceMap.put(device.getCode(), device);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("修改")
	public R update(@RequestBody DeviceEntity device){
		deviceService.update(device);
		MainCatche.deviceMap.put(device.getCode(), device);
		return R.ok();
	}

	/**
	 * 修改上下机状态
	 */
	@PostMapping("/updateOnline")
	@ApiOperation("修改上下机状态")
	public R updateOnline(@RequestBody DeviceEntity device){
		DeviceEntity deviceEntity = MainCatche.deviceMap.get(device.getDeviceId());
		CommonUtils.copyBeanFieldsValue(device, deviceEntity);
		deviceService.updateById(deviceEntity);
		MainCatche.deviceMap.put(deviceEntity.getCode(), deviceEntity);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("删除")
	public R delete(Long id, String code){
		deviceService.removeById(id);
		MainCatche.deviceMap.remove(code);
		return R.ok();
	}

}
