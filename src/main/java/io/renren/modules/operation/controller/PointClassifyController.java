
package io.renren.modules.operation.controller;

import io.renren.common.utils.R;
import io.renren.modules.operation.entity.PointClassifyEntity;
import io.renren.modules.operation.service.PointClassifyService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 参数归类Controller
 *
 */
@RestController
@RequestMapping("/operation/pointClassify")
@Api("参数归类")
public class PointClassifyController extends AbstractController {

	@Autowired
	private PointClassifyService pointClassifyService;


	/**
	 * 机组列表
	 */
	@GetMapping("/select")
	@ApiOperation("参数归类分页")
	public R select(@RequestParam Map<String, Object> params){
		List<PointClassifyEntity> list = pointClassifyService.queryListByParams(params);
		return R.ok().put("list", list);
	}

	@GetMapping("/list")
	@ApiOperation("参数归类列表")
	public R list(){
		List<PointClassifyEntity> list = pointClassifyService.list();
		return R.ok().put("list", list);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@ApiOperation("参数归类保存")
	public R save(@RequestBody PointClassifyEntity pointClassify){
		pointClassifyService.save(pointClassify);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("参数归类修改")
	public R update(@RequestBody PointClassifyEntity pointClassify){
		pointClassifyService.updateById(pointClassify);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("参数归类删除")
	public R delete(Long id){
		pointClassifyService.removeById(id);
		return R.ok();
	}

}
