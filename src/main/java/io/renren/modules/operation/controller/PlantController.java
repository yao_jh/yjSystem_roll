
package io.renren.modules.operation.controller;

import io.renren.common.utils.R;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.operation.entity.PlantEntity;
import io.renren.modules.operation.service.PlantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 产线
 *
 */
@RestController
@RequestMapping("/operation/plant")
@Api("产线")
public class PlantController extends AbstractController {

	@Autowired
	private PlantService plantService;


	/**
	 * 产线列表
	 */
	@GetMapping("/select")
	@ApiOperation("获取产线列表")
	public R select(){
		Long userId = getUserId();
		List<PlantEntity> list = plantService.queryListByUserId(userId);
		return R.ok().put("list", list);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@ApiOperation("保存产线")
	@ApiImplicitParams({
			@ApiImplicitParam(name="plantId",value="主键",dataType="Long", paramType = "query",example="motor1"),
			@ApiImplicitParam(name="name",value="名称",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="code",value="编码",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="type",value="类型0：冷轧   1：热轧",dataType="Integer", paramType = "query"),
			@ApiImplicitParam(name="param",value="产线参数",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="remark",value="备注",dataType="String", paramType = "query")
	})
	public R save(PlantEntity plant){
		plantService.save(plant);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("修改产线")
	@ApiImplicitParams({
			@ApiImplicitParam(name="plantId",value="主键",dataType="Long", paramType = "query",example="motor1"),
			@ApiImplicitParam(name="name",value="名称",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="code",value="编码",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="type",value="类型0：冷轧   1：热轧",dataType="Integer", paramType = "query"),
			@ApiImplicitParam(name="param",value="产线参数",dataType="String", paramType = "query"),
			@ApiImplicitParam(name="remark",value="备注",dataType="String", paramType = "query")
	})
	public R update(PlantEntity plant){
		plantService.updateById(plant);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("删除产线")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="主键",dataType="Long", paramType = "query",example="motor1")})
	public R delete(Long id){
		plantService.removeById(id);
		return R.ok();
	}

}
