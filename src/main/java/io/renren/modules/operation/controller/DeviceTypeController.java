
package io.renren.modules.operation.controller;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.DeviceTypeEntity;
import io.renren.modules.operation.service.DeviceService;
import io.renren.modules.operation.service.DeviceTypeService;
import io.renren.modules.operation.vo.TreeVo;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 设备Controller
 *
 */
@RestController
@RequestMapping("/operation/deviceType")
@Api("设备类型接口")
public class DeviceTypeController extends AbstractController {

	@Autowired
	private DeviceTypeService deviceTypeService;

	/**
	 * 设备类型列表
	 */
	@GetMapping("/select")
	@ApiOperation("设备类型列表")
	public R select(){
		List<DeviceTypeEntity> list = deviceTypeService.list();
		return R.ok().put("list", list);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@ApiOperation("保存")
	public R save(@RequestBody DeviceTypeEntity deviceType){
		deviceTypeService.save(deviceType);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("修改")
	public R update(@RequestBody DeviceTypeEntity deviceType){
		deviceTypeService.updateById(deviceType);
		return R.ok();
	}



	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("删除")
	public R delete(Long id, String code){
		deviceTypeService.removeById(id);
		return R.ok();
	}

}
