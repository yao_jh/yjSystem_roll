
package io.renren.modules.operation.controller;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.R;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 机组Controller
 *
 */
@RestController
@RequestMapping("/operation/crew")
@Api("机组")
public class CrewController extends AbstractController {

	@Autowired
	private CrewService crewService;

	/**
	 * 机组列表
	 */
	@GetMapping("/select")
	@ApiOperation("机组列表")
	public R select(Long plantId){
		List<CrewEntity> list = crewService.queryListByPlantId(plantId);
		return R.ok().put("list", list);
	}


	/**
	 * 新增
	 */
	@PostMapping("/save")
	@ApiOperation("新增机组")
	public R save(@RequestBody CrewEntity crew){
		crewService.save(crew);
		MainCatche.crewMap.put(crew.getCrewId(), crew);
		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperation("修改")
	public R update(@RequestBody CrewEntity crew){
		crewService.updateById(crew);
		MainCatche.crewMap.put(crew.getCrewId(), crew);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@ApiOperation("删除")
	public R delete(Long id){
		crewService.removeById(id);
		MainCatche.crewMap.remove(id);
		return R.ok();
	}

}
