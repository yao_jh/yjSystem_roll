
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.operation.entity.CrewEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 机组Dao
 *
 */
@Mapper
public interface CrewDao extends BaseMapper<CrewEntity> {

    List<CrewEntity> queryListByPlantId(Long plantId);

}
