
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.operation.entity.DeviceTypeEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * 设备类型Dao
 *
 */
@Mapper
public interface DeviceTypeDao extends BaseMapper<DeviceTypeEntity> {

    List<DeviceTypeEntity> queryListByCode(String code);

}
