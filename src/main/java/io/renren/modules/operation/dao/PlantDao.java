
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.operation.entity.PlantEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产线Dao
 *
 */
@Mapper
public interface PlantDao extends BaseMapper<PlantEntity> {

    /**
     * 产线列表
     * @param userId
     * @return
     */
    List<PlantEntity> queryListByUserId(Long userId);

    /**
     * 获取所有的产线信息
     * @return
     */
    List<PlantEntity> queryAll();
}
