
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.vo.DeviceVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 设备Dao
 *
 */
@Mapper
public interface DeviceDao extends BaseMapper<DeviceEntity> {

    /**
     * 获取机组的设备列表
     * @param crewId
     * @return
     */
    List<DeviceEntity> queryListByCrewId(Long crewId);

    /**
     * 获取用户设备
     * @param userId
     * @return
     */
    List<DeviceEntity> queryListByUserId(Long userId);

}
