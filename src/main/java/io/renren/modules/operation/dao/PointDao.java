
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.PointEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 参数Dao
 *
 */
@Mapper
public interface PointDao extends BaseMapper<PointEntity> {

    /**
     * 获取设备参数
     * @param deviceId
     * @return
     */
    List<PointEntity> queryListByDeviceId(Long deviceId);

    /**
     * 获取设备类型参数
     * @param deviceId
     * @param classCode
     * @return
     */
    List<PointEntity> queryListByLikeClassCode(Long deviceId, String classCode);


    /**
     * 获取机组类型参数
     * @param crewId
     * @param classCode
     * @return
     */
    List<PointEntity> queryListByCrewLikeClassCode(Long crewId, String classCode);

    /**
     * 获取设备模拟/开关点
     * @param deviceId
     * @param mode
     * @return
     */
    List<PointEntity> queryListByMode(Long deviceId, Integer mode);

    /**
     * 获取分析对象参数点
     * @param params
     * @return
     */
    List<PointEntity> queryListForAnalysisByParams(@Param("params") QueryParam params);

    /**
     * 查询设备的类型参数
     * @param params
     * @return
     */
    List<PointEntity> queryListByParams(@Param("params") QueryParam params);


}
