
package io.renren.modules.operation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.operation.entity.PointClassifyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 归类Dao
 *
 */
@Mapper
public interface PointClassifyDao extends BaseMapper<PointClassifyEntity> {

    List<PointClassifyEntity> queryListByParams(@Param("params") Map<String, Object> params);

}
