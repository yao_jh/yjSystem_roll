package io.renren.modules.operation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 机组管理
 * 机组按照生产工序排列
 */
@Data
@TableName("tb_crew")
@EqualsAndHashCode(callSuper = false)
public class CrewEntity extends FrameBase implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 产线ID
	 */
	@TableId
	private Long crewId;

	/**
	 * 类型
	 * mill:轧机
	 */
	private String type;

	/**
	 * 产线ID
	 */
	private Long plantId;

	/**
	 * 质量
	 * 轧机的机架轧制力等，默认标定数值
	 */
	private BigDecimal quantity;

	/**
	 * 是否有效
	 * 0:有效;1:删除
	 */
	private Integer hidden;

	/**
	 * 动作状态
	 */
	@Transient
	private transient Integer state;

}
