package io.renren.modules.operation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 产线管理
 *
 */
@Data
@TableName("tb_plant")
public class PlantEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 产线ID
	 */
	@TableId
	private Long plantId;

	/**
	 * 编码
	 */
	private String code;

	/**
	 * 产线名称
	 */
	@NotBlank(message="名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String name;

	/**
	 * 类型	0：冷轧   1：热轧
	 */
	private Integer type;

	/**
	 * 产线参数
	 */
	private String param;

	/**
	 * 备注
	 */
	private String remark;

}
