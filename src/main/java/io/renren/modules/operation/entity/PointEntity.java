package io.renren.modules.operation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 参数管理
 */
@Data
@TableName("tb_point")
public class PointEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long pointId;

	/**
	 * 编号
	 */
	@NotBlank(message="编号不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String code;

	/**
	 * 名称
	 */
	@NotBlank(message="名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String name;

	/**
	 * 类型码
	 */
	private String classCode;

	/**
	 * 设备ID
	 */
	private Long deviceId;

	/**
	 * 设备Code
	 * 添加设备Code字段，减少耦合
	 */
	private String deviceCode;

	/**
	 * 机组ID
	 * 添加机组ID字段，减少耦合
	 */
	private Long crewId;

	/**
	 * 单位
	 */
	private String unit;

	/**
	 * 数据类型
	 */
	private String dataType;

	/**
	 * 点地址
	 */
	private String address;

	/**
	 * 保留小数位数
	 */
	private Integer format;

	/**
	 * 转换公式
	 */
	private String express;

	/**
	 * 转义
	 * 0:开;1:关
	 */
	private String expValue;

	/**
	 * 类型
	 * 0:AI;1:DI
	 */
	private Integer mode;

	/**
	 * 更新时间
	 */
	@ApiModelProperty(hidden = true)
	private Date realDate;

	/**
	 * 实时值
	 */
	private BigDecimal value;

}
