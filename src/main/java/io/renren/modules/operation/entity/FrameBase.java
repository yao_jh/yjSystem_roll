package io.renren.modules.operation.entity;

import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotBlank;

/**
 * 设备机组基础类
 */
@Data
public class FrameBase {
    /**
     * 机组编号
     */
    @NotBlank(message="编号不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String code;

    /**
     * 机组名称
     */
    @NotBlank(message="名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;

    /**
     * 备注
     */
    private String remark;


    //平台对接ID
    @Transient
    private transient String dockId;

}
