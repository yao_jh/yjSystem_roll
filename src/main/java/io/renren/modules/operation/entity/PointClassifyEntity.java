package io.renren.modules.operation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 参数归类管理
 */
@Data
@TableName("tb_point_classify")
public class PointClassifyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long classId;

	/**
	 * 编号
	 */
	@NotBlank(message="编号不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String code;

	/**
	 * 名称
	 */
	@NotBlank(message="名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String name;

	/**
	 * 备注
	 */
	private String remark;

}
