package io.renren.modules.operation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 设备管理
 */
@Data
@TableName("tb_device")
@EqualsAndHashCode(callSuper = false)
public class DeviceEntity extends FrameBase implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 设备ID
	 */
	@TableId
	private Long deviceId;

	/**
	 * 机组ID
	 */
	private Long crewId;

	/**
	 * PID
	 */
	private Long pid;

	/**
	 * 设备类型
	 */
	private String typeCode;

	/**
	 * 安装时间
	 */
	@ApiModelProperty(hidden = true)
	private Date createDate;

	/**
	 * 运行状态
	 * 0:停止;1:启动;
	 */
	private Integer state;

	/**
	 * 上下机状态
	 * 0:下机;1:上机;
	 */
	private Integer online;

	/**
	 * 上下机时间
	 */
	@ApiModelProperty(hidden = true)
	private Date onlineDate;

	/**
	 * 设备型号
	 */
	private String devModel;

	/**
	 * 厂家
	 */
	private String manufactor;

	//开始时间
	@ApiModelProperty(hidden = true)
	@Transient
	private transient Date startDate;

	//结束时间
	@ApiModelProperty(hidden = true)
	@Transient
	private transient Date endDate;

	//更新状态
	@ApiModelProperty(hidden = true)
	@Transient
	private transient Boolean toUpdate = false;

	//工作状态 0:空载;1:有载;
	@ApiModelProperty(hidden = true)
	@Transient
	private transient Integer jobStatus;


}
