package io.renren.modules.rules.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.warning.entity.WarningLogBase;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("tb_warning_rules_log")
@EqualsAndHashCode(callSuper = false)
public class WarningRulesLogEntity extends WarningLogBase {

    @TableId
    private Long id;

    /**
     * 报警来源
     * 0:报警配置;
     * 1:运行时间报警规则;
     * 2:动作速度;
     * 3:动作时间;
     */
    private Integer model;

}
