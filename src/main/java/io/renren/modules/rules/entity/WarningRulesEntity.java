package io.renren.modules.rules.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("tb_warning_rules")
public class WarningRulesEntity {
    
    @TableId
    private Long id;
    //产线ID
    //1:热轧2250;2:镀锌3#;3:镀锌4#
    private Long plantId;
    //设备类型
    private String deviceType;
    //状态
    private Integer state;
    //类型
    //0:报警配置;1:运行时间规则;2:动作速度规则;3:动作时间规则
    private Integer model;
    //符号
    private Integer symbol;
    //限值
    private BigDecimal limitVal;
    //提示信息
    private String info;
    //报警等级
    private Integer level;
    //是否推送
    private Integer push;
    //推送接收人
    private String receiver;
    //备注
    private String remark;
}
