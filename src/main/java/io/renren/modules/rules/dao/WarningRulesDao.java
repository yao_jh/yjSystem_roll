package io.renren.modules.rules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.rules.entity.WarningRulesEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 报警规则配置Dao
 *
 */
@Mapper
public interface WarningRulesDao extends BaseMapper<WarningRulesEntity> {

    List<WarningRulesEntity> selectListByRules(Long plantId, String deviceType, Integer level_, Integer model);
}
