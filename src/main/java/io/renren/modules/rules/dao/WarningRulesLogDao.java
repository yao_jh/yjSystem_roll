package io.renren.modules.rules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.rules.entity.WarningRulesLogEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 报警规则配置Dao
 *
 */
@Mapper
public interface WarningRulesLogDao extends BaseMapper<WarningRulesLogEntity> {


}
