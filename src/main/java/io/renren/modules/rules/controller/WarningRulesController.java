
package io.renren.modules.rules.controller;

import io.renren.common.utils.R;
import io.renren.modules.rules.entity.WarningRulesEntity;
import io.renren.modules.rules.service.WarningRulesService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 报警规则Controller
 *
 */
@RestController
@RequestMapping("/warning/rules")
public class WarningRulesController extends AbstractController {
    @Autowired
    private WarningRulesService warningRulesService;

    /**
     * 报警规则列表
     */
    @GetMapping("/select")
    @ApiOperation("获取列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name="plantId",value="产线ID 1:热轧2250;2:镀锌3#;3:镀锌4#   默认-1 是全部查询",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="deviceType",value="设备类型   默认-1 是全部查询",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="model",value="0:报警配置;1:运行时间规则;2:动作速度规则;3:动作时间规则 默认是1:运行时间规则",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="level_",value="报警等级  默认-1 是全部查询",dataType="String", paramType = "query")})
    public R select(
            @RequestParam(defaultValue = "1")Integer model,
            Long plantId,
            String deviceType,
            Integer  level_
    ){
        WarningRulesEntity warningRulesEntity=new WarningRulesEntity();
        warningRulesEntity.setPlantId(plantId);
        warningRulesEntity.setDeviceType(deviceType);
        warningRulesEntity.setModel(model);
        warningRulesEntity.setLevel(level_);

        List<WarningRulesEntity> list = warningRulesService.selectListByRules(warningRulesEntity);
        return R.ok().put("list", list);
    }


    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation("新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name="plantId",value="产线ID 1:热轧2250;2:镀锌3#;3:镀锌4#",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="deviceType",value="设备类型",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="state",value="状态",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="model",value="0:报警配置;1:运行时间规则;2:动作速度规则;3:动作时间规则",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="symbol",value="符号",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="limitVal",value="限值",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="info",value="提示信息",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="level_",value="报警等级",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="push",value="是否推送",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="receiver",value="推送接收人",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="remark",value="备注",dataType="String", paramType = "query")})
    public R save(WarningRulesEntity warningRulesEntity){
        warningRulesService.save(warningRulesEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation("修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="编码号",dataType="Long", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="plantId",value="产线ID 1:热轧2250;2:镀锌3#;3:镀锌4#",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="deviceType",value="设备类型",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="state",value="状态",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="model",value="0:报警配置;1:运行时间规则;2:动作速度规则;3:动作时间规则",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="symbol",value="符号",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="limitVal",value="限值",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="info",value="提示信息",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="level_",value="报警等级",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="push",value="是否推送",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="receiver",value="推送接收人",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="remark",value="备注",dataType="String", paramType = "query")})
    public R update(WarningRulesEntity warningRulesEntity){
        warningRulesService.updateById(warningRulesEntity);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="编码号",dataType="Long", paramType = "query",example="motor1")})
    public R delete(Long id){
        warningRulesService.removeById(id);
        return R.ok();
    }

}
