package io.renren.modules.rules.utils;

import io.renren.common.sys.MainCatche;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.rules.entity.WarningRulesEntity;
import io.renren.modules.rules.entity.WarningRulesLogEntity;
import io.renren.modules.rules.thread.RulesLogThread;
import io.renren.modules.warning.entity.WarningConfigEntity;
import io.renren.modules.warning.entity.WarningLogEntity;
import io.renren.modules.warning.thread.WarningThread;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 运行时间报警规则工具
 */
public class RunDurationRulesUtils {
	// 报警配置
	public static Map<Long, WarningRulesEntity> warningRulesMap = new ConcurrentHashMap<Long, WarningRulesEntity>();

	// 报警日志缓存
	public static Map<String, WarningRulesLogEntity> rulesLogMap = new ConcurrentHashMap<String, WarningRulesLogEntity>();

	/**
	 *
	 * @Title: checkRulesWarning
	 * @Description: 报警判断
	 * @param runDuration
	 * @return: void
	 */
	public static void checkRulesWarning(DeviceRunDurationEntity runDuration) {
		DeviceEntity device = getDeviceById(runDuration.getDeviceId());
		if(device != null){
			String type = device.getTypeCode();
			Long plantId = getPlantIdByDevice(device);
			runDuration.setCrewId(device.getCrewId());
			if(plantId != null){
				//循环判断是否符合报警条件
				for(WarningRulesEntity rule : warningRulesMap.values()){
					if(type.equals(rule.getDeviceType()) && plantId.compareTo(rule.getPlantId()) == 0){
						if(runDuration.getState().compareTo(rule.getState()) == 0){
							boolean isAllBeyond = checkDeviceBeyondConfig(runDuration.getDuration(), rule);
							if(isAllBeyond){
								//保存日志
								createNewWarning(runDuration, rule, false);
							}
						}
					}
				}
			}
		}
	}

	/**
	 *
	 * @Title: createNewWarning
	 * @Description: 创建新的报警
	 * @param config
	 * @return: Warning
	 */
	public static WarningRulesLogEntity createNewWarning(DeviceRunDurationEntity runDuration, WarningRulesEntity config, boolean isTemp) {
		WarningRulesLogEntity warning = new WarningRulesLogEntity();
		BeanUtils.copyProperties(config, warning);
		//设置报警属性
		warning.setCrewId(runDuration.getCrewId());
		warning.setDeviceId(runDuration.getDeviceId());
		warning.setDate(new Date());
		warning.setWarningId(config.getId());
		warning.setId(null);
		if (!isTemp){//保存报警
			warning.setWarningVal(BigDecimal.valueOf(runDuration.getDuration()));
			saveOrUpdateWarningLog(warning);
		}
		return warning;
	}

	/**
	 *
	 * @Title: checkDeviceBeyondConfig
	 * @Description: 判断是否达到 或超过限值
	 * @param config
	 * @return
	 * @return: boolean
	 */
	private static boolean checkDeviceBeyondConfig(Long value, WarningRulesEntity config) {
		if (null == config || null == value || null == config.getLimitVal())
			return false;

		// 0>; 1>=; 2=; 3<; 4<=;
		int operChar = config.getSymbol();
		Long limitVal = config.getLimitVal().longValue();

		if (operChar == 0 && value.compareTo(limitVal) > 0) {
			return true;
		} else if (operChar == 1 && value.compareTo(limitVal) >= 0) {
			return true;
		} else if (operChar == 2 && value.compareTo(limitVal) == 0) {
			return true;
		} else if (operChar == 3 && value.compareTo(limitVal) < 0) {
			return true;
		} else if (operChar == 4 && value.compareTo(limitVal) <= 0) {
			return true;
		}
		return false;
	}


	/**
	 * 获取产线ID
	 * @param device
	 * @return
	 */
	private static Long getPlantIdByDevice(DeviceEntity device) {
		CrewEntity crew =  MainCatche.crewMap.get(device.getCrewId());
		Long plantId = null;
		if(crew != null){
			plantId = crew.getPlantId();
		}
		return plantId;
	}

	/**
	 * 获取设备
	 * @param deviceId
	 * @return
	 */
	private static DeviceEntity getDeviceById(Long deviceId){
		DeviceEntity device =null;
		for(DeviceEntity v : MainCatche.deviceMap.values()){
			if(deviceId.compareTo(v.getDeviceId()) == 0){
				device = v;
				break;
			}
		}
		return device;
	}


	/**
	 * 保存电机均值趋势信息
	 * @param log
	 */
	private static void saveOrUpdateWarningLog(WarningRulesLogEntity log){
		//保存趋势数据
		RulesLogThread thread = new RulesLogThread(log);
		thread.start();
	}
}
