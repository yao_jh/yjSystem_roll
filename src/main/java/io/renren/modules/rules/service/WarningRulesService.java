
package io.renren.modules.rules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.rules.entity.WarningRulesEntity;

import java.util.List;


/**
 * 报警规则配置接口
 *
 */
public interface WarningRulesService extends IService<WarningRulesEntity> {
    List<WarningRulesEntity> selectListByRules(WarningRulesEntity warningRulesEntity);

}
