package io.renren.modules.rules.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.rules.dao.WarningRulesDao;
import io.renren.modules.rules.dao.WarningRulesLogDao;
import io.renren.modules.rules.entity.WarningRulesEntity;
import io.renren.modules.rules.entity.WarningRulesLogEntity;
import io.renren.modules.rules.service.WarningRulesLogService;
import io.renren.modules.rules.service.WarningRulesService;
import org.springframework.stereotype.Service;


@Service("warningRulesLogService")
public class WarningRulesLogServiceImpl extends ServiceImpl<WarningRulesLogDao, WarningRulesLogEntity> implements WarningRulesLogService {


}
