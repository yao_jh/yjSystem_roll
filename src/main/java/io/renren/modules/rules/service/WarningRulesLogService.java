
package io.renren.modules.rules.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.rules.entity.WarningRulesLogEntity;


/**
 * 规则日志配置接口
 *
 */
public interface WarningRulesLogService extends IService<WarningRulesLogEntity> {


}
