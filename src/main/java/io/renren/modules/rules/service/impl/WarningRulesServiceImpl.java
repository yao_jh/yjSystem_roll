package io.renren.modules.rules.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.rules.dao.WarningRulesDao;
import io.renren.modules.rules.entity.WarningRulesEntity;
import io.renren.modules.rules.service.WarningRulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("warningRulesService")
public class WarningRulesServiceImpl extends ServiceImpl<WarningRulesDao, WarningRulesEntity> implements WarningRulesService {
    @Autowired
    private WarningRulesDao warningRulesDao;


    @Override
    public List<WarningRulesEntity> selectListByRules(WarningRulesEntity warningRulesEntity) {
        return warningRulesDao.selectListByRules(warningRulesEntity.getPlantId(),warningRulesEntity.getDeviceType(),warningRulesEntity.getLevel(),warningRulesEntity.getModel());
    }

}
