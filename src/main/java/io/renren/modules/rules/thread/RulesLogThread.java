package io.renren.modules.rules.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.rules.entity.WarningRulesLogEntity;
import io.renren.modules.rules.service.WarningRulesLogService;

/**
 * @ClassName RulesLogThread
 * @Deacription 保存规则日志线程
 * @Author lee
 * @Date 2020/8/4 15:33
 * @Version 1.0
 **/
public class RulesLogThread extends Thread{
    //报警日志
    private WarningRulesLogEntity logEntity;

    public RulesLogThread(){
        super();
    }

    public RulesLogThread(WarningRulesLogEntity logEntity){
        this.logEntity = logEntity;
    }

    @Override
    public void run() {
        synchronized (logEntity){
            //获取spring bean
            WarningRulesLogService warningRulesLogService = (WarningRulesLogService) SpringContextUtils.getBean("warningRulesLogService");
            //id空保存，有id修改
            if(logEntity.getId() == null){
                warningRulesLogService.save(logEntity);
            }else{
                warningRulesLogService.updateById(logEntity);
            }
        }
    }
}
