package io.renren.modules.material.utils;

import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    public static String bytesToBase64(byte[] bytes){
        String fileBase64 = null;
        BASE64Encoder encoder = new BASE64Encoder();
        try {
            //上传base64码
            fileBase64 = encoder.encode(bytes);
            fileBase64.replaceAll("[\r\n]", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileBase64;
    }

    public static String transferToFile(MultipartFile file, String path) throws IOException {
        // 文件名
        String fileName = file.getOriginalFilename();
        // 完整上传路径 路径+文件名
        // 完整上传路径 路径+文件名
        String pathName = path + "/" + fileName;

        File dest = new File(pathName);
        //判断文件父目录是否存在
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdir();
        }
        //保存文件
        file.transferTo(dest);
        return pathName;
    }
}
