
package io.renren.modules.material.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.material.entity.DocEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 文件上传Dao
 *
 */
@Mapper
public interface DocDao extends BaseMapper<DocEntity> {

}
