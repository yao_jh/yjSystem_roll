
package io.renren.modules.material.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.material.entity.MeansEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 资料上传Dao
 *
 */
@Mapper
public interface MeansDao extends BaseMapper<MeansEntity> {


}
