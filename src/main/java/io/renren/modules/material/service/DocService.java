
package io.renren.modules.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.material.entity.DocEntity;
import org.springframework.web.multipart.MultipartFile;


public  interface DocService extends IService<DocEntity> {

    Boolean upload(MultipartFile file, DocEntity docEntity);
}
