package io.renren.modules.material.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.material.dao.MeansDao;
import io.renren.modules.material.entity.MeansEntity;
import io.renren.modules.material.service.MeansService;
import io.renren.modules.material.utils.FileUtils;
import io.renren.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service("meansService")
public class MeansServiceImpl extends ServiceImpl<MeansDao, MeansEntity> implements MeansService {
	@Autowired
	private SysConfigService sysConfigService;

	@Override
	public Boolean upload(MultipartFile file, MeansEntity meansEntity) {
		Boolean flag = false;
		if (file.isEmpty()) {
			return false;
		}
		//文件保存
		if (meansEntity.getType() == 0) {
			//上传路径
			String path = sysConfigService.getValue("FILE_UPLOAD_PATH_KEY");
			try {
				String pathName = FileUtils.transferToFile(file, path);
				//保存上传文件记录
				meansEntity.setDate(new Date());
				meansEntity.setFilePath(pathName);
				this.save(meansEntity);
				flag = true;
			}catch (Exception e) {
				e.printStackTrace();
				flag = false;
			}
		} else {//保存base64
			try {
				String fileBase64 = FileUtils.bytesToBase64(file.getBytes());
				meansEntity.setDate(new Date());
				meansEntity.setFileBase64(fileBase64);
				this.save(meansEntity);
				flag = true;
			}catch (Exception e) {
				e.printStackTrace();
				flag = false;
			}
		}
		return flag;
	}

	public Boolean download(String filename){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
		//HttpServletResponse response = ((ServletWebRequest)RequestContextHolder.getRequestAttributes()).getResponse();

		//模拟文件,获取文件下载路径，file为需要下载的文件名
	    //String filename = sysConfigService.getValue("FILE_UPLOAD_PATH_KEY")+"/"+file;
		String file="";
		if (filename!=null&&filename.length()>=14){
			 file = filename.substring(14,filename.length());
		}
		Boolean flag = false;
		//获取输入流
		InputStream bis = null;
		try {
			bis = new BufferedInputStream(new FileInputStream(new File(filename)));
			flag =true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			flag =false;
		}
		//运用正则表达式判断文件名是否含有中文字符,进行转码,避免乱码
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(file);
		//判断是否存在 存在进入循环
		if (m.find()) {
		//转码，避免下载文件时文件名中文乱
		String userAgent = request.getHeader("User-Agent");
			byte[] bytes = new byte[0]; // file.getBytes("UTF-8")处理safari的乱码问题
			try {
				bytes = userAgent.contains("MSIE") ? file.getBytes() : file.getBytes("UTF-8");
				flag =true;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				flag =false;
			}

		}
	    //设置文件下载头
		//response.setContentType("text/html;charset=utf-8");
		//response.addHeader("Content-Type", "application/json;charset=UTF-8");
		try {
			response.addHeader("Content-Disposition", "attachment;filename=" + new String(file.getBytes(),"utf-8"));
		} catch (UnsupportedEncodingException e) {//ISO8859-1
			e.printStackTrace();
		}
	    //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型
		response.setContentType("multipart/form-data");
		BufferedOutputStream out = null;
		try {
			out = new BufferedOutputStream(response.getOutputStream());
			flag =true;
		} catch (IOException e) {
			e.printStackTrace();
			flag =false;
		}
		int len = 0;
		while(true){
			try {
				if (!((len = bis.read()) != -1)) break;
			out.write(len);
			out.flush();
				flag =true;
			} catch (IOException e) {
				e.printStackTrace();
				flag =false;
			}
		}
		try {
			out.close();
			flag =true;
		} catch (IOException e) {
			e.printStackTrace();
			flag =false;
		}
		return flag;
	}

}