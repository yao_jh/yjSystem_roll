package io.renren.modules.material.service;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.material.entity.MeansEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface MeansService extends IService<MeansEntity> {

	Boolean upload(MultipartFile file, MeansEntity meansEntity);
}
