package io.renren.modules.material.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.material.dao.DocDao;
import io.renren.modules.material.entity.DocEntity;
import io.renren.modules.material.service.DocService;
import io.renren.modules.material.utils.FileUtils;
import io.renren.modules.sys.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.Date;

@Service("docService")
public class DocServiceImpl extends ServiceImpl<DocDao, DocEntity> implements DocService {

    @Autowired
    private SysConfigService sysConfigService;

    @Override
    public Boolean upload(MultipartFile file, DocEntity docEntity) {
        Boolean flag = false;
        if (file.isEmpty()) {
            return false;
        }
        //文件保存
        if (docEntity.getType() == 0) {
            //上传路径
            String path = sysConfigService.getValue("FILE_UPLOAD_PATH_KEY");
            try {
                String pathName = FileUtils.transferToFile(file, path);
                //保存上传文件记录
                docEntity.setDate(new Date());
                docEntity.setFilePath(pathName);
                this.save(docEntity);
                flag = true;
            }catch (Exception e) {
                e.printStackTrace();
                flag = false;
            }
        } else {//保存base64
            try {
                String fileBase64 = FileUtils.bytesToBase64(file.getBytes());
                docEntity.setDate(new Date());
                docEntity.setFileBase64(fileBase64);
                this.save(docEntity);
                flag = true;
            }catch (Exception e) {
                e.printStackTrace();
                flag = false;
            }
        }
        return flag;
    }
}
