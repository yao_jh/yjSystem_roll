package io.renren.modules.material.controller;

import io.renren.common.utils.R;
import io.renren.modules.material.entity.MeansEntity;
import io.renren.modules.material.service.MeansService;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.sys.service.SysConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * 资料上传Controller
 *
 */

@RestController
@RequestMapping("/material/means")
@Api(value="资料、文件上传接口", tags = {"资料、文件上传接口"})
public class MeansController extends AbstractController {

	@Autowired
	private MeansService meansService;

	//@Autowired
	//private SysConfigService sysConfigService;
	/**
	 * 上传
	 */

	@ApiOperation("/upload")
	@RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
	@ResponseBody
	public R upload(@RequestParam("file") MultipartFile file,MeansEntity meansEntity){

		Boolean flag = meansService.upload(file, meansEntity);
		//判断文件是否上传成功
		if (flag){
			return R.ok();
		} else {
			return R.error();
		}
	}

	/**
	 * 下载
	 * @return
	 */
	@ApiOperation(value="/download",produces="application/octet-stream")
	@GetMapping("/download")  //上传了之后再下载
	public R download( @RequestParam("filename") String filename)throws Exception{
		Boolean flag = meansService.download(filename);
		if (flag){
			return R.ok();
		} else {
			return R.error();
		}
	}

	/**
	 * 删除
	 */
	@GetMapping("/delete")
	@ResponseBody
	@ApiOperation("删除")
	public R delete(Long id) {
		Boolean flag = meansService.removeById(id);
		//判断是否成功
		if (flag){
			return R.ok();
		} else {
			return R.error();
		}
	}


}