package io.renren.modules.material.controller;

import io.renren.common.utils.R;
import io.renren.modules.material.entity.DocEntity;
import io.renren.modules.material.service.DocService;
import io.renren.modules.sys.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传Controller
 *
 */
@RestController
@RequestMapping("/material/doc")
public class DocController extends AbstractController {

	@Autowired
	private	DocService docService;

	/**
	 * 上传
	 */
	@RequestMapping("upload")
	@ResponseBody
	public R upload(@RequestParam("file") MultipartFile file, DocEntity docEntity) {
		Boolean flag = docService.upload(file, docEntity);
		//判断文件是否上传成功
		if (flag){
			return R.ok();
		} else {
			return R.error();
		}
	}

	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public R delete(Long id) {
		Boolean flag = docService.removeById(id);
		//判断是否成功
		if (flag){
			return R.ok();
		} else {
			return R.error();
		}
	}


}
	


