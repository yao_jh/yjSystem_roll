package io.renren.modules.material.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 文件管理
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tb_upload_doc")
public class DocEntity extends FileBaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Long id;

    /**
     * 归类
     */
    private String suite;
}

