package io.renren.modules.material.entity;

import lombok.Data;

import java.util.Date;

@Data
public class FileBaseEntity {

    //编号
    private String code;
    //名称
    private String name;
    //检索码
    private String retrieval;
    //路径
    private String filePath;
    //base64编码
    private String fileBase64;
    //保存类型 0:原样; 1:base64
    private Integer type;
    //创建时间
    private Date date;
    //备注
    private String remark;
}
