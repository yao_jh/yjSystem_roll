package io.renren.modules.material.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 资料管理
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tb_upload_means")
public class MeansEntity extends FileBaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId
	private Long id;

	/**
	 * 层级
	 * 0:产线;1:机组;2:设备
	 */
	private Integer Level;

	/**
	 * 关联ID
	 * 产线/机组/设备的id
	 */
	private Long associationId;


}
