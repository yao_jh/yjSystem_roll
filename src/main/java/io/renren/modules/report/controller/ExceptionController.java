package io.renren.modules.report.controller;

import io.renren.common.utils.R;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.warning.entity.WarningLogEntity;
import io.renren.modules.warning.utils.WarningUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


/**
 * 异常事件报表
 *
 */
@RestController
@RequestMapping("/report/exception")
@Api("异常事件报表接口")
public class ExceptionController {

    @Autowired
    private PointService pointService;

    /**
     * 列表
     */
    @GetMapping("/select")
    @ApiImplicitParams({
            @ApiImplicitParam(name="crewId",value="机组ID",dataType="Long", paramType = "query"),
            @ApiImplicitParam(name="deviceId",value="设备ID",dataType="Long", paramType = "query",example="XXX")})
    public R select(QueryParam params){
        //缓存报警日志
        Map<String, WarningLogEntity> map = WarningUtils.warningLogMap;
        if(map == null || map.isEmpty()){
            return R.ok().put("list", null);
        }
        List<PointEntity> pointList = pointService.queryListByParams(params);
        if(pointList == null || pointList.isEmpty()){
            return R.ok().put("list", null);
        }
        Map<Long, Long> pointMap = new HashMap<Long, Long>();
        for(PointEntity point : pointList){
            pointMap.put(point.getPointId(), point.getPointId());
        }
        List<WarningLogEntity> list = new ArrayList<WarningLogEntity>();
        for(WarningLogEntity log : map.values()){
            Long flag = pointMap.get(log.getPointId());
            if(flag != null){
                list.add(log);
            }
        }
        //时间倒序排序
        list.sort(Comparator.comparing(WarningLogEntity::getDate).reversed());
        return R.ok().put("list", list);
    }


}
