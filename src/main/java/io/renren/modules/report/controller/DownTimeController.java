package io.renren.modules.report.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.currency.entity.DeviceRunDurationEntity;
import io.renren.modules.currency.service.DeviceRunDurationService;
import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 停机报表
 *
 */
@RestController
@RequestMapping("/report/downTime")
@Api("停机报表接口")
public class DownTimeController {

    @Autowired
    private DeviceRunDurationService deviceRunDurationService;

    /**
     * 统计设备运行时长
     * @param code
     * @return
     */
    @GetMapping("/selectDownListForPage")
    @ApiOperation("统计设备运行时长")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="page",value="页码",dataType="String", paramType = "query",example="1"),
            @ApiImplicitParam(name="size",value="条目数",dataType="String", paramType = "query",example="10"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectDownListForPage(@RequestParam("code") String code , @RequestParam("page") String page, @RequestParam("size") String size,
                               @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        params.setStartDateStr(startDateStr);
        params.setEndDateStr(endDateStr);

        //查询报警历史记录
        PageUtils list = deviceRunDurationService.queryPageDownListByParam(params.getPageMap(page, size));
        return R.ok().put("data", list);
    }

    /**
     * 批量更新停机报告明细
     * @param jsonData
     * @return
     */
    @GetMapping("/updateDownInfoForJsonArray")
    @ApiOperation("批量更新停机报告明细")
    @ApiImplicitParams(
            @ApiImplicitParam(name="jsonData",value="设备编码",dataType="String", paramType = "query",example="[{id:1,reason:1,major:0,remark:'XXX'}]"))
    public R updateDownInfoForJsonArray(@RequestParam("jsonData") String jsonData){
        if(CommonUtils.isNull(jsonData)){
            return R.error("更新参数为空");
        }
        JSONArray arr = JSONArray.parseArray(jsonData);
        for (int i=0; i<arr.size(); i++){
            //设置明细
            DeviceRunDurationEntity entity = placeInfoByJson(arr.getJSONObject(i));
            if(entity != null){
                //保存更新后的明细
                deviceRunDurationService.updateById(entity);
            }
        }
        return R.ok();
    }

    /**
     * 更新停机报告明细
     * @param jsonData
     * @return
     */
    @GetMapping("/updateDownInfoForJson")
    @ApiOperation("更新停机报告明细")
    @ApiImplicitParams(
            @ApiImplicitParam(name="jsonData",value="设备编码",dataType="String", paramType = "query",example="[{id:1,reason:1,major:0,remark:'XXX'}]"))
    public R updateDownInfoForJson(@RequestParam("jsonData") String jsonData){
        if(CommonUtils.isNull(jsonData)){
            return R.error("更新参数为空");
        }
        JSONObject json = JSONObject.parseObject(jsonData);
        //设置明细
        DeviceRunDurationEntity entity = placeInfoByJson(json);
        if(entity != null){
            //保存更新后的明细
            deviceRunDurationService.updateById(entity);
        }
        return R.ok();
    }

    /**
     * 放置修改内容到实体
     * @param obj
     * @return
     */
    private DeviceRunDurationEntity placeInfoByJson(JSONObject obj){
        DeviceRunDurationEntity entity = null;
        Long id = obj.getLong("id");
        if(id == null){
            return null;
        }
        //修改的值
        Integer reason = obj.getInteger("reason");
        Integer major = obj.getInteger("major");
        String remark = obj.getString("remark");
        entity = deviceRunDurationService.getById(id);
        //更新到实体
        if(entity != null){
            entity.setReason(reason);
            entity.setMajor(major);
            entity.setRemark(remark);
        }
        return entity;
    }

}
