package io.renren.modules.data.utils;

import io.renren.common.utils.DateUtils;
import io.renren.modules.data.vo.TitileVo;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.PointEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据处理工具
 */
public class DataUtils {
    /**
     * 获取查询题头
     * @param pointList
     * @return
     */
    public static List<TitileVo> getTitlesByPointList(List<PointEntity> pointList){
        List<TitileVo> list = new ArrayList<TitileVo>();
        TitileVo vo = null;
        for(PointEntity point : pointList){
            if(point != null){
                vo = new TitileVo(point);
                list.add(vo);
                vo = null;
            }
        }
        return list;
    }

    /**
     * 获取查询题头
     * @param point
     * @return
     */
    public static List<TitileVo> getTitlesByPoint(PointEntity point){
        List<TitileVo> list = new ArrayList<TitileVo>();
        if(point != null){
            list.add(new TitileVo(point));
        }
        return list;
    }

    /**
     * 获取时间间隔（0秒，1分，2小时）
     * 默认根据时段匹配
     * @param param
     * @return
     */
    public static Integer getIntervalByParam(QueryParam param){
        //时间间隔 0:秒;1:分;2:小时
        Integer intervalUnit = param.getIntervalUnit();
        //时间间隔为空:根据时间段长度匹配间隔
        if(intervalUnit == null){
            //时间差 小于两天分钟间隔，大于2天小时间隔
            long diff = DateUtils.getDateDiffForDay(param.getStartDate(), param.getEndDate());
            if(diff > 0){
                intervalUnit = 1;
            }else if(diff > 2){
                intervalUnit = 2;
            }
        }
        return intervalUnit;
    }

    /**
     * 获取查询点ClassCode集合
     * @param titles
     * @return
     */
    public static List<String> getQueryPointList(List<TitileVo> titles) {
        //参数清单,list转数组
        List<String> points = null;
        if(titles != null && !titles.isEmpty()){
            points = new ArrayList<String>();
            TitileVo vo = null;
            for(int i =0;i<titles.size();i++){
                vo = titles.get(i);
                if(vo != null){
                    points.add(vo.getClassCode());
                }
            }
        }
        return points;
    }
}
