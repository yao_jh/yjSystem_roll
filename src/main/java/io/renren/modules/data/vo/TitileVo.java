package io.renren.modules.data.vo;

import io.renren.modules.operation.entity.PointEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * 数据题头
 */
@Data
public class TitileVo {
    /**
     * 代码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 单位
     */
    private String unit;
    /**
     * 类型码
     */
    private String classCode;

    public TitileVo(){
        super();
    }

    public TitileVo(PointEntity point){
        BeanUtils.copyProperties(point,this);
    }
}
