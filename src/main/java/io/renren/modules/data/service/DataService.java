package io.renren.modules.data.service;

import com.alibaba.fastjson.JSONArray;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.vo.PointVo;

import java.util.List;
import java.util.Map;

public interface DataService  {


	/**
	 * 获取实时数据
	 * @param code
	 * @return
	 */
	Map<String, PointVo> queryRealDataForMap(String code);

	/**
	 * 获取实时数据列表
	 * @param code
	 * @return
	 */
	List<PointVo> queryRealDataForList(String code);

	/**
	 * 查询历史数据
	 * @param queryParam
	 * @return
	 */
	JSONArray queryHistoryDataByParam(QueryParam queryParam);

}
