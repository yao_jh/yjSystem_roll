package io.renren.modules.data.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.iot.exception.BadAuthorizedException;
import io.renren.common.sys.MainCatche;
import io.renren.common.sys.Topic;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.data.service.DataService;
import io.renren.modules.data.utils.DataUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.FrameBase;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.operation.vo.PointVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("dataService")
public class DataServiceImpl implements DataService {

	@Autowired
	private PointService pointService;

	@Override
	public Map<String, PointVo> queryRealDataForMap(String code) {
		if(CommonUtils.isNull(code)){
			return null;
		}else{
			//输出参数的视图格式
			Map<String, PointVo> map = new HashMap<String, PointVo>();
			if(MainCatche.dataMap.containsKey(code)){
				Map<String, PointEntity> deviceData = MainCatche.dataMap.get(code);
				if(deviceData != null){
					for(PointEntity point : deviceData.values()){
						map.put(point.getClassCode(), new PointVo(point));
					}
				}
				return map;
			}else{
				return null;
			}
		}
	}

	@Override
	public List<PointVo> queryRealDataForList(String code) {
		if(CommonUtils.isNull(code)){
			return null;
		}else{
			//输出参数的视图格式
			List<PointVo> list = new ArrayList<PointVo>();
			if(MainCatche.dataMap.containsKey(code)){
				Map<String, PointEntity> deviceData = MainCatche.dataMap.get(code);
				if(deviceData != null){
					for(PointEntity point : deviceData.values()){
						list.add(new PointVo(point));
					}
				}
				return list;
			}else{
				return null;
			}
		}
	}

	@Override
	public JSONArray queryHistoryDataByParam(QueryParam queryParam) {
		JSONArray data = null;
		if(queryParam.getTitileVoList() == null){
			return null;
		}

		//格式化请求参数
		JSONObject param = getHistoryParams(queryParam);
		//请求地址
		String address = "/iot-data-query/api/metric/history";
		try{
			JSONObject jsonObject = Topic.getIotRestRequest(param, address);
			if(jsonObject != null && "success".equals(jsonObject.getString("code"))){
				data = jsonObject.getJSONArray("data");
			}
		} catch (BadAuthorizedException e) {
			e.printStackTrace();
		}

		return data;
	}

	/**
	 * 格式化请求参数
	 * @param queryParam
	 * @return
	 */
	private JSONObject getHistoryParams(QueryParam queryParam){
		//请求参数
		JSONObject param = new JSONObject();
		//查询点codes
		List<String> points = DataUtils.getQueryPointList(queryParam.getTitileVoList());

		//时间间隔 0:秒;1:分;2:小时
		Integer intervalUnit = DataUtils.getIntervalByParam(queryParam);

		//返回数据列表页码
		Integer page = queryParam.getPage() == null ? 1 : queryParam.getPage();

		//单页条目数
		//空值表示全查询，使用一个比较大的值(100000)表示无上限
		Integer size = queryParam.getLimit() == null ? 100000 : queryParam.getLimit();

		//获取平台设备ID
		String dockId = null;
		FrameBase frame = MainCatche.frameMap.get(queryParam.getCode());
		if(frame != null){
			dockId = frame.getDockId();
		}

		//参数条件
		param.put("page", page);
		param.put("size", size);
		param.put("deviceId", dockId);
		param.put("selectParams", points);
		param.put("intervalUnit", intervalUnit);
		param.put("startTime", queryParam.getStartDateStr());
		param.put("endTime", queryParam.getEndDateStr());

		return param;
	}
}