package io.renren.modules.feature.utils;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.DateUtils;
import io.renren.modules.currency.utils.CrewStateUtils;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.feature.thread.ForceThread;
import io.renren.modules.feature.vo.ForceTrendVo;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 轧制力评估包分流工具
 */
public class ForceShuntUtils {

    // 轧制力均值趋势 key:crewId
    public static Map<Long, ForceTrendVo> forceTrendMap = new ConcurrentHashMap<Long, ForceTrendVo>();

    /**
     * 轧制力分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        ForceTrendVo trend = forceTrendMap.get(point.getCrewId());
        if(trend == null){
            return ;
        }
        //过滤轧制力参数
        boolean flag =  setTrendPoints(point, trend);
        if(flag){
            forceTrendMap.put(point.getCrewId(), trend);
        }
    }

    /**
     * 设置轧制力均值实体
     * @param point
     * @param trend
     */
    private static boolean setTrendPoints(PointEntity point, ForceTrendVo trend){
        boolean flag = false;
        BigDecimal value = point.getValue();
        if(trend.getStartDate() != null){
            //冲击波动
            if(trend.getImpactWave() == null){
                long secondDiff = DateUtils.getDateDiffForSecond(trend.getStartDate(), point.getRealDate());
                if(secondDiff >= 1){//冲击波动=咬钢后1s的值
                    trend.setImpactWave(value);
                }
            }
        }

        //验证总轧制力
        if("force_total_act".equals(point.getClassCode())){
            //最大轧制力
            if(trend.getMaxForceTotal() == null){
                trend.setMaxForceTotal(value);
            }else if(trend.getMaxForceTotal().compareTo(value) < 0){
                trend.setMaxForceTotal(value);
            }
            //最小轧制力
            if(trend.getMinForceTotal() == null){
                trend.setMinForceTotal(value);
            }else if(trend.getMinForceTotal().compareTo(value) > 0){
                trend.setMinForceTotal(value);
            }
            return processJobStatus(point, trend);
        }

        // 开始时间有值开始计算周期趋势均值
        if(trend.getStartDate() == null){
            return flag;
        }

        if(value == null) return flag;
        //平均数通过累加器运算s
        switch(point.getClassCode()){
            case "force_os_pt":
                // 轧制力操作侧(传感器)
                trend.toAccumSumForceOsPt(value);
                flag = true;
                break;
            case "force_ds_pt":
                // 轧制力传动侧(传感器)
                trend.toAccumSumForceDsPt(value);
                flag = true;
                break;
            case "force_os_lc":
                // 轧制力操作侧(压头)
                trend.toAccumForceOsLc(value);
                flag = true;
                break;
            case "force_ds_lc":
                // 轧制力传动侧(压头)
                trend.toAccumForceDsLc(value);
                flag = true;
                break;
            case "pass":
                //道次
                trend.setPass(value.intValue());
                break;

        }
        //钢卷号
        if(point.getClassCode().indexOf("coil_no") >= 0){
            String coilNo = getCoilNoByPoint(point, trend);
            trend.setCoilNo(coilNo);
        }

        return flag;

    }

    /**
     * 返回
     * @param point
     * @param trend
     * @return
     */
    private static String getCoilNoByPoint(PointEntity point, ForceTrendVo trend) {
        //String coilNo = trend.getCoilNo();
        //数值转字符（ASCII）
        int value = point.getValue().intValue();
        int offset = getNumberByStr(point.getClassCode(), "coil_no")-1;
        char[] coils = trend.getCoilChar();
        //更新钢卷号数位
        coils[offset] = (char) value;
        trend.setCoilChar(coils);
        String coilNo = String.valueOf(coils);
        return coilNo;
    }

    /**
     * 字符转数值
     * @param str
     * @param trimStr
     * @return
     */
    private static Integer getNumberByStr(String str, String trimStr){
        String surplus = str.replaceAll(trimStr,"");
        return Integer.valueOf(surplus);
    }

    /**
     * 处理作业状态
     * @param point
     * @param trend
     * @return
     */
    private static boolean processJobStatus(PointEntity point, ForceTrendVo trend){
        boolean flag = false;
        Integer state = CrewStateUtils.getCrewMillState(point);
        // 更新状态
        if(trend.getState() == null){
            // 系统首次运行，更新状态
            trend.setState(state);
            flag = true;
        }else if(trend.getState().compareTo(state) != 0){
            if(state == 0) {// 抛钢
                // 保存一个周期的事件
                // 开始和结束时间形成闭合的周期
                if(trend.getStartDate() != null){
                    // 保存趋势数据
                    trend.setEndDate(point.getRealDate());
                    saveTrendEntity(trend);

                    // 释放空间
                    trend = null;
                    // 新建趋势
                    trend = initTrendEntity(point, state);
                }
            }else{// 咬钢
                trend.setStartDate(point.getRealDate());
            }
            trend.setState(state);
            flag = true;
        }
        return flag;
    }

    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(ForceTrendVo trend){
        //保存趋势数据
        ForceAvgtrendEntity trendEntity = new ForceAvgtrendEntity(trend);
        //保存线程
        ForceThread thread = new ForceThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param point
     * @param state
     * @return
     */
    private static ForceTrendVo initTrendEntity(PointEntity point, Integer state){
        CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
        if(crew == null){
            return null;
        }
        ForceTrendVo trend = new ForceTrendVo(crew.getCrewId());
        //trend.setStartDate(point.getRealDate());
        trend.setState(state);
        return trend;
    }
}
