package io.renren.modules.feature.utils;

import io.renren.common.utils.DateUtils;
import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.thread.ServoValveThread;
import io.renren.modules.feature.vo.ServoValveTrendVo;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 伺服阀评估包分流工具
 */
public class ServoValveShuntUtils {

    // 伺服阀均值趋势 key:deviceId
    public static Map<Long, ServoValveTrendVo> servoValveTrendMap = new ConcurrentHashMap<Long, ServoValveTrendVo>();

    /**
     * 伺服阀分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        ServoValveTrendVo trend = servoValveTrendMap.get(point.getDeviceId());
        if(trend == null){
            return ;
        }
        //过滤伺服阀参数
        boolean flag =  setTrendPoints(point, trend);
        //状态改变, 更新缓存
        if(flag){
            servoValveTrendMap.put(point.getDeviceId(), trend);
        }
    }

    /**
     * 设置伺服阀均值实体
     * @param point
     * @param trend
     */
    private static boolean setTrendPoints(PointEntity point, ServoValveTrendVo trend){
        boolean flag = false;
        if(!"servo_actual".equals(point.getClassCode())){
            return false;
        }
        //取样间隔，10分鐘做一次分析
        Long diff = DateUtils.getDateDiffForMini(trend.getDate(), new Date());
        //每隔10分钟取样一次
        if(diff > 10 || trend.getDate() == null){
            flag = true;
        }else {
            return false;
        }

        //单调区间
        BigDecimal value = point.getValue();
        BigDecimal last = trend.getLastValue();
        //0:上升;1:下降
        Integer state = MeansUtils.getMonotoneState(last, value);
        trend.setLastValue(value);
        if(state == null){
            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            return false;
        }
        //相同单调区间
        if(state.equals(trend.getState())){
            BigDecimal initValue = trend.getInitValue();
            //变化幅度>800
            if(Math.abs(initValue.subtract(value).doubleValue()) > 800){
                if(state.equals(0)){//上升
                    trend.setMinValue(initValue);
                    trend.setMaxValue(value);
                    trend.setEndDate(point.getRealDate());
                }else{//下降
                    trend.setMaxValue(initValue);
                    trend.setMinValue(value);
                    trend.setEndDate(point.getRealDate());
                }
            }
        }else{//单调区间变更保存最新的结果并重新构建比较
            if(trend.getEndDate() != null){
                //保存记录
                saveTrendEntity(trend);
                //新的趋势
                trend = initTrendEntity(point);
                trend.setDate(new Date());
                flag = true;
            }

            trend.setInitValue(value);
            trend.setStartDate(point.getRealDate());
            trend.setState(state);
        }
        return flag;
    }

    /**
     * 保存均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(ServoValveTrendVo trend){
        //保存趋势数据
        ServoValveAvgtrendEntity trendEntity = new ServoValveAvgtrendEntity(trend);
        //保存线程
        ServoValveThread thread = new ServoValveThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化均值趋势
     * @param point
     * @return
     */
    private static ServoValveTrendVo initTrendEntity(PointEntity point){
        ServoValveTrendVo trend = new ServoValveTrendVo();
        //trend.setStartDate(point.getRealDate());
        return trend;
    }

    /**
     * 判断两个量是否近似相等
     * @param gieven
     * @param val
     * @return
     */
    private static boolean isSimilarEqual(BigDecimal gieven, BigDecimal val){
        boolean flag = false;
        if(gieven == null || val == null) return flag;
        //相差0.001被认为相等
        if(Math.abs(gieven.subtract(val).doubleValue()) < 0.001){
            return flag;
        }
        return flag;
    }

}
