package io.renren.modules.feature.utils;

import io.renren.modules.currency.utils.CrewStateUtils;
import io.renren.modules.feature.thread.MotorThread;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.feature.vo.MotorTrendVo;
import io.renren.modules.operation.entity.PointEntity;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 电机评估包分流工具
 */
public class MotorShuntUtils {

    // 电机均值趋势 key:deviceId
    public static Map<Long, MotorTrendVo> motorTrendMap = new ConcurrentHashMap<Long, MotorTrendVo>();

    /**
     * 电机均值分流发生器
     * @param point
     */
    public static void grantShuntHandler(PointEntity point){
        MotorTrendVo trend = motorTrendMap.get(point.getDeviceId());
        if(trend == null){
            return ;
        }
        //设置电机均值
        boolean flag =  setTrendPoints(point, trend);
        if(flag){
            motorTrendMap.put(point.getDeviceId(), trend);
        }
    }

    /**
     * 设置电机均值实体
     * @param point
     * @param trend
     */
    private static boolean setTrendPoints(PointEntity point, MotorTrendVo trend){
        boolean flag = false;
        if("run_sig".equals(point.getClassCode())){//停机就停止运算
            if(BigDecimal.ZERO.compareTo(point.getValue()) == 0){
                return flag;
            }
        }

        //处置状态,根据状态判定事件是否结束
        Boolean status = processJobStatus(point, trend);
        if(status != null){
            return status;
        }

        // 开始时间有值开始计算周期趋势均值
        if(trend.getStartDate() == null){
            return flag;
        }

        BigDecimal value = point.getValue();
        if(value == null) return flag;
        switch(point.getClassCode()){
            case "speed_act":
                //最大速度
                if(trend.getMaxSpeed() == null){
                    trend.setMaxSpeed(value);
                }else if(value.compareTo(trend.getMaxSpeed()) > 0){
                    trend.setMaxSpeed(value);
                }
                //速度均值=速度总和/次数
                //累加速度,累加方法自动运算均值
                trend.toAccumSumSpeed(value);
                flag = true;
                break;
            case "current":
                //最大电流
                if(trend.getMaxCurrent() == null){
                    trend.setMaxCurrent(value);
                }else if(value.compareTo(trend.getMaxCurrent()) > 0){
                    trend.setMaxCurrent(value);
                }
                //电流均值=电流总和/次数
                //累加电流,累加方法自动运算均值
                trend.toAccumSumCurrent(value);
                flag = true;
                break;
            case "torque":
                //最大转矩
                if(trend.getMaxTorque() == null){
                    trend.setMaxTorque(value);
                }else if(value.compareTo(trend.getMaxTorque()) > 0){
                    trend.setMaxTorque(value);
                }
                //转矩均值=转矩总和/次数
                //累加转矩,累加方法自动运算均值
                trend.toAccumSumTorque(value);
                flag = true;
                break;
            case "power":
                //功率均值=功率总和/次数
                //累加功率,累加方法自动运算均值
                trend.toAccumSumPower(value);
                flag = true;
                break;
            case "temp":
                //温度均值=温度总和/次数
                //累加温度,累加方法自动运算均值
                trend.toAccumSumTemp(value);
                flag = true;
                break;
        }
        return flag;
    }

    /**
     * 处理趋势状态
     * 冷热轧不同，区分类型
     * @param point
     * @param trend
     * @return
     */
    private static Boolean processJobStatus(PointEntity point, MotorTrendVo trend) {
        Integer state = CrewStateUtils.getJobStatus(point);
        Boolean flag = null;
        if(state != null){
            flag = renewTrendState(point, trend, state);
        }
        return flag;
    }

    /**
     * 缓存趋势状态
     * @param point
     * @param trend
     * @param state
     *        0:结束;1:开始
     * @return
     */
    private static Boolean renewTrendState(PointEntity point, MotorTrendVo trend, Integer state){
        boolean flag = false;
        // 更新状态
        if(trend.getState() == null){
            // 系统首次运行，更新状态
            trend.setState(state);
            flag = true;
        }else if(trend.getState().compareTo(state) != 0){
            if(state == 0) {// 结束
                // 保存一个周期的事件
                // 开始和结束时间形成闭合的周期
                if(trend.getStartDate() != null){
                    // 保存趋势数据
                    trend.setEndDate(point.getRealDate());
                    trend.setDeviceId(point.getDeviceId());
                    saveTrendEntity(trend);

                    // 释放空间
                    trend = null;
                    // 新建趋势
                    trend = initTrendEntity(state);
                }
            }else{// 开始
                trend.setStartDate(point.getRealDate());
            }
            trend.setState(state);
            flag = true;
        }
        return flag;
    }

    /**
     * 保存电机均值趋势信息
     * @param trend
     */
    private static void saveTrendEntity(MotorTrendVo trend){
        //保存趋势数据
        MotorAvgtrendEntity trendEntity = new MotorAvgtrendEntity(trend);
        //保存线程
        MotorThread thread = new MotorThread(trendEntity);
        thread.start();
    }

    /**
     * 初始化电机均值趋势
     * @param state
     * @return
     */
    private static MotorTrendVo initTrendEntity(Integer state){
        MotorTrendVo trend = new MotorTrendVo();
        //trend.setStartDate(point.getRealDate());
        trend.setState(state);
        return trend;
    }

}
