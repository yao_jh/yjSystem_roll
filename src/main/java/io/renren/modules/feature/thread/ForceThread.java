package io.renren.modules.feature.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.feature.service.ForceAvgtrendService;

import java.util.Date;

/**
 * 保存轧制力均值趋势
 */
public class ForceThread extends Thread{

    private ForceAvgtrendEntity trendEntity;

    public ForceThread(){
        super();
    }

    public ForceThread(ForceAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 保存
            trendEntity.setDate(new Date());
            //获取spring bean
            ForceAvgtrendService forceAvgtrendService = (ForceAvgtrendService) SpringContextUtils.getBean("forceAvgtrendService");

            forceAvgtrendService.save(trendEntity);
        }
    }
}