package io.renren.modules.feature.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.feature.service.MotorAvgtrendService;


import java.util.Date;

/**
 * 保存电机均值趋势
 */
public class MotorThread extends Thread{

    private MotorAvgtrendEntity trendEntity;

    public MotorThread(){
        super();
    }

    public MotorThread(MotorAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 保存
            trendEntity.setDate(new Date());
            //获取spring bean
            MotorAvgtrendService motorAvgtrendService = (MotorAvgtrendService) SpringContextUtils.getBean("motorAvgtrendService");

            motorAvgtrendService.save(trendEntity);
        }
    }
}