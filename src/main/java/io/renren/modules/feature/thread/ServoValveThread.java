package io.renren.modules.feature.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.service.ServoValveAvgtrendService;
import io.renren.modules.normal.utils.ServoValveNormalUtils;

import java.util.Date;

/**
 * 保存伺服阀均值趋势
 */
public class ServoValveThread extends Thread{

    private ServoValveAvgtrendEntity trendEntity;

    public ServoValveThread(){
        super();
    }

    public ServoValveThread(ServoValveAvgtrendEntity trendEntity){
        this.trendEntity = trendEntity;
    }

    @Override
    public void run() {
        synchronized (trendEntity){
            // 保存
            trendEntity.setDate(new Date());
            //获取spring bean
            ServoValveAvgtrendService servoValveAvgtrendService = (ServoValveAvgtrendService) SpringContextUtils.getBean("servoValveAvgtrendService");
            servoValveAvgtrendService.save(trendEntity);
            //正态分析验证速度趋势报警
            ServoValveNormalUtils.checkWarningHandler(trendEntity);
        }
    }
}