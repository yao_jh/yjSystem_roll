
package io.renren.modules.feature.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.normal.vo.ServoValveSampleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * 伺服阀均值趋势Dao
 */
@Mapper
public interface ServoValveAvgtrendDao extends BaseMapper<ServoValveAvgtrendEntity> {

    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ServoValveSampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    List<ServoValveAvgtrendEntity> selectListByDate(Long deviceId, Date startDate, Date endDate);
}
