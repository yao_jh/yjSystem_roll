
package io.renren.modules.feature.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.normal.vo.MotorSampleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 电机均值趋势Dao
 *
 */
@Mapper
public interface MotorAvgtrendDao extends BaseMapper<MotorAvgtrendEntity> {


    /**
     * 统计电机运行时长
     * 0:空载;1:有载
     * @param deviceId
     * @return
     */
    List<Map> countDurationByState(Long deviceId);

    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<MotorSampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param deviceId
     * @param state
     * @param startDate
     * @param endDate
     * @return
     */
    List<MotorAvgtrendEntity> selectListByDate(Long deviceId, Integer state, Date startDate, Date endDate);
}
