
package io.renren.modules.feature.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.normal.vo.ForceSampleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;


/**
 * 轧制力均值趋势Dao
 *
 */
@Mapper
public interface ForceAvgtrendDao extends BaseMapper<ForceAvgtrendEntity> {


    /**
     * 查询时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ForceSampleVo> selectMergeListByDate(Date startDate, Date endDate);

    /**
     * 查询时段趋势列表
     * @param crewId
     * @param startDate
     * @param endDate
     * @return
     */
    List<ForceAvgtrendEntity> selectListByDate(Long crewId, Date startDate, Date endDate);

}
