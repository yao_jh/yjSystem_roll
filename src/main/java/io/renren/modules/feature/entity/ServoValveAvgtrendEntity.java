package io.renren.modules.feature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.modules.feature.vo.ServoValveTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 伺服阀均值趋势表
 */
@Data
@TableName("tb_servo_valve_avgtrend")
public class ServoValveAvgtrendEntity {

    @TableId
    private Long id;

    //设备ID
    private Long deviceId;

    //最大开度值
    private BigDecimal maxValue;
    //最小开度值
    private BigDecimal minValue;
    //开度速度
    private BigDecimal speed;
    //开度用时（毫秒）
    private Long duration;
    //0:上升;1:下降
    private Integer state;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public ServoValveAvgtrendEntity(){
        super();
    }

    public ServoValveAvgtrendEntity(ServoValveTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

}
