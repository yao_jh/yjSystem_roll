package io.renren.modules.feature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.CommonUtils;
import io.renren.modules.feature.vo.ForceTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 轧制力均值趋势表
 */
@Data
@TableName("tb_force_avgtrend")
public class ForceAvgtrendEntity{

    @TableId
    private Long id;

    //机组ID
    private Long crewId;
    //道次
    private Integer pass;
    //钢卷编号
    private String coilNo;

    //平均轧制力操作侧(传感器) = 总数/次数
    private BigDecimal avgForceOsPt;
    //平均轧制力传动侧(传感器) = 总数/次数
    private BigDecimal avgForceDsPt;
    //平均轧制力操作侧(压头) = 总数/次数
    private BigDecimal avgForceOsLc;
    //平均轧制力传动侧(压头) = 总数/次数
    private BigDecimal avgForceDsLc;
    //平均总轧制力
    private BigDecimal avgForceTotal;

    //最大轧制力
    private BigDecimal maxForceTotal;
    //最小轧制力
    private BigDecimal minForceTotal;
    //咬钢冲击
    private BigDecimal biteImpact;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    //平均总轧制力（传感器）
    @Transient
    private transient BigDecimal avgForceTotalPt;

    //平均轧制力偏差（OS-DS）（传感器）
    @Transient
    private transient BigDecimal avgForceDiffOsDsPt;

    //平均总轧制力（压头）
    @Transient
    private transient BigDecimal avgForceTotalLc;

    //平均轧制力偏差（OS-DS）（压头）
    @Transient
    private transient BigDecimal avgForceDiffOsDsLc;

    //OS平均轧制力偏差（压头-传感器）
    @Transient
    private transient BigDecimal avgForceDiffOsPtLc;

    //DS平均轧制力偏差（压头-传感器）
    @Transient
    private transient BigDecimal avgForceDiffDsPtLc;

    //平均总轧制力偏差（压头-传感器）
    @Transient
    private transient BigDecimal avgForceTotalPtLc;

    //轧制力波动幅值
    @Transient
    private transient BigDecimal diffForceTotal;

    public BigDecimal getAvgForceTotalPt() {
        avgForceTotalPt = CommonUtils.getRealValue(getAvgForceOsPt()).add(CommonUtils.getRealValue(getAvgForceDsPt()));
        return avgForceTotalPt;
    }

    public BigDecimal getAvgForceTotalLc() {
        avgForceTotalLc = CommonUtils.getRealValue(getAvgForceOsLc()).add(CommonUtils.getRealValue(getAvgForceDsLc()));
        return avgForceTotalLc;
    }

    public BigDecimal getAvgForceDiffOsDsPt() {
        avgForceDiffOsDsPt = CommonUtils.getRealValue(getAvgForceOsPt()).subtract(CommonUtils.getRealValue(getAvgForceDsPt()));
        return avgForceDiffOsDsPt;
    }

    public BigDecimal getAvgForceDiffOsDsLc() {
        avgForceDiffOsDsLc = CommonUtils.getRealValue(getAvgForceOsLc()).subtract(CommonUtils.getRealValue(getAvgForceDsLc()));
        return avgForceDiffOsDsLc;
    }

    public BigDecimal getAvgForceDiffOsPtLc() {
        avgForceDiffOsPtLc = CommonUtils.getRealValue(getAvgForceOsLc()).subtract(CommonUtils.getRealValue(getAvgForceOsPt()));
        return avgForceDiffOsPtLc;
    }

    public BigDecimal getAvgForceDiffDsPtLc() {
        avgForceDiffDsPtLc = CommonUtils.getRealValue(getAvgForceDsLc()).subtract(CommonUtils.getRealValue(getAvgForceDsPt()));
        return avgForceDiffDsPtLc;
    }

    public BigDecimal getAvgForceTotalPtLc() {
        avgForceTotalPtLc = getAvgForceTotalLc().subtract(getAvgForceTotalPt());
        return avgForceTotalPtLc;
    }

    public BigDecimal getDiffForceTotal() {
        diffForceTotal = CommonUtils.getRealValue(getMaxForceTotal()).subtract(CommonUtils.getRealValue(getMinForceTotal()));
        return diffForceTotal;
    }

    public ForceAvgtrendEntity(){
        super();
    }

    public ForceAvgtrendEntity(ForceTrendVo vo){
        BeanUtils.copyProperties(vo, this);
    }


}
