package io.renren.modules.feature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.vo.MotorTrendVo;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 电机均值趋势表
 */
@Data
@TableName("tb_motor_avgtrend")
public class MotorAvgtrendEntity {

    @TableId
    private Long id;

    //设备ID
    private Long deviceId;
    //平均功率
    private BigDecimal avgPower;
    //平均温度
    private BigDecimal avgTemp;
    //平均速度
    private BigDecimal avgSpeed;
    //平均电流
    private BigDecimal avgCurrent;
    //平均转矩
    private BigDecimal avgTorque;

    //最大速度
    private BigDecimal maxSpeed;
    //最大电流
    private BigDecimal maxCurrent;
    //最大转矩
    private BigDecimal maxTorque;

    //状态 0:空载;1:有载
    private Integer state;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    @Transient
    private transient String dateStr;
    @Transient
    private transient String startDateStr;
    @Transient
    private transient String endDateStr;

    public MotorAvgtrendEntity(){
        super();
    }

    public MotorAvgtrendEntity(MotorTrendVo trend){
        BeanUtils.copyProperties(trend, this);
    }

    public String getDateStr() {
        if(dateStr == null){
            dateStr = DateUtils.format(date, DateUtils.DATE_MSTIME_PATTERN);
        }
        return dateStr;
    }
}
