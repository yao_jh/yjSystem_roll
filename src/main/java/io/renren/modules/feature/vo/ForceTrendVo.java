package io.renren.modules.feature.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 轧制力均值趋势视图
 */
@Data
public class ForceTrendVo {

    //机组ID
    private Long crewId;
    //道次
    private Integer pass;
    //钢卷编号
    private String coilNo;

    //平均轧制力操作侧(传感器) = 总数/次数
    private BigDecimal avgForceOsPt;
    private BigDecimal sumForceOsPt;//总数
    private long numForceOsPt = 0;//次数

    //平均轧制力传动侧(传感器) = 总数/次数
    private BigDecimal avgForceDsPt;
    private BigDecimal sumForceDsPt;//总数
    private long numForceDsPt = 0;//次数

    //平均轧制力操作侧(压头) = 总数/次数
    private BigDecimal avgForceOsLc;
    private BigDecimal sumForceOsLc;//总数
    private long numForceOsLc = 0;//次数

    //平均轧制力传动侧(压头) = 总数/次数
    private BigDecimal avgForceDsLc;
    private BigDecimal sumForceDsLc;//总数
    private long numForceDsLc = 0;//次数

    //平均总轧制力
    private BigDecimal avgForceTotal;
    private BigDecimal sumForceTotal;//总数
    private long numForceTotal = 0;//次数


    //最大轧制力
    private BigDecimal maxForceTotal;
    //最小轧制力
    private BigDecimal minForceTotal;

    //咬钢冲击
    private BigDecimal biteImpact;
    //冲击波动
    private BigDecimal impactWave;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;
    //状态 咬钢抛钢
    private Integer state;
    //钢卷编号
    private char[] coilChar = new char[14];

    /**
     * 累加轧制力操作侧(传感器)
     * @param value
     */
    public void toAccumSumForceOsPt(BigDecimal value){
        setSumForceOsPt(getSumForceOsPt().add(value));
    }
    public void setSumForceOsPt(BigDecimal sum) {
        this.sumForceOsPt = sum;
        this.numForceOsPt ++;
    }

    public BigDecimal getAvgForceOsPt() {
        if(avgForceOsPt == null){
            if(numForceOsPt > 0 && sumForceOsPt != null){
                return sumForceOsPt.divide(BigDecimal.valueOf(numForceOsPt)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgForceOsPt;
    }

    /**
     * 累加轧制力传动侧(传感器)
     * @param value
     */
    public void toAccumSumForceDsPt(BigDecimal value){
        setSumForceDsPt(getSumForceDsPt().add(value));
    }
    public void setSumForceDsPt(BigDecimal sum) {
        this.sumForceDsPt = sum;
        this.numForceDsPt ++;
    }

    public BigDecimal getAvgForceDsPt() {
        if(avgForceDsPt == null){
            if(numForceDsPt > 0 && sumForceDsPt != null){
                return sumForceDsPt.divide(BigDecimal.valueOf(numForceDsPt)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgForceDsPt;
    }

    /**
     * 累加轧制力操作侧(压头)
     * @param value
     */
    public void toAccumForceOsLc(BigDecimal value){
        setSumForceOsLc(getSumForceOsLc().add(value));
    }
    public void setSumForceOsLc(BigDecimal sum) {
        this.sumForceOsLc = sum;
        this.numForceOsLc ++;
    }

    public BigDecimal getAvgForceOsLc() {
        if(avgForceOsLc == null){
            if(numForceOsLc > 0 && sumForceOsLc != null){
                return sumForceOsLc.divide(BigDecimal.valueOf(numForceOsLc)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgForceOsLc;
    }

    /**
     * 累加轧制力传动侧(压头)
     * @param value
     */
    public void toAccumForceDsLc(BigDecimal value){
        setSumForceDsLc(getSumForceDsLc().add(value));
    }
    public void setSumForceDsLc(BigDecimal sum) {
        this.sumForceDsLc = sum;
        this.numForceDsLc ++;
    }

    public BigDecimal getAvgForceDsLc() {
        if(avgForceDsLc == null){
            if(numForceDsLc > 0 && sumForceDsLc != null){
                return sumForceDsLc.divide(BigDecimal.valueOf(numForceDsLc)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgForceDsLc;
    }

    /**
     * 累加总轧制力
     * @param value
     */
    public void toAccumForceTotal(BigDecimal value){
        setSumForceTotal(getSumForceTotal().add(value));
    }
    public void setSumForceTotal(BigDecimal sum) {
        this.sumForceTotal = sum;
        this.numForceTotal ++;
    }

    public BigDecimal getAvgForceTotal() {
        if(avgForceTotal == null){
            if(numForceTotal > 0 && sumForceTotal != null){
                return sumForceTotal.divide(BigDecimal.valueOf(numForceTotal)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
            }
        }
        return avgForceTotal;
    }

    public BigDecimal getSumForceOsPt() {
        if(sumForceOsPt == null){
            sumForceOsPt = BigDecimal.ZERO;
        }
        return sumForceOsPt;
    }

    public BigDecimal getSumForceDsPt() {
        if(sumForceDsPt == null){
            sumForceDsPt = BigDecimal.ZERO;
        }
        return sumForceDsPt;
    }

    public BigDecimal getSumForceOsLc() {
        if(sumForceOsLc == null){
            sumForceOsLc = BigDecimal.ZERO;
        }
        return sumForceOsLc;
    }

    public BigDecimal getSumForceDsLc() {
        if(sumForceDsLc == null){
            sumForceDsLc = BigDecimal.ZERO;
        }
        return sumForceDsLc;
    }

    public BigDecimal getBiteImpact() {
        if(maxForceTotal != null && impactWave != null){
            return getMaxForceTotal().divide(getImpactWave()).multiply(BigDecimal.valueOf(100)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return biteImpact;
    }

    public ForceTrendVo(){
        super();
    }

    public ForceTrendVo(Long crewId){
        this.crewId = crewId;
    }


}
