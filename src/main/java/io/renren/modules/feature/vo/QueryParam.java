package io.renren.modules.feature.vo;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.modules.data.vo.TitileVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "查询表单")
public class QueryParam {
    //查询编号
    private String code;
    //机组ID
    private Long crewId;
    //设备ID
    private Long deviceId;
    //类型编码
    private String classCode;
    //开始时间
    private String startDateStr;
    //结束时间
    private String endDateStr;
    //时间间隔
    private Integer intervalUnit;
    //Codes
    private String pointCodes;
    //类型 0:AI;1:DI
    private Integer mode;
    //页码
    private Integer page;
    //单页条目数
    private Integer limit;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //查询题头
    private List<TitileVo> titileVoList;
    //评估对象 motor:电机;force:轧机;servo_valve:伺服阀
    private String typeCode;

    public Date getStartDate() {
        if(startDate == null){
            if(CommonUtils.isNotNull(startDateStr)){
                startDate = DateUtils.stringToDate(startDateStr);
            }
        }
        return startDate;
    }

    public Date getEndDate() {
        if(endDate == null){
            if(CommonUtils.isNotNull(endDateStr)){
                endDate = DateUtils.stringToDate(endDateStr);
            }
        }
        return endDate;
    }

    public Map<String, Object> getMap(){
        Map<String, Object> map = new HashMap<String,Object>();
        map.put("crewId", crewId);
        map.put("deviceId", deviceId);
        map.put("startDateStr", startDateStr);
        map.put("endDateStr", endDateStr);
        return map;
    }

    public Map<String, Object> getPageMap(String page, String size){
        Map<String, Object> map = new HashMap<String,Object>();
        map.put("crewId", crewId);
        map.put("deviceId", deviceId);
        map.put("startDateStr", startDateStr);
        map.put("endDateStr", endDateStr);
        map.put("page", page);
        map.put("size", size);
        return map;
    }
}
