package io.renren.modules.feature.vo;

import io.renren.common.utils.DateUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.*;

/**
 * 伺服阀均值趋势视图
 */
@Data
public class ServoValveTrendVo {

    //设备ID
    private Long deviceId;
    //最大开度值
    private BigDecimal maxValue;
    //最小开度值
    private BigDecimal minValue;
    //开度速度
    private BigDecimal speed;
    //开度用时
    private Long duration;
    //0:上升;1:下降
    private Integer state;

    //初始量
    private BigDecimal initValue;
    //上一个值
    private BigDecimal lastValue;

    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public BigDecimal getSpeed() {
        //时间毫秒
        Long milis = getDuration();
        if(milis.compareTo(0L) != 0){
            //移动距离 = 最大开口-最小开口
            BigDecimal value = maxValue.subtract(minValue);
            //速度 = 移动距离/时间
            speed = value.divide(BigDecimal.valueOf(milis)).setScale(3, BigDecimal.ROUND_HALF_DOWN);
        }
        return speed;
    }

    public Long getDuration() {
        return DateUtils.getDateDiffForMilis(startDate, endDate);
    }
}
