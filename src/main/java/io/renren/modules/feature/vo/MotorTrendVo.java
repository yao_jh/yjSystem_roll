package io.renren.modules.feature.vo;

import io.renren.common.utils.CommonUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 电机均值趋势视图
 */
@Data
public class MotorTrendVo {

    //平均功率
    private BigDecimal avgPower;
    //总功率
    private BigDecimal sumPower;
    //功率次数
    private long numPower = 0;

    //平均温度
    private BigDecimal avgTemp;
    //总温度
    private BigDecimal sumTemp;
    //温度次数
    private long numTemp = 0;

    //平均速度
    private BigDecimal  avgSpeed;
    //总速度
    private BigDecimal  sumSpeed;
    //速度次数
    private long numSpeed = 0;

    //平均电流
    private BigDecimal avgCurrent;
    //总电流
    private BigDecimal sumCurrent;
    //电流次数
    private long numCurrent = 0;

    //平均转矩
    private BigDecimal avgTorque;
    //总转矩
    private BigDecimal sumTorque;
    //转矩次数
    private long numTorque = 0;

    //最大速度
    private BigDecimal maxSpeed;

    //最大电流
    private BigDecimal maxCurrent;

    //最大转矩
    private BigDecimal maxTorque;

    //设备ID
    private Long deviceId;
    //状态
    private Integer state;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //记录时间
    private Date date;

    public BigDecimal getAvgPower() {
        if(numPower > 0 && sumPower != null){
            return sumPower.divide(BigDecimal.valueOf(numPower)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgPower;
    }

    public BigDecimal getAvgTemp() {
        if(numTemp > 0 && sumTemp != null){
            return sumTemp.divide(BigDecimal.valueOf(numTemp)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgTemp;
    }


    public BigDecimal getAvgSpeed() {
        if(numSpeed > 0 && sumSpeed != null){
            return sumSpeed.divide(BigDecimal.valueOf(numSpeed)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgSpeed;
    }

    public BigDecimal getAvgCurrent() {
        if(numCurrent > 0 && sumCurrent != null){
            return sumCurrent.divide(BigDecimal.valueOf(numCurrent)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgCurrent;
    }

    public BigDecimal getAvgTorque() {
        if(numTorque > 0 && sumTorque != null){
            return sumTorque.divide(BigDecimal.valueOf(numTorque)).setScale(3,BigDecimal.ROUND_HALF_DOWN);
        }
        return avgTorque;
    }

    public void setSumPower(BigDecimal sumPower) {
        this.sumPower = sumPower;
        this.numPower ++;
    }

    public void setSumTemp(BigDecimal sumTemp) {
        this.sumTemp = sumTemp;
        this.numTemp ++;
    }

    public void setSumSpeed(BigDecimal sumSpeed) {
        this.sumSpeed = sumSpeed;
        this.numSpeed ++;
    }

    public void setSumCurrent(BigDecimal sumCurrent) {
        this.sumCurrent = sumCurrent;
        this.numCurrent ++;
    }

    public void setSumTorque(BigDecimal sumTorque) {
        this.sumTorque = sumTorque;
        this.numTorque ++;
    }

    public BigDecimal getSumPower() {
        return CommonUtils.getRealValue(sumPower);
    }

    public BigDecimal getSumTemp() {
        return CommonUtils.getRealValue(sumTemp);
    }

    public BigDecimal getSumSpeed() {
        return CommonUtils.getRealValue(sumSpeed);
    }

    public BigDecimal getSumCurrent() {
        return CommonUtils.getRealValue(sumCurrent);
    }

    public BigDecimal getSumTorque() {
        return CommonUtils.getRealValue(sumTorque);
    }


    //累加功率
    public BigDecimal toAccumSumPower(BigDecimal value){
        setSumPower(getSumPower().add(value));
        return getSumPower();
    }

    //累加温度
    public BigDecimal toAccumSumTemp(BigDecimal value){
        setSumTemp(getSumTemp().add(value));
        return getSumTemp();
    }

    //累加速度
    public BigDecimal toAccumSumSpeed(BigDecimal value){
        setSumSpeed(getSumSpeed().add(value));
        return getSumSpeed();
    }

    //累加电流
    public BigDecimal toAccumSumCurrent(BigDecimal value){
        setSumCurrent(getSumCurrent().add(value));
        return getSumCurrent();
    }

    //累加转矩
    public BigDecimal toAccumSumTorque(BigDecimal value){
        setSumTorque(getSumTorque().add(value));
        return getSumTorque();
    }

}
