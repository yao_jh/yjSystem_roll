
package io.renren.modules.feature.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.feature.dao.ForceAvgtrendDao;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.feature.service.ForceAvgtrendService;
import io.renren.modules.normal.vo.ForceSampleVo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("forceAvgtrendService")
public class ForceAvgtrendServiceImpl extends ServiceImpl<ForceAvgtrendDao, ForceAvgtrendEntity> implements ForceAvgtrendService {

    @Override
    public List<ForceSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<ForceAvgtrendEntity> getListByDate(Long crewId, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(crewId, startDate, endDate);
    }

}
