
package io.renren.modules.feature.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.normal.vo.ForceSampleVo;

import java.util.Date;
import java.util.List;

/**
 * 轧制力均值趋势接口
 *
 */
public interface ForceAvgtrendService extends IService<ForceAvgtrendEntity> {

    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ForceSampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 获取轧机均值趋势
     * @param crewId
     * @param startDate
     * @param endDate
     * @return
     */
    List<ForceAvgtrendEntity> getListByDate(Long crewId, Date startDate, Date endDate);

}
