
package io.renren.modules.feature.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.feature.dao.MotorAvgtrendDao;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.feature.service.MotorAvgtrendService;
import io.renren.modules.normal.vo.MotorSampleVo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("motorAvgtrendService")
public class MotorAvgtrendServiceImpl extends ServiceImpl<MotorAvgtrendDao, MotorAvgtrendEntity> implements MotorAvgtrendService {

    @Override
    public Map countDurationByState(Long deviceId) {
        // 返回次数统计
        Map<String, Long> map = new HashMap<String, Long>();
        map.put("noLoadDuration",0L);//空载时长
        map.put("loadDuration",0L);//有载时长
        map.put("noLoadNums",0L);//空载次数
        map.put("loadNums",0L);//有载次数

        //查询开机和停机次数记录
        List<Map> list = baseMapper.countDurationByState(deviceId);
        if(list == null || list.isEmpty()){
            return map;
        }

        //更新次数
        String state = null;
        Long duration = 0L;
        Long nums = 0L;
        for(Map res : list){
            duration = Long.parseLong(res.get("duration").toString());
            nums = Long.parseLong(res.get("nums").toString());
            // 0:空载;1:有载
            state = res.get("state").toString();
            if("0".equals(state)){
                map.put("noLoadDuration", duration);//空载时长
                map.put("noLoadNums", nums);//空载次数
            }else{
                map.put("loadDuration", duration);//有载时长
                map.put("loadNums", nums);//有载次数
            }
        }

        return map;
    }

    @Override
    public List<MotorSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<MotorAvgtrendEntity> getListByDate(Long deviceId, Integer state, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, state, startDate, endDate);
    }
}
