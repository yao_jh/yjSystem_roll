
package io.renren.modules.feature.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.feature.dao.ServoValveAvgtrendDao;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.service.ServoValveAvgtrendService;
import io.renren.modules.normal.vo.ServoValveSampleVo;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service("servoValveAvgtrendService")
public class ServoValveAvgtrendServiceImpl extends ServiceImpl<ServoValveAvgtrendDao, ServoValveAvgtrendEntity> implements ServoValveAvgtrendService {


    @Override
    public List<ServoValveSampleVo> getMergeListByDate(Date startDate, Date endDate) {
        return baseMapper.selectMergeListByDate(startDate, endDate);
    }

    @Override
    public List<ServoValveAvgtrendEntity> getListByDate(Long deviceId, Date startDate, Date endDate) {
        return baseMapper.selectListByDate(deviceId, startDate, endDate);
    }

}
