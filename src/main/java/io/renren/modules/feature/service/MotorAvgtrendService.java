
package io.renren.modules.feature.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.normal.vo.MotorSampleVo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 电机均值趋势接口
 *
 */
public interface MotorAvgtrendService extends IService<MotorAvgtrendEntity> {

    /**
     * 统计设备运行时长和次数
     * @param deviceId
     * @return
     */
    Map countDurationByState(Long deviceId);


    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<MotorSampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 获取时段均值列表
     * @param deviceId
     * @param state
     * @param startDate
     * @param endDate
     * @return
     */
    List<MotorAvgtrendEntity> getListByDate(Long deviceId, Integer state, Date startDate, Date endDate);
}
