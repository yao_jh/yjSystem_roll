
package io.renren.modules.feature.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.normal.vo.ServoValveSampleVo;

import java.util.Date;
import java.util.List;

/**
 * 伺服阀均值趋势接口
 *
 */
public interface ServoValveAvgtrendService extends IService<ServoValveAvgtrendEntity> {

    /**
     * 获取时段合并均值列表
     * @param startDate
     * @param endDate
     * @return
     */
    List<ServoValveSampleVo> getMergeListByDate(Date startDate, Date endDate);

    /**
     * 获取轧机均值趋势
     * @param deviceId
     * @param startDate
     * @param endDate
     * @return
     */
    List<ServoValveAvgtrendEntity> getListByDate(Long deviceId, Date startDate, Date endDate);

}
