package io.renren.modules.feature.controller;

import com.alibaba.fastjson.JSONArray;
import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.data.service.DataService;
import io.renren.modules.data.utils.DataUtils;
import io.renren.modules.data.vo.TitileVo;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.operation.vo.PointVo;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.warning.service.WarningConfigService;
import io.renren.modules.warning.service.WarningLogDayService;
import io.renren.modules.warning.service.WarningLogService;
import io.renren.modules.warning.vo.WarningConfigVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 功能分析包
 *
 */
@RestController
@RequestMapping("/feature")
@Api("功能分析包接口")
public class FeatureController extends AbstractController {

    @Autowired
    private DataService dataService;
    @Autowired
    private WarningLogService warningLogService;
    @Autowired
    private WarningLogDayService warningLogDayService;
   /* @Autowired
    private WarningConfigService warningConfigService;*/
    @Autowired
    private PointService pointService;
    @Autowired
    private CrewService crewService;

    /**
     * 获取设备实时数据
     * @param code
     * @return
     */
    @GetMapping("/selectReal")
    @ApiOperation("获取设备/机组实时数据")
    @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备/机组查询无效");
        }
        Long crewId = params.getCrewId();
        Long deviceId = params.getDeviceId();
        Map<String, PointVo> map = dataService.queryRealDataForMap(code);
        return R.ok().put("data", map);
    }

    /**
     * 获取设备实时数据
     * @param code
     * @return
     */
    @GetMapping("/selectRealList")
    @ApiOperation("获取设备/机组实时数据")
    @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealList(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备/机组查询无效");
        }
        Long crewId = params.getCrewId();
        Long deviceId = params.getDeviceId();
        List<PointVo> list = dataService.queryRealDataForList(code);
        return R.ok().put("data", list);
    }

    /**
     * 获取设备点检
     * @return
     */
    @GetMapping("/selectSpotCheck")
    @ApiOperation("获取设备点检")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="typeCode",value="评估对象，motor:电机;mill:轧机;servo_valve:伺服阀",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectHisTrendByPointClass(@RequestParam("code") String code, @RequestParam("typeCode") String typeCode,
                                        @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam param = MeansUtils.getParamsByCode(code);
        if(param == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        //查询条件
        param.setCode(code);
        param.setStartDateStr(startDateStr);
        param.setEndDateStr(endDateStr);
        param.setIntervalUnit(2);//小时间隔，每小时一条
        param.setTypeCode(typeCode);
        //查询点
        List<PointEntity> pointList = pointService.queryListForAnalysisByParams(param);
        //请求参数题头
        List<TitileVo> title = DataUtils.getTitlesByPointList(pointList);
        param.setTitileVoList(title);
        //返回历史数据
        JSONArray data = dataService.queryHistoryDataByParam(param);
        return R.ok().put("data", data).put("title", title);
    }

    /**
     * 获取设备历史趋势
     * @return
     */
    @GetMapping("/selectHistoryByPointClass")
    @ApiOperation("获取设备历史趋势")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="pointClass",value="参数类型码",dataType="String", paramType = "query",example="temp"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectHistoryByPointClass(@RequestParam("code") String code, @RequestParam("pointClass") String pointClass,
                                       @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        params.setClassCode(pointClass);
        //获取查询点
        List<PointEntity> list = pointService.queryListByParams(params);

        //请求参数题头
        List<TitileVo> title = DataUtils.getTitlesByPointList(list);
        ////查询条件
        params.setCode(code);
        params.setStartDateStr(startDateStr);
        params.setEndDateStr(endDateStr);
        params.setTitileVoList(title);
        params.setIntervalUnit(2);//小时间隔，每小时一条
        //返回历史数据
        JSONArray data = dataService.queryHistoryDataByParam(params);
        //报警阈值线
        //List<WarningConfigVo> limitLines = warningConfigService.queryListByDeviceAndCode(code, pointClass);

        return R.ok().put("data", data).put("title", title);
    }

    /**
     * 获取设备健康度
     * @param code
     * @return
     */
    @GetMapping("/selectHealth")
    @ApiOperation("获取设备/机组健康度")
    @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1")
    public R selectHealth(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        //查询最近7天报警次数
        Date endDate = new Date();
        Date startDate = DateUtils.patchDateToAM(DateUtils.addDateDays(endDate,-7));
        params.setStartDate(startDate);
        params.setEndDate(endDate);
        //查询日表中的报警统计数量
        Long total = warningLogDayService.queryTotalByParams(params);
        total = total == null? 0:total;
        return R.ok().put("total", total);
    }

    /**
     * 获取历史报警次数
     * @return
     */
    @GetMapping("/selectWarningTotalForDay")
    @ApiOperation("获取历史报警次数")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectWarningTotalForDay(@RequestParam("code") String code , @RequestParam("startDateStr") String startDateStr,
                                      @RequestParam("endDateStr") String endDateStr){
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        params.setStartDate(DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr)));
        params.setEndDate(DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr)));
        //查询统计结果 {"date":'2020-05-06',"total":10}
        List<Map> list = warningLogDayService.queryTotalForDayByParams(params);
        return R.ok().put("total", list);
    }

    /**
     * 设备/机组报警趋势
     * @return
     */
    @GetMapping("/selectWarningTotalForPoints")
    @ApiOperation("设备/机组报警趋势")
    @ApiImplicitParams({
        @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1"),
        @ApiImplicitParam(name="typeCode",value="评估对象，motor:电机;mill:轧机;servo_valve:伺服阀",dataType="String", paramType = "query"),
        @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
        @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectWarningTotalForPoints(@RequestParam("code") String code , @RequestParam("typeCode") String typeCode,
                                         @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        params.setTypeCode(typeCode);
        params.setStartDate(DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr)));
        params.setEndDate(DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr)));
        List<Map> map = warningLogDayService.queryTotalForPoints(params);
        return R.ok().put("total", map);
    }

    /**
     * 获取报警明细
     * @return
     */
    @GetMapping("/selectWarningForPage")
    @ApiOperation("获取报警明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="page",value="页码",dataType="String", paramType = "query",example="1"),
            @ApiImplicitParam(name="size",value="条目数",dataType="String", paramType = "query",example="10"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectWarningForPage(@RequestParam("code") String code , @RequestParam("page") String page, @RequestParam("size") String size,
                               @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        params.setStartDateStr(startDateStr);
        params.setEndDateStr(endDateStr);

        //查询报警历史记录
        PageUtils list = warningLogService.queryPageListByParam(params.getPageMap(page, size));
        return R.ok().put("data", list);
    }


}
