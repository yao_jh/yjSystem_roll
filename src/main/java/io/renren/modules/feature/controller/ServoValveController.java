package io.renren.modules.feature.controller;

import com.alibaba.fastjson.JSONArray;
import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.R;
import io.renren.modules.data.service.DataService;
import io.renren.modules.data.utils.DataUtils;
import io.renren.modules.data.vo.TitileVo;
import io.renren.modules.feature.entity.MotorAvgtrendEntity;
import io.renren.modules.feature.entity.ServoValveAvgtrendEntity;
import io.renren.modules.feature.service.ServoValveAvgtrendService;
import io.renren.modules.feature.utils.ServoValveShuntUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.feature.vo.ServoValveTrendVo;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.service.ServoValveNormalService;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.DeviceEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.operation.vo.PointVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 伺服阀分析包
 *
 */
@RestController
@RequestMapping("/feature/servoValve")
@Api("伺服阀评估包接口")
public class ServoValveController extends FeatureController {

    @Autowired
    private CrewService crewService;
    @Autowired
    private ServoValveAvgtrendService servoValveAvgtrendService;
    @Autowired
    private ServoValveNormalService servoValveNormalService;
    @Autowired
    private PointService pointService;
    @Autowired
    private DataService dataService;

    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealList")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealList(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //获取设备实时数据
        R r = getRealMapForDevice(code);
        Map<String, PointVo> map = (Map<String, PointVo>) r.get("data");
        List<PointVo> list = null;
        if(map != null){
            list = new ArrayList<PointVo>();
            for(PointVo p: map.values()){
                list.add(p);
            }
            r.put("data", list);
        }
        return r;
    }

    /**
     * 获取设备实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealMap")
    @ApiOperation("获取设备实时数据")
    @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        return getRealMapForDevice(code);
    }

    /**
     * 获取设备实时数据
     * @param code
     * @return
     */
    private R getRealMapForDevice(String code){
        //获取设备实时数据
        R r = super.selectRealMap(code);
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device != null){
            //加入当前的伺服阀动作
            Long deviceId = device.getDeviceId();
            ServoValveTrendVo trend = ServoValveShuntUtils.servoValveTrendMap.get(deviceId.toString());
            if(trend != null){
                Map<String, PointVo> map = (Map<String, PointVo>) r.get("data");
                if(map != null){
                    map.put("maxValue", getPointVoByCode("maxValue", trend.getMaxValue()));
                    map.put("minValue", getPointVoByCode("minValue", trend.getMinValue()));
                    map.put("speed", getPointVoByCode("speed", trend.getSpeed()));
                    map.put("duration", getPointVoByCode("duration", trend.getDuration()));
                }
            }
        }
        return r;
    }

    /**
     * 转参数视图
     * @param code
     * @param value
     * @return
     */
    private PointVo getPointVoByCode(String code, Object value) {
        PointVo pointVo = new PointVo();
        pointVo.setClassCode(code);
        pointVo.setRealValue(CommonUtils.getBigDecimal(value).toPlainString());
        return pointVo;
    }

    /**
     * 获取均值趋势
     * @return
     */
    @GetMapping("/selectAvgtrend")
    @ApiOperation("获取均值趋势")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectAvgtrend(@RequestParam("code") String code, @RequestParam("startDateStr") String startDateStr,
                            @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        List<ServoValveAvgtrendEntity> trendList = servoValveAvgtrendService.getListByDate(device.getDeviceId(), startDate, endDate);
        return R.ok().put("data", trendList);
    }

    /**
     * 获取开度设定和反馈曲线
     * @return
     */
    @GetMapping("/selectSpeedContrast")
    @ApiOperation("获取开度设定和反馈曲线")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectSpeedContrast(@RequestParam("code") String code , @RequestParam("startDateStr") String startDateStr,
                                 @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        //请求参数题头
        List<PointEntity> pointList = pointService.queryListByLikeClassCode(device.getDeviceId(), "servo%");
        List<TitileVo> title = DataUtils.getTitlesByPointList(pointList);
        QueryParam queryParam = new QueryParam();
        queryParam.setCode(code);
        queryParam.setStartDateStr(startDateStr);
        queryParam.setEndDateStr(endDateStr);
        queryParam.setTitileVoList(title);
        queryParam.setIntervalUnit(2);//小时间隔，每小时一条
        //返回历史数据
        JSONArray data = dataService.queryHistoryDataByParam(queryParam);
        return R.ok().put("data", data).put("title", title);
    }

    /**
     * 获取设备的监测点正态分析
     * @return
     */
    @GetMapping("/selectNormalAnalysis")
    @ApiOperation("获取设备的监测点正态分析")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectNormalAnalysis(@RequestParam("code") String code, @RequestParam("startDateStr") String startDateStr,
                                  @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        DeviceEntity device = MainCatche.deviceMap.get(code);
        if(device == null){
            return R.error("设备查询无效");
        }
        if(CommonUtils.isNull(startDateStr) || CommonUtils.isNull(endDateStr)){
            return R.error("时间为空");
        }
        int inT = startDateStr.indexOf("T");
        if(inT > 0){
            startDateStr = startDateStr.substring(0, inT);
            endDateStr = endDateStr.substring(0, inT);
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        //查询正态模型时段的结果
        NormalVo vo = servoValveNormalService.queryCollectByDate(device.getDeviceId(), startDate, endDate);
        if(vo != null){
            //创建模型
            StandardNormalModel mode = new StandardNormalModel(vo.getDatas(), vo.getMean());
            //输出正太模型曲线
            vo.setSampleList(mode.getDataSampleList());
            //输出阈值线
            vo.setWarningLines(mode.getWarningLines());
        }else{
            return R.error("无正态分析查询结果，请确认查询时间");
        }
        return R.ok().put("data", vo);
    }

}
