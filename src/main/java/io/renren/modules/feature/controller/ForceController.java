package io.renren.modules.feature.controller;

import com.alibaba.fastjson.JSONArray;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.DateUtils;
import io.renren.common.utils.R;
import io.renren.modules.data.service.DataService;
import io.renren.modules.data.utils.DataUtils;
import io.renren.modules.data.vo.TitileVo;
import io.renren.modules.feature.entity.ForceAvgtrendEntity;
import io.renren.modules.feature.service.ForceAvgtrendService;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.normal.model.StandardNormalModel;
import io.renren.modules.normal.service.ForceNormalService;
import io.renren.modules.normal.vo.NormalVo;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.CrewService;
import io.renren.modules.operation.service.PointService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


/**
 * 轧制力分析包
 *
 */
@RestController
@RequestMapping("/feature/force")
@Api("轧制力评估包接口")
public class ForceController extends FeatureController {

    @Autowired
    private CrewService crewService;
    @Autowired
    private ForceAvgtrendService forceAvgtrendService;
    @Autowired
    private ForceNormalService forceNormalService;
    @Autowired
    private PointService pointService;
    @Autowired
    private DataService dataService;

    /**
     * 获取机组实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealList")
    @ApiOperation("获取机组实时数据")
    @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealList(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        return super.selectRealList(code);
    }

    /**
     * 获取机组实时数据列表
     * @param code
     * @return
     */
    @GetMapping("/selectRealMap")
    @ApiOperation("获取机组实时数据")
    @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1")
    public R selectRealMap(@RequestParam(value="code") String code){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        return super.selectRealMap(code);
    }

    /**
     * 获取均值趋势
     * @return
     */
    @GetMapping("/selectAvgtrend")
    @ApiOperation("获取均值趋势")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectAvgtrend(@RequestParam("code") String code, @RequestParam("startDateStr") String startDateStr,
                            @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        CrewEntity crew = crewService.queryCrewEntityByCode(code);
        if(crew == null){
            return R.error("机组查询无效");
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        List<ForceAvgtrendEntity> trendList = forceAvgtrendService.getListByDate(crew.getCrewId(), startDate, endDate);
        return R.ok().put("data", trendList);
    }

    /**
     * 获取轧制力设定和反馈曲线
     * @return
     */
    @GetMapping("/selectSpeedContrast")
    @ApiOperation("获取轧制力设定和反馈曲线")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectSpeedContrast(@RequestParam("code") String code , @RequestParam("startDateStr") String startDateStr,
                            @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        CrewEntity crew = crewService.queryCrewEntityByCode(code);
        if(crew == null){
            return R.error("机组查询无效");
        }
        //请求参数题头
        List<PointEntity> pointList = pointService.queryListByCrewLikeClassCode(crew.getCrewId(), "force_total%");
        List<TitileVo> title = DataUtils.getTitlesByPointList(pointList);
        QueryParam queryParam = new QueryParam();
        queryParam.setCode(code);
        queryParam.setStartDateStr(startDateStr);
        queryParam.setEndDateStr(endDateStr);
        queryParam.setTitileVoList(title);
        queryParam.setIntervalUnit(2);//小时间隔，每小时一条
        //返回历史数据
        JSONArray data = dataService.queryHistoryDataByParam(queryParam);
        return R.ok().put("data", data).put("title", title);
    }

    /**
     * 获取机组的监测点正态分析
     * @return
     */
    @GetMapping("/selectNormalAnalysis")
    @ApiOperation("获取机组的监测点正态分析")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="pointClass",value="参数类型码",dataType="String", paramType = "query",example="temp"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R selectNormalAnalysis(@RequestParam("code") String code, @RequestParam("pointClass") String pointClass, @RequestParam("jobStatus") Integer jobStatus,
                                        @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        CrewEntity crew = crewService.queryCrewEntityByCode(code);
        if(crew == null){
            return R.error("机组查询无效");
        }
        Date startDate = DateUtils.patchDateToAM(DateUtils.stringToDate(startDateStr));
        Date endDate = DateUtils.patchDateToMidnight(DateUtils.stringToDate(endDateStr));
        //查询正态模型时段的结果
        NormalVo vo = forceNormalService.queryCollectByDate(crew.getCrewId(), pointClass, startDate, endDate);
        if(vo != null){
            //创建模型
            StandardNormalModel mode = new StandardNormalModel(vo.getDatas(), vo.getMean());
            //输出正太模型曲线
            vo.setSampleList(mode.getDataSampleList());
            //输出阈值线
            vo.setWarningLines(mode.getWarningLines());
        }else{
            return R.error("无正态分析查询结果，请确认查询时间");
        }
        return R.ok().put("data", vo);
    }

}
