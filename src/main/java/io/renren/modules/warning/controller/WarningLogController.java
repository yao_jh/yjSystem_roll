
package io.renren.modules.warning.controller;

import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.currency.utils.MeansUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.warning.service.WarningLogService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * 报警信息Controller
 *
 */
@RestController
@RequestMapping("/warning/log")
public class WarningLogController extends AbstractController {

    @Autowired
    private WarningLogService warningLogService;

    /**
     * 列表
     */
    @GetMapping("/select")
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",value="设备/机组编码",dataType="String", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="startDateStr",value="开始时间",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="endDateStr",value="结束时间",dataType="String", paramType = "query")})
    public R select(@RequestParam("code") String code,   @RequestParam("startDateStr") String startDateStr, @RequestParam("endDateStr") String endDateStr){
        if(CommonUtils.isNull(code)){
            return R.error("请求参数为空");
        }
        //查询条件
        QueryParam params = MeansUtils.getParamsByCode(code);
        if(params == null){
            return R.error("设备/机组查询无效");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("crewId", params.getCrewId());
        map.put("deviceId", params.getDeviceId());
        map.put("startDateStr", startDateStr);
        map.put("endDateStr", endDateStr);
        PageUtils pageList = warningLogService.queryPageListByParam(map);
        return R.ok().put("page", pageList);
    }

    /**
     * 设备报警等级
     */
    @GetMapping("/levelTotal")
    @ApiImplicitParams({
            @ApiImplicitParam(name="crewId",value="机组ID",dataType="Long", paramType = "query"),
            @ApiImplicitParam(name="deviceId",value="设备ID",dataType="Long", paramType = "query",example="XXX")})
    public R select(Map<String, Object> params){
        PageUtils pageList = warningLogService.queryPageListByParam(params);
        return R.ok().put("page", pageList);
    }


}
