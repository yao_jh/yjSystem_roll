
package io.renren.modules.warning.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.sys.controller.AbstractController;
import io.renren.modules.warning.entity.WarningConfigEntity;
import io.renren.modules.warning.service.WarningConfigService;
import io.renren.modules.warning.utils.WarningUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 报警配置Controller
 *
 */
@RestController
@RequestMapping("/warning/config")
public class WarningConfigController extends AbstractController {

    @Autowired
    private WarningConfigService warningConfigService;

    /**
     * 列表
     */
    @GetMapping("/select")
    public R select(@RequestParam("code") String code){
        List<WarningConfigEntity> list = warningConfigService.queryListByCode(code);
        return R.ok().put("list", list);
    }


    /**
     * 分页列表
     */
    @ResponseBody
    @GetMapping("/list")
    public R list(@RequestParam("code") String code, @RequestParam("page") String page, @RequestParam("size") String size){
        //初始化参数
        Map<String, Object> param = new HashMap<String,Object>();
        param.put("code", code);
        param.put("page", page);
        param.put("size", size);

        PageUtils pageList = warningConfigService.queryPageByParam(param);
        return R.ok().put("page", pageList);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody WarningConfigEntity warningConfig){
        warningConfigService.save(warningConfig);
        //更新配置缓存
        WarningUtils.warningConfigMap.put(warningConfig.getId(), warningConfig);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody WarningConfigEntity warningConfig){
        warningConfigService.updateById(warningConfig);
        //更新配置缓存
        WarningUtils.warningConfigMap.put(warningConfig.getId(), warningConfig);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(Long id){
        warningConfigService.removeById(id);
        WarningUtils.warningConfigMap.remove(id);
        return R.ok();
    }

}
