package io.renren.modules.warning.vo;

import io.renren.modules.warning.entity.WarningConfigEntity;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;

@Data
public class WarningConfigVo {
    //设备Code
    private String deviceCode;
    //参数类别
    private Long pointClass;
    //符号
    private Integer symbol;
    //限值
    private BigDecimal limitVal;
    //报警等级
    private Integer level_;
    //持续时间限值 0:瞬时；>0持续
    private Long duration;

    public WarningConfigVo(){
        super();
    }

    public WarningConfigVo(WarningConfigEntity config){
        BeanUtils.copyProperties(config, this);
    }

}
