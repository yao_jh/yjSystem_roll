package io.renren.modules.warning.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.renren.common.sys.MainCatche;
import io.renren.common.utils.DateUtils;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.warning.entity.WarningConfigEntity;
import io.renren.modules.warning.entity.WarningLogEntity;
import io.renren.modules.warning.thread.WarningThread;
import org.springframework.beans.BeanUtils;

/**
 * 报警工具
 */
public class WarningUtils {
	// 报警配置
	public static Map<Long, WarningConfigEntity> warningConfigMap = new ConcurrentHashMap<Long, WarningConfigEntity>();

	// 报警日志缓存
	public static Map<String, WarningLogEntity> warningLogMap = new ConcurrentHashMap<String, WarningLogEntity>();
	// 临时缓存，存放有持续时间的
	public static Map<String, WarningLogEntity> warningTempMap = new ConcurrentHashMap<String, WarningLogEntity>();

	// 过期清理时间15分钟
	private static final long overdueMinute = 15;

	/**
	 * 
	 * @Title: checkWarning
	 * @Description: 报警判断
	 * @param point
	 * @return: void
	 */
	public static void checkWarning(PointEntity point) {
		if (null == point || null == point.getCode())
			return;
		// 1. 获取该参数点相关的配置，如果为空，则无该参数点相关的报警配置
		List<WarningConfigEntity> configList = getWarinConfigByPoint(point);
		if (null == configList || configList.isEmpty())
			return;
		String configId = null;
		for (WarningConfigEntity config : configList) {// 同一个参数点，有可能有多个报警配置
			// 2.判断是否有达到限值现象 true:有报警;false:无报警
			boolean isAllBeyond = checkPointBeyondConfig(config);
			configId = config.getId().toString();
			//无报警发生
			if (!isAllBeyond) {
				// 不是所有条件都达到，不构成报警。这时要判断一下是否是原来有报警后恢复了
				// 如果有则，在报警Map里写入恢复时间
				WarningLogEntity warning = warningLogMap.get(configId);
				if (null != warning) {
					warning.setRecoverDate(point.getRealDate());
					//更新日志的恢复时间
					saveOrUpdateWarningLog(warning);
					// 对已恢复 的报警。要在Map中把key改了。以免新的报警进不来
					warningLogMap.remove(configId);
					warningLogMap.put(configId + DateUtils.format(new Date(), "MMddhhmmss"), warning);
				}
				// 在未达到持续时间的报警Map里进行去除
				warningTempMap.remove(configId);

				// 不需要再往下判断 。退出。
				continue;
			}

			// 3.判断是否有持续时间
			if (config.getDuration() == 0) {//无持续的报警
				// 3.1 如果没有持续时间并且所有条件都已满足.
				// 3.11 如果报警Map里有该报警，则跳过。
				WarningLogEntity warning = warningLogMap.get(configId);

				// 3.12 如果报警Map里没有该报警。放入Map。
				if (null == warning) {
					warning = createNewWarning(config, false);
					warningLogMap.put(configId, warning);
				}
			} else {//有持续的报警
				// 3.2 如果有持续时间，并且所有条件都已满足。
				// 3.21 判断报警Map中是否有。如果有，则判断是否恢复。如未恢复则跳过。如已恢复则走下面判断
				WarningLogEntity warning = warningLogMap.get(configId);
				if (null != warning && warning.getRecoverDate() == null)
					continue;
				// 3.22 如果临时Map里有该报警，则跳过。 会有定时器自动 判断持续时长是否到达报警要求。
				WarningLogEntity warningTemp = warningTempMap.get(configId);
				if (null != warningTemp)
					continue;
				// 3.23 如果临时Map里没有该报警，放入临时Map.
				warningTemp = createNewWarning(config, true);
				warningTempMap.put(configId, warningTemp);
			}
		}

	}

	/**
	 * 
	 * @Title: createNewWarning
	 * @Description: 创建新的报警
	 * @param config
	 * @return: Warning
	 */
	public static WarningLogEntity createNewWarning(WarningConfigEntity config, boolean isTemp) {
		PointEntity point = getPointByConfig(config);
		WarningLogEntity warning = new WarningLogEntity();
		BeanUtils.copyProperties(config, warning);
		//设置报警属性
		warning.setPointId(point.getPointId());
		warning.setDate(point.getRealDate());
		warning.setWarningId(config.getId());
		warning.setId(null);
		if (!isTemp){//保存报警
			warning.setWarningVal(point.getValue());
			saveOrUpdateWarningLog(warning);
		}
		return warning;
	}

	/**
	 * 
	 * @Title: checkPointBeyondConfig
	 * @Description: 判断是否达到 或超过限值
	 * @param config
	 * @return
	 * @return: boolean
	 */
	private static boolean checkPointBeyondConfig(WarningConfigEntity config) {
		if (null == config || null == config.getDeviceCode() || null == config.getLimitVal())
			return false;

		// 0>; 1>=; 2=; 3<; 4<=;
		int operChar = config.getSymbol();
		PointEntity point = getPointByConfig(config);
		BigDecimal value = point.getValue();
		if (null == point || null == value)
			return false;

		if (operChar == 0 && value.compareTo(config.getLimitVal()) > 0) {
			return true;
		} else if (operChar == 1 && value.compareTo(config.getLimitVal()) >= 0) {
			return true;
		} else if (operChar == 2 && value.compareTo(config.getLimitVal()) == 0) {
			return true;
		} else if (operChar == 3 && value.compareTo(config.getLimitVal()) < 0) {
			return true;
		} else if (operChar == 4 && value.compareTo(config.getLimitVal()) <= 0) {
			return true;
		}

		return false;
	}

	/**
	 *
	 * @Title: getWarinConfigByPointId
	 * @Description: 查询与该参数点有关的报警配置
	 * @param point
	 * @return
	 * @return: WarningConfig
	 */
	private static List<WarningConfigEntity> getWarinConfigByPoint(PointEntity point) {
		List<WarningConfigEntity> list = new ArrayList<WarningConfigEntity>();
		String code = getCodeByPoint(point);
		for (WarningConfigEntity config : warningConfigMap.values()) {
			if (config.getDeviceCode().equals(code)) {
				if(config.getPointClass().equals(point.getClassCode())){
					// 与要判断的参数点有关。进行报警判断
					list.add(config);
				}
			}
		}
		return list;
	}

	/**
	 * 获取参数上级所属设备Code
	 * @param point
	 * @return
	 */
	private static String getCodeByPoint(PointEntity point){
		String code = point.getDeviceCode();
		if(code == null){
			CrewEntity crew = MainCatche.crewMap.get(point.getCrewId());
			if(crew != null){
				code = crew.getCode();
			}
		}
		return code;
	}

	/**
	 * 
	 * @Title: cleanOverWarningMap
	 * @Description: 清除报警Map中已恢复且确认的 且大于15分钟的
	 * @return: void
	 */
	public static void cleanOverWarningMap() {
		List<String> removeList = new ArrayList<>();
		for (String key : warningLogMap.keySet()) {
			WarningLogEntity warning = warningLogMap.get(key);
			if (null != warning.getConfirmDate()
					&& DateUtils.getDateDiffForMini(new Date(), warning.getConfirmDate()) > overdueMinute)
				removeList.add(key);
		}
		if (!removeList.isEmpty()) {
			for (String id : removeList) {
				warningLogMap.remove(id);
			}
		}
	}

	/**
	 * 
	 * @Title: checkDurationWarning
	 * @Description: 判断临时Map里的报警是否达到持续时间
	 * @return: void
	 */
	public static void checkDurationWarning() {
		List<String> removeList = new ArrayList<>();
		for (String key : warningTempMap.keySet()) {
			WarningLogEntity warning = warningTempMap.get(key);
			if (Math.abs(DateUtils.getDateDiffForMini(new Date(), warning.getDate())) >= warning.getDuration()) {
				// 已达到持续时间。放入实际报警Map中。并在临时Map中去除
				warning = createNewWarning(warningConfigMap.get(warning.getWarningId()), false);

				warningLogMap.put(key, warning);
				removeList.add(key);
			}
		}
		if (!removeList.isEmpty()) {
			for (String id : removeList) {
				warningTempMap.remove(id);
			}
		}
	}

	/**
	 * 根据配置获取检查参数
	 * @param config
	 * @return
	 */
	private static PointEntity getPointByConfig(WarningConfigEntity config){
		PointEntity point = null;
		Map<String, PointEntity> map = MainCatche.dataMap.get(config.getDeviceCode());
		if(map != null){
			point = map.get(config.getPointClass());
		}
		return point;
	}

	/**
	 * 
	 * @Title: getWarningLeveStr
	 * @Description: 获取级别中文
	 * @param level 0:Ⅰ级、1:Ⅱ级、2:Ⅲ级、3:Ⅳ级
	 * @return
	 * @return: String
	 */
	public static String getWarningLeveStr(int level) {
		if (level == 0) {
			return "Ⅰ级";
		} else if (level == 1) {
			return "Ⅱ级";
		} else if (level == 2) {
			return "Ⅲ级";
		} else
			return "Ⅳ级";
	}

	/**
	 * 
	 * @Title: getWarningOperCharStr
	 * @Description: 获取操作符转换符号
	 * @param operChar 运算符 0>; 1>=; 2=; 3<; 4<=;
	 * @return
	 * @return: String
	 */
	public static String getWarningOperCharStr(int operChar) {
		if (operChar == 0) {
			return ">";
		} else if (operChar == 1) {
			return ">=";
		} else if (operChar == 2) {
			return "=";
		} else if (operChar == 3) {
			return "<";
		} else {
			return "<=";
		}
	}

	/**
	 * 保存电机均值趋势信息
	 * @param log
	 */
	private static void saveOrUpdateWarningLog(WarningLogEntity log){
		//保存趋势数据
		WarningThread thread = new WarningThread(log);
		thread.start();
	}
}
