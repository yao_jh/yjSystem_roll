package io.renren.modules.warning.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.sys.MainCatche;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.operation.entity.CrewEntity;
import io.renren.modules.warning.dao.WarningConfigDao;
import io.renren.modules.warning.entity.WarningConfigEntity;
import io.renren.modules.warning.service.WarningConfigService;
import io.renren.modules.warning.utils.WarningUtils;
import io.renren.modules.warning.vo.WarningConfigVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("warningConfigService")
public class WarningConfigServiceImpl extends ServiceImpl<WarningConfigDao, WarningConfigEntity> implements WarningConfigService {

    @Override
    public List<WarningConfigEntity> queryListByCode(String code) {
        if(CommonUtils.isNull(code)){
            return null;
        }
        List<WarningConfigEntity> list = new ArrayList<WarningConfigEntity>();
        for(WarningConfigEntity config : WarningUtils.warningConfigMap.values()){
            if(config.getDeviceCode().equals(code)){
                list.add(config);
            }
        }
        return list;
    }

    @Override
    public PageUtils queryPageByParam(Map<String, Object> param) {
        String code = (String) param.get("code");
        if(CommonUtils.isNull(code)){
            return null;
        }
        //分页查询
        IPage<WarningConfigEntity> page = this.page(new Query<WarningConfigEntity>().getPage(param),
                new QueryWrapper<WarningConfigEntity>().eq("device_code", code));
        return new PageUtils(page);
    }

    @Override
    public List<WarningConfigVo> queryListByDeviceAndCode(String code, String pointClass) {
        if(CommonUtils.isNull(code) || CommonUtils.isNull(pointClass)){
            return null;
        }
        List<WarningConfigVo> list = new ArrayList<WarningConfigVo>();
        for (WarningConfigEntity config : WarningUtils.warningConfigMap.values()) {
            if (config.getDeviceCode().equals(code)) {
                if(config.getPointClass().equals(pointClass)){
                    // 与要判断的参数点有关。进行报警判断
                    list.add(new WarningConfigVo(config));
                }
            }
        }
        return list;
    }

    @Override
    public void initWarningConfigMap() {
        List<WarningConfigEntity> list = this.list();
        Long plantId = null;
        CrewEntity crew = null;
        for (WarningConfigEntity config : list) {
            //更新产线信息
            crew = MainCatche.crewMap.get(config.getCrewId());
            if(crew != null){
                plantId = crew.getPlantId();
                config.setPlantId(plantId);
            }
            WarningUtils.warningConfigMap.put(config.getId(), config);
        }
    }

    /**
     * 获取传参元素
     * @param params
     * @param key
     * @return
     */
    private Long getElementByParam(Map<String, Object> params, String key){
        Object obj = params.get(key);
        Long eId = null;
        if(obj != null){
            eId = Long.valueOf(obj.toString());
        }
        return eId;
    }
}
