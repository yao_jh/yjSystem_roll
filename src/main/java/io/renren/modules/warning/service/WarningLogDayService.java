
package io.renren.modules.warning.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.warning.entity.WarningLogDayEntity;

import java.util.List;
import java.util.Map;


/**
 * 报警日志天汇总接口
 *
 */
public interface WarningLogDayService extends IService<WarningLogDayEntity> {

    /**
     * 保存天汇总的报警日志
     */
    void saveTotalDayLog();

    /**
     * 统计时段报警数量
     * @param params
     * @return
     */
    Long queryTotalByParams(QueryParam params);

    /**
     * 按天统计时段报警数量
     * @param params
     * @return
     */
    List<Map> queryTotalForDayByParams(QueryParam params);

   /**
     * 按照点统计时段报警数量
     * @param params
     * @return
     */
    List<Map> queryTotalForPointsByParams(QueryParam params);

    /**
     * 获取格式化后的报警统计
     * @param params
     * @return
     */
    List<Map> queryTotalForPoints(QueryParam params);

}
