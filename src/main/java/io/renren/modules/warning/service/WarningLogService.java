
package io.renren.modules.warning.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.warning.entity.WarningLogEntity;

import java.util.Map;


/**
 * 报警日志接口
 *
 */
public interface WarningLogService extends IService<WarningLogEntity> {

    /**
     * 获取报警明细
     * @param params
     * @return
     */
    PageUtils queryPageListByParam(Map<String, Object> params);
}
