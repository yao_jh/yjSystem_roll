package io.renren.modules.warning.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.CommonUtils;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.warning.dao.WarningLogDao;
import io.renren.modules.warning.entity.WarningLogEntity;
import io.renren.modules.warning.service.WarningLogService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 报警日志接口
 */
@Service("warningLogService")
public class WarningLogServiceImpl extends ServiceImpl<WarningLogDao, WarningLogEntity> implements WarningLogService {

    @Override
    public PageUtils queryPageListByParam(Map<String, Object> params) {
        Long crewId = CommonUtils.getLongByParamMap(params, "crewId");
        Long deviceId = CommonUtils.getLongByParamMap(params, "deviceId");
        //查询条件
        QueryWrapper<WarningLogEntity> queryWrapper = new QueryWrapper<WarningLogEntity>();
        queryWrapper.eq("crew_id", crewId);
        if(deviceId != null){
            queryWrapper.eq("device_id", deviceId);
        }
        queryWrapper.between("date", params.get("startDateStr"), params.get("endDateStr"));
        queryWrapper.orderByDesc("date");
        //分页查找
        IPage<WarningLogEntity> page = this.page(
            new Query<WarningLogEntity>().getPage(params),
            queryWrapper
        );
        return new PageUtils(page);
    }
}
