
package io.renren.modules.warning.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.warning.entity.WarningConfigEntity;
import io.renren.modules.warning.vo.WarningConfigVo;

import java.util.List;
import java.util.Map;


/**
 * 报警配置接口
 *
 */
public interface WarningConfigService extends IService<WarningConfigEntity> {


    /**
     * 列表
     * @param code
     * @return
     */
    List<WarningConfigEntity> queryListByCode(String code);

    /**
     * 分页列表
     * @param param
     * @return
     */
    PageUtils queryPageByParam(Map<String, Object> param);

    /**
     * 列表
     * @param deviceCode
     * @param pointClass
     * @return
     */
    List<WarningConfigVo> queryListByDeviceAndCode(String deviceCode, String pointClass);

    /**
     * 初始内存配置Map
     */
    void initWarningConfigMap();
}
