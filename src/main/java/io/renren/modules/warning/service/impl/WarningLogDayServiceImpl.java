package io.renren.modules.warning.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.DateUtils;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.operation.entity.PointEntity;
import io.renren.modules.operation.service.PointService;
import io.renren.modules.warning.dao.WarningLogDayDao;
import io.renren.modules.warning.entity.WarningLogDayEntity;
import io.renren.modules.warning.service.WarningLogDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 报警日志天汇总接口
 */
@Service("warningLogDayService")
public class WarningLogDayServiceImpl extends ServiceImpl<WarningLogDayDao, WarningLogDayEntity> implements WarningLogDayService {

    @Autowired
    private PointService pointService;

    @Override
    public void saveTotalDayLog() {
        baseMapper.saveTotalDayLog();
    }

    @Override
    public Long queryTotalByParams(QueryParam params) {
        return baseMapper.queryTotalByParams(params);
    }

    @Override
    public List<Map> queryTotalForDayByParams(QueryParam params) {
        return baseMapper.queryTotalForDayByParams(params);
    }

    @Override
    public List<Map> queryTotalForPointsByParams(QueryParam params) {
        return baseMapper.queryTotalForPointsByParams(params);
    }

    @Override
    public List<Map> queryTotalForPoints(QueryParam params) {
        //查询统计结果 {"date":'2020-05-06',"classCode":"current","name":"电流","unit":"A","total":10}
        List<Map> list = queryTotalForPointsByParams(params);
        Map<String, Map<String ,Map>> totalData = mergeRowToColumn(list);
        //查询分析点
        List<PointEntity> pointList = pointService.queryListForAnalysisByParams(params);
        //返回数据
       List<Map> data = new ArrayList<Map>();
        for(String dateStr: totalData.keySet()){
            Map<String, Map> element = totalData.get(dateStr);
            //统计行 每行一个时间
            Map row = new LinkedHashMap<String, Map<String, Map>>();
            for(PointEntity point : pointList){
                //每列一个元素
                Map col = element.get(point.getClassCode());
                if(col == null){
                    col = getSpaceElement(dateStr, point);
                }
                //行数据
                row.put(point.getClassCode(), col);
            }
            //添加行的时间属性
            row.put("dateStr", dateStr);
            data.add(row);
        }
        return data;
    }

    /**
     * 空白数据补充统计0
     * @param dateStr
     * @param point
     * @return
     */
    private Map<String, Object> getSpaceElement(String dateStr, PointEntity point){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("dateStr", dateStr);
        map.put("point_id", point.getPointId());
        map.put("classCode", point.getClassCode());
        map.put("name", point.getName());
        map.put("unit",point.getUnit());
        map.put("total",0);
        return map;
    }

    /**
     * 合并同时间的统计值
     * @param list
     * @return
     */
    private Map<String, Map<String ,Map>> mergeRowToColumn(List<Map> list) {
        Map<String, Map<String, Map>> data = new LinkedHashMap<String, Map<String, Map>>();
        String dateStr = null;
        Map<String, Map> row = null;
        String classCode = null;
        //遍历合并同时间的元素
        for(Map map : list){
            dateStr = map.get("dateStr").toString();
            row = data.get(dateStr);
            classCode = map.get("classCode").toString();
            if(row == null){
                row = new LinkedHashMap<String, Map>();
            }
            row.put(classCode, map);
            data.put(dateStr, row);
        }
        return data;
    }


}
