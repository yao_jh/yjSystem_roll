package io.renren.modules.warning.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.warning.entity.WarningConfigEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 报警配置Dao
 *
 */
@Mapper
public interface WarningConfigDao extends BaseMapper<WarningConfigEntity> {


}
