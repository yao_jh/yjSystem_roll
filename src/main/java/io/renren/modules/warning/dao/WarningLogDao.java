package io.renren.modules.warning.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.warning.entity.WarningLogEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 报警日志Dao
 */
@Mapper
public interface WarningLogDao extends BaseMapper<WarningLogEntity> {

}
