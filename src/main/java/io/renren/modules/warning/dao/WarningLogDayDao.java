package io.renren.modules.warning.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.feature.vo.QueryParam;
import io.renren.modules.warning.entity.WarningLogDayEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 报警日志天汇总Dao
 */
@Mapper
public interface WarningLogDayDao extends BaseMapper<WarningLogDayEntity> {


    /**
     * 以天为单位汇总报警数量
     */
    void saveTotalDayLog();

    /**
     * 统计时段总报警数量
     * @param params
     * @return
     */
    Long queryTotalByParams(@Param("params") QueryParam params);

    /**
     * 按天统计时段报警数量
     * @param params
     * @return
     */
    List<Map> queryTotalForDayByParams(@Param("params") QueryParam params);

    /**
     * 按照点统计时段报警数量
     * @param params
     * @return
     */
    List<Map> queryTotalForPointsByParams(@Param("params") QueryParam params);
}
