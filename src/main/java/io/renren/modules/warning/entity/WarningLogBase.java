package io.renren.modules.warning.entity;

import io.renren.common.utils.DateUtils;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName WarningLogBase
 * @Deacription 报警日志基础表
 * @Author lee
 * @Date 2020/8/4 14:33
 * @Version 1.0
 **/
@Data
public class WarningLogBase {

    /**
     * 机组ID
     */
    private Long crewId;

    /**
     * 设备ID
     */
    private Long deviceId;

   /**
     * 报警等级
     * 1-4级别
     */
    private Integer level;

    /**
     * 配置/规则ID
     */
    private Long warningId;

    /**
     * 报警时间
     */
    private Date date;

    /**
     * 确认时间
     */
    private Date confirmDate;

    /**
     * 处置时间
     */
    private Date handleDate;

    /**
     * 提示信息
     */
    private String info;

    /**
     * 报警阈值
     */
    private BigDecimal limitVal;

    /**
     * 报警瞬时值
     */
    private BigDecimal warningVal;

    //报警发生时间字符串
    @Transient
    private transient String dateStr;
    //报警确认时间字符串
    @Transient
    private transient String confirmDateStr;

    //推送日期
    //private String pushDate;

    public String getDateStr() {
        if(date != null){
            dateStr = DateUtils.format(date, DateUtils.DATE_TIME_PATTERN);
        }
        return dateStr;
    }

    public String getConfirmDateStr() {
        if(confirmDate != null){
            confirmDateStr = DateUtils.format(confirmDate, DateUtils.DATE_TIME_PATTERN);
        }
        return confirmDateStr;
    }
}
