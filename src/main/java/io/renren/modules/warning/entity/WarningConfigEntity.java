package io.renren.modules.warning.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;

@Data
@TableName("tb_warning_config")
public class WarningConfigEntity {
    @TableId
    private Long id;
    //机组ID
    private Long crewId;
    //设备Code
    private String deviceCode;
    //参数类别
    private Long pointClass;
    //符号
    private Integer symbol;
    //限值
    private BigDecimal limitVal;
    //持续时间限值 0:瞬时；>0持续
    private Long duration;
    //报警等级
    private Integer level;
    //提示信息
    private String info;
    //产线ID
    @Transient
    private transient Long plantId;
    //是否推送
    //private Integer push;
    //推送接收人
    //private String receiver;

}
