package io.renren.modules.warning.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import java.util.Date;

@Data
@TableName("tb_warning_log_day")
public class WarningLogDayEntity{

    @TableId
    private Long id;

    /**
     * 参数ID
     */
    private Long pointId;

    /**
     * 报警等级
     */
    private Integer level;

    /**
     * 汇总数量
     */
    private Long total;

    /**
     * 时间
     */
    private Date date;

}
