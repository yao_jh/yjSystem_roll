package io.renren.modules.warning.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.utils.DateUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("tb_warning_log")
@EqualsAndHashCode(callSuper = false)
public class WarningLogEntity extends WarningLogBase{

    @TableId
    private Long id;

    /**
     * 参数ID
     */
    private Long pointId;

    /**
     * 恢复时间
     */
    private Date recoverDate;

    /**
     * 报警时长
     */
    @Transient
    private transient Long duration;

    /**
     * 恢复时间字符串
     */
    @Transient
    private transient String recoverDateStr;

    public Long getDuration() {
        if(recoverDate == null){
            duration = DateUtils.getDateDiffForMini(getDate(), recoverDate);
        }else{
            duration = DateUtils.getDateDiffForMini(getDate(), new Date());
        }
        return duration;
    }
}
