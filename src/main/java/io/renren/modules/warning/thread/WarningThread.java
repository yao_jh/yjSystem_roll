package io.renren.modules.warning.thread;

import io.renren.common.utils.SpringContextUtils;
import io.renren.modules.warning.entity.WarningLogEntity;
import io.renren.modules.warning.service.WarningLogService;

/**
 * @ClassName WarningThread
 * @Deacription 保存报警日志线程
 * @Author lee
 * @Date 2020/8/4 15:33
 * @Version 1.0
 **/
public class WarningThread extends Thread{
    //报警日志
    private WarningLogEntity logEntity;

    public WarningThread(){
        super();
    }

    public WarningThread(WarningLogEntity logEntity){
        this.logEntity = logEntity;
    }

    @Override
    public void run() {
        synchronized (logEntity){
            //获取spring bean
            WarningLogService warningLogService = (WarningLogService) SpringContextUtils.getBean("warningLogService");
            //id空保存，有id修改
            if(logEntity.getId() == null){
                warningLogService.save(logEntity);
            }else{
                warningLogService.updateById(logEntity);
            }
        }
    }
}
