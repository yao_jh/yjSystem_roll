package io.renren.modules.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.analysis.entity.AnalysisConfigEntity;

import java.util.List;

/**
 * 分析对象配置
 *
 */
public interface AnalysisConfigService extends IService<AnalysisConfigEntity> {
}
