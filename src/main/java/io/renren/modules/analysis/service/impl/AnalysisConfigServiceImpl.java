package io.renren.modules.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.analysis.dao.AnalysisConfigDao;
import io.renren.modules.analysis.entity.AnalysisConfigEntity;
import io.renren.modules.analysis.service.AnalysisConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("analysisConfigService")
public class AnalysisConfigServiceImpl extends ServiceImpl<AnalysisConfigDao, AnalysisConfigEntity> implements AnalysisConfigService {

}
