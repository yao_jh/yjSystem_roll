package io.renren.modules.analysis.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 配置表
 */
@Data
@TableName("tb_analysis_config")
public class AnalysisConfigEntity {

    @TableId
    private Long id;

    //type编号
    private String typeCode;

    //class编号
    private String classCode;





}
