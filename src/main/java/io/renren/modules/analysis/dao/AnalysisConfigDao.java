package io.renren.modules.analysis.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.analysis.entity.AnalysisConfigEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 分析对象配置Dao
 *
 */
@Mapper
public interface AnalysisConfigDao extends BaseMapper<AnalysisConfigEntity> {

}
