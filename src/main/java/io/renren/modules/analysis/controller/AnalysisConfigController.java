package io.renren.modules.analysis.controller;

import io.renren.common.utils.R;
import io.renren.modules.analysis.entity.AnalysisConfigEntity;
import io.renren.modules.analysis.service.AnalysisConfigService;
import io.renren.modules.sys.controller.AbstractController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/analysis/config")
@Api("分析对象配置")
public class AnalysisConfigController extends AbstractController {

    @Autowired
    private AnalysisConfigService analysisConfigService;

    /**
     * 分析对象配置列表
     */
    @GetMapping("/select")
    @ApiOperation("获取列表")
    public R select(){
        List<AnalysisConfigEntity> list = analysisConfigService.list();
        return R.ok().put("list", list);
    }


    /**
     * 新增
     */
    @PostMapping("/save")
    @ApiOperation("新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name="typeCode",value="type编号",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="classCode",value="class编号",dataType="String", paramType = "query")})
    public R save(AnalysisConfigEntity analysisConfig){
        analysisConfigService.save(analysisConfig);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation("修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name="plant_id",value="编码号",dataType="Long", paramType = "query",example="motor1"),
            @ApiImplicitParam(name="typeCode",value="type编号",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="classCode",value="class编号",dataType="String", paramType = "query")})
    public R update(AnalysisConfigEntity analysisConfig){
        analysisConfigService.updateById(analysisConfig);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="编码号",dataType="Long", paramType = "query",example="motor1")})
    public R delete(Long id){
        analysisConfigService.removeById(id);
        return R.ok();
    }




















}
